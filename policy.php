<!doctype html>
<html>

<head>
    <!-- Meta Data -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Asian Herbs - UniSap Nutri Care</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico" />
    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico" />

    <!-- Dependency Styles -->
    <link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
    <link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
    <link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="dependencies/aos/css/aos.css">
    <link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

    <!-- Site Stylesheet -->
    <link rel="stylesheet" href="assets/css/app.css" type="text/css">

    <link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

    <!-- Google Web Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body id="home-version-1" class="home-version-1" data-style="default" onload="myFunction()">
    <div id="loading" style="text-align:center;background:#eef3f5;">
        <img src="assets/img/logo.jpg" alt="" style="width:20%;margin-top:21%;">
    </div>


    <div id="site">
        <?php include 'includes/headerhome.php'; ?>
        <section class="the-first-section" style="background:#eef3f5;">
            <div class="container-fluid ">
                <div class="row  container-first " style="margin-top:125px;background:#eef3f5; ">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-top:50px;    color:#0e598c;">
                            Privacy Policy for Asian Herbs
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">At Asian Herbs, accessible from http://asianherbs.in/, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by Asian Herbs and how we use it.</p>
                                <br>
                                <p class="customer-para" style="color:black;">If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us.</p>
                                <br>
                                <p class="customer-para" style="color:black;">This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in Asian Herbs. This policy is not applicable to any information collected offline or via channels other than this website.</p>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Consent
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">By using our website, you hereby consent to our Privacy Policy and agree to its terms.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Information we collect
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">The personal information that you are asked to provide, and the reasons why you are asked to provide it, will be made clear to you at the point we ask you to provide your personal information.</p>
                                <br>
                                <p class="customer-para" style="color:black;">If you contact us directly, we may receive additional information about you such as your name, email address, phone number, the contents of the message and/or attachments you may send us, and any other information you may choose to provide.</p>
                                <br>
                                <p class="customer-para" style="color:black;">When you register for an Account, we may ask for your contact information, including items such as name, company name, address, email address, and telephone number.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            How we use your information
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">We use the information we collect in various ways, including to:
                                    <ul>
                                        <li>Provide, operate, and maintain our website</li>
                                        <li>Improve, personalize, and expand our website</li>
                                        <li>Understand and analyze how you use our website</li>
                                        <li>Develop new products, services, features, and functionality</li>
                                        <li>Communicate with you, either directly or through one of our partners, including for customer service, to provide you with updates and other information relating to the website, and for marketing and promotional purposes</li>
                                        <li>Send you emails</li>
                                        <li>Find and prevent fraud</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Log Files
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Asian Herbs follows a standard procedure of using log files. These files log visitors when they visit websites. All hosting companies do this and a part of hosting services' analytics. The information collected by log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information. Our Privacy Policy was created with the help of the Privacy Policy Generator and the Privacy Policy Template.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Cookies and Web Beacons
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Like any other website, Asian Herbs uses 'cookies'. These cookies are used to store information including visitors' preferences, and the pages on the website that the visitor accessed or visited. The information is used to optimize the users' experience by customizing our web page content based on visitors' browser type and/or other information.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Advertising Partners Privacy Policies
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">You may consult this list to find the Privacy Policy for each of the advertising partners of Asian Herbs.<br><br>

                                    Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on Asian Herbs, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.<br><br>

                                    Note that Asian Herbs has no access to or control over these cookies that are used by third-party advertisers.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                        Third Party Privacy Policies
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Asian Herbs's Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options. You may find a complete list of these Privacy Policies and their links here: Privacy Policy Links.<br><br>

You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers' respective websites. What Are Cookies?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                        CCPA Privacy Rights (Do Not Sell My Personal Information)
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Request that a business that collects a consumer's personal data disclose the categories and specific pieces of personal data that a business has collected about consumers.<br><br>

Request that a business delete any personal data about the consumer that a business has collected.<br><br>

Request that a business that sells a consumer's personal data, not sell the consumer's personal data.<br><br>

If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                        GDPR Data Protection Rights
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">We would like to make sure you are fully aware of all of your data protection rights. Every user is entitled to the following:<br><br>

The right to access – You have the right to request copies of your personal data. We may charge you a small fee for this service.<br><br>

The right to rectification – You have the right to request that we correct any information you believe is inaccurate. You also have the right to request that we complete the information you believe is incomplete.<br><br>

The right to erasure – You have the right to request that we erase your personal data, under certain conditions.<br><br>

The right to restrict processing – You have the right to request that we restrict the processing of your personal data, under certain conditions.<br><br>

The right to object to processing – You have the right to object to our processing of your personal data, under certain conditions.<br><br>

The right to data portability – You have the right to request that we transfer the data that we have collected to another organization, or directly to you, under certain conditions.<br><br>

If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                        Children's Information
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.<br><br>

Asian Herbs does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
    </div>
    <script>
        var preloader = document.getElementById('loading');

        function myFunction() {
            preloader.style.display = 'none';
        }
    </script>
</body>

</html>