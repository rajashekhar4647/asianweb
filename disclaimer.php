<!doctype html>
<html>

<head>
    <!-- Meta Data -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Asian Herbs - UniSap Nutri Care</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico" />
    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico" />

    <!-- Dependency Styles -->
    <link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
    <link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
    <link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="dependencies/aos/css/aos.css">
    <link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

    <!-- Site Stylesheet -->
    <link rel="stylesheet" href="assets/css/app.css" type="text/css">

    <link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

    <!-- Google Web Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body id="home-version-1" class="home-version-1" data-style="default" onload="myFunction()">
    <div id="loading" style="text-align:center;background:#eef3f5;">
        <img src="assets/img/logo.jpg" alt="" style="width:20%;margin-top:21%;">
    </div>


    <div id="site">
        <?php include 'includes/headerhome.php'; ?>
        <section class="the-first-section" style="background:#eef3f5;">
            <div class="container-fluid ">
                <div class="row  container-first " style="margin-top:125px;background:#eef3f5; ">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-top:50px; margin-bottom:50px;   color:#0e598c;">
                            Webiste Disclaimer
                        </h1>
                        <div class="row">
							<div class="col-md-12" style="color:black;padding:5px 20px;">
							<p class="customer-para" style="color:black;">The information provided by Asian Herbs on site http://asianherbs.in/ is for general informational purposes only. All information on the Site is provided in good faith, however we make no representation or warranty of any kind, express or implied, regarding the accuracy, adequacy, validity, reliability, availability or completeness of any information on the Site.</p>
<br>
				<p class="customer-para" style="color:black;">UNDER NO CIRCUMSTANCE SHALL WE HAVE ANY LIABILITY TO YOU FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF THE SITE OR RELIANCE ON ANY INFORMATION PROVIDED ON THE SITE. YOUR USE OF THE SITE AND YOUR RELIANCE ON ANY INFORMATION ON THE SITE IS SOLELY AT YOUR OWN RISK.</p>
							</div>


						</div>
                    </div>
                </div>
                <div class="row  container-first " style="margin-top:25px;background:#eef3f5;  margin-bottom:50px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-top:25px; margin-bottom:50px;   color:#0e598c;">
                        PROFESSIONAL Disclaimer
                        </h1>
                        <div class="row">
							<div class="col-md-12" style="color:black;padding:5px 20px;">
							<p class="customer-para" style="color:black;">The Site cannot and does not contain fitness advice. The fitness information is provided for general informational and educational purposes only and is not a substitute for professional advice. Accordingly, before taking any actions based upon such information, we encourage you to consult with the appropriate professionals. We do not provide any kind of fitness advice.</p>
<br>
				<p class="customer-para" style="color:black;">THE USE OR RELIANCE OF ANY INFORMATION CONTAINED ON THIS SITE IS SOLELY AT YOUR OWN RISK.</p>
							</div>


						</div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
    </div>
    <script>
        var preloader = document.getElementById('loading');

        function myFunction() {
            preloader.style.display = 'none';
        }
    </script>
</body>

</html>