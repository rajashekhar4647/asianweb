<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Uni Health Care</title>

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">


</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/header.php')?>
		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="breadcrumb_area">
			<div class="vigo_container_two">
				<div class="page_header">
					<h1>FAQ</h1>
				</div>
				<!-- /.page-header -->
			</div>
			<!-- /.vigo_container_two -->
		</section>
		<!-- /.breadcrumb_area -->

		<!--==========================-->
		<!--=       Faq Custom       =-->
		<!--==========================-->
		<section class="faq_custom_area">
			<div class="vigo_container_two">
				<div class="section_title_four">
					<h2>FREQUENTLY ASKED</h2>
				</div>
				<div class="faq_custom_all_post">
					<div class="faq_custom_single_post">
						<div class="faq_custom_single_post_content">
							<article class="faq_custom_single_post_container show-more">
								<h4>While lorem ipsum's still resembles classical Latin, it actually has no meaning whatsoever?
								</h4>
								<p>
									Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen derit. Do eiusmod tempor incid dunt ut labore
									et dolore magna enim ad minim veniam, quis nostrud exercitation ullam . . . laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.
								</p>
								<div class="faq_custom_single_post_more_text">
									<p>
										Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen derit. Do eiusmod tempor incid dunt ut labore
										et dolore magna enim ad minim veniam, quis nostrud exercitation.
									</p>
								</div>
								<a href="#" class="faq_custom_single_post_link"></a>
							</article>
						</div>
						<div class="faq_custom_single_post_img">
							<img src="media/images/home6/faq-side-img.jpg" alt="!!">
						</div>
					</div>
					<div class="faq_custom_single_post">
						<div class="faq_custom_single_post_content">
							<article class="faq_custom_single_post_container">
								<h4>In a professional context it often happens that private or corporate clients corder a publication to be made?</h4>
								<p>
									Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen derit. Do eiusmod tempor incid dunt ut labore
									et dolore magna enim ad minim veniam, quis nostrud exercitation ullam . . .
								</p>
								<div class="faq_custom_single_post_more_text">
									<p>
										Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen derit. Do eiusmod tempor incid dunt ut labore
										et dolore magna enim ad minim veniam, quis nostrud exercitation.
									</p>
								</div>
								<a href="#" class="faq_custom_single_post_link"></a>
							</article>
						</div>
						<div class="faq_custom_single_post_img">
							<img src="media/images/home6/faq-side-img.jpg" alt="!!">
						</div>
					</div>
					<div class="faq_custom_single_post">
						<div class="faq_custom_single_post_content">
							<article class="faq_custom_single_post_container">
								<h4>While lorem ipsum's still resembles classical Latin, it actually has no meaning whatsoever?
								</h4>
								<p>
									Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen derit. Do eiusmod tempor incid dunt ut labore
									et dolore magna enim ad minim veniam, quis nostrud exercitation ullam . . .
								</p>
								<div class="faq_custom_single_post_more_text">
									<p>
										Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen derit. Do eiusmod tempor incid dunt ut labore
										et dolore magna enim ad minim veniam, quis nostrud exercitation.
									</p>
								</div>
								<a href="#" class="faq_custom_single_post_link"></a>
							</article>
						</div>
						<div class="faq_custom_single_post_img">
							<img src="media/images/home6/faq-side-img.jpg" alt="!!">
						</div>
					</div>
					<div class="faq_custom_single_post">
						<div class="faq_custom_single_post_content">
							<article class="faq_custom_single_post_container">
								<h4>While lorem ipsum's still resembles classical Latin, it actually has no meaning whatsoever?
								</h4>
								<p>
									Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen derit do eiusmod tempor.
								</p>
								<a href="#" class="faq_custom_single_post_link no-more"></a>
							</article>
						</div>
						<div class="faq_custom_single_post_img">
							<img src="media/images/home6/faq-side-img.jpg" alt="!!">
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<section class="call_to_action_green">
			<div class="vigo_container_two">
				<div class="call_to_action_area_two">
					<div class="row">
						<div class="col-xl-10 offset-xl-2">
							<div class="call_to_action_hello">
								<div class="call_to_action_left_two">
									<h2>LIVE HEALTHY?</h2>
									<p>Try out our suppliment & enjoy the healthiest life. Discounts end soon!</p>
								</div>
								<div class="call_to_action_right_two">
									<a href="#" class="btn_four">Purchase</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        footer2         =-->
		<!--==========================-->
<?php include('includes/footer.php')?>
	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>


</body>


<!-- Mirrored from themeim.com/demo/gymer/faq.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Aug 2019 08:43:50 GMT -->
</html>