<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Uni Health Care</title>
    

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">


</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
	<?php include('includes/header.php')
    ?>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="breadcrumb_area">
			<div class="vigo_container_two">
				<div class="page_header">
					<h1>Supplement</h1>
				</div>
				<!-- /.page-header -->
			</div>
			<!-- /.vigo_container_two -->
		</section>
		<!-- /.breadcrumb_area -->

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="supplement_page_top">
			<div class="vigo_container_two">
				<div class="row">
					<div class="col-xl-7 col-md-6">
						<div class="supplement_page_top_left">
							<div class="section_title_four">
								<h2>WHY NEED THIS?</h2>
							</div>
							<div class="supplement_page_top_left_img">
								<img src="media/images/home6/supplement-top-left.jpg" alt="im">
							</div>
						</div>
					</div>
					<div class="col-xl-5 col-md-6">
						<div class="supplement_page_right_slider owl-carousel">
							<div class="supplement_page_top_right_slide">
								<div class="supplement_page_top_right">
									<h3>Your Body Needs High energy during workout.</h3>
									<p>
										Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.
									</p>
								</div>
							</div>
							<div class="supplement_page_top_right_slide">
								<div class="supplement_page_top_right">
									<h3>Your Body Needs High energy during workout.</h3>
									<p>
										Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="supplement_benefit supplement_benefit_two">
			<div class="vigo_container_two">
				<div class="supplement_benefit_all_items">
					<div class="supplement_benefit_item">
						<div class="supplement_benefit_inner">
							<i class="fas fa-leaf"></i>
							<p>Supplies 10 Vitamins & 8 Minerals</p>
							<div class="after">
								<i class="fas fa-leaf"></i>
								<p>Supplies 10 Vitamins & 8 Minerals</p>
							</div>
						</div>
					</div>
					<div class="supplement_benefit_item">
						<div class="supplement_benefit_inner">
							<i class="fas fa-flask"></i>
							<p>Contains Standard Concentrate</p>
							<div class="after">
								<i class="fas fa-flask"></i>
								<p>Contains Standard Concentrate</p>
							</div>
						</div>
					</div>
					<div class="supplement_benefit_item">
						<div class="supplement_benefit_inner">
							<i class="fas fa-vial"></i>
							<p>No Artificial Additive Used</p>
							<div class="after">
								<i class="fas fa-vial"></i>
								<p>No Artificial Additive Used</p>
							</div>
						</div>
					</div>
					<div class="supplement_benefit_item">
						<div class="supplement_benefit_inner">
							<i class="fas fa-cube"></i>
							<p>Materials & Valuable Food Factors</p>
							<div class="after">
								<i class="fas fa-cube"></i>
								<p>Materials & Valuable Food Factors</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<article class="supplement_hot_selling_group">
			<section class="supplement_hot_selling">
				<div class="vigo_container_two">
					<div class="row">
						<div class="col-xl-7">
							<div class="supplement_hot_selling_content">
								<div class="section_title_four">
									<h2>Hot Selling</h2>
								</div>
								<div class="supplement_hot_selling_slider owl-carousel">
									<div class="home5_recent_slide">
										<div class="off">
											-5%
										</div>
										<div class="home5_recent_thumb">
											<img src="media/images/home6/recent-two.png" alt="gg">
											<span>$12</span>
										</div>
										<div class="home5_recent_appreciate">
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
										</div>
										<div class="home5_recent_title">
											<h3><a href="#">Vaxin Regular (500mg), Mild Intake</a></h3>
										</div>
										<div class="home5_recent_view_cart clearfix">
											<a href="#" class="trigger"><i class="fas fa-expand"></i> QUICK VIEW</a>
											<a href="#"><i class="fas fa-shopping-cart"></i> ADD TO CART</a>
										</div>
									</div>
									<div class="home5_recent_slide">
										<div class="home5_recent_thumb">
											<img src="media/images/home6/recent-two.png" alt="gg">
											<span>$12</span>
										</div>
										<div class="home5_recent_appreciate">
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
										</div>
										<div class="home5_recent_title">
											<h3><a href="#">Vaxin Regular (500mg), Mild Intake</a></h3>
										</div>
										<div class="home5_recent_view_cart clearfix">
											<a href="#" class="trigger"><i class="fas fa-expand"></i> QUICK VIEW</a>
											<a href="#"><i class="fas fa-shopping-cart"></i> ADD TO CART</a>
										</div>
									</div>
									<div class="home5_recent_slide">
										<div class="home5_recent_thumb">
											<img src="media/images/home6/recent-two.png" alt="gg">
											<span>$12</span>
										</div>
										<div class="home5_recent_appreciate">
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
											<a href="#">
										<i class="far fa-star"></i>
									</a>
										</div>
										<div class="home5_recent_title">
											<h3><a href="#">Vaxin Regular (500mg), Mild Intake</a></h3>
										</div>
										<div class="home5_recent_view_cart clearfix">
											<a href="#" class="trigger"><i class="fas fa-expand"></i> QUICK VIEW</a>
											<a href="#"><i class="fas fa-shopping-cart"></i> ADD TO CART</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="supplement_what_we_say">
				<div class="vigo_container_two">
					<div class="section_title_four">
						<h2>WHAT THEY SAY</h2>
					</div>
					<div class="supplement_we_say_content">
						<div class="slider supplement_we_say_content_nav">
							<div class="supplement_we_say_slide">
								<div class="supplement_we_say_slide-img">
									<div class="supplement_we_say_slide-author">
										<img src="media/images/home6/tesm-one.jpg" alt="rr">
									</div>
									<a href="#">JACOB WILSON <span>PHD, CSCS, BODYBUILDING.COM</span></a>
								</div>
								<div class="supplement_we_say_slide_content">
									<p>
										Lorem ipsum dolor sit ametures & about consectetur adipisicing elit, sed do eiusmod tempor incididunt utlab.
									</p>
								</div>
							</div>
							<div class="supplement_we_say_slide">
								<div class="supplement_we_say_slide-img">
									<div class="supplement_we_say_slide-author">
										<img src="media/images/home6/tesm-two.jpg" alt="rr">
									</div>
									<a href="#">JACOB WILSON <span>PHD, CSCS, BODYBUILDING.COM</span></a>
								</div>
								<div class="supplement_we_say_slide_content">
									<p>
										Lorem ipsum dolor sit ametures & about consectetur adipisicing elit, sed do eiusmod tempor incididunt utlab.
									</p>
								</div>
							</div>
							<div class="supplement_we_say_slide">
								<div class="supplement_we_say_slide-img">
									<div class="supplement_we_say_slide-author">
										<img src="media/images/home6/tesm-one.jpg" alt="rr">
									</div>
									<a href="#">JACOB WILSON <span>PHD, CSCS, BODYBUILDING.COM</span></a>
								</div>
								<div class="supplement_we_say_slide_content">
									<p>
										Lorem ipsum dolor sit ametures & about consectetur adipisicing elit, sed do eiusmod tempor incididunt utlab.
									</p>
								</div>
							</div>
						</div>
						<div class="slider supplement_we_say_content_for">
							<div class="supplement_we_say_content_slide" style="position:relative; height: 400px;">
								<div class="youtube-wrapper home5_video_right">
									<div class="youtube-poster" data-bg-image="media/images/home6/video-6.jpg"></div>
									<iframe src="https://www.youtube.com/embed/C1s_2au4qcM?controls=0" allowfullscreen></iframe>
									<i class="material-icons play">
								play_arrow
							</i>
									<i class="material-icons pause">
								pause
							</i>
								</div>
							</div>
							<div class="supplement_we_say_content_slide" style="position:relative; height: 400px;">
								<div class="youtube-wrapper home5_video_right">
									<div class="youtube-poster" data-bg-image="media/images/home6/video-7.jpg"></div>
									<iframe src="https://www.youtube.com/embed/C1s_2au4qcM?controls=0" allowfullscreen></iframe>
									<i class="material-icons play">
								play_arrow
							</i>
									<i class="material-icons pause">
								pause
							</i>
								</div>
							</div>
							<div class="supplement_we_say_content_slide" style="position:relative; height: 400px;">
								<div class="youtube-wrapper home5_video_right">
									<div class="youtube-poster" data-bg-image="media/images/home6/video-6.jpg"></div>
									<iframe src="https://www.youtube.com/embed/C1s_2au4qcM?controls=0" allowfullscreen></iframe>
									<i class="material-icons play">
								play_arrow
							</i>
									<i class="material-icons pause">
								pause
							</i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</article>




		<div class="modal quickview-wrapper">
			<div class="quickview">
				<div class="container">
					<div class="row">
						<span class="close-qv">
					<i class="material-icons">close</i>
				</span>
						<div class="ingredient_slider_flex">
							<div class="ingredient_slider_main">
								<div class="ingredient_slider_one">
									<div>
										<img src="media/images/ingredient2/ing-one-small.png" alt="">
									</div>
									<div>
										<img src="media/images/ingredient2/ing-two-small.png" alt="">
									</div>
									<div>
										<img src="media/images/ingredient2/ing-three-small.png" alt="">
									</div>
									<div>
										<img src="media/images/ingredient2/ing-one-small.png" alt="">
									</div>
									<div>
										<img src="media/images/ingredient2/ing-two-small.png" alt="">
									</div>
									<div>
										<img src="media/images/ingredient2/ing-three-small.png" alt="">
									</div>
								</div>
								<div class="ingredient_slider_two">
									<div>
										<div class="ingredient-img">
											<img src="media/images/ingredient2/ing-one.png" alt="">
										</div>
									</div>
									<div>
										<div class="ingredient-img">
											<img src="media/images/ingredient2/ing-two.png" alt="">
										</div>
									</div>
									<div>
										<div class="ingredient-img">
											<img src="media/images/ingredient2/ing-three.png" alt="">
										</div>
									</div>

									<div>
										<div class="ingredient-img">
											<img src="media/images/ingredient2/ing-four.png" alt="">
										</div>
									</div>

									<div>
										<div class="ingredient-img">
											<img src="media/images/ingredient2/ing-five.png" alt="">
										</div>
									</div>
									<div>
										<div class="ingredient-img">
											<img src="media/images/ingredient2/ing-one.png" alt="">
										</div>
									</div>
								</div>
							</div>
							<div class="ingredient_slider_detail">
								<h4 class="product_title">Vaxin Regular (500mg), Mild Intake lorem ipsum dolor</h4>
								<p class="product_ratting woocommerce-product-rating">

									<a href="#">
								<i class="far fa-star"></i>
							</a>
									<a href="#">
								<i class="far fa-star"></i>
							</a>
									<a href="#">
								<i class="far fa-star"></i>
							</a>
									<a href="#">
								<i class="far fa-star"></i>
							</a>
									<a href="#">
								<i class="far fa-star"></i>
							</a>

									<span>(30 Reviews)</span>
								</p>

								<div class="product_price">
									<p class="in-stock">IN STOCK</p>
									<p class="out-stock">OUT OF STOCK</p>
									<div class="price">
										<ins>
									<span class="woocommerce-Price-amount">
										$12.00
									</span>
								</ins>

										<del>
									<span class="woocommerce-Price-amount">
										$20.00
									</span>
								</del>
									</div>
								</div>

								<form action="#" class="product-cart" method="post">
									<div class="product-quantity quantity">
										<input name="quantity" value="1" data-product-qty="" class="cart__quantity-selector quantity-selector" type="text">
										<input value="-" class="qtyminus looking" type="button">
										<input value="+" class="qtyplus looking" type="button">
									</div>
									<div class="ingredient_slider_btn">
										<a href="#" class="single_add_to_cart_button">
									<i class="fas fa-shopping-cart"></i>
									ADD TO CART
								</a>
										<a href="#">
									<i class="far fa-heart"></i>
								</a>
									</div>
									<div class="share-wrap">
										<a href="#">BUY FROM AMAZON</a>
										<a href="#">BUY FROM FLIPCART</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="supplement_more">
			<div class="vigo_container_two">
				<div class="supplement_more_detail">
					<div class="section_title_four">
						<h2>MORE SUPPLIMENT</h2>
					</div>
				</div>
				<div class="supplement_more_related_products">
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<section class="call_to_action_grey">
			<div class="vigo_container_two">
				<div class="call_to_action_area_two">
					<div class="row">
						<div class="col-xl-10 offset-xl-2">
							<div class="call_to_action_hello">
								<div class="call_to_action_left_two">
									<h2>LIVE HEALTHY?</h2>
									<p>Try out our suppliment & enjoy the healthiest life. Discounts end soon!</p>
								</div>
								<div class="call_to_action_right_two">
									<a href="#" class="btn_four">Purchase</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Footer         =-->
		<!--==========================-->
	<?php include('includes/footer.php')
    ?>

	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>


</body>

</html>