<!doctype html>
<html>
<?php
session_start();
require('./includes/connection.php');
$productid='1';
$sqlbasic=mysqli_query($link,"SELECT * FROM `products` WHERE `id`='$productid'");
$productbasic=mysqli_fetch_array($sqlbasic);
?>
<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UniSap Nutri Care</title>

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">
<style>
.addcart_custom_btn
{
	cursor: pointer;
    background: white;
    height: 50px;
    line-height: 48px;
    display: inline-block;
    border: 2px solid #222;
    padding: 0 18px;
    border-radius: 0;
    font-size: 14px;
    color: #222;
    font-weight: 700;
    letter-spacing: 2px;
    -webkit-transition: 0.3s;
    -o-transition: 0.3s;
    transition: 0.3s;
    margin-left: 5px;
}
.addcart_custom_btn:hover
{
	    background: #3ad82e;
    color: #fff;
    border: 2px solid #fff;
}
</style>

</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
	<?php include('includes/header.php')
    ?>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="breadcrumb_area_list">
			<div class="vigo_container_two">
				<div class="page_header_list">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">SHOP</a></li>
						<li><a href="#">Suppliment</a></li>
						<li><?php echo $productbasic['product_name'];?></li>
					</ul>
				</div>
				<!-- /.page-header -->
			</div>
			<!-- /.vigo_container_two -->
		</section>
		<!-- /.breadcrumb_area -->

		<!--=========================-->
		<!--=        Ingredient2         =-->
		<!--=========================-->
		<section class="ingredeint_section ingredeint2_section ingredeint4_section">
			<div class="vigo_container_two">
				<div class="ingredient_slider_flex">
					<div class="ingredient_slider_main">
						<div class="ingredient_slider_one">
							<div>
								<img src="./products/uploads/<?php echo $productid?>/cover/<?php echo $productbasic['cover_img'];?>" alt="">
							</div>
							<?php
							$sqlimg=mysqli_query($link,"SELECT * FROM `showcase_image` WHERE `product_id`='$productid'");
							WHILE($runimg=mysqli_fetch_array($sqlimg))
							{
							?>
							<div>
								<img src="./products/uploads/<?php echo $productid?>/showcaseimages/<?php echo $runimg['filename'];?>" alt="<?php echo $runimg['filename'];?>">
							</div>
							<?php
							}
							?>
						</div>
						<div class="ingredient_slider_two">
							<div>
								<div class="ingredient-img">
									<img src="./products/uploads/<?php echo $productid?>/cover/<?php echo $productbasic['cover_img'];?>" alt="">
								</div>
							</div>
							<?php
							$sqlimg=mysqli_query($link,"SELECT * FROM `showcase_image` WHERE `product_id`='$productid'");
							WHILE($runimg=mysqli_fetch_array($sqlimg))
							{
							?>
							<div>
								<img src="./products/uploads/<?php echo $productid?>/showcaseimages/<?php echo $runimg['filename'];?>" alt="<?php echo $runimg['filename'];?>">
							</div>
							<?php
							}
							?>
						</div>
					</div>
					<div class="ingredient_slider_detail">
						<h4 class="product_title"><?php echo $productbasic['product_name'];?></h4>
						<div class="product_desc woocommerce-product-details__short-description">
							<p style="max-width:600px;">
							<?php echo $productbasic['product_description'];?>
							</p>
						</div>

						<div class="product_quantity">
							<h4>Select A Quantity:</h4>
							<ul>
							<?php
							$sqlqty=mysqli_query($link,"SELECT * FROM `product_qty` WHERE `product_id`='$productid'");
							WHILE($qty=mysqli_fetch_array($sqlqty))
							{
							?>
								<li style="background:none;"><?php if ($qty['product_pack_id']=='1'){ echo 'Micro'; } elseif($qty['product_pack_id']=='3'){ echo 'Mini'; } elseif($qty['product_pack_id']=='6'){ echo 'Large'; }?><span><?php echo $qty['product_pack_id'].' Month\'s'; ?></span></li>
								<?php
							}
							?>
							</ul>
							<ul>
							<?php
							$qtycount='1';
							$sqlqty=mysqli_query($link,"SELECT * FROM `product_qty` WHERE `product_id`='$productid'");
							$rowcount=mysqli_num_rows($sqlqty);
							WHILE($qty=mysqli_fetch_array($sqlqty))
							{
							?>
								<li onclick="price_cal(<?php echo $qty['price'];?>,<?php echo $qty['product_pack_id'];?>);" class="<?php if($qtycount==$rowcount){ echo 'border'; }?>"><?php echo $qty['tabs_qty'];?> <span>Tablets</span></li>
								<?php
							$qtycount=$qtycount+1;
							$price=$qty['price'];
							$productpackid=$qty['product_pack_id'];
							}
							?>
							</ul>
						</div>

						<div class="product_price">
							<p class="in-stock">IN STOCK</p>
							<p class="out-stock">OUT OF STOCK</p>
							<div class="price">
								<ins>
							<span class="woocommerce-Price-amount" id="price_selling">
								₹<?php echo $price;?>
							</span>
						</ins>

								<del>
							<span class="woocommerce-Price-amount" id="price_mrp">
								₹<?php echo $price*0.15;?>
							</span>
						</del>
							</div>
						</div>

						<form id="addbookcart"  class="product-cart" method="post">
							<div class="product-quantity quantity">
							<input type="hidden" value="<?php echo $productid;?>" name="itemid">
							<input type="hidden" id="packid" value="<?php echo $productpackid;?>" name="packid">
								<input id="Quantity" name="qty" value="1" id="item_quantity" data-product-qty="" class="cart__quantity-selector quantity-selector" type="text">
								<input value="-" class="qtyminus looking" type="button">
								<input value="+" class="qtyplus looking" type="button">
							</div>
							<h5 id="addcartsuccess" style="display:none; color:red;">Item Added To Cart</h5>
							<button type="submit" name="add-to-cart" value="40" class="addcart_custom_btn"><i class="fas fa-plus"></i> Add To Cart</button>
							<a href="cart.php"><button onclick="location.href='cart.php'" id="gotocart" value="40" class="addcart_custom_btn" style="display:none;"><i class="fas fa-shopping-cart"></i> Go To Cart</button></a>
						</form>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Content         =-->
		<!--==========================-->
		<section class="vaxine_all_fact2">
			<div class="vigo_container_two">
				<div class="row">
					<div class="col-xl-12">
						<div class="product_review_tab">
							<ul class="nav" role="tablist">
								<li style="width:49.6%;">
									<a class="active" data-toggle="tab" href="#description" role="tab">DESCRIPTION</a>
								</li>
								<li style="width:50%;">
									<a class="" data-toggle="tab" href="#nutritionfact" role="tab">NUTRITION FACT</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade show active" id="description" role="tabpanel">
									<p>
										<?php echo $productbasic['product_description'];?>
									</p>
								</div>
								<div class="tab-pane fade" id="nutritionfact" role="tabpanel">
									<div class="supplement_nutrition_fact">
										<h3>Suppliment Nutrition facts:</h3>
										<p>
										<?php echo $productbasic['nutri_fact'];?>
										</p>
										<img src="./products/uploads/<?php echo $productid?>/nutri/<?php echo $productbasic['nutri_img'];?>" alt="UniSap Nutricare <?php echo $productbasic['nutri_img'];?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="product_share">
					<ul>
						<li class="facebook">
							<a href="#">
						<i class="fab fa-facebook-f"></i>
						<span><i class="fab fa-facebook-f"></i> - SHARE ON FACEBOOK</span>
					</a>
						</li>
						<li class="twitter">
							<a href="#">
						<i class="fab fa-twitter"></i>
						<span><i class="fab fa-twitter"></i> - SHARE ON Twitter</span>
					</a>
						</li>
						<li class="instagram">
							<a href="#">
						<i class="fab fa-instagram"></i>
						<span><i class="fab fa-instagram"></i> - SHARE ON INSTAGRAM</span>
					</a>
						</li>
						<li class="gplus">
							<a href="#">
						<i class="fab fa-google-plus-g"></i>
						<span><i class="fab fa-google-plus-g"></i> - SHARE ON GOOGLE PLUS</span>
					</a>
						</li>
					</ul>
				</div>
			</div>
		</section>
<?php include('includes/footer.php')
    ?>


	</div>
	<!-- /#site -->
	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>
<script>
function price_cal(price,packid)
{
	$('#price_selling').text('₹'+price);
	$('#price_mrp').text('₹'+price*1.18);
	$('#packid').val(packid);
}
</script>
<script>
$("form#addbookcart").submit(function(e)
{
    e.preventDefault();
    var session='<?php if(isset($_SESSION['UNISAP_USER_ID'])){ echo '1'; } else { echo '';} ?>';
    if(session!='')
    {
    var formData = new FormData(this);
    console.log(formData);
    $.ajax({
       url: "./functions/addtocart.php",
        type: 'POST',
        data: formData,
        success: function (data) {
        $('#gotocart').show();
		$('#addcartsuccess').show();
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
else
{
	var url = "sign-in.php";
	$(location).attr('href',url);
}
});
</script>
</body>
</html>
