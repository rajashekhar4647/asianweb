<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asianher_blog');

/** MySQL database username */
define('DB_USER', 'asianher_blog');

/** MySQL database password */
define('DB_PASSWORD', 'admin@asian');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dIfZ{M?UScSL+T#Vx}3(uB[%b)$%wWVjT[XR-iC(JfxF$=J1`n4KWBi[iVLM9nxF');
define('SECURE_AUTH_KEY',  'CH9&S50R@e VCY(>5ml+z8;UX{b!NcYSZZr=t}xO1%$%9w<Kx,(Nx`/KX=OB 9xC');
define('LOGGED_IN_KEY',    'yVpHRQI~C$q}a,k(u;7Xm$S+RsA`d.,_vB3eo67~D8h#GzmkQEW}uxkQV^x6c>LG');
define('NONCE_KEY',        '$lR~ A]LO*4zY.u^^0vt9g`h7=Y|Pr2]QTL+:S{`HBvJ>s#9b-gqHA2tS3DC3&nM');
define('AUTH_SALT',        '+Ee~QgGZFuuoAn{aK:hi%Z%M.7{|c@er`:X197jQlNln_iE~S#!sk.|c$>kX}O /');
define('SECURE_AUTH_SALT', 'XYC~r;#:xCWr&b85243{mcffj$8kyP>uLl8cScyPzH$IR*#N4,^V@#es7(P?+,j:');
define('LOGGED_IN_SALT',   'J?U4zf^5duJy+O-!qZ]V3v=Q~NO+&-CPyH2?ZN[#PC,3CA0`g6jq5+1)c/Usy?Tf');
define('NONCE_SALT',       '|sZrxZ=@R!7b8D<5pF}Sq2-+fAu5d,Zw8i!Mr5%jT-fINpREdBBTf>gejemVF=BF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
