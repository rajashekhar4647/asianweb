<?php
include './includes/config.php';
session_start();
$order_id=$_SESSION['NEW_ORDER_ID'];
$addquery=mysqli_query($link,"SELECT * FROM `orders_addresses` WHERE `order_id`='$order_id'");
$adddata=mysqli_fetch_array($addquery);
?>
<!DOCTYPE html>
<html>
    <head><title></title>
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<style>
    p
    {
        font-size:18px;
        font-weight:bolder;
        
    }
	      
	  @media print {
  #hide1 * { display:none !important; }
  #hide2 * { display:none !important; }
   
}
.button {
  background-color:  #0e598c; /* Green */
  border: none;
  color: white;
  padding: 10px 28px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  font-weight:700;
}
</style>
</head>
    <body>
         <div class="container my-5  " style=" background:white; border:1px solid  #0e598c;">
                 <div class="row mt-5 ml-5 ">
                     <div class="col-6">
                         <h1>ASIAN HERBS</h1>
                     </div>
                 <div class="col-6" id="hide2">
<input class="button" onclick="myFunction()" style="display:inline-block;float:right;margin-right:20px;" type="button" value="Print">
                     </div>
				 </div>
                 <div class="row mt-5 ">
                        <div class="col-12">
                        <div style="width:100%;white-space:nowrap; position: relative; width:100%;text-align:right; height: 60px; background: #0e598c; "></div>
                            <p class="px-4 " style="position: absolute; top:-20px; right:100px; background: white; font-size:65px; ">INVOICE</p>
                        </div>
                    </div>
                <div class="row m-5">
                    <div class="col-lg-6  " >
                         <h4><b>Invoice to:</b></h4>
                         <p><?php echo $adddata['name'];?></p>
                         <p style="color:gray;font-size:20px;"><Br> <?php echo $adddata['address'].','.$adddata['city'].','.$adddata['state'].','.$adddata['country'].' - '.$adddata['pincode'];?></P>
                         
                    </div>
                    <div class="col-lg-6 ">
                            <div  style="width:100%;white-space:nowrap; width:100%;text-align:right;  "> <p class="  text-right " style=" width:100%;"><b style="font-size:20px;">GST No.</b> &nbsp; &nbsp;<span style="text-align: right;color:gray;">29AACCU4858K1ZP</span> </p></div>
                            <div  style="width:100%;white-space:nowrap; width:100%;text-align:right;  "> <p class="  text-right " style=" width:100%;"><b style="font-size:20px;">Order ID#</b> &nbsp; &nbsp;<span style="text-align: right;color:gray;"><?php echo $order_id;?></span> </p></div>
                            <div class="mt-0 pt-0"  style="width:100%;white-space:nowrap; width:100%;text-align:right;  "> <p class="  text-right " style=" width:100%;"><b style="font-size:20px;">Date</b> &nbsp; &nbsp;<span style="text-align: right; color:gray;"> <?php echo date("d/m/Y"); ?></span> </p></div>
                           
                    </div>
                    
                </div>
               
                
                <div class="table-responsive  px-5 ">
                        <table class="table  text-center table-striped mx-auto  " style="width:100%; border:1px solid grey;"  >
                            <thead class="table-dark">
                          <tr >
                                <th scope="col">Product</th>
                                <th scope="col">PACK QTY</th>
                                <th scope="col">PRICE</th>
                          </tr>
                        </thead>
                        <tbody class="mb-5" style="">
						  <tr>
                              <td>Ketogenic - Advance Weight Loss</td>
                              <td><?php echo $_SESSION['product_pack_id']; ?></td>
                              <td><?php echo $_SESSION['product_price']; ?></td>
                          </tr>
						</tbody>
                        </table>
                        <div class="row mt-5">
						  <div class="col">
                                <h5>Payment Mode: <span style="color:red;"><?php echo $_SESSION['payment_mode'];?></span></h5>
								 </br><h3>Thank You for your Order!</h3>
                            </div>
							<div class="col">
                                
                                    <div style="width:100%;white-space:nowrap; width:100%;text-align:right;"> <p>Sub Total: &nbsp; &nbsp;<span style="text-align: right">₹ <?php echo $_SESSION['product_price']?></span> </p></div>
								    <!--<div style="width:100%;white-space:nowrap; width:100%; text-align:right;"> <p>Tax(10%): &nbsp; &nbsp;<span style="text-align: right">₹ <?php echo $_SESSION['product_price'];?></span> </p></div>-->
                                    <div  style="width:100%;white-space:nowrap; width:100%;text-align:right;  "> <p class=" p-3 py-2 text-right " style=" font-size: 30px;background-color: #0e598c; width:100%;color:#eef3f5">Total: &nbsp; &nbsp;<span style="text-align: right">₹ <?php echo $_SESSION['product_price'];?></span> </p></div>
                                            
                                                                             
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                                
                                <hr  style="border:3px solid  #0e598c; margin-bottom:50px; ">
                                
                      </div>   
                </div>
            
           
         </div>
         
    </body>
	<script>
function myFunction() {
  window.print();
}
</script>
</html> 