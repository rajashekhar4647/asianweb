<!doctype html>
<html>

<head>
    <!-- Meta Data -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Asian Herbs - UniSap Nutri Care</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico" />
    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico" />

    <!-- Dependency Styles -->
    <link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
    <link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
    <link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
    <link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="dependencies/aos/css/aos.css">
    <link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

    <!-- Site Stylesheet -->
    <link rel="stylesheet" href="assets/css/app.css" type="text/css">

    <link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

    <!-- Google Web Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body id="home-version-1" class="home-version-1" data-style="default" onload="myFunction()">
    <div id="loading" style="text-align:center;background:#eef3f5;">
        <img src="assets/img/logo.jpg" alt="" style="width:20%;margin-top:21%;">
    </div>


    <div id="site">
        <?php include 'includes/headerhome.php'; ?>
        <section class="the-first-section" style="background:#eef3f5;">
            <div class="container-fluid ">
                <div class="row  container-first " style="margin-top:125px;background:#eef3f5; ">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-top:50px; margin-bottom:50px;   color:#0e598c;">
                            Terms and Conditions
                        </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Welcome to Asian Herbs!<br><br>

                                    These terms and conditions outline the rules and regulations for the use of Asian Herbs's Website, located at asianherbss@gmail.com.<br><br>

                                    By accessing this website we assume you accept these terms and conditions. Do not continue to use Asian Herbs if you do not agree to take all of the terms and conditions stated on this page. Our Terms and Conditions were created with the help of the Terms And Conditions Generator and the Free Terms & Conditions Generator.<br><br>

                                    The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of India. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Cookies </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">We employ the use of cookies. By accessing Asian Herbs, you agreed to use cookies in agreement with the Asian Herbs's Privacy Policy.<br><br>

                                    Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            License </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Unless otherwise stated, Asian Herbs and/or its licensors own the intellectual property rights for all material on Asian Herbs. All intellectual property rights are reserved. You may access this from Asian Herbs for your own personal use subjected to restrictions set in these terms and conditions.<br><br>

                                    You must not:<br><br>
                                    <ul>
                                        <li>Republish material from Asian Herbs</li>
                                        <li>Sell, rent or sub-license material from Asian Herbs</li>
                                        <li>Reproduce, duplicate or copy material from Asian Herbs</li>
                                        <li>Redistribute content from Asian Herbs</li>
                                    </ul>
                                </p>
                                <p class="customer-para" style="color:black;">
                                    This Agreement shall begin on the date hereof.<br><br>

                                    Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. Asian Herbs does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of Asian Herbs,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, Asian Herbs shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.<br><br>

                                    Asian Herbs reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.<br><br>

                                    You warrant and represent that:<br><br>
                                    <ul>
                                        <li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li>
                                        <li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li>
                                        <li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li>
                                        <li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>
                                    </ul>
                                </p>
                                <p class="customer-para" style="color:black;">
                                    You hereby grant Asian Herbs a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Hyperlinking to our Content </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">The following organizations may link to our Website without prior written approval:
                                    <ul>
                                        <li>Government agencies;</li>
                                        <li>Search engines;</li>
                                        <li>News organizations;</li>
                                        <li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</li>
                                        <li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li>
                                    </ul>
                                </p>
                                <p class="customer-para" style="color:black;">
                                    These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site.<br><br>

                                    We may consider and approve other link requests from the following types of organizations:
                                    <ul>
                                        <li>commonly-known consumer and/or business information sources;</li>
                                        <li>dot.com community sites;</li>
                                        <li>associations or other groups representing charities;</li>
                                        <li>online directory distributors;</li>
                                        <li>internet portals;</li>
                                        <li>accounting, law and consulting firms; and</li>
                                        <li>educational institutions and trade associations.</li>
                                    </ul>
                                </p>
                                <p class="customer-para" style="color:black;">
                                    We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of Asian Herbs; and (d) the link is in the context of general resource information.<br><br>

                                    These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party’s site.<br><br>

                                    If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to Asian Herbs. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.<br><br>

                                    Approved organizations may hyperlink to our Website as follows:
                                    <ul>
                                        <li>By use of our corporate name; or</li>
                                        <li>By use of the uniform resource locator being linked to; or</li>
                                        <li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party&rsquo;s site.</li>
                                    </ul>
                                </p>
                                <p class="customer-para" style="color:black;">
                                    No use of Asian Herbs's logo or other artwork will be allowed for linking absent a trademark license agreement.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            iFrames </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Content Liability </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Your Privacy </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">Please read Privacy Policy</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Reservation of Rights </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Removal of links from our website </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.<br><br>

                                    We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row  " style="background:#eef3f5;  margin-bottom:25px">
                    <div class="col-md-12 col-sm-12 col-12 pl-5 first-first" style=" margin-left:0;">
                        <h1 style=" margin-bottom:10px;   color:#0e598c;">
                            Disclaimer </h1>
                        <div class="row">
                            <div class="col-md-12" style="color:black;padding:5px 20px;">
                                <p class="customer-para" style="color:black;">To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:
                                    <ul>
                                        <li>limit or exclude our or your liability for death or personal injury;</li>
                                        <li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
                                        <li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
                                        <li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
                                    </ul>
                                </p>
                                <p class="customer-para" style="color:black;">
                                The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.<br><br>

As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
    </div>
    <script>
        var preloader = document.getElementById('loading');

        function myFunction() {
            preloader.style.display = 'none';
        }
    </script>
</body>

</html>