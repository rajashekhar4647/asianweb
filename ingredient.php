<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home — Gymer Static Build System</title>

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">


</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/header.php')?>
		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="breadcrumb_area">
			<div class="vigo_container_two">
				<div class="page_header">
					<h1>Ingredient</h1>
				</div>
				<!-- /.page-header -->
			</div>
			<!-- /.vigo_container_two -->
		</section>
		<!-- /.breadcrumb_area -->

		<!--=========================-->
		<!--=    Ingredients key    =-->
		<!--=========================-->
		<div class="ingredients_key_area">
			<div class="vigo_container_two">
				<div class="section_title_four">
					<h2>KEY INGRADIENTS</h2>
				</div>
				<div class="ingredients_key_featurs">
					<div class="ingredients_key_featurs_menu">
						<ul>
							<li>
								<a href="#a" class="key_feature_btn active">
							<i class="material-icons">arrow_upward</i>
						</a>
							</li>
							<li>
								<a href="#b" class="key_feature_btn">
							<i class="material-icons">arrow_upward</i>
						</a>
							</li>
							<li>
								<a href="#c" class="key_feature_btn">
							<i class="material-icons">arrow_upward</i>
						</a>
							</li>
							<li>
								<a href="#d" class="key_feature_btn">
							<i class="material-icons">arrow_upward</i>
						</a>
							</li>
							<li>
								<a href="#e" class="key_feature_btn">
							<i class="material-icons">arrow_upward</i>
						</a>
							</li>
							<li>
								<a href="#f" class="key_feature_btn">
							<i class="material-icons">arrow_upward</i>
						</a>
							</li>
						</ul>
						<div class="ingredients_key_featurs_img">
							<img src="media/images/home6/key-menu-bg.png" alt="#">
						</div>
					</div>
					<div class="ingredients_key_features_all">
						<div class="ingredients_key_single_feature a">
							<h3>ACONITE TUBER</h3>
							<p>Helps to strengthen your bone joints, improve attention & enhances athletic performance, weight loss.</p>
							<span>1</span>
						</div>
						<div class="ingredients_key_single_feature b">
							<h3>CREOSOTE BUSH</h3>
							<p>Helps to strengthen your bone joints, improve attention & enhances athletic performance, weight loss.</p>
							<span>2</span>
						</div>
						<div class="ingredients_key_single_feature c">
							<h3>CAFFEINE POWDER</h3>
							<p>Helps to strengthen your bone joints, improve attention & enhances athletic performance, weight loss.</p>
							<span>3</span>
						</div>
						<div class="ingredients_key_single_feature d">
							<h3>LEUCINE AMINO</h3>
							<p>Helps to strengthen your bone joints, improve attention & enhances athletic performance, weight loss.</p>
							<span>4</span>
						</div>
						<div class="ingredients_key_single_feature e">
							<h3>KARNITINE TANTRATE</h3>
							<p>Helps to strengthen your bone joints, improve attention & enhances athletic performance, weight loss.</p>
							<span>5</span>
						</div>
						<div class="ingredients_key_single_feature f">
							<h3>BLACKWORT COMFREY</h3>
							<p>Helps to strengthen your bone joints, improve attention & enhances athletic performance, weight loss.</p>
							<span>6</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--=========================-->
		<!--=      Hot selling      =-->
		<!--=========================-->
		<article class="supplement_hot_selling_group supplement_hot_selling_group-two">
			<section class="supplement_what_we_say">
				<div class="vigo_container_two">
					<div class="section_title_four">
						<h2>WHAT THEY SAY</h2>
					</div>
					<div class="supplement_we_say_content">
						<div class="slider supplement_we_say_content_nav">
							<div class="supplement_we_say_slide">
								<div class="supplement_we_say_slide-img">
									<img src="media/images/home6/tesm-one.jpg" alt="rr">
									<a href="#">JACOB WILSON <span>PHD, CSCS, BODYBUILDING.COM</span></a>
								</div>
								<div class="supplement_we_say_slide_content">
									<p>
										Lorem ipsum dolor sit ametures & about consectetur adipisicing elit, sed do eiusmod tempor incididunt utlab.
									</p>
								</div>
							</div>
							<div class="supplement_we_say_slide">
								<div class="supplement_we_say_slide-img">
									<img src="media/images/home6/tesm-two.jpg" alt="rr">
									<a href="#">JACOB WILSON <span>PHD, CSCS, BODYBUILDING.COM</span></a>
								</div>
								<div class="supplement_we_say_slide_content">
									<p>
										Lorem ipsum dolor sit ametures & about consectetur adipisicing elit, sed do eiusmod tempor incididunt utlab.
									</p>
								</div>
							</div>
							<div class="supplement_we_say_slide">
								<div class="supplement_we_say_slide-img">
									<img src="media/images/home6/tesm-one.jpg" alt="rr">
									<a href="#">JACOB WILSON <span>PHD, CSCS, BODYBUILDING.COM</span></a>
								</div>
								<div class="supplement_we_say_slide_content">
									<p>
										Lorem ipsum dolor sit ametures & about consectetur adipisicing elit, sed do eiusmod tempor incididunt utlab.
									</p>
								</div>
							</div>
						</div>
						<div class="slider supplement_we_say_content_for">
							<div class="supplement_we_say_content_slide" style="position:relative; height: 400px;">
								<div class="youtube-wrapper home5_video_right">
									<div class="youtube-poster" data-bg-image="media/images/home6/video-6.jpg"></div>
									<iframe src="https://www.youtube.com/embed/C1s_2au4qcM?controls=0" allowfullscreen></iframe>
									<i class="material-icons play">
						play_arrow
					</i>
									<i class="material-icons pause">
						pause
					</i>
								</div>
							</div>
							<div class="supplement_we_say_content_slide" style="position:relative; height: 400px;">
								<div class="youtube-wrapper home5_video_right">
									<div class="youtube-poster" data-bg-image="media/images/home6/video-7.jpg"></div>
									<iframe src="https://www.youtube.com/embed/C1s_2au4qcM?controls=0" allowfullscreen></iframe>
									<i class="material-icons play">
						play_arrow
					</i>
									<i class="material-icons pause">
						pause
					</i>
								</div>
							</div>
							<div class="supplement_we_say_content_slide" style="position:relative; height: 400px;">
								<div class="youtube-wrapper home5_video_right">
									<div class="youtube-poster" data-bg-image="media/images/home6/video-6.jpg"></div>
									<iframe src="https://www.youtube.com/embed/C1s_2au4qcM?controls=0" allowfullscreen></iframe>
									<i class="material-icons play">
						play_arrow
					</i>
									<i class="material-icons pause">
						pause
					</i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</article>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="supplement_more">
			<div class="vigo_container_two">
				<div class="supplement_more_detail">
					<div class="section_title_four">
						<h2>MORE SUPPLIMENT</h2>
					</div>
				</div>
				<div class="supplement_more_related_products">
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
					<div class="sn_related_product">
						<div class="sn_pd_img">
							<a href="#">
						<img src="media/images/banner-two/relate-pd-two.png" alt="">
					</a>
						</div>
						<div class="sn_pd_rating">
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
							<a href="#">
						<i class="far fa-star"></i>
					</a>
						</div>
						<div class="sn_pd_detail">
							<h5><a href="#">Vaxin Regular (500mg), Mild Intake</a></h5>
							<ins>
						<span>$16.00</span>
					</ins>
							<del>
						<span>$20.00</span>
					</del>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<section class="call_to_action_grey">
			<div class="vigo_container_two">
				<div class="call_to_action_area_two">
					<div class="row">
						<div class="col-xl-10 offset-xl-2">
							<div class="call_to_action_hello">
								<div class="call_to_action_left_two">
									<h2>LIVE HEALTHY?</h2>
									<p>Try out our suppliment & enjoy the healthiest life. Discounts end soon!</p>
								</div>
								<div class="call_to_action_right_two">
									<a href="#" class="btn_four">Purchase</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Footer         =-->
		<!--==========================-->
<?php include('includes/footer.php')?>
	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>


</body>


<!-- Mirrored from themeim.com/demo/gymer/ingredient.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Aug 2019 08:43:46 GMT -->
</html>