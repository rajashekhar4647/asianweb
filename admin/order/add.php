<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Base CSS -->
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">

		<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="../css/demo.css" />
		<link rel="stylesheet" type="text/css" href="../css/component.css" />

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Fontawesome Icons -->
    <link href="../assets/css/fontawesome/fontawesome-all.min.css" rel="stylesheet">

    <title>Cosmo - Responsive Dashboard Admin Template</title>
    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
    <style>
    @media screen and(max-width:400px){
      .col , .resh{
        margin-top:30px !important;
      }
    }

        .mat-ink-bar {
        position: absolute;
    height: 2px;
    transition: .10s cubic-bezier(.35,0,.25,1);
}
/* a-field_a3 */
==== DEMO ===== */
button.collapsible:hover {
    background-color: #6f6f6f;
}
   button.collapsible: {
    background-color: #6f6f6f;
}
        .content {
  padding: 0 18px;
            margin-top:20px;

  overflow: hidden;
  background-color: white;
}
 .page {
	 padding-left: 15px;
	 padding-right: 15px;
}
 @media (max-width: 1000px) {

	 .page__field {
		 width: 100%;
	}
	 .page__field:nth-child(n+2) {
		 margin-top: 40px;
	}
}

 @media (min-width: 1001px) {

	 .page {
		 box-sizing: border-box;
		 width: 100%;
		 margin: auto;
		 display: flex;
		 align-items: flex-end;
	}
	 .page__field {
		 margin-left: 2%;
		 margin-right: 2%;
		 flex-grow: 1;
	}
}
/* Patreon */


        .link{
    color:#2c2c2c;
}
      a.link:hover {
    color: #2c2c2c;
    text-decoration: none;


}
        h6.this{
            font-size: 17px;
        }
        @media (min-width: 600px){
            #phone{
                display: none;
            }
            #laptop{
                display:block;
            }

              #p-phone{
                display: none;
            }
            #p-laptop{
                display:block;
            }

        }
        @media (max-width: 600px){
            #s-phone{
             overflow-x: scroll;
    overflow-y: hidden;
            }
.col-sm-3 {
    flex: 0 0 33.3333%;
    max-width: 33.3333%;
            }
            .col-sm-2 {
    flex: 0 0 25%;
    max-width: 25%;
            }
            h6.this{
                font-size: 80%;
            }

             #laptop{
                display: none;
            }
             #phone{
                display: block;
            }
               #p-laptop{
                display: none;
            }
             #p-phone{
                display: block;
            }
            .col-sm-8{
    flex: 0 0 70%;
    max-width: 70%;
            }
              .col-sm-4{
    flex: 0 0 30%;
    max-width: 30%;
            }
            .col-sm-6{
                flex: 0 0 50%;
    max-width: 50%;
            }
        }
    </style>

  </head>
  <body>


      <section class="wrapper">


          <!-- SIDEBAR -->
<?php include('../includes/sidebar.php') ?>
<?php
 include('../includes/connection.php');
?>


          <!--RIGHT CONTENT AREA-->
          <div class="content-area">
<?php include('../includes/header.php') ?>

            <div class="content-wrapper">

                <div class="row page-tilte align-items-center">
                  <div class="col-md-auto">
                    <a href="#" class="mt-3 d-md-none float-right toggle-controls"><span class="material-icons">keyboard_arrow_down</span></a>
                    <h1 class="weight-300 h3 title"> Order #<?php echo $_GET['orderid']; ?></h1>

                  </div>

                </div>

                <div >
                <div style="width:100%;border-radius:10px;background-color:white;padding-bottom:0px;">
                    <div class="row" style="padding:20px;">
                  <div class="col-md-4 col-sm-3" style="text-align:center">
                     <a class="link" href="javascript:info('info','image','first','second','third','inventory')">     <h5>
Order Details
</h5></a>
                       </div>
                         <div class="col-md-4 col-sm-3" style="text-align:center">
 <h5>
     <a class="link" href="javascript:image('info','image','first','second','third','inventory')"> Product</a></h5>

                        </div>

                        <div class="col-md-4 col-sm-3" style="text-align:center">
                            <a class="link" href="javascript:inventory('info','image','first','second','third','inventory')">           <h5>Invoice</h5></a>
                        </div>

                  </div>
                          <div class="row" style="margin-left:0px;margin-right:0px;">
                      <div class="col-md-4 col-sm-3" style="text-align:center;padding-left:0px;padding-right:0px;">
             <mat-ink-bar class="mat-ink-bar" id="first" style="visibility: visible;display:block;  width: 100%;background-color:#2c2c2c"></mat-ink-bar>
                       </div>
                         <div class="col-md-4 col-sm-3" style="text-align:center;padding-left:0px;padding-right:0px;">
<mat-ink-bar class="mat-ink-bar" id="second" style="visibility: visible;display:none;  width: 100%;background-color:#2c2c2c"></mat-ink-bar>

                        </div>

                        <div class="col-md-4 col-sm-3" style="text-align:center;padding-left:0px;padding-right:0px;">
                       <mat-ink-bar class="mat-ink-bar" id="third" style="visibility: visible;display:none;  width: 100%;background-color:#2c2c2c"></mat-ink-bar>
                        </div>

                  </div>
                  <hr style="margin:0px;">

                    <div id="info" style="display:block;">
                <div class="page" style="margin-top:10px;">

        <div class="container-fluid">
            <div id="laptop">
            <div style="margin-top:20px;margin-bottom:35px;margin-left:20px;">
                    <h4 style=" font-weight: 400; font-size: 22px; color: #2c2c2c;"><i class="far fa-user"></i> Customer</h4>


                    </div>
                    <div class="row">
        <div class="col-md-4 col-sm-3 ">
                    <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Name </h6>

            </div></div>
                        <div class="col-md-4 col-sm-3 ">

                        <div style="text-align:center">
            <h6 style="font-size: 17px;color: #404040;font-weight: 400;"> Email </h6>

            </div>           </div>
                        <div class="col-md-4 col-sm-3 ">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Phone </h6>

            </div>
         </div>
        </div>
        <hr>
             <div class="row">
        <div class="col-md-4 col-sm-3 ">
                    <div style="text-align:center">
            <h6 class="this" style="color: #2c2c2c; font-weight: 400;"> <?php
            $pass = $_GET['orderid'];
			
			$ordersql=mysqli_query($link,"SELECT * FROM `orders` WHERE `order_id`='$pass'");
			$orderarray=mysqli_fetch_array($ordersql);
            $orderaddsql = mysqli_query($link,"SELECT * FROM orders_addresses WHERE order_id=$pass");
            $orderaddarray = mysqli_fetch_array($orderaddsql);
            echo $orderaddarray["name"];?> </h6>

            </div></div>
                        <div class="col-md-4 col-sm-3 ">

                        <div style="text-align:center">
            <h6 class="this" style="color: #2c2c2c;font-weight: 400;"> <?php echo $orderaddarray["email"];?> </h6>

            </div>           </div>
                        <div class="col-md-4 col-sm-3 ">

                        <div style="text-align:center">
            <h6 class="this" style="  color: #2c2c2c; font-weight: 400;"> <?php echo $orderaddarray["phone"];?> </h6>

            </div>
         </div>
        </div><hr></div>
        
            <div class="row" style="box-shadow: 0px 0px 3px grey;border-radius:10px;background-color:white; color: #2c2c2c;margin-top:30px;">

              <button class="collapsible" style="border-radius:10px;background-color:white; color: #2c2c2c;cursor: pointer;padding: 12px;width: 100%;border: none;text-align: left;outline: none;font-size: 17px;text-align:center;">Shipping Address</button>
          <div class="content" >
             <div class="row">
               <?php
               
               $address=$orderaddarray["address"];
               echo $address;
               echo '
<iframe frameborder="0" src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . str_replace(",", "", str_replace(" ", "+", $address)) . '&z=14&output=embed" style="width:100%; height:450px;border:2px solid gray; border-radius:10px;" class="loc"></iframe>';?>
                 <div>
                  <h3 style=" font-size: 26px; color: #2c2c2c; font-weight: 400;margin-top:20px;">
                  <?php
                  echo $orderaddarray["address"];
              ?></h3>
                 </div>

</div>
</div>
            </div>
        

            <div id="p-laptop">
                  <div style="margin-top:50px;margin-bottom:35px;margin-left:20px;">
                    <h4 style=" font-weight: 400; font-size: 22px; color: #2c2c2c;"><i class="fas fa-rupee-sign"></i> Payment</h4>


                    </div>
                    <div class="row">
        <div class="col-md-3">
                    <div style="text-align:center">
            <h6 style=" font-size: 17px; color: #404040; font-weight: 400;"> Transaction Id </h6>

            </div></div>
                        <div class="col-md-3">

                        <div style="text-align:center">
            <h6 style="font-size: 17px;color: #404040;font-weight: 400;"> Payment Mode </h6>

            </div>           </div>
                        <div class="col-md-3">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Amount </h6>

            </div>
         </div>
                                    <div class="col-md-3">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Date </h6>

            </div>
         </div>
        </div>
        <hr>
		<?php
            $pass = $_GET['orderid'];
            $sql = mysqli_query($link,"SELECT * FROM `booking_payment` WHERE `booking_id`=$pass");
            $result = mysqli_fetch_array($sql);
			?>
             <div class="row">
        <div class="col-md-3">
                    <div style="text-align:center">
            <h6 style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"><?php echo $result['id'];?></h6>

            </div></div>
                        <div class="col-md-3">

                        <div style="text-align:center">
            <h6 style="font-size: 17px;color: #2c2c2c;font-weight: 400;"><?php echo $result['payment_type'];?></h6>

            </div>           </div>
                        <div class="col-md-3">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"><?php echo '₹'.$result['total'];?></h6>

            </div>
         </div>
                          <div class="col-md-3">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"><?php echo $result['date'];?></h6>

            </div>
         </div>
        </div></div><hr>

                             <div id="p-phone">
                  <div style="margin-top:50px;margin-bottom:35px;margin-left:20px;">
                    <h4 style=" font-weight: 400; font-size: 22px; color: #2c2c2c;"><i class="fas fa-rupee-sign"></i> Payment</h4>


                    </div>
                    <div class="row">
        <div class="col-sm-6">
                    <div style="text-align:center">
            <h6 style=" font-size: 17px; color: #404040; font-weight: 400;"> Transaction Id: </h6>

            </div>

                        <div style="text-align:center">
            <h6 style="font-size: 17px;color: #404040;font-weight: 400;"> Payment Mode: </h6>

            </div>

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Amount: </h6>

            </div>
                           <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Date: </h6>
            </div>

         </div>

        <div class="col-sm-6">
                    <div style="text-align:center">
            <h6 style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"> #9876</h6>

            </div> <div style="text-align:center">
            <h6 style="font-size: 17px;color: #2c2c2c;font-weight: 400;">Credit Card</h6>

            </div>   <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"> ₹2000/-</h6>

            </div>
         <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #2c2c2c; font-weight: 400;">08/05/2019</h6>

            </div>
         </div>
        </div><hr></div>

                    </div>


</div>


                    </div>
                                <div id="image" style="display:none;">
                                     <div class="page" style="margin:0;padding:0;width:100%;">
          		<div class="container-fluid" style="padding:0;">


                <!-- <div class=" table-responsive"> -->
                  <div class="content-wrapper" style="padding:0;">

                         <div class="row">
                                <div class="col-lg-12 mb-12">
                                    <div class="card mb-4" style="box-shadow:none;border:none;margin-bottom:0 !important;">
                                        <div class="card-body">

                                            <div class="row" style="border-bottom:1px solid black;padding-bottom:20px;">
                                                <div class="col-md-4 pb-4">
                                                    <img style="height:300px;width:100%;" src="http://asianherbs.in/img/<?php echo $orderarray['product_pack_id'].'pack.png';?>">
                                                </div>
                                                <div class="col-md-8">
                                                    <h4>This Week</h4>
                                                    <div class="weight-400"><?php

                                                    $pa = $_GET['orderid'];
                                                    $sql = "SELECT * FROM orders WHERE order_id=$pa";

                                                    $result = $link->query($sql);

                                                if ($result->num_rows > 0) {
                                                   // output data of each row
                                                   while($row = $result->fetch_assoc()) {
                                                       $pid = $row["product_id"];
                                                   }
                                                }

                                                    $sql = "SELECT * FROM products WHERE id=$pid";

                                                    $result = $link->query($sql);

                                               if ($result->num_rows > 0) {
                                                   // output data of each row
                                                   while($row = $result->fetch_assoc()) {
                                                       echo $row["product_description"];
                                                   }
                                               }

                                                ?></div>

      											 <div class="row"><div class="col" style="font-size:25px; padding-top:40px;">Months Supply: <?php
                             $pass = $_GET['orderid'];
                             $sql = "SELECT * FROM orders WHERE order_id=$pass";

                             $result = $link->query($sql);

                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo $row["qty"];
                            }
                        }

                         ?></div> <div class="col" style="font-size:150%; padding-top:40px;">Price: <?php
                             $pass = $_GET['orderid'];
                             $sql = "SELECT * FROM booking_payment WHERE booking_id=$pass";

                             $result = $link->query($sql);

                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo '₹'.$row["total"].'/-';
                            }
                        }

                         ?></div></div>
                                                </div>
                                             </div>
                                          </div>
      								   <div class="card-body">
                                          <div class="row">

                                                <div class="col-md-8">




                                                </div>
                                             </div>
                                          </div>
      									<div class="card-body" style="background:#69BA6D;border-bottom-right-radius:6px;border-bottom-left-radius:6px;opacity:0.9">
                                         <div class="row">
                                                <div class="col-md-9">
                                                    </div>
                                                <div class="col-md-3">
                                                    <h4 style="color:white;">Total: <?php
                                                        $pass = $_GET['orderid'];
                                                        $sql = "SELECT * FROM booking_payment WHERE booking_id=$pass";

                                                        $result = $link->query($sql);

                                                   if ($result->num_rows > 0) {
                                                       // output data of each row
                                                       while($row = $result->fetch_assoc()) {
                                                           echo '₹'.$row["total"].'/-';
                                                       }
                                                   }

                                                    ?></h4>
                                                 </div>
      										   <div class="col-xs-0">
                                                    <h4></h4>
                                                 </div>
      										   <div class="col-xs-0">
                                                 </div>
                                          </div>

      									<div class="row">

      										   <div class="col-xs-0">
                                                    <h4></h4>
                                                 </div>
      										   <div class="col-xs-0">
                                                 </div>
                                          </div>
                                          </div>
                                    </div>
                                </div>
                            </div>



                  </div>

                  <!-- <table class="table mb-4 responsive-table table-bordered bg-white">
                    <thead class="thead-light2">
                      <tr class="this">

                        <th scope="col" class="resizeable" style="    width: 90px;"> ID <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
                        <th scope="col" class="resizeable" style="width: 138px;">Image <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
                        <th scope="col" class="resizeable" >Name <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
                        <th scope="col" class="resizeable" style="    width: 100px;" >Price <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
                        <th scope="col" class="resizeable" style="    width: 30px;">Quantity <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>

                      </tr>
                    </thead>
                    <tbody>
                 <tr class="this">

                       <td class="align-middle" data-label="ID">1</td>
                        <td class="align-middle" data-label="Image"><img src="../assets/images/products/4.jpg" width="100"></td>
                        <td class="align-middle" data-label="Name">Chair 1</td>
                        <td class="align-middle" data-label="Price">₹ 500</td>
                        <td class="align-middle" data-label="Quantity">3</td>

                      </tr>
                      <tr class="this">

                        <td class="align-middle" data-label="ID">2</td>
                        <td class="align-middle" data-label="Image"><img src="../assets/images/products/1.jpg" width="100"></td>
                        <td class="align-middle" data-label="Name">Chair 2</td>
                        <td class="align-middle" data-label="Price">₹ 1000</td>
                        <td class="align-middle" data-label="Quantity">1</td>

                      </tr>
                      <tr class="this">

                        <td class="align-middle" data-label="ID">3</td>
                        <td class="align-middle" data-label="Image"><img src="../assets/images/products/5.jpg" width="100"></td>
                        <td class="align-middle" data-label="Name">Chair 3</td>
                        <td class="align-middle" data-label="Price">₹ 350</td>
                        <td class="align-middle" data-label="Quantity">1</td>

                      </tr>
                   </tbody>
                  </table> -->
                <!-- </div> -->
</div></div>


</div>
                     <div id="inventory" style="display: none;">
                <div class="page" style="margin-top:10px;">
    <div class="container">

                    <div class="row">

        <div style="font-size: 14px;
    color: rgba(0,0,0,.54);
    margin-bottom: 32px;"><?php
    $pa = $_GET['orderid'];
    $sql = "SELECT * FROM booking WHERE order_id=$pa";

    $result = $link->query($sql);

if ($result->num_rows > 0) {
   // output data of each row
   while($row = $result->fetch_assoc()) {
       echo $row["date"];
   }
}

?></div></div>
        <div class="row">
            <div style="col">
                <div style="font-size: 23px;
    color: rgba(0,0,0,.54);
  ">INVOICE NO. :- <span style="color:black;"><?php echo $_GET['orderid']; ?></span>
            </div>
             <div style="font-size: 14px;
    color: rgba(0,0,0,.54);
   margin-top:5px"><?php echo $orderaddarray["name"];?></div>
                 <div style="font-size: 14px;
    color: rgba(0,0,0,.54);
    margin-top:5px"><?php echo $orderaddarray["phone"];?></div>
<div style="font-size: 14px;
color: rgba(0,0,0,.54);
margin-top:5px"><?php echo $orderaddarray["email"];?></div>
                <div style="font-size: 14px;
    color: rgba(0,0,0,.54);
    margin-bottom: 32px;margin-top:5px"><?php echo $orderaddarray["address"];?></div>


            </div>

          </div>

             <div id="s-phone">
                    <div class="row">
        <div class="col-md-3 col-sm-2">
                    <div style="text-align:center">
            <h6 style=" font-size: 17px; color: #404040; font-weight: 400;"> Product </h6>

            </div></div>
                        <div class="col-md-3 col-sm-2">

                         </div>
                        <div class="col-md-3 col-sm-2">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Month Supply </h6>

            </div>
         </div>
                                    <div class="col-md-3 col-sm-2">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #404040; font-weight: 400;"> Total </h6>

            </div>
         </div>
        </div>
        <hr>
        <div class="products">
             <div class="row">
        <div class="col-md-3 col-sm-2">
                    <div style="text-align:center">
            <h6 style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"><?php
            $pa = $_GET['orderid'];
            $sql = "SELECT * FROM orders WHERE order_id=$pa";

            $result = $link->query($sql);

       if ($result->num_rows > 0) {
           // output data of each row
           while($row = $result->fetch_assoc()) {
               $product_id = $row["product_id"];
           }
       }
       $sql = "SELECT * FROM products WHERE id=$product_id";

       $result = $link->query($sql);

  if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
          echo $row["product_name"];
      }
  }

        ?> </h6>

            </div></div>
                        
                        <div class="col-md-3 col-sm-2">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"><?php echo $orderarray['product_pack_id'];?></h6>


         </div>
        </div><hr>
                 <div class="col-md-3 col-sm-2">

                        <div style="text-align:center">
            <h6  style=" font-size: 17px; color: #2c2c2c; font-weight: 400;"><?php
           
               echo '₹'.$orderarray['purchasing_price'].'/-';
         ?></h6>


         </div>
        </div>
        </div><hr>

             <div class="row">


                      <hr>

        </div></div><br><br>
        <div class="total">
        <div class="row">
          <div class="col-md-3 col-sm-0" ></div><div class="col-md-3 col-sm-0"></div>
           
            <div class="col-md-3 col-sm-6">

           
           </div>
            </div><hr>

                <div class="row">
                  <div class="col-md-3 col-sm-0"></div><div class="col-md-3 col-sm-0"></div>
            <div class="col-md-3 col-sm-6">
                <div style="text-align:left">

                   <p style="    font-size: 23px;
    font-weight: 600;
    color: Black;
    border-bottom: none;
    padding: 4px 8px;">GRAND TOTAL</p>


                </div>
            </div>

            <div class="col-md-3 col-sm-6">

               <div style="text-align:right">

                   <p style="    font-size: 23px;
    font-weight: 600;
    color: black;
    border-bottom: none;
    padding: 4px 8px;"> <?php echo '₹'.$orderarray['purchasing_price'].'/-' ?></p>


                </div>
           </div>
            </div>




        </div></div>
    </div>
</div>

                    </div>

                    </div>
<script type="text/javascript">
function info(div1,div2,first,second,third,div3)
{
    d3=document.getElementById(first);
    d4=document.getElementById(second);
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
     d5 = document.getElementById(div3);
     d6 = document.getElementById(third);
   if( d1.style.display == "none" )
   {
      d2.style.display = "none";
       d5.style.display = "none";
      d1.style.display = "block";
       d3.style.display = "block";
       d4.style.display = "none";
        d6.style.display = "none";
   }

}
</script>
<script type="text/javascript">
function image(div1,div2,first,second,third,div3)
{d3=document.getElementById(first);
   d5 = document.getElementById(div3);
    d4=document.getElementById(second);
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
  d6=document.getElementById(third);
   if( d2.style.display == "none" )
   {
      d1.style.display = "none";
         d5.style.display = "none";
      d2.style.display = "block";
        d3.style.display = "none";
         d6.style.display = "none";
       d4.style.display = "block";
   }

}
</script>
<script type="text/javascript">
function inventory(div1,div2,first,second,third,div3)
{
    d3=document.getElementById(first);
   d5 = document.getElementById(div3);
 d6=document.getElementById(third);
    d4=document.getElementById(second);
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
   if( d5.style.display == "none" )
   {
      d1.style.display = "none";
         d5.style.display = "block";
      d2.style.display = "none";
        d3.style.display = "none";
       d4.style.display = "none";
        d6.style.display = "block";
   }

}
</script>



                    </div>
                </div>
    </div>
   </section>



      <script src="../assets/js/lib/jquery.min.js"></script>
      <script src="../assets/js/lib/popper.min.js"></script>
      <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="../assets/js/chosen-js/chosen.jquery.js"></script>
      <script src="../assets/js/custom.js"></script>


      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
<script src="../js/custom-file-input.js"></script>

    <script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>
  </body>

<!-- Mirrored from cosmoadmin.com/preview/ecommerce-products.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:24:45 GMT -->
</html>
