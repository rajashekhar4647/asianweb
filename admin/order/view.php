<?php
 include('../includes/connection.php');
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Base CSS -->
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Fontawesome Icons -->
    <link href="../assets/css/fontawesome/fontawesome-all.min.css" rel="stylesheet">

    <title>UNISAP - All Orders</title>

  </head>
  <body>



      <section class="wrapper">


          <!-- SIDEBAR -->
<?php include('../includes/sidebar.php') ?>



          <!--RIGHT CONTENT AREA-->
          <div class="content-area">


<?php include('../includes/header.php') ?>

            <div class="content-wrapper">

                   <div class="row">
                          <div class="col-lg-12 mb-12">
                              <div class="card mb-4">
                                  <div class="card-body">
                                      <h6 class="weight-400 mb-3 border-left border-warning pl-2 border-width-medium"><span class="text-muted">ORDER ID: </span>#1</h6>
                                      <div class="row">
                                          <div class="col-md-4">
                                              <img style="height:350px;width:280px;" src="http://localhost/fitness/products/uploads/4/showcaseimages/Screenshot (242).png">
                                          </div>
                                          <div class="col" style="width:75%;">
                                              <h4>This Week</h4>
                                              <div class="weight-400">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>

											 <div class="row"><div class="col" style="font-size:25px; padding-top:40px;">Quantity: 5</div> <div class="col" style="font-size:25px; padding-top:40px;">Price: ₹5</div></div>
                                          </div>
                                       </div>
                                    </div>
								   <div class="card-body">
                                    <div class="row">
                                          <div class="col-md-4">
                                              <img style="height:350px;width:280px;" src="http://localhost/fitness/products/uploads/4/showcaseimages/Screenshot (242).png">
                                          </div>
                                          <div class="col" style="width:75%;">
                                              <h4>This Week</h4>
                                              <div class="weight-400">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>

											 <div class="row"><div class="col" style="font-size:25px; padding-top:40px;">Quantity: 5</div> <div class="col" style="font-size:25px; padding-top:40px;">Price: ₹5</div></div>
                                          </div>
                                       </div>
                                    </div>
									<div class="card-body">
                                   <div class="row">
                                          <div class="col-md-9">
                                              </div>
                                          <div class="col-md-1">
                                              <h4>Total:</h4>
                                           </div>
										   <div class="col-md-1">
                                              <h4> 55,000</h4>
                                           </div>
										   <div class="col-md-1">
                                           </div>
                                    </div>
									<div class="row">
                                          <div class="col-md-9">
                                              </div>
                                          <div class="col-md-1">
                                              <h4>Total:</h4>
                                           </div>
										   <div class="col-md-1">
                                              <h4> 55,000</h4>
                                           </div>
										   <div class="col-md-1">
                                           </div>
                                    </div>
									<div class="row">
                                          <div class="col-md-9">
                                              </div>
                                          <div class="col-md-1">
                                              <h4>Total:</h4>
                                           </div>
										   <div class="col-md-1">
                                              <h4> 55,000</h4>
                                           </div>
										   <div class="col-md-1">
                                           </div>
                                    </div>
                                    </div>
                              </div>
                          </div>
                      </div>



            </div>



          </div>


      </section>





      <script src="../assets/js/lib/jquery.min.js"></script>
      <script src="../assets/js/lib/popper.min.js"></script>
      <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="../assets/js/chosen-js/chosen.jquery.js"></script>
      <script src="../assets/js/custom.js"></script>

      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
      <script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

  </body>

<!-- Mirrored from cosmoadmin.com/preview/ecommerce-orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:21:24 GMT -->
</html>
