<?php
 include('../includes/connection.php');
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Base CSS -->
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Fontawesome Icons -->
    <link href="../assets/css/fontawesome/fontawesome-all.min.css" rel="stylesheet">

    <title>UNISAP - All Orders</title>

  </head>
  <body>


      <section class="wrapper">
<?php include('../includes/sidebar.php') ?>

          <!-- SIDEBAR -->


          <!--RIGHT CONTENT AREA-->
          <div class="content-area">

           <?php include('../includes/header.php') ?>
            <div class="content-wrapper">

                <div class="row page-tilte align-items-center">
                  <div class="col-md-auto">
                    <a href="#" class="mt-3 d-md-none float-right toggle-controls"><span class="material-icons">keyboard_arrow_down</span></a>
                    <h1 class="weight-300 h3 title">All Orders </h1>

                  </div>
                  <div class="col controls-wrapper mt-3 mt-md-0 d-none d-md-block ">
                    <div class="controls d-flex justify-content-center justify-content-md-end">
                    <input type="search"  id="myInput" onkeyup="myFunction()" class="form-control d-inline-block" placeholder="Search in tabel...">
                    </div>
                  </div>
                </div>

                <div class=" table-responsive">
                  <table class="table mb-4 responsive-table table-bordered bg-white" >
                    <thead class="thead-light2">
                      <tr>

                        <th scope="col" class="resizeable">Order ID <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
                        <th scope="col" class="resizeable">Customer <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
                        <th scope="col" class="resizeable">Total <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
                        <th scope="col" width="1">Payment Type</th>
						<th scope="col" width="1">Status</th>
						<th scope="col" class="resizeable">Date <span class="material-icons align-text-bottom ml-1 md-18">sort</span></th>
						<th scope="col" width="1">More</th>
                      </tr>
                    </thead>
                    <tbody id="myTable">
					<?php
$sql=mysqli_query($link,"SELECT * FROM `orders` ORDER BY `id` DESC");
WHILE($order=mysqli_fetch_array($sql))
{
$orderid=$order['order_id'];
$user_id=$order['user_id'];
$sqluser=mysqli_query($link,"SELECT * FROM `orders_addresses` WHERE `order_id`='$orderid'");
$userdata=mysqli_fetch_array($sqluser);
$sqlbookpay=mysqli_query($link,"SELECT * FROM `booking_payment` WHERE `booking_id`='$orderid'");
$paydata=mysqli_fetch_array($sqlbookpay);
?>
                      <tr>
                        <td class="align-middle" data-label="Product Name">#<?php echo $orderid; ?></td>
                        <td class="align-middle" data-label="Order ID"><?php echo $userdata['name']; ?></td>
                        <td class="align-middle" data-label="Customer">₹<?php echo $order['purchasing_price']; ?>/-</td>
                        <td class="align-middle" data-label="Status"><?php echo $paydata['payment_type']; ?></td>
						<td class="align-middle" data-label="Status"><span class="badge badge-pill text-white px-3 py-2 badge-success">
						<?php if($paydata['payment_status']=='1'){echo'Paid';} else {echo'Unpaid';}?></span></td>
                       	<td class="align-middle" data-label="Date of order"><?php echo $paydata['date']; ?></td>
                      
					   <td class="align-middle" data-label="Actions" class="text-md-center dropdown dropleft">
                        <button class="btn btn-danger"><a href="http://asianherbs.in/admin/order/add.php?orderid=<?php echo $orderid; ?>" style="text-decoration:none;color:white">View</a></button>
                        </td>
					</tr>
<?php
}
?>
                    </tbody>
                  </table>
                </div>



            </div>



          </div>


      </section>


      <script src="../assets/js/lib/jquery.min.js"></script>
      <script src="../assets/js/lib/popper.min.js"></script>
      <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="../assets/js/chosen-js/chosen.jquery.js"></script>
      <script src="../assets/js/custom.js"></script>

      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
      <script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

  </body>

<!-- Mirrored from cosmoadmin.com/preview/ecommerce-orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:21:24 GMT -->
</html>
