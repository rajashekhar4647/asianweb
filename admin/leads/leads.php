<?php
     include('../includes/connection.php');


?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Base CSS -->
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Fontawesome Icons -->
    <link href="../assets/css/fontawesome/fontawesome-all.min.css" rel="stylesheet">

    <title>Cosmo - Responsive Dashboard Admin Template</title>
    <style>


    	.modal-confirm {
		color: #636363;
		width: 400px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
        text-align: center;
		font-size: 14px;
	}
	.modal-confirm .modal-header {
		border-bottom: none;
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -10px;
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -2px;
	}
	.modal-confirm .modal-body {
		color: #999;
	}
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;
		border-radius: 5px;
		font-size: 13px;
		padding: 10px 15px 25px;
	}
	.modal-confirm .modal-footer a {
		color: #999;
	}
	.modal-confirm .icon-box {
		width: 80px;
		height: 80px;
		margin: 0 auto;
		border-radius: 50%;
		z-index: 9;
		text-align: center;
		border: 3px solid #f15e5e;
	}
	.modal-confirm .icon-box i {
		color: #f15e5e;
		font-size: 46px;
		display: inline-block;
		margin-top: 13px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #60c7c1;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
		min-width: 120px;
        border: none;
		min-height: 40px;
		border-radius: 3px;
		margin: 0 5px;
		outline: none !important;
    }
	.modal-confirm .btn-info {
        background: #c1c1c1;
    }
    .modal-confirm .btn-info:hover, .modal-confirm .btn-info:focus {
        background: #a8a8a8;
    }
    .modal-confirm .btn-danger {
        background: #f15e5e;
    }
    .modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
        background: #ee3535;
    }
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
    </style>
  </head>
  <body>


      <section class="wrapper">


          <!-- SIDEBAR -->
<?php include('../includes/sidebar.php') ?>


          <!--RIGHT CONTENT AREA-->
          <div class="content-area">
<?php include('../includes/header.php') ?>

            <div class="content-wrapper">

                <div class="row page-tilte align-items-center">
                  <div class="col-md-auto">
                    <a href="#" class="mt-3 d-md-none float-right toggle-controls"><span class="material-icons">keyboard_arrow_down</span></a>
                    <h1 class="weight-300 h3 title">Website Leads </h1>

                  </div>
                  <div class="col controls-wrapper mt-3 mt-md-0 d-none d-md-block ">
                    <div class="controls d-flex justify-content-center justify-content-md-end">
                    <input type="search" id="myInput"  class="form-control d-inline-block" placeholder="Search in tabel...">

                      </div>



                  </div>
                </div>

                <div >
                  <table id="data" class="table mb-4 responsive-table table-bordered bg-white">

                         <thead>
                          <tr>
                            <th scope="col" width="1" class="border-top-0"># ID</th>
                            <th scope="col" class="border-top-0">Full Name</th>

                            <th scope="col" class="border-top-0">State</th>
                            <th scope="col" class="border-top-0">Phone No.</th>
                            <th scope="col" class="border-top-0">Date</th>

                          </tr>
                        </thead>
                        <tbody id="myTable">


                          <?php
                $sql=mysqli_query($link,"SELECT * FROM `enquiryusers` ORDER BY `id` DESC");
                WHILE($users=mysqli_fetch_array($sql))
                {
                  $customerid=$users['id'];
                  $username=$users['name'];
                  $useremail=$users['email'];
                  $userphone=$users['phone'];
				  $userstate=$users['state'];
				  $userdate=$users['date'];
                  

                ?>
                          <tr>
                            <td class=" align-middle text-center">
                              <span class=" text-success"># <?php echo $customerid; ?> </span>
                            </td>
                            <td class="align-middle">
                              <small class="text-muted weight-300"><?php echo '<span style="color:black;font-size:20px;">' . $username . '<span>' .'<br>';echo '<span style="color:blue;font-size:12px;">'. $useremail . '</span>'; ?></small>
                            </td>
                            <td class="align-middle">

                             <div class="weight-400"><?php echo $userstate; ?></div>
                            </td>
                            <td class="align-middle"><div class="weight-400"><?php echo $userphone . ''; ?><a style="margin-left:20px;font-size:20px;color:#2bf04e;" href="tel:<?php echo $userphone; ?>"><i class="fas fa-phone-volume"></i></a></div></td>
                            <td class="align-middle">
                             <div class="weight-400"><?php echo $userdate; ?></div>
                            </td>
                          </tr>


                          <?php
                          }
                          ?>



                        </tbody>

                    </table>
                </div>






            </div>



          </div>


      </section>



      <script src="../assets/js/lib/jquery.min.js"></script>
      <script src="../assets/js/lib/popper.min.js"></script>
      <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="../assets/js/chosen-js/chosen.jquery.js"></script>
      <script src="../assets/js/custom.js"></script>


      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
     <script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
  </body>
 <!-- Modal -->
    <div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>
				<h4 class="modal-title">Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger">Delete</button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
    $('#data').after('<div id="nav" style=""></div>');
    var rowsShown = 10;
    var rowsTotal = $('#data tbody tr').length;
    var numPages = rowsTotal/rowsShown;
    for(i = 0;i < numPages;i++) {
        var pageNum = i + 1;
        $('#nav').append('<a href="#" class="page-link" style="display:unset;margin-left: 0;color: #6c757d;font-weight: 400;" rel="'+i+'">'+pageNum+'</a> ');
    }
    $('#data tbody tr').hide();
    $('#data tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function(){

        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#data tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
});

</script>

</html>
