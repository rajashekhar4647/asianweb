<!doctype html>
<html lang="en">
  
<!-- Mirrored from cosmoadmin.com/preview/ecommerce-dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:24:44 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="Cosmo - Responsive Dashboard Admin Template">
    <meta name="author" content="Akshay Kumar">

    <link rel="icon" href="assets/images/favicon.ico">

    <!-- Base CSS -->
    <link rel="stylesheet" href="assets/css/basestyle/style.css">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="assets/css/daterangepicker/daterangepicker.css" />

     
    <title></title>

    
  </head>
  <body>

      <!-- Pre Loader-->
     
      <!-- Pre Loader-->



      <section class="wrapper">


          <!-- SIDEBAR -->
<?php include('includes/sidebar.php') ?>



          <!--RIGHT CONTENT AREA-->
          <div class="content-area">


<?php include('includes/header.php') ?>
            
            <div class="content-wrapper">

                <div class="row page-tilte align-items-center">
                  <div class="col-md-auto">
                    <a href="#" class="mt-3 d-md-none float-right toggle-controls"><span class="material-icons">keyboard_arrow_down</span></a>
                    <h1 class="weight-300 h3 title">eCommerce Dashboard</h1>
                    <p class="text-muted m-0 desc">Get a quick overview of eCommerce</p>
                  </div> 
                  <div class="col controls-wrapper mt-3 mt-md-0 d-none d-md-block ">
                    <div class="controls d-flex justify-content-center justify-content-md-end">
                      <input type="text" class="form-control date-rage width-260" >
                      <button type="button" class="btn btn-danger">Apply Range</button>
                      <span class="dropdown">
                      <button type="button" id="downloadGrid" data-toggle="dropdown" class="btn btn-secondary py-1 px-2" ><span class="material-icons align-text-bottom">save_alt</span></button>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="downloadGrid">
                        <a class="dropdown-item" href="#">Save as PDF</a>
                        <a class="dropdown-item" href="#">Save as Image</a>
                        <a class="dropdown-item" href="#">Save as Word</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Export Excel</a>
                      </div>
                    </div>
                  </div>
                </div> 


                      
                <div class="content">
                      

                      
                      <div class="card mb-4">
                          <div class="card-body  p-lg-4">
                              <div class="row">

                                  

                                  <div class="col-md-6 col-lg-3  mb-4 mb-lg-0">
                                      <div class="media align-items-center">
                                          <span class="material-icons text-info mr-4 circle p-3 border border-info bg-info-light25">shopping_cart</span>
                                          <div class="media-body">
                                              <h4 class="weight-400 m-0">4982</h4>
                                              <small class="text-muted">Order Recevied</small>
                                          </div>
                                      </div>
                                      <div class="progress mt-3" style="height: 6px;">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                  </div>

                                  <div class="col-md-6 col-lg-3  mb-4 mb-lg-0">
                                      <div class="media align-items-center">
                                          <span class="material-icons text-danger mr-3 circle p-3 border border-danger bg-danger-light25">monetization_on</span>
                                          <div class="media-body">
                                              <h4 class="weight-400 m-0">$23,470.00</h4>
                                              <small class="text-muted">Total Earnings</small>
                                          </div>
                                      </div>
                                      <div class="progress mt-3" style="height: 6px;">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                  </div>

                                  <div class="col-md-6 col-lg-3  mb-4 mb-md-0 ">
                                      <div class="media align-items-center">
                                          <span class="material-icons text-success mr-3 circle p-3 border border-success bg-success-light25">person_add</span>
                                          <div class="media-body">
                                              <h4 class="weight-400 m-0">619</h4>
                                              <small class="text-muted">New Customers</small>
                                          </div>
                                      </div>
                                      <div class="progress mt-3" style="height: 6px;">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                  </div>

                                  <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                                      <div class="media align-items-center">
                                          <span class="material-icons text-warning mr-3 circle p-3 border border-warning bg-warning-light25">group</span>
                                          <div class="media-body">
                                              <h4 class="weight-400 m-0">395</h4>
                                              <small class="text-muted">Churned Users</small>
                                          </div>
                                      </div>
                                      <div class="progress mt-3" style="height: 6px;">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>



                      <div class="row">
                          <div class="col-lg-8 mb-4">
                              <div class="card  h-100">
                                  <div class="card-header py-3">Sales &amp; Users Report</div>
                                  <div class="card-body ">
                                        
                                        <canvas id="canvas"></canvas>

                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-4 mb-4">
                              <div class="card mb-4">
                                  <div class="card-body">
                                      <h6 class="weight-400 mb-3 border-left border-warning pl-2 border-width-medium">USA <span class="text-muted">Sales Report</span></h6>
                                      <div class="row">
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                      </div>
                                      <div class="progress mt-3" style="height: 6px;">
                                        <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="card mb-4">
                                  <div class="card-body">
                                      <h6 class="weight-400 mb-3 border-left border-primary pl-2 border-width-medium">USA <span class="text-muted">Sales Report</span></h6>
                                      <div class="row">
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                      </div>
                                      <div class="progress mt-3" style="height: 6px;">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 35%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 15%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="card">
                                  <div class="card-body">
                                      <h6 class="weight-400 mb-3 border-left border-danger pl-2 border-width-medium">USA <span class="text-muted">Sales Report</span></h6>
                                      <div class="row">
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                          <div class="col">
                                              <small class="text-muted weight-300">This Week</small>
                                              <div class="weight-400">$28,343</div>
                                          </div>
                                      </div>
                                      <div class="progress mt-3" style="height: 6px;">
                                        <div class="progress-bar bg-secondary" role="progressbar" style="width: 35%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                        <div class="progress-bar " role="progressbar" style="width: 10%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-lg-4 mb-4 mb-lg-0 ">
                              <div class="card h-100 ">
                                  <div class="card-header">Order Status</div>
                                  <div class="card-body">
                                      <h3 class="weight-400 text-center mb-0"><span class="material-icons mr-2 text-success">arrow_upward</span>73,239</h3>
                                      <p class="text-muted text-center">Lorem ipsum dolor sit amet</p>
                                      <canvas id="chart-area" height="220"></canvas>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-8">
                              <div class="card">
                                  <div class="card-body table-responsive p-0">

                                      <table class="table  m-0">
                                      <thead>
                                        <tr>
                                          <th scope="col" width="1" class="border-top-0">#</th>
                                          <th scope="col" class="border-top-0">Full Name</th>
                                          
                                          <th scope="col" class="border-top-0">Address</th>
                                          <th scope="col" class="border-top-0">Avg. Session</th>
                                          <th scope="col" class="border-top-0 text-right">Actions</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td class=" align-middle text-center">
                                            <span class="user-initials bg-success-light25 text-success">MD</span>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Mark Doessy</small>
                                            <div><a href="#" class="weight-400">mark.doessy@gmail.com</a></div>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">New York</small>
                                           <div class="weight-400"> 591  Valley Drive, Philadelphia</div>
                                          </td>
                                          <td class="align-middle"><span class="material-icons align-middle md-18 text-danger">expand_more</span> 32 Sec</td>
                                          <td class="align-middle text-right">@mdo</td>
                                        </tr>
                                        <tr>
                                          <td class=" align-middle text-center">
                                            <span class="user-initials bg-danger-light25 text-danger">AK</span>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Akshay Kumar</small>
                                            <div><a href="#" class="weight-400">akshay.azaste@gmail.com</a></div>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Philadelphia</small>
                                           <div class="weight-400"> 3318 Lilac Lane</div>
                                          </td>
                                          <td class="align-middle"><span class="material-icons align-middle md-18 text-success">expand_less</span> 12 Sec</td>
                                          <td class="align-middle text-right">@mdo</td>
                                        </tr>
                                        <tr>
                                          <td class=" align-middle text-center">
                                            <span class="user-initials bg-warning-light25 text-warning">GT</span>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Giselle K Trivett</small>
                                            <div><a href="#" class="weight-400">willy_dicki@yahoo.com</a></div>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Indianapolis</small>
                                           <div class="weight-400"> 2961 Clay Street,</div>
                                          </td>
                                          <td class="align-middle"><span class="material-icons align-middle md-18 text-success">expand_less</span> 5.4 Mins</td>
                                          <td class="align-middle text-right">@mdo</td>
                                        </tr>
                                        <tr>
                                          <td class=" align-middle text-center">
                                            <span class="user-initials bg-primary-light25 text-primary">DG</span>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">David Gonsalves</small>
                                            <div><a href="#" class="weight-400">wilburn.magg@yahoo.com</a></div>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">London</small>
                                           <div class="weight-400"> 9009  Lorem Drive, Elphia</div>
                                          </td>
                                          <td class="align-middle"><span class="material-icons align-middle md-18 text-success">expand_less</span> 1.3 Mins</td>
                                          <td class="align-middle text-right">@mdo</td>
                                        </tr>
                                        <tr>
                                          <td class=" align-middle text-center">
                                            <span class="user-initials bg-info-light25 text-info">AR</span>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Annie Roughton</small>
                                            <div><a href="#" class="weight-400">manu.akshay@love.com</a></div>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Green Bay</small>
                                           <div class="weight-400"> 848 Tail Ends Road</div>
                                          </td>
                                          <td class="align-middle"><span class="material-icons align-middle md-18 text-success">expand_less</span> 45 Sec</td>
                                          <td class="align-middle text-right">@mdo</td>
                                        </tr>
                                        <tr>
                                          <td class=" align-middle text-center">
                                            <span class="user-initials bg-danger-light25 text-danger">LE</span>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">LoriH Edenfield</small>
                                            <div><a href="#" class="weight-400">haan_dougla10@hotmail.com</a></div>
                                          </td>
                                          <td class="align-middle">
                                            <small class="text-muted weight-300">Newark</small>
                                           <div class="weight-400"> 49 Spring Haven Trail</div>
                                          </td>
                                          <td class="align-middle"><span class="material-icons align-middle md-18 text-danger">expand_more</span> 2.32 Mins</td>
                                          <td class="align-middle text-right">@mdo</td>
                                        </tr>
                                        
                                      </tbody>
                                    </table>

                                  </div>
                                </div>
                          </div>
                      </div>
                          


                </div>



                <footer class="footer">
                  <p class="text-muted m-0"><small class="float-right">Made with <span class="material-icons md-16 text-danger align-middle">favorite</span> by Akshay Kumar </small><small >FreakPixels © 2017–2018 - freakpixels.com</small></p>
                </footer>




            </div>



          </div>


      </section>







      <div class="modal fade" id="switchApp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header px-4">
              <h5 class="modal-title" id="exampleModalLabel">Select Application</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="material-icons ">close</span>
              </button>
            </div>
            <div class="modal-body p-4">
              
              <div class="row align-items-stretch app-switcher">
                <div class="col-lg-3 col-6 mb-4">
                    <a href="index.html" class="text-dark">
                    <div class="card h-100">
                      <div class="card-body text-center">
                        <span class="material-icons md-48 " style="color:#21C3E0;">bubble_chart</span>
                        <p class="weight-400 m-0">Dashboard</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                      </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-3 col-6 mb-4">
                    <a href="ecommerce-dashboard.html" class="text-dark">
                    <div class="card h-100">
                      <div class="card-body text-center">
                        <span class="material-icons md-48 text-primary">security</span>
                        <p class="weight-400 m-0">Ecommerce</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                      </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-3 col-6 mb-4">
                    <a href="mailbox.html" class="text-dark">
                    <div class="card h-100 active-app">
                      <div class="card-body text-center">
                        <span class="material-icons md-48 text-danger">blur_on</span>
                        <p class="weight-400 m-0">Mail Box</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                        <span class="material-icons app-selected md-16">check</span>
                      </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-3 col-6 mb-4">
                    <a href="blogger-feed-list.html" class="text-dark">
                    <div class="card h-100">
                      <div class="card-body text-center">
                        <span class="material-icons md-48 text-warning">camera</span>
                        <p class="weight-400 m-0">Blogger</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                      </div>
                    </div>
                    </a>
                </div>
                
                
                <div class="col-lg-3 col-6 mb-lg-0 mb-4">
                    <a href="task-manager.html" class="text-dark">
                    <div class="card h-100">
                      <div class="card-body text-center">
                        <span class="material-icons md-48" style="color:#f98e69;">leak_add</span>
                        <p class="weight-400 m-0">Task Manager</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                      </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-3 col-6 mb-lg-0 mb-4">
                    <a href="notes.html" class="text-dark">
                    <div class="card h-100">
                      <div class="card-body text-center">
                        <span class="material-icons md-48 text-success">color_lens</span>
                        <p class="weight-400 m-0">Notes</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                      </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-3 col-6 ">
                    <a href="file-uploder.html" class="text-dark">
                    <div class="card h-100">
                      <div class="card-body text-center">
                        <span class="material-icons md-48 " style="color:#9b84fb">category</span>
                        <p class="weight-400 m-0">File Manager</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                      </div>
                    </div>
                    </a>
                </div> 
                <div class="col-lg-3 col-6 ">
                    <a href="photos-videos.html" class="text-dark">
                    <div class="card h-100">
                      <div class="card-body text-center">
                        <span class="material-icons md-48 text-muted" >layers</span>
                        <p class="weight-400 m-0">Videos & Photos</p>
                        <small class="weight-300 text-muted">This is some text within a card body.</small>
                      </div>
                    </div>
                    </a>
                </div>
              </div>

            </div>
            <div class="modal-footer px-4">
              <div class="custom-control custom-checkbox mr-auto">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Default open this application</label>
              </div>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Switch Application</button>
            </div>
          </div>
        </div>
      </div>




      <a target="_blank" href="https://themeforest.net/item/cosmo-bootstrap-4-responsive-admin-dashboard-template/22392885" class="buynow-btn btn btn-success text-white"><span class="material-icons mr-2 align-middle">shopping_cart</span> <span class="text">Buy Now $23</span></a>


      <!-- ChartJS -->
      <script src="assets/js/chartjs/Chart.js"></script>
      <script src="assets/js/chartjs/utils.js"></script>

      
      <script src="assets/js/lib/moment.min.js"></script>
      <script src="assets/js/lib/jquery.min.js"></script>
      <script src="assets/js/lib/popper.min.js"></script>
      <script src="assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="assets/js/chosen-js/chosen.jquery.js"></script>
      
      
      <script src="assets/js/custom.js"></script>

      <script src="assets/js/daterangepicker/daterangepicker.min.js"></script>

      <script src="assets/js/pages/ecommerce-dashboard.js"></script>




      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
      </section>
  </body>

<!-- Mirrored from cosmoadmin.com/preview/ecommerce-dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:24:45 GMT -->
</html>