<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Base CSS -->
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Fontawesome Icons -->
    <link href="../assets/css/fontawesome/fontawesome-all.min.css" rel="stylesheet">

    <title>Cosmo - Responsive Dashboard Admin Template</title>

  </head>
  <body>


      <section class="wrapper">
<?php include('../includes/sidebar.php') ?>

          <!-- SIDEBAR -->
 

          <!--RIGHT CONTENT AREA-->
          <div class="content-area">

           <?php include('../includes/header.php') ?>
            <div class="content-wrapper">

           
 
      <section class="wrapper">

<div class="container-fluid">
          <div class="row">
    
    <div class="col-md-3">
              </div>
                 <div class="col-md-6" style="
    padding: 20px;
    background-color: white;
">
                              <div class="login">
             
              <div class="form">

                  <div class="text-center mb-4"><span class="material-icons text-danger" style="font-size:6rem;" ><i class="far fa-user-circle"></i></span></div>

                  <h3 class="h4 mb-5 text-center">Create Account</h3>



                  <form>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Full Name</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email </label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Re Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    
                    <a href="index.html" class="btn mt-4 btn-primary btn-block">Sign Up</a>
                  </form>

              </div>
          </div>
                    

              </div>
    <div class="col-md-3">
              </div>  
    </div>
          
          
          </div>
 
      </section>


      <div class="modal fade " id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title" id="exampleModalLabel">Forgot Your Password ?</div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email or Username</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Reset Password</button>
            </div>
          </div>
        </div>
      </div>


            </div>



          </div>


      </section>
   

      <script src="../assets/js/lib/jquery.min.js"></script>
      <script src="../assets/js/lib/popper.min.js"></script>
      <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="../assets/js/chosen-js/chosen.jquery.js"></script>
      <script src="../assets/js/custom.js"></script>

      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
      <script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<script>
$(document).ready(function(){
    $('#data').after('<div id="nav" style=""></div>');
    var rowsShown = 10;
    var rowsTotal = $('#data tbody tr').length;
    var numPages = rowsTotal/rowsShown;
    for(i = 0;i < numPages;i++) {
        var pageNum = i + 1;
        $('#nav').append('<a href="#" class="page-link" style="display:unset;margin-left: 0;color: #6c757d;font-weight: 400;" rel="'+i+'">'+pageNum+'</a> ');
    }
    $('#data tbody tr').hide();
    $('#data tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function(){

        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#data tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
});

</script>
  </body>

<!-- Mirrored from cosmoadmin.com/preview/ecommerce-orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:21:24 GMT -->
</html>