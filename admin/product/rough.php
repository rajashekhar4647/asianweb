<!doctype html>
<html lang="en">
  
<!-- Mirrored from cosmoadmin.com/preview/ecommerce-products.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:24:45 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
		
    <!-- Base CSS -->
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">
   
		<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="../css/demo.css" />
		<link rel="stylesheet" type="text/css" href="../css/component.css" />

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Fontawesome Icons -->
    <link href="../assets/css/fontawesome/fontawesome-all.min.css" rel="stylesheet">

    <title>Cosmo - Responsive Dashboard Admin Template</title>
    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
    <style>
 .field {
	 --uiFieldPlaceholderColor: var(--fieldPlaceholderColor, #767676);
}
 .field__input {
	 background-color: transparent;
	 border-radius: 0;
	 border: none;
	 -webkit-appearance: none;
	 -moz-appearance: none;
	 font-family: inherit;
	 font-size: 1em;
}
 .field__input:focus::-webkit-input-placeholder {
	 color: var(--uiFieldPlaceholderColor);
}
 .field__input:focus::-moz-placeholder {
	 color: var(--uiFieldPlaceholderColor);
	 opacity: 1;
}
/* ===== LEVEL 2. CORE STYLES ===== */
 .a-field {
	 display: inline-block;
}
 .a-field__input {
	 display: block;
	 box-sizing: border-box;
	 width: 100%;
}
 .a-field__input:focus {
	 outline: none;
}
/* ===== LEVEL 3. PRESENTATION STYLES ===== */
/* a-field */
 .a-field {
	 --uiFieldHeight: var(--fieldHeight, 40px);
	 --uiFieldBorderWidth: var(--fieldBorderWidth, 2px);
	 --uiFieldBorderColor: var(--fieldBorderColor);
	 --uiFieldFontSize: var(--fieldFontSize, 1em);
	 --uiFieldHintFontSize: var(--fieldHintFontSize, 1em);
	 --uiFieldPaddingRight: var(--fieldPaddingRight, 15px);
	 --uiFieldPaddingBottom: var(--fieldPaddingBottom, 15px);
	 --uiFieldPaddingLeft: var(--fieldPaddingLeft, 15px);
	 position: relative;
	 box-sizing: border-box;
	 font-size: var(--uiFieldFontSize);
	 padding-top: 1em;
}
 .a-field__input {
	 height: var(--uiFieldHeight);
	 padding: 0 var(--uiFieldPaddingRight) 0 var(--uiFieldPaddingLeft);
	 border-bottom: var(--uiFieldBorderWidth) solid var(--uiFieldBorderColor);
}
 .a-field__input::-webkit-input-placeholder {
	 opacity: 0;
	 transition: opacity 0.2s ease-out;
}
 .a-field__input::-moz-placeholder {
	 opacity: 0;
	 transition: opacity 0.2s ease-out;
}
 .a-field__input:not(:placeholder-shown) ~ .a-field__label-wrap .a-field__label {
	 opacity: 0;
	 bottom: var(--uiFieldPaddingBottom);
}
 .a-field__input:focus::-webkit-input-placeholder {
	 opacity: 1;
	 transition-delay: 0.2s;
}
 .a-field__input:focus::-moz-placeholder {
	 opacity: 1;
	 transition-delay: 0.2s;
}
 .a-field__label-wrap {
	 box-sizing: border-box;
	 width: 100%;
	 height: var(--uiFieldHeight);
	 pointer-events: none;
	 cursor: text;
	 position: absolute;
	 bottom: 0;
	 left: 0;
     font-size: 120%;
}
 .a-field__label {
	 position: absolute;
	 left: var(--uiFieldPaddingLeft);
	 bottom: calc(50% - .5em);
	 line-height: 1;
	 font-size: var(--uiFieldHintFontSize);
	 pointer-events: none;
	 transition: bottom 0.2s cubic-bezier(0.9, -0.15, 0.1, 1.15), opacity 0.2s ease-out;
	 will-change: bottom, opacity;
}
 .a-field__input:focus ~ .a-field__label-wrap .a-field__label {
	 opacity: 1;
	 bottom: var(--uiFieldHeight);
}
/* a-field_a1 */
 .a-field_a1 .a-field__input {
	 transition: border-color 0.2s ease-out;
	 will-change: border-color;
}
 .a-field_a1 .a-field__input:focus {
	 border-color: var(--fieldBorderColorActive);
}
/* a-field_a2 */
 .a-field_a2 .a-field__label-wrap::after {
	 content: "";
	 box-sizing: border-box;
	 width: 0;
	 height: var(--uiFieldBorderWidth);
	 background-color: var(--fieldBorderColorActive);
	 position: absolute;
	 bottom: 0;
	 left: 0;
	 will-change: width;
	 transition: width 0.285s ease-out;
}
 .a-field_a2 .a-field__input:focus ~ .a-field__label-wrap::after {
	 width: 100%;
}
/* a-field_a3 */
 .a-field_a3 {
	 padding-top: 1.5em;
}
 .a-field_a3 .a-field__label-wrap::after {
	 content: "";
	 box-sizing: border-box;
	 width: 100%;
	 height: 0;
	 opacity: 0;
	 border: var(--uiFieldBorderWidth) solid var(--fieldBorderColorActive);
	 position: absolute;
	 bottom: 0;
	 left: 0;
	 will-change: opacity, height;
	 transition: height 0.2s ease-out, opacity 0.2s ease-out;
}
 .a-field_a3 .a-field__input:focus ~ .a-field__label-wrap::after {
	 height: 100%;
	 opacity: 1;
}
 .a-field_a3 .a-field__input:focus ~ .a-field__label-wrap .a-field__label {
	 bottom: calc(var(--uiFieldHeight) + .5em);
}
/* ===== LEVEL 4. SETTINGS ===== */
 .field {
	 --fieldBorderColor: rgba(105, 186, 109, 0.74);
	 --fieldBorderColorActive: #69BA6D;
}
/* ===== DEMO ===== */
 body {
	 font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Open Sans, Ubuntu, Fira Sans, Helvetica Neue, sans-serif;
	 margin: 0;
}
 .page {
	 padding-left: 15px;
	 padding-right: 15px;
}
 @media (max-width: 1000px) {
	 .page {
		 padding-top: 100px;
	}
	 .page__field {
		 width: 100%;
	}
	 .page__field:nth-child(n+2) {
		 margin-top: 40px;
	}
}
      
 @media (min-width: 1001px) {
	 body {
		 min-height: 100vh;
		 display: flex;
	}
	 .page {
		 box-sizing: border-box;
		 width: 1000px;
		 margin: auto;
		 display: flex;
		 align-items: flex-end;
	}
	 .page__field {
		 margin-left: 2%;
		 margin-right: 2%;
		 flex-grow: 1;
	}
}
/* Patreon */
 .patreon {
	 width: 100%;
	 padding-top: 15px;
	 padding-bottom: 15px;
	 text-align: center;
	 background-color: #fffdee;
	 position: absolute;
	 top: 0;
	 left: 0;
}
 .patreon__container {
	 padding-left: 10px;
	 padding-right: 10px;
}
 .patreon__link {
	 text-decoration: underline;
	 color: #ff4242;
}
    </style>
  </head>
  <body>

     
      <section class="wrapper">


          <!-- SIDEBAR -->
<?php include('../includes/sidebar.php') ?>


          <!--RIGHT CONTENT AREA-->
          <div class="content-area">
<?php include('../includes/header.php') ?>
         
            <div class="content-wrapper">

                <div class="row page-tilte align-items-center">
                  <div class="col-md-auto">
                    <a href="#" class="mt-3 d-md-none float-right toggle-controls"><span class="material-icons">keyboard_arrow_down</span></a>
                    <h1 class="weight-300 h3 title">Add Products </h1>
                    <p class="text-muted m-0 desc">Total products catelog inventory</p>
                  </div> 
                  <div class="col controls-wrapper mt-3 mt-md-0 d-none d-md-block ">
                    <div class="controls d-flex justify-content-center justify-content-md-end">
                   
                    
                        <button class="btn btn-danger"><a href="#" style="text-decoration:none;color:white">Add Product</a></button>
                    </div>

                  </div>
                </div> 

                <div >
                <div style="width:100%;border-radius:10px;background-color:white;padding-bottom:30px;">
                    <div class="row" style="padding:20px;">
                    <div class="col-md-3" style="text-align:center">
                        <h5>
<a href="#">Basic Info</a>
</h5>
                       </div>
                         <div class="col-md-3" style="text-align:center">
                           <p style="text-align:center; font-weight:bold; font-style:italic;">
<a href="">(Swap Divs)</a>
</p>
                        </div>
                        <div class="col-md-3" style="text-align:center">
                        <h5>Price</h5>
                        </div>
                        <div class="col-md-3" style="text-align:center">
                        <h5>Inventory</h5>
                        </div>
                    
                    </div><hr>
                    <div id="swapper-first" style="display:block;">
                <div class="page">
     		<div class="container">
			
			<div class="content">

			

				<div class="box">
					<input type="file" name="file-5[]" id="file-5" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple style="    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;"/>
					<label for="file-5"><figure><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg></figure> <span>Choose a file&hellip;</span></label>
				</div>

			

			


			</div>
			<!-- Related demos -->
			
		</div>
  
</div>
                           <div class="page">
    
    <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Category</span>
      </span>
    </label>
                    <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Tags</span>
      </span>
    </label>
</div></div>
          
                    </div>
<script type="text/javascript">
function myfunction(div1,div2)
{
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
   if( d2.style.display == "none" )
   {
      d1.style.display = "none";
      d2.style.display = "block";
   }
   else
   {
      d1.style.display = "block";
      d2.style.display = "none";
   }
}
</script>





<script type="text/javascript">
function SwapDivsWithClick(div1,div2)
{
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
   if( d2.style.display == "none" )
   {
      d1.style.display = "none";
      d2.style.display = "block";
   }
   else
   {
      d1.style.display = "block";
      d2.style.display = "none";
   }
}
</script>

                    
                    </div>
                </div>

            </div>


      </section>





     

      <script src="../assets/js/lib/jquery.min.js"></script>
      <script src="../assets/js/lib/popper.min.js"></script>
      <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="../assets/js/chosen-js/chosen.jquery.js"></script>
      <script src="../assets/js/custom.js"></script>


      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
<script src="../js/custom-file-input.js"></script>
  </body>

<!-- Mirrored from cosmoadmin.com/preview/ecommerce-products.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:24:45 GMT -->
</html>