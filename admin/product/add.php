<?php
     include('../includes/connection.php');

   /*  if (! $conn){echo "<h1>disconnect</h1>";}
         else{echo "<h1> Server connected</h1>" . "<br>";}
         if (! $db){echo "<h1> database disconnected</h1>";}
         else {echo "<h1> database connected</h1>". "<br>";} */

        /*server connect  */

     if(isset($_POST['addbutton']))
     {
       $p_name = $_POST['name'];
       $p_price = $_POST['price'] ;
       $p_description =mysqli_real_escape_string($conn,$_POST['description']);
	   $nutri_fact = mysqli_real_escape_string($conn,$_POST['nutri_fact']);
       $micro_tab_qty=$_POST['micro_tab_qty'];
	   $micro_in_qty=$_POST['micro_in_qty'];
	   if($micro_in_qty>='1')
	   {
		   $micro_in_status='1';
	   }
	   else
	   {
		   $micro_in_status='0';
	   }
	   $mini_tab_qty=$_POST['mini_tab_qty'];
	   $mini_in_qty=$_POST['mini_in_qty'];
	   if($mini_in_qty>='1')
	   {
		   $mini_in_status='1';
	   }
	   else
	   {
		   $mini_in_status='0';
	   }
	   $large_tab_qty=$_POST['large_tab_qty'];
	   $large_in_qty=$_POST['large_in_qty'];
	   if($large_in_qty>='1')
	   {
		   $large_in_status='1';
	   }
	   else
	   {
		   $large_in_status='0';
	   }

       $auto_id=mysqli_query($conn,"SHOW TABLE STATUS LIKE 'products'");

       $rowid=mysqli_fetch_array($auto_id);

       $productid=$rowid['Auto_increment'];


       $toCreate = array(
        '../../products/uploads/'.$productid.'/cover/',
		'../../products/uploads/'.$productid.'/nutriimg/',
        '../../products/uploads/'.$productid.'/showcaseimages/'
      );
      $permissions = 0755;
      foreach ($toCreate as $dir)
      {
      mkdir($dir, $permissions, TRUE);
      }

/* Add Cover Image*/
$target_dir = "../../products/uploads/".$productid."/cover/";
$target_file = $target_dir . basename($_FILES["cover_image"]["name"]);
$imagename=basename($_FILES["cover_image"]["name"]);
move_uploaded_file($_FILES["cover_image"]["tmp_name"], $target_file);
/* Add Cover Image End*/
/* Add Nutri Image*/
$target_dir_nutri = "../../products/uploads/".$productid."/nutriimg/";
$target_file_nutri = $target_dir_nutri . basename($_FILES["nutri_image"]["name"]);
$nutriimagename=basename($_FILES["nutri_image"]["name"]);
move_uploaded_file($_FILES["nutri_image"]["tmp_name"], $target_file_nutri);
/* Add Nutri Image End*/
  $sql = "INSERT INTO products (`product_name`, `product_price`, `product_description`,`cover_img`,`nutri_fact`,`nutri_img`) VALUES ('$p_name', ' $p_price', ' $p_description','$imagename','$nutri_fact','nutriimagename')";
   if (mysqli_query($conn, $sql))
  {
    echo "sucessfully<br>";
  }
  else{
    echo "nosend";
    }
 $sql2=mysqli_query($conn,"INSERT INTO `product_qty`(`product_id`,`product_pack_id`,`tabs_qty`,`inhouse_qty`,`product_status`) VALUES ('$productid','1','$micro_tab_qty','$micro_in_qty','$micro_in_status'),('$productid','2','$mini_tab_qty','$mini_in_qty','$mini_in_status'),('$productid','3','$large_tab_qty','$large_in_qty','$large_in_status')");

/* add  showcase image */
// Count # of uploaded files in array
$total = count($_FILES['showcase_images']['name']);

// Loop through each file
for( $i=0 ; $i < $total ; $i++ ) {

  //Get the temp file path
$tmpFilePath = $_FILES['showcase_images']['tmp_name'][$i];
$filename=$_FILES['showcase_images']['name'][$i];
  //Make sure we have a file path
  if ($tmpFilePath != ""){
    //Setup our new file path
    $newFilePath = "../../products/uploads/$productid/showcaseimages/". $filename;

    //Upload the file into the temp dir
    if(move_uploaded_file($tmpFilePath, $newFilePath)) 	{

    $query_mul_img=mysqli_query($conn,"INSERT INTO `showcase_image` (`product_id`,`location`,`filename`) VALUES ('$productid','$newFilePath','$filename')");

    }
  }
}
    }
?>




<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Base CSS -->
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">
    <link rel="stylesheet" href="../assets/css/basestyle/style.css">

		<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="../css/demo.css" />
		<link rel="stylesheet" type="text/css" href="../css/component.css" />

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Fontawesome Icons -->
    <link href="../assets/css/fontawesome/fontawesome-all.min.css" rel="stylesheet">

    <title>Cosmo - Responsive Dashboard Admin Template</title>
    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
    <style>
 .field {
	 --uiFieldPlaceholderColor: var(--fieldPlaceholderColor, #767676);
}
 .field__input {
	 background-color: transparent;
	 border-radius: 0;
	 border: none;
	 -webkit-appearance: none;
	 -moz-appearance: none;
	 font-family: inherit;
	 font-size: 1em;
}
 .field__input:focus::-webkit-input-placeholder {
	 color: var(--uiFieldPlaceholderColor);
}
 .field__input:focus::-moz-placeholder {
	 color: var(--uiFieldPlaceholderColor);
	 opacity: 1;
}
/* ===== LEVEL 2. CORE STYLES ===== */
 .a-field {
	 display: inline-block;
}
 .a-field__input {
	 display: block;
	 box-sizing: border-box;
	 width: 100%;
}
 .a-field__input:focus {
	 outline: none;
}
/* ===== LEVEL 3. PRESENTATION STYLES ===== */
/* a-field */
 .a-field {
	 --uiFieldHeight: var(--fieldHeight, 40px);
	 --uiFieldBorderWidth: var(--fieldBorderWidth, 2px);
	 --uiFieldBorderColor: var(--fieldBorderColor);
	 --uiFieldFontSize: var(--fieldFontSize, 1em);
	 --uiFieldHintFontSize: var(--fieldHintFontSize, 1em);
	 --uiFieldPaddingRight: var(--fieldPaddingRight, 15px);
	 --uiFieldPaddingBottom: var(--fieldPaddingBottom, 15px);
	 --uiFieldPaddingLeft: var(--fieldPaddingLeft, 15px);
	 position: relative;
	 box-sizing: border-box;
	 font-size: var(--uiFieldFontSize);
	 padding-top: 1em;
}
 .a-field__input {
	 height: var(--uiFieldHeight);
	 padding: 0 var(--uiFieldPaddingRight) 0 var(--uiFieldPaddingLeft);
	 border-bottom: var(--uiFieldBorderWidth) solid var(--uiFieldBorderColor);
}
 .a-field__input::-webkit-input-placeholder {
	 opacity: 0;
	 transition: opacity 0.2s ease-out;
}
 .a-field__input::-moz-placeholder {
	 opacity: 0;
	 transition: opacity 0.2s ease-out;
}
 .a-field__input:not(:placeholder-shown) ~ .a-field__label-wrap .a-field__label {
	 opacity: 0;
	 bottom: var(--uiFieldPaddingBottom);
}
 .a-field__input:focus::-webkit-input-placeholder {
	 opacity: 1;
	 transition-delay: 0.2s;
}
 .a-field__input:focus::-moz-placeholder {
	 opacity: 1;
	 transition-delay: 0.2s;
}
 .a-field__label-wrap {
	 box-sizing: border-box;
	 width: 100%;
	 height: var(--uiFieldHeight);
	 pointer-events: none;
	 cursor: text;
	 position: absolute;
	 bottom: 0;
	 left: 0;
     font-size: 120%;
}
 .a-field__label {
	 position: absolute;
	 left: var(--uiFieldPaddingLeft);
	 bottom: calc(50% - .5em);
	 line-height: 1;
	 font-size: var(--uiFieldHintFontSize);
	 pointer-events: none;
	 transition: bottom 0.2s cubic-bezier(0.9, -0.15, 0.1, 1.15), opacity 0.2s ease-out;
	 will-change: bottom, opacity;
}
 .a-field__input:focus ~ .a-field__label-wrap .a-field__label {
	 opacity: 1;
	 bottom: var(--uiFieldHeight);
}
/* a-field_a1 */
 .a-field_a1 .a-field__input {
	 transition: border-color 0.2s ease-out;
	 will-change: border-color;
}
 .a-field_a1 .a-field__input:focus {
	 border-color: var(--fieldBorderColorActive);
}
/* a-field_a2 */
 .a-field_a2 .a-field__label-wrap::after {
	 content: "";
	 box-sizing: border-box;
	 width: 0;
	 height: var(--uiFieldBorderWidth);
	 background-color: var(--fieldBorderColorActive);
	 position: absolute;
	 bottom: 0;
	 left: 0;
	 will-change: width;
	 transition: width 0.285s ease-out;
}
 .a-field_a2 .a-field__input:focus ~ .a-field__label-wrap::after {
	 width: 100%;
}
        .mat-ink-bar {
        position: absolute;
    height: 2px;
    transition: .10s cubic-bezier(.35,0,.25,1);
}
/* a-field_a3 */
 .a-field_a3 {
	 padding-top: 1.5em;
}
 .a-field_a3 .a-field__label-wrap::after {
	 content: "";
	 box-sizing: border-box;
	 width: 100%;
	 height: 0;
	 opacity: 0;
	 border: var(--uiFieldBorderWidth) solid var(--fieldBorderColorActive);
	 position: absolute;
	 bottom: 0;
	 left: 0;
	 will-change: opacity, height;
	 transition: height 0.2s ease-out, opacity 0.2s ease-out;
}
 .a-field_a3 .a-field__input:focus ~ .a-field__label-wrap::after {
	 height: 100%;
	 opacity: 1;
}
 .a-field_a3 .a-field__input:focus ~ .a-field__label-wrap .a-field__label {
	 bottom: calc(var(--uiFieldHeight) + .5em);
}
/* ===== LEVEL 4. SETTINGS ===== */
 .field {
	 --fieldBorderColor: rgba(105, 186, 109, 0.74);
	 --fieldBorderColorActive: #69BA6D;
}
/* ===== DEMO ===== */

 .page {
	 padding-left: 15px;
	 padding-right: 15px;
}
 @media (max-width: 1000px) {

	 .page__field {
		 width: 100%;
	}
	 .page__field:nth-child(n+2) {
		 margin-top: 40px;
	}
}

 @media (min-width: 1001px) {

	 .page {
		 box-sizing: border-box;
		 width: 1000px;
		 margin: auto;
		 display: flex;
		 align-items: flex-end;
	}
	 .page__field {
		 margin-left: 2%;
		 margin-right: 2%;
		 flex-grow: 1;
	}
}
/* Patreon */
 .patreon {
	 width: 100%;
	 padding-top: 15px;
	 padding-bottom: 15px;
	 text-align: center;
	 background-color: #fffdee;
	 position: absolute;
	 top: 0;
	 left: 0;
}
 .patreon__container {
	 padding-left: 10px;
	 padding-right: 10px;
}
 .patreon__link {
	 text-decoration: underline;
	 color: #ff4242;
}

        .link{
    color:black;
}
      a.link:hover {
    color: black;
    text-decoration: none;


}
        @media (max-width: 576px){
.col-sm-3 {
    flex: 0 0 33.3333%;
    max-width: 33.3333%;
            }}
    </style>

  </head>
  <body>



      <section class="wrapper">


          <!-- SIDEBAR -->
<?php include('../includes/sidebar.php') ?>


          <!--RIGHT CONTENT AREA-->

          <div class="content-area">
<?php include('../includes/header.php') ?>
<form action="#"  method="POST" enctype="multipart/form-data">
            <div class="content-wrapper">

                <div class="row page-tilte align-items-center">
                  <div class="col-md-auto">
                    <a href="#" class="mt-3 d-md-none float-right toggle-controls"><span class="material-icons">keyboard_arrow_down</span></a>
                    <h1 class="weight-300 h3 title">Add Product</h1>

                  </div>
                  <div class="col controls-wrapper mt-3 mt-md-0 d-none d-md-block ">
                    <div class="controls d-flex justify-content-center justify-content-md-end">


                        <button class="btn btn-lg btn-danger" name="addbutton" type="submit">Add</button>

                    </div>

                  </div>
                </div>

                <div >
                <div style="width:100%;border-radius:10px;background-color:white;padding-bottom:30px;">
                    <div class="row" style="padding:20px;">
                  <div class="col-md-4 col-sm-3" style="text-align:center">
                     <a class="link" href="javascript:info('info','image','first','second','third','inventory')">     <h5>
Basic Info
</h5></a>
                       </div>
                         <div class="col-md-4 col-sm-3" style="text-align:center">
 <h5>
     <a class="link" href="javascript:image('info','image','first','second','third','inventory')">Slider Image's</a></h5>

                        </div>

                        <div class="col-md-4 col-sm-3" style="text-align:center">
                            <a class="link" href="javascript:inventory('info','image','first','second','third','inventory')">           <h5>Inventory</h5></a>
                        </div>

                  </div>
                          <div class="row" style="margin-left:0px;margin-right:0px;">
                      <div class="col-md-4 col-sm-3" style="text-align:center;padding-left:0px;padding-right:0px;">
             <mat-ink-bar class="mat-ink-bar" id="first" style="visibility: visible;display:block;  width: 100%;background-color:black"></mat-ink-bar>
                       </div>
                         <div class="col-md-4 col-sm-3" style="text-align:center;padding-left:0px;padding-right:0px;">
<mat-ink-bar class="mat-ink-bar" id="second" style="visibility: visible;display:none;  width: 100%;background-color:black"></mat-ink-bar>

                        </div>

                        <div class="col-md-4 col-sm-3" style="text-align:center;padding-left:0px;padding-right:0px;">
                       <mat-ink-bar class="mat-ink-bar" id="third" style="visibility: visible;display:none;  width: 100%;background-color:black"></mat-ink-bar>
                        </div>

                  </div>
                  <hr style="margin:0px;">

                    <div id="info" style="display:block;">
                <div class="page" style="margin-top:10px;">
     <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" name="name" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Product Title</span>
      </span>
    </label> 

</div>

<div class="page" style="margin-top:10px;">
      <label class="field a-field a-field_a3 page__field">
      <textarea class="field__input a-field__input" name="description" placeholder=" "  required></textarea>
      <span class="a-field__label-wrap" >
        <span class="a-field__label" id="textarea">Description</span>
      </span>
    </label>
</div>
<div class="page" style="margin-top:10px;">
<h5 style="font-size: 14px;margin-left: 20px;">Cover Image</h5>
</div>
<div class="page" style="margin-top:10px;">
      <label class="field a-field a-field_a3 page__field">
     <input type="file" name="cover_image" class="field__input a-field__input">
      <span class="a-field__label-wrap" >
        <span class="a-field__label" id="textarea">Cover Image</span>
      </span>
    </label>
 </div>
 <div class="page" style="margin-top:10px;">
      <label class="field a-field a-field_a3 page__field">
      <textarea class="field__input a-field__input" name="nutri_fact" placeholder=" "  required></textarea>
      <span class="a-field__label-wrap" >
        <span class="a-field__label" id="textarea">Nutritional Fact</span>
      </span>
    </label>
</div>
<div class="page" style="margin-top:10px;">
<h5 style="font-size: 14px;margin-left: 20px;">Nutrition Details Image</h5>
</div>
<div class="page" style="margin-top:10px;">
      <label class="field a-field a-field_a3 page__field">
     <input type="file" name="nutri_image" class="field__input a-field__input">
      <span class="a-field__label-wrap" >
        <span class="a-field__label" id="textarea">Nutrition Details Image</span>
      </span>
    </label>
</div>


                    </div>
                                <div id="image" style="display:none;">
                                     <div class="page">

          		<div class="container">

			<div class="content">



        <div class="box" style="margin-top:20px;">

					<input type="file" name="showcase_images[]" id="file-5" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple  style="width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;"/>
					<label for="file-5"><figure><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg></figure> <span>Choose a file&hellip;</span></label>

          </div>

</div></div></div>


</div>
                     <div id="inventory" style="display: none;">
<div class="page" style="margin-top:10px;">
     <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" name="micro_tab_qty" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Micro Pack: Tablet Qty</span>
      </span>
    </label>
    <label class="field a-field a-field_a3 page__field">
     <input class="field__input a-field__input" name="micro_price" placeholder=" " required>
     <span class="a-field__label-wrap">
       <span class="a-field__label">Micro Pack: Price</span>
     </span>
   </label>
    <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" name="micro_in_qty" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Micro Pack: Inhouse Stocks</span>
      </span>
    </label>
</div>
<div class="page" style="margin-top:10px;">
     <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" name="mini_tab_qty" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Mini Pack: Tablet Qty</span>
      </span>
    </label>
    <label class="field a-field a-field_a3 page__field">
     <input class="field__input a-field__input" name="mini_price" placeholder=" " required>
     <span class="a-field__label-wrap">
       <span class="a-field__label">Mini Pack: price</span>
     </span>
   </label>
    <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" name="mini_in_qty" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Mini Pack: Inhouse Stocks</span>
      </span>
    </label>
</div>
<div class="page" style="margin-top:10px;">
     <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" name="large_tab_qty" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Large Pack: Tablet Qty</span>
      </span>
    </label>
    <label class="field a-field a-field_a3 page__field">
     <input class="field__input a-field__input" name="large_price" placeholder=" " required>
     <span class="a-field__label-wrap">
       <span class="a-field__label">Large Pack: price</span>
     </span>
   </label>
    <label class="field a-field a-field_a3 page__field">
      <input class="field__input a-field__input" name="large_in_qty" placeholder=" " required>
      <span class="a-field__label-wrap">
        <span class="a-field__label">Large Pack: Inhouse Stocks</span>
      </span>
    </label>
</div>

                    </div>

                    </div>

                    </form>

<script type="text/javascript">
function info(div1,div2,first,second,third,div3)
{
    d3=document.getElementById(first);
    d4=document.getElementById(second);
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
     d5 = document.getElementById(div3);
     d6 = document.getElementById(third);
   if( d1.style.display == "none" )
   {
      d2.style.display = "none";
       d5.style.display = "none";
      d1.style.display = "block";
       d3.style.display = "block";
       d4.style.display = "none";
        d6.style.display = "none";
   }

}
</script>
<script type="text/javascript">
function image(div1,div2,first,second,third,div3)
{d3=document.getElementById(first);
   d5 = document.getElementById(div3);
    d4=document.getElementById(second);
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
  d6=document.getElementById(third);
   if( d2.style.display == "none" )
   {
      d1.style.display = "none";
         d5.style.display = "none";
      d2.style.display = "block";
        d3.style.display = "none";
         d6.style.display = "none";
       d4.style.display = "block";
   }

}
</script>
<script type="text/javascript">
function inventory(div1,div2,first,second,third,div3)
{
    d3=document.getElementById(first);
   d5 = document.getElementById(div3);
 d6=document.getElementById(third);
    d4=document.getElementById(second);
   d1 = document.getElementById(div1);
   d2 = document.getElementById(div2);
   if( d5.style.display == "none" )
   {
      d1.style.display = "none";
         d5.style.display = "block";
      d2.style.display = "none";
        d3.style.display = "none";
       d4.style.display = "none";
        d6.style.display = "block";
   }

}
</script>



                    </div>
                </div>
    </div>
   </section>



      <script src="../assets/js/lib/jquery.min.js"></script>
      <script src="../assets/js/lib/popper.min.js"></script>
      <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
      <script src="../assets/js/chosen-js/chosen.jquery.js"></script>
      <script src="../assets/js/custom.js"></script>


      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56821827-7"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-56821827-7');
      </script>
<script src="../js/custom-file-input.js"></script>


  </body>

<!-- Mirrored from cosmoadmin.com/preview/ecommerce-products.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 12:24:45 GMT -->
</html>
