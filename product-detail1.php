<!doctype html>
<html>
=
<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Uni Health Care</title>

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">


</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
	<?php include('includes/header.php')
    ?>

		<section class="mobile-menu-inner mobile-menu-inner-two">
			<div class="mobile_accor_togo">
				<div class="mobile_accor_logo">
					<a href="index-2.html">
				<img src="assets/img/hm-two-logo.png" class="svg" alt="">
			</a>
				</div>
				<div class="close-menu">
					<span></span>
				</div>
			</div>
			<nav id="accordian">
				<ul class="accordion-menu">
					<li class="current_page_item">
						<a href="index-2.html" class="dropdownlink">Home</a>
						<ul class="submenuItems">
							<li><i class="flaticon-right-arrow-angle"></i><a href="index-2.html">Home One</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="index2.html">Home Two</a></li>
						</ul>
					</li>
					<li>
						<a href="supplement.html">Supplement</a>
					</li>
					<li>
						<a href="feature.html">Feature</a>
					</li>
					<li>
						<a href="collection.html" class="dropdownlink">Productlist</a>
						<ul class="submenuItems">
							<li><i class="flaticon-right-arrow-angle"></i><a href="collection.html">Productlist</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="collection-all.html">Product Sidebar</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="product-detail.html">Product Detail</a></li>
						</ul>
					</li>
					<li>
						<a href="ingredient.html">Ingredient</a>
					</li>
					<li>
						<a href="blog.html" class="dropdownlink">blog</a>
						<ul class="submenuItems">
							<li><i class="flaticon-right-arrow-angle"></i><a href="blog.html"> Blog</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="blog-details.html"> Blog details</a></li>
						</ul>
					</li>
					<li>
						<a href="contact.html" class="dropdownlink">Contact</a>
						<ul class="submenuItems">
							<li><i class="flaticon-right-arrow-angle"></i><a href="contact.html">Contact page</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="about.html">About us</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="privacy.html">Privacy</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="reset-pass.html">Reset Pass</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="faq.html">FAQ</a></li>
						</ul>
					</li>
				</ul>
			</nav>
			<form action="#" id="moble-search">
				<input type="text" placeholder="Search....">
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
			<ul class="footer-social-link">
				<li class="fb-bg"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
				<li class="in-bg"><a href="#"><i class="fab fa-instagram"></i></a></li>
				<li class="tw-bg"><a href="#"><i class="fab fa-twitter"></i></a></li>
				<li class="gp-bg"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
			</ul>
		</section>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="breadcrumb_area">
			<div class="vigo_container_two">
				<div class="page_header">
					<h1>Collection</h1>
				</div>
				<!-- /.page-header -->
			</div>
			<!-- /.vigo_container_two -->
		</section>
		<!-- /.breadcrumb_area -->

		<!--=========================-->
		<!--=        Product         =-->
		<!--=========================-->
		<section class="product_all_collection woocommerce">
			<div class="vigo_container_two">
				<div class="column-3">
					<div class="products">
						<div class="product">
							<div class="product_single_collection">
								<a href="product-detail.html" class="woocommerce-LoopProduct-link">
									<div class="product-thumb">
										<img src="media/images/home6/collection-one.png" alt="!!">
									</div>
									<h3 class="woocommerce-loop-product__title">
										<span>WOMEN<br>
								DOSES
								<span class="after">VIEW ITEMS <br>OF THIS COLLECTION <i class="fas fa-arrow-circle-right"></i></span>
										</span>
									</h3>
								</a>
							</div>
						</div>
						<div class="product">
							<div class="product_single_collection">
								<a href="product-detail.html" class="woocommerce-LoopProduct-link">
									<div class="product-thumb">
										<img src="media/images/home6/collection-two.png" alt="!!">
									</div>
									<h3 class="woocommerce-loop-product__title">
										<span>STRONG<br>
								DOSES
								<span class="after">VIEW ITEMS <br>OF THIS COLLECTION<i class="fas fa-arrow-circle-right"></i></span>
										</span>
									</h3>
								</a>
							</div>
						</div>
						<div class="product">
							<div class="product_single_collection">
								<a href="product-detail.html" class="woocommerce-LoopProduct-link">
									<div class="product-thumb">
										<img src="media/images/home6/collection-one.png" alt="!!">
									</div>
									<h3 class="woocommerce-loop-product__title">
										<span>WOMEN<br>
								DOSES
									<span class="after">VIEW ITEMS <br>OF THIS COLLECTION<i class="fas fa-arrow-circle-right"></i></span>
										</span>
									</h3>
								</a>
							</div>
						</div>
						<div class="product">
							<div class="product_single_collection">
								<a href="product-detail.html" class="woocommerce-LoopProduct-link">
									<div class="product-thumb">
										<img src="media/images/home6/collection-three.png" alt="!!">
									</div>
									<h3 class="woocommerce-loop-product__title">
										<span>REGULAR<br>
								DOSES
								<span class="after">VIEW ITEMS <br>OF THIS COLLECTION<i class="fas fa-arrow-circle-right"></i></span>
										</span>
									</h3>
								</a>
							</div>
						</div>
						<div class="product">
							<div class="product_single_collection">
								<a href="product-detail.html" class="woocommerce-LoopProduct-link">
									<div class="product-thumb">
										<img src="media/images/home6/collection-four.png" alt="!!">
									</div>
									<h3 class="woocommerce-loop-product__title">
										<span>STRONG<br>
								DOSES
									<span class="after">VIEW ITEMS <br>OF THIS COLLECTION<i class="fas fa-arrow-circle-right"></i></span>
										</span>
									</h3>
								</a>
							</div>
						</div>
						<div class="product">
							<div class="product_single_collection">
								<a href="product-detail.html" class="woocommerce-LoopProduct-link">
									<div class="product-thumb">
										<img src="media/images/home6/collection-three.png" alt="!!">
									</div>
									<h3 class="woocommerce-loop-product__title">
										<span>REGULAR<br>
								DOSES
								<span class="after">VIEW ITEMS <br>OF THIS COLLECTION<i class="fas fa-arrow-circle-right"></i></span>
										</span>
									</h3>
								</a>
							</div>
						</div>
					</div>

					<nav class="blog_list_pagination">
						<ul class="blog_list_nav_links">
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">6</a></li>
							<li><a href="#">
						<span>.</span>
						<span>.</span>
						<span>.</span>
						<span>.</span>
					</a></li>
							<li><a href="#">9</a></li>
						</ul>
						<ul class="blog_list_nav_links two">
							<li><a href="#" class="prev"><i class="fas fa-angle-double-left"></i> prev</a></li>
							<li><a href="#" class="next">next <i class="fas fa-angle-double-right"></i></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Footer         =-->
		<!--==========================-->
<?php include('includes/footer.php')
    ?>

	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>


</body>
</html>