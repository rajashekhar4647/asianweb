<?php
include './includes/config.php';
session_start();
error_reporting(E_ALL & ~E_NOTICE);
$userid=$_SESSION['UNISAP_USER_ID'];
?>
<!doctype html>
<html>
<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Uni Health Care</title>
    
    
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="./style.css">

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">
   <style>
   </style>

</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	
		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/header.php')?>
		<!--=========================-->
		<!--=        Navbar         =-->
        <!--=========================-->
       
		<h1 style="padding-top:200px; padding-bottom:50px; text-align:center;">Shopping Cart</h1>

<div class="shopping-cart ">

  <div class="column-labels" style="" >
    <label class="product-removal" style="color:#3ad82e;text-indent: 0;">Remove</label>
    <label class="product-image" style="color:#3ad82e;text-indent: 0;">Image</label>
    <label class="product-details" style="color:#3ad82e;text-indent: 0;">Product</label>
    <label class="product-price" style="color:#3ad82e;text-align:right;">Price</label>
    <label class="product-quantity" style="color:#3ad82e;text-align:right;">Quantity</label>
      <label class="product-line-price" style="color:#3ad82e; text-align:center;">Total</label>
  </div>
 <?php
 	$totalprice=0;
	$query=mysqli_query($link,"SELECT * FROM `cart` WHERE `user_id`='$userid'");
	while($cartdata=mysqli_fetch_array($query))
	{
	$getproductid=$cartdata['itemid'];
	$getpackid=$cartdata['packid'];
	$queryproduct=mysqli_query($link,"SELECT * FROM `products` WHERE `id`='$getproductid'");
	$productdata=mysqli_fetch_array($queryproduct);	
	$productpack=mysqli_query($link,"SELECT * FROM `product_qty` WHERE `product_id`='$getproductid' AND `product_pack_id`='$getpackid'");
	$packdata=mysqli_fetch_array($productpack);
 ?>
  <div class="product">
  <div class="product-removal">
      <button class="remove-product"style="background:#3ad82e; margin-left:20px;">
        Remove
      </button>
    </div>
    <div class="product-image">
      <img src="http://asianherbs.in/products/uploads/<?php echo $getproductid;?>/cover/<?php echo $productdata['cover_img'];?>">
    </div>
    <div class="product-details">
      <div class="product-title"><?php echo $productdata['product_name'];?></div>
      <p class="product-description"><?php echo substr($productdata['product_description'],0,100);?></p>
    </div>
    <div class="product-price"><?php echo $packdata['price'];?></div>
    <div class="product-quantity">
      <input type="number" value="<?php echo $cartdata['qty'];?>" min="1" style="border-radius:30px; width:50px;">
    </div>
    <div class="product-line-price "style="padding-right:50px;"><?php echo $packdata['price']*$cartdata['qty'];?></div>
  </div>
<?php
$totalprice=$totalprice+($packdata['price']*$cartdata['qty']);
}
?>
  <div class="totals">
    <div class="totals-item">
      <label>Subtotal</label>
      <div class="totals-value" id="cart-subtotal"><?php echo $totalprice;?></div>
    </div>
     <div class="totals-item">
      <label>Shipping</label>
      <div class="totals-value" id="cart-shipping">100</div>
    </div>
    <div class="totals-item totals-item-total">
      <label>Grand Total</label>
      <div class="totals-value" id="cart-total"><?php echo $totalprice+100;?></div>
    </div>
  </div>
    <p class="sign-up-single-button" style="float: right;padding-bottom: 100px;padding-top:30px;">
		<input type="submit" name="Proceed" id="proceed" value="Proceed">
	</p>
</div>
<!-- partial -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script  src="./script.js"></script>
<script>
$("#proceed").click(function(){
	 $.ajax({
       url: "./functions/booking.php",
        type: 'POST',
        success: function (data) {
        var url = "address.php";
		$(location).attr('href',url);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});	
</script>

		<!--==========================-->
		<!--=        footer2         =-->
		<!--==========================-->
<?php include('includes/footer.php')?>
	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>
</body>
</html>