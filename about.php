<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Uni Health Care</title>

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">

<style media="screen">
@media screen and (max-width:500px){
	.video iframe{
		height:300px;
		width:100%;
		margin-bottom:-5px;
	}
}
@media screen and (max-width:800px){
	.video iframe{
		height:500px;
		width:100%;
		margin-bottom:-8px;
	}
}
@media screen and (min-width:801px){
	.video iframe{
		height:700px;
		width:100%;
		margin-bottom:-10px;
	}
}
		@media screen and (min-width:999px){
			.about_right_side{
				padding-top:0;
			}
		}

</style>
</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/header.php')?>
		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="breadcrumb_area">
			<div class="vigo_container_two">
				<div class="page_header">
					<h1>About <span>Us</span></h1>
				</div>
				<!-- /.page-header -->
			</div>
			<!-- /.vigo_container_two -->
		</section>
		<!-- /.breadcrumb_area -->

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="about_area">
			<div class="vigo_container_two">
				<div class="row">
					<div class="col-md-6">
						<div class="about_left_side">
							<img src="media/images/home6/about-left.jpg" alt="ff">
						</div>
					</div>
					<div class="col-md-6">
						<div class="about_right_side">
							<h2>Asian herbs</h2>
							<p>We are for you, it’s that simple. We had a dream to change lives and hence, created <b><i>AsianHerbs</i></b> – a company that is dedicated to your dreams.
<b><i>Asian Herb</i></b> is a product of <b>UniSap Nutricare PVT.LTD</b> located in Bangalore. Asian Herbs prepares <b>weight loss products</b> by using G4 formula. The intention was simple, make a product that can be used by anyone and can be used effectively. That is what we set out to do and hopefully, have done.
</p>
<p>We believe that the world needs to live healthily. Practicing what we preach, we have ensured that our products give you that healthy lifestyle in a healthy way because at the end of the day, we are for you; be it your dreams, your aspirations or your goals – we are here.</p>
<p>Loved us? Call us!</p>
<p>Want us to become better in serving you? PLEASE CALL US!</p>

					</div>
				</div>
			</div>
		</section>
		<!-- <section class="about_area_bottom">
		<div class="vigo_container_two">
				<div class="row">
					<div class="col-md-6">
						<div class="about_area_bottom_left">
							<p>Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
							</p>
							<div class="about_area_bottom_video">

							<i class="material-icons play">
								play_arrow
							</i>
							<h4>WATCH VIDEO</h4>
						</a>
							</div>
							<div class="section_title_four">
								<h2>THE PROCESS</h2>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="about_bottom_right_side">
							<img src="media/images/home6/about-bottom.jpg" alt="ff">
						</div>
					</div>
				</div>
			</div>
		</section> -->
		<section class="video">
			<iframe src="https://www.youtube.com/embed/b-TGpa0SDXw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

		</section>



		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<section class="call_to_action_green" style="padding:0;">
			<div class="vigo_container_two" style="margin:0;max-width:100%;background:#3ad82e;" >
				<div class="call_to_action_area_two">
					<div class="row">
						<div class="col-xl-10 offset-xl-2">
							<div class="call_to_action_hello">
								<div class="call_to_action_left_two">
									<h2>LIVE HEALTHY?</h2>
									<p>Try out our suppliment & enjoy the healthiest life. Discounts end soon!</p>
								</div>
								<div class="call_to_action_right_two">
									<a href="index.php" class="btn_four">Purchase</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<!--==========================-->
		<!--=        Footer         =-->
		<!--==========================-->
<?php include('includes/footer.php')?>

	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>


</body>
</html>
