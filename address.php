<?php
include './includes/config.php';
session_start();
?>
<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Asian Herbs - Checkout Address</title>


    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico"/>
    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico"/>
	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">
<style>
    .sign-up-single-button input 
    {
        border: 2px solid #0e598c;
    color: #0e598c;
    }
    .sign-up-single-button input:hover {
    color: #fff;
    background: #0e598c;
}
</style>

</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/headerhome.php')?>
		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<section class="sign-up-area" style="background: #eef3f5;">
			<div class="sign-up-section-title">
				<h4>Whom and Where to Deliver?</h4>
				<span>Please fill below delivery details!</span>
			</div>
			<div class="sign-up-inner">
			 	<div class="sign-up-form">
					<form method="post" id="add_address">
						<p class="sign-up-single-input"  style="width: 100%;">
							<label>FULL NAME*</label>
							<input type="text" name="name" value="<?php echo $_SESSION['username'];?>" REQUIRED>
						</p>
						<p class="sign-up-single-input">
							<label>EMAIL ADDRESS*</label>
							<input type="text" name="email" value="<?php echo $_SESSION['useremail'];?>" REQUIRED>
						</p>
						<p class="sign-up-single-input">
							<label>CONTACT NUMBER</label>
							<input type="text" name="phone" value="<?php echo $_SESSION['userphone'];?>" REQUIRED>
						</p>
						<p class="sign-up-single-input">
							<label>COUNTRY*</label>
							<input type="text" name="country" value="India" READONLY REQUIRED>
						</p>
						<p class="sign-up-single-input">
							<label>STATE*</label>
							<select name="state" style="width: 100%;height: 50px;border: 1px solid #ddd;border-radius: 0px;padding: 0 20px;" REQUIRED>
							  <option value="" hidden="">Select State</option>
							  <option value="Andhra Pradesh">Andhra Pradesh</option>
							  <option value="Gujarat">Gujarat</option>
							  <option value="Goa">Goa</option>
							  <option value="Karnataka">Karnataka</option>
							  <option value="Kerala">Kerala</option>
							  <option value="Maharashtra">Maharashtra</option>
							  <option value="Odisha">Odisha</option>
							  <option value="Tamil Nadu">Tamil Nadu</option>
							  <option value="Telangana">Telangana</option>
							  <option value="Arunachal Pradesh">Arunachal Pradesh</option>
							  <option value="Assam">Assam</option>
							  <option value="Bihar">Bihar</option>
							  <option value="Chandigarh">Chandigarh</option>
							  <option value="Chhattisgarh">Chhattisgarh</option>
							  <option value="Delhi">Delhi</option>
							  <option value="Haryana">Haryana</option>
							  <option value="Himachal Pradesh">Himachal Pradesh</option>
							  <option value="Jammu and Kashmir">Jammu and Kashmir</option>
							  <option value="Jharkhand">Jharkhand</option>
							  <option value="Madhya Pradesh">Madhya Pradesh</option>
							  <option value="Manipur">Manipur</option>
							  <option value="Meghalaya">Meghalaya</option>
							  <option value="Mizoram">Mizoram</option>
							  <option value="Nagaland">Nagaland</option>
							  <option value="Punjab">Punjab</option>
							  <option value="Rajasthan">Rajasthan</option>
							  <option value="Sikkim">Sikkim</option>
							  <option value="Uttar Pradesh">Uttar Pradesh</option>
							  <option value="Uttrakhand">Uttrakhand</option>
							  <option value="West Bengal">West Bengal</option>
							</select>
						</p>
						<p class="sign-up-single-input">
							<label>CITY*</label>
							<input type="text" name="city" REQUIRED>
						</p>
						<p class="sign-up-single-input">
							<label>PINCODE*</label>
							<input type="text" name="pincode" REQUIRED>
						</p>
						<p class="sign-up-single-input" style="width: 100%;">
							<label>ADDRESS*</label>
							<textarea name="address" REQUIRED></textarea>
						</p>
						<p class="sign-up-single-button" style="width: 100%;">
							<input type="submit" name="add_address" value="CONTINUE TO CHECKOUT">
						</p>
					</form>
				</div>
				</div>
		</section>

		<!--==========================-->
		<!--=        footer2         =-->
		<!--==========================-->
<?php include('includes/headerhome.php')?>
	</div>
	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>
</body>
<script>    
$("form#add_address").submit(function(e) 
{
    e.preventDefault();
    var session='<?php if(isset($_SESSION['UNISAP_USER_ID'])){ echo '1'; } else { echo '';} ?>';
    var formData = new FormData(this);
    console.log(formData);
    $.ajax({
       url: "./functions/add_address.php",
        type: 'POST',
        data: formData,
        success: function (data) {
        var url = "checkout.php";
		$(location).attr('href',url);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>
</html>