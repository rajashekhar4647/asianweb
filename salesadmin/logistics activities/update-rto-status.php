<!DOCTYPE html>
<!-- saved from url=(0101)http://asianherbs.in/salesadminadmin/index.php?route=logistics/rto&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Orders</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../includes/head.php'); ?>
<?php include ('../includes/side-nav.php'); ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1 style="color:black;">Return to Origin </h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/rto&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Orders</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i>Recieve RTO products</h3>
      </div>
      <div class="panel-body">

      <form method="post" enctype="multipart/form-data" id="form-order">

        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <label class="control-label" for="input-order-id">Scan Order ID one at a time.</label>
                  <input type="text" name="order_id" value="" placeholder="Order ID" id="input-order-id" class="form-control">
                </div>
				<div id="msg"></div>
              </div>
          </div>
        </div>
      </form>


      </div>
    </div>
  </div>

<script type="text/javascript">




$('#form-order').on('submit', function(e) {
	e.preventDefault();

	   var api_id='';
       var order_id = $('input[name=\'order_id\']').val();



         if(order_id){

         	/*$.ajax({
                url: 'index.php?route=logistics1/rto/update_status&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id,
                dataType: 'json',
                success: function(json) { */

              $.ajax({
				url: 'index.php?route=logistics/rto/update_status&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id,
                dataType: 'json',
                success: function(json) {
               	//alert(json);

						//alert(success_portal);
						$('input[name=\'order_id\']').val('');
						Lobibox.notify('success', {
							size: 'mini',
							delayIndicator: false,
							position: 'top right',
							msg: json['success_portal']
						});



							  $('.alert').remove();
			                  if (json['response'] == 1) {
										 // alert(json['response']);
										 	Lobibox.notify('success', {
												size: 'mini',
												delayIndicator: false,
												position: 'button right',
												msg: 'RTO for order id '+order_id+' added successfully to the System...!'
											});

										$('#msg').prepend('<span class="alert alert-success">RTO entry added successfully to the System.</span>');
										$('input[name=\'order_id\']').val('');
			                    }

								if (json['response'] == 0) {
									Lobibox.notify('error', {
										size: 'large',
										position: 'button left',
										msg: 'May be this order ('+order_id+')  has been already received as RTO. Go to system and search it in Sales/Delivery panel. If it exist here then it should be accepted as RTO. If it is not here then search in RTO list, if order is there then it means it has already done earlier.'
									});
									$('#msg').prepend('<span class="alert alert-danger">Record for this order ID does not exist in The system, Please check manually.</span>');
									$('input[name=\'order_id\']').val('');
								}

								if (json['response'] == 2) {
									Lobibox.notify('warning', {
										size: 'mini',
										delayIndicator: false,
										position: 'top right',
										msg: 'Oops Some thing went wrong. Please retry for order '+order_id
									});

									$('#msg').prepend('<span class="alert alert-danger">Oops.. Some error occurred. </span>');
								}





                },
                error: function(xhr, ajaxOptions, thrownError) {
              //  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });


          } else {
             alert("Please enter valid Order Id ...!");
          }

});

$(document).ready(function(){


	$('li.lobibox-notify-error a').click(function(e) {
		e.preventDefault();
	});
});

</script>

<style>
li.lobibox-notify-error .fa-times-circle:before {
    content: '\f05a';
}</style>



</div>



<?php include ('../includes/footer.php'); ?></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Update RTO status_files/classie.js.download" type="text/javascript"></script>
<script src="./Update RTO status_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
