<!DOCTYPE html>
<!-- saved from url=(0103)http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Orders</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../includes/head.php'); ?>
<?php include ('../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
         <button type="submit" id="button-download-awb" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/orderdownload&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="Download Order Reports"><i class="fa fa-download"></i></button>
        <button type="submit" id="button-manifast" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/manifest&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-success" disabled="" data-original-title="Print Manifest"><i class="fa fa-folder"></i></button>
        <button type="submit" id="button-generate-pickup" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/pickupgenerate&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-info" disabled="" data-original-title="Generate Pickup"><i class="fa fa-taxi"></i></button>
        <button type="submit" id="button-shipping" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/shipping&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562591" data-toggle="tooltip" title="" class="btn btn-info" disabled="" data-original-title="Print Shipping List"><i class="fa fa-truck"></i></button>
         <button type="submit" id="button-label" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/label&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-warning" disabled="" data-original-title="Print Label"><i class="fa fa-print"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/invoice&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562591" data-toggle="tooltip" title="" class="btn btn-warning" disabled="" data-original-title="Print Invoice"><i class="fa fa-print"></i></button>
        <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/add&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add New"><i class="fa fa-plus"></i></a></div>
      <h1 style="color:black;">Orders</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Orders</a></li>
              </ul>
    </div>
  </div>
  <style type="text/css">
.is_service{ padding: 0px; text-align: center; margin: 0px; position: absolute;  border-bottom-left-radius: 400px;  width: 15px;  font-size: 8px;  height: 15px;  right: 0px;    top: 0px;}
</style>
  <div class="container-fluid"><div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Warning: You do not have permission to access the API! <button type="button" class="close" data-dismiss="alert">×</button></div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Order List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">

            <div class="col-sm-2">
              <div class="form-group">
                <label class="control-label" for="input-order-id">Order ID</label>
                <input type="text" name="filter_order_id" value="" placeholder="Order ID" id="input-order-id" class="form-control">
              </div>
              <div class="form-group">
                <label class="control-label" for="input-customer">Customer</label>
                <input type="text" name="filter_customer" value="" placeholder="Customer" id="input-customer" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-shipement-date">Shipment Date</label>
                <div class="input-group date">
                  <input type="text" name="filter_shipement_date" value="" placeholder="Shipped Date" data-date-format="YYYY-MM-DD" id="input-shipement-date" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>

            <div class="col-sm-2">
              <div class="form-group">
                <label class="control-label" for="input-order-status">Order Status</label>
                <select name="filter_order_status" id="input-order-status" class="form-control">
                  <option value="*"></option>
                                    <option value="0">Missing Orders</option>

                                                    <option value="41">Address Verified</option>

                     <!-- END CONDITIONS -->

                                                    <option value="35">Aproval Pending</option>

                     <!-- END CONDITIONS -->

                                                    <option value="7">Canceled</option>

                     <!-- END CONDITIONS -->

                                                    <option value="9">Canceled Reversal</option>

                     <!-- END CONDITIONS -->

                                                    <option value="17">COD &nbsp;Confirmed</option>

                     <!-- END CONDITIONS -->

                                                    <option value="37">Completed</option>

                     <!-- END CONDITIONS -->

                                                    <option value="29">Converted Lead</option>

                     <!-- END CONDITIONS -->

                                                    <option value="8">Customer Refuse To Accept</option>

                     <!-- END CONDITIONS -->

                                                    <option value="22">Delivered</option>

                     <!-- END CONDITIONS -->

                                                    <option value="36">Disapproved</option>

                     <!-- END CONDITIONS -->

                                                    <option value="30">Duplicate Order</option>

                     <!-- END CONDITIONS -->

                                                    <option value="40">Entry Blocked - Price Issue</option>

                     <!-- END CONDITIONS -->

                                                    <option value="10">Failed</option>

                     <!-- END CONDITIONS -->

                                                    <option value="39">Incomplete Address</option>

                     <!-- END CONDITIONS -->
                                         <!-- END CONDITIONS -->

                                                    <option value="23">Miss Route</option>

                     <!-- END CONDITIONS -->

                                                    <option value="27">Non - serviceable area</option>

                     <!-- END CONDITIONS -->

                                                    <option value="24">ODA- Out Of Delivery Area</option>

                     <!-- END CONDITIONS -->

                                                    <option value="34">On hold</option>

                     <!-- END CONDITIONS -->

                                                    <option value="38">Order Assigned</option>

                     <!-- END CONDITIONS -->

                                                    <option value="42">Payment Gateway Entry</option>

                     <!-- END CONDITIONS -->

                                                    <option value="1">Pending</option>

                     <!-- END CONDITIONS -->

                                                    <option value="18">Pick Up Generated</option>

                     <!-- END CONDITIONS -->

                                                    <option value="19">Prepaid</option>

                     <!-- END CONDITIONS -->

                                                    <option value="15">Processed</option>

                     <!-- END CONDITIONS -->

                                                    <option value="2">Processing</option>

                     <!-- END CONDITIONS -->

                                                    <option value="26">Re Order - dont use it</option>

                     <!-- END CONDITIONS -->

                                                    <option value="20">Ready for Dispatch</option>

                     <!-- END CONDITIONS -->

                                                    <option value="11">Refunded</option>

                     <!-- END CONDITIONS -->

                                                    <option value="21">Reschedule</option>

                     <!-- END CONDITIONS -->

                                                    <option value="12">Reversed</option>

                     <!-- END CONDITIONS -->

                                                    <option value="14">RTO-Returned To Origin</option>

                     <!-- END CONDITIONS -->

                                                    <option value="43">Shipment Damaged</option>

                     <!-- END CONDITIONS -->

                                                    <option value="13">Shipment Lost</option>

                     <!-- END CONDITIONS -->

                                                    <option value="3">Shipped</option>

                     <!-- END CONDITIONS -->
                                         <!-- END CONDITIONS -->

                                                    <option value="5">Undelivered</option>

                     <!-- END CONDITIONS -->

                                                    <option value="16">Voided</option>

                     <!-- END CONDITIONS -->

                                                    <option value="25">Wrong Delivery</option>

                     <!-- END CONDITIONS -->

                </select>
                </div>
                 <div class="form-group">
                  <label class="control-label" for="input-mobile">Mobile No</label>
                  <input type="text" name="filter_mobile" value="" placeholder="Mobile No" id="input-mobile" class="form-control">
                </div>

                 <div class="form-group">
                <label class="control-label" for="input-payment-mode">Payment Mode</label>
                <select name="filter_payment_mode" id="input-payment-mode" class="form-control">
                  <option value="*" selected="selected" diasabled="diasabled"></option>
                  <option value="cod">COD</option>
                  <option value="payu">Prepaid</option>
                  <option value="lead">Lead</option>
                  <option value="bank_transfer">Bank Transfer</option>
                </select>
              </div>

              </div>
              <div class="col-sm-2">
                    <div class="form-group">
                      <label class="control-label" for="input-date-added">Date Added</label>
                      <div class="input-group date">
                        <input type="text" name="filter_date_added" value="" placeholder="Date Added" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control">
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span></div>
                    </div>
                    <div class="form-group">
                    <label class="control-label" for="input-date-fron_date">From Date</label>
                    <div class="input-group date">
                      <input type="text" name="filter_fron_date" value="" placeholder="From Date" data-date-format="YYYY-MM-DD" id="input-date-fron-date" class="form-control">
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                      </span></div>
                    </div>
              </div>

              <div class="col-sm-2">
                 <div class="form-group">
                    <label class="control-label" for="input-date-modified">Date Modified</label>
                    <div class="input-group date">
                      <input type="text" name="filter_date_modified" value="" placeholder="Date Modified" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control">
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                      </span></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="input-date-to-date">To Date</label>
                    <div class="input-group date">
                      <input type="text" name="filter_to_date" value="" placeholder="To Date" data-date-format="YYYY-MM-DD" id="input-date-to-date" class="form-control">
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                      </span></div>
                  </div>

              </div>
              <div class="col-sm-2">
                  <div class="form-group">
                      <label class="control-label" for="input-api-name"> API Name</label>
                      <input type="text" name="filter_api_name" value="" placeholder="API NAME" id="input-city" class="form-control">
                   </div>
                   <div class="form-group">
                      <label class="control-label" for="input-awb-number">AWB Number</label>
                      <input type="text" name="filter_awb_number" value="" placeholder="AWB Number" id="input-awb-number" class="form-control">
                   </div>
                    <div class="pull-right"><a style="margin-right: 10px;" href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" class="btn btn-alt btn-warning"><i class="fa fa-refresh"></i> Refresh</a></div>
              </div>
              <div class="col-sm-2">
                   <div class="form-group">
                      <label class="control-label" for="input-state">State</label>
                      <input type="text" name="filter_state" value="" placeholder="State" id="input-state" class="form-control">
                   </div>
                   <div class="form-group">
                      <label class="control-label" for="input-city">City</label>
                      <input type="text" name="filter_city" value="" placeholder="City" id="input-city" class="form-control">
                   </div>

                <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>

            </div>
          </div>
        </div>


      <section class="col-sm-6 order_process_details">   <!--  ORDER LIST START HERE -->
        <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right">Showing 1 to 5 of 5 (1 Pages)</div>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table id="orders-list" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th style="width: 1px;" class="text-center"><input type="checkbox" name="selected[]" value="0" onclick="$(&#39;input[name*=\&#39;selected\&#39;]&#39;).prop(&#39;checked&#39;, this.checked);"></th>
                  <th class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.order_id&amp;order=ASC" class="desc">Order ID</a>
                    </th>
                  <!-- <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=customer&amp;order=ASC">Customer</a>
                    </td> -->
                  <th class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=status&amp;order=ASC">Status</a>
                    </th>
                  <th class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.total&amp;order=ASC">Total</a>
                    </th>
                 <!--  <th class="text-center">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.date_added&amp;order=ASC">Date Added</a>
                    </th> -->
                  <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.date_modified&amp;order=ASC">Date Modified</a>
                    </td>
                 <!-- <td class="text-right">Action</td> -->
                </tr>
              </thead>
              <tbody>
                                                    <tr data-id="99998000562589" class="generated active">
                      <td class="text-center">                        <input type="checkbox" name="selected[]" value="99998000562589">
                                                <input type="hidden" name="shipping_code[]" value=""></td>
                      <td class="text-left" style="position:relative">


                        <span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562589<br>Pick Up Generated

                                             <span href="javascript:void(0)" data-toggle="tooltip" class="is_service btn-success" data-original-title="Service Available">S</span>


                      </td>
                      <!-- <td class="text-left">Sakshi Rathore</td> -->
                      <td class="text-left">
                        <i style="color:red;" class="fa fa-files-o"></i>                        Sakshi Rathore                        <br>ECOM EXPRESS-8563545236                      </td>
                      <td class="text-left">Rs. 2,288</td>
                      <!-- <td class="text-center">04/11/2019 13:26:18</td> -->
                      <td class="text-left">04/11/2019 13:52:01</td>
                       <!--  <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/info&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562589" data-toggle="tooltip" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a> <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562589" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <button type="button" value="99998000562589" id="button-delete99998000562589" data-loading-text="Loading..." data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></td> -->
                    </tr>

                                      <tr data-id="99998000562585" class="generated">
                      <td class="text-center">                        <input type="checkbox" name="selected[]" value="99998000562585">
                                                <input type="hidden" name="shipping_code[]" value=""></td>
                      <td class="text-left" style="position:relative">


                        <span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562585<br>Pick Up Generated

                                             <span href="javascript:void(0)" data-toggle="tooltip" class="is_service btn-success" data-original-title="Service Available">S</span>


                      </td>
                      <!-- <td class="text-left">Nandha  kumar</td> -->
                      <td class="text-left">
                        <i style="color:red;" class="fa fa-files-o"></i>                        Nandha  Kumar                        <br>XPRESSBEES-1341117150808                      </td>
                      <td class="text-left">Rs. 2,288</td>
                      <!-- <td class="text-center">03/11/2019 16:43:01</td> -->
                      <td class="text-left">04/11/2019 10:45:05</td>
                       <!--  <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/info&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562585" data-toggle="tooltip" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a> <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562585" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <button type="button" value="99998000562585" id="button-delete99998000562585" data-loading-text="Loading..." data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></td> -->
                    </tr>

                                      <tr data-id="99998000562583" class="generated">
                      <td class="text-center">                        <input type="checkbox" name="selected[]" value="99998000562583">
                                                <input type="hidden" name="shipping_code[]" value=""></td>
                      <td class="text-left" style="position:relative">


                        <span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562583<br>Pick Up Generated

                                             <span href="javascript:void(0)" data-toggle="tooltip" class="is_service btn-success" data-original-title="Service Available">S</span>


                      </td>
                      <!-- <td class="text-left">Hemavathi.r Hema</td> -->
                      <td class="text-left">
                        <i style="color:red;" class="fa fa-files-o"></i>                        Hemavathi.r Hema                        <br>XPRESSBEES-1341117150809                      </td>
                      <td class="text-left">Rs. 2,288</td>
                      <!-- <td class="text-center">03/11/2019 09:14:32</td> -->
                      <td class="text-left">04/11/2019 10:45:06</td>
                       <!--  <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/info&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562583" data-toggle="tooltip" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a> <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562583" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <button type="button" value="99998000562583" id="button-delete99998000562583" data-loading-text="Loading..." data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></td> -->
                    </tr>

                                      <tr data-id="99998000562580" class="generated">
                      <td class="text-center">                        <input type="checkbox" name="selected[]" value="99998000562580">
                                                <input type="hidden" name="shipping_code[]" value=""></td>
                      <td class="text-left" style="position:relative">


                        <span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562580<br>Pick Up Generated

                                             <span href="javascript:void(0)" data-toggle="tooltip" class="is_service btn-success" data-original-title="Service Available">S</span>


                      </td>
                      <!-- <td class="text-left">kamatham bhargav</td> -->
                      <td class="text-left">
                        <i style="color:red;" class="fa fa-files-o"></i>                        Kamatham Bhargav                        <br>XPRESSBEES-1341117150810                      </td>
                      <td class="text-left">Rs. 2,288</td>
                      <!-- <td class="text-center">02/11/2019 18:39:23</td> -->
                      <td class="text-left">04/11/2019 10:45:06</td>
                       <!--  <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/info&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562580" data-toggle="tooltip" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a> <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562580" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <button type="button" value="99998000562580" id="button-delete99998000562580" data-loading-text="Loading..." data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></td> -->
                    </tr>

                                      <tr data-id="99998000562578" class="generated">
                      <td class="text-center">                        <input type="checkbox" name="selected[]" value="99998000562578">
                                                <input type="hidden" name="shipping_code[]" value=""></td>
                      <td class="text-left" style="position:relative">


                        <span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562578<br>Pick Up Generated

                                             <span href="javascript:void(0)" data-toggle="tooltip" class="is_service btn-success" data-original-title="Service Available">S</span>


                      </td>
                      <!-- <td class="text-left">Azra Khan</td> -->
                      <td class="text-left">
                        <i style="color:red;" class="fa fa-files-o"></i>                        Azra Khan                        <br>ECOM EXPRESS-8563499885                      </td>
                      <td class="text-left">Rs. 2,288</td>
                      <!-- <td class="text-center">01/11/2019 22:48:50</td> -->
                      <td class="text-left">04/11/2019 10:45:07</td>
                       <!--  <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/info&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562578" data-toggle="tooltip" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a> <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562578" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <button type="button" value="99998000562578" id="button-delete99998000562578" data-loading-text="Loading..." data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></td> -->
                    </tr>


                              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right">Showing 1 to 5 of 5 (1 Pages)</div>
        </div>
       </section>  <!--  ORDER LIST END HERE -->


      <!--  ORDER DETAILS START HERE -->
         <section class="col-sm-6 order_process_details" id="order_shortinfo">

    <div class="page-header">
     <div class="container-fluid">
       <div class="pull-right">
             <button type="submit" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/label&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562589" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="Print Shipping List"><i class="fa fa-truck"></i></button>
            <button type="submit" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/invoice&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562589" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Print Invoice"><i class="fa fa-print"></i></button>




                                                         <button class="btn btn-alt btn-info " onclick="paymentstatus(&#39;99998000562589&#39;)" id="ship_now">Shipped                                     </button>



            <button class="btn btn-alt btn-danger" onclick="paymentstatus(&#39;99998000562589&#39;)" href="javascript:void(0);" id="mark_as_canceled">Update Status</button>



       </div>
        <h1><i class="fa fa-info-circle"></i> Order (#99998000562589)</h1>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12  col-sale">
          <div class="panel panel-default panel-popup-sale">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Order Details</h3>
            </div>
            <table class="table">
              <tbody>
                <tr>
                  <td style="width: 1%;"><button data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Store"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
                  <td><a href="http://www.purelyherbs.in/" target="_blank">Purely Herbs</a></td>
                </tr>
                <tr>
                  <td><button data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Date Added"><i class="fa fa-calendar fa-fw"></i></button></td>
                  <td>04/11/2019</td>
                </tr>
                <tr>
                  <td><button data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Payment Method"><i class="fa fa-credit-card fa-fw"></i></button></td>
                  <td>Cash On Delivery</td>
                </tr>
                                 <tr>
                <td><!-- <button  class="btn btn-info btn-xs" onClick="findapistatus('99998000562589','MP','483501')"><i class="fa fa-truck fa-fw"></i><strong>R-AWB</strong></button> -->

                  <button class="btn btn-primary btn-xs"><i class="fa fa-truck fa-fw"></i><strong>R-AWB</strong></button>
                </td>
                <td>
                    <span style="font-size:12px;color:#f00">ECOM EXPRESS-8563545236 </span>
                 </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 col-sale">
          <div class="panel panel-default panel-popup-sale">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-user"></i> Customer Details</h3>
            </div>
            <table class="table">
              <tbody><tr>
                <td><button data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Customer"><i class="fa fa-user fa-fw"></i></button></td>
                <td>                  <a href="http://asianherbs.in/salesadminadmin/Sakshi%20Rathore" target="_blank">Sakshi Rathore</a>
                  </td>
              </tr>
              <tr>
                <td><a href="javascript:void(0)" onclick="enquirypopup()" data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Edit"><i class="fa fa-pencil"></i></a>


                 </td>
                <td><a href="javascript:void(0)" onclick="enquirypopup()" data-toggle="tooltip" title="" data-original-title="Edit">Edit Address</a>
                 <!-- <a class="md-trigger" onclick="findaddressupdation('99998000562589')" href="javascript:void(0)" data-modal="modal-1">Edit Address</a> --> </td>
              </tr>


               <tr>
                <td><button data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Customer Group"><i class="fa fa-group fa-fw"></i></button></td>
                <td>Landing Page</td>
              </tr>

              <tr>
                <td><button data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="E-Mail"><i class="fa fa-envelope-o fa-fw"></i></button></td>
                <td><a href="mailto:sakshirathore665@gmail.com">sakshirathore665@gmail.com</a></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="" class="btn btn-primary btn-xs" data-original-title="Telephone"><i class="fa fa-phone fa-fw"></i></button></td>
                <td>8839486499</td>
              </tr>
              <!--  <tr>
                <td><button  class="btn btn-info btn-xs"><i class="fa fa-truck fa-fw"></i><strong>AWB:</strong></button></td>
                <td>
                    <span style="font-size:12px;color:#f00">ECOM EXPRESS-8563545236 </span>
                 </td>
              </tr>  -->
            </tbody></table>
          </div>
        </div>
         <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 col-sale">
          <div class="panel panel-default panel-popup-sale">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-cog"></i> Options</h3>
            </div>
            <table class="table">
              <tbody>
                <tr>
                  <td>Invoice</td>
                  <td id="invoice" class="text-right">PRH-2017-14231409</td>
                  <td style="width: 1%;" class="text-center">                    <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-refresh"></i></button>
                    </td>
                </tr>
                <tr>
                  <td>Reward Points</td>
                  <td class="text-right">0</td>
                  <td class="text-center">                    <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                    </td>
                </tr>
                <tr>
                  <td>Affiliate                    </td>
                  <td class="text-right">Rs. 0</td>
                  <td class="text-center">                    <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                    </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
     </div>
     <div class="row">
        <div class="panel panel-default">
          <div class="panel-body" style="padding:0px; ">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td style="width: 50%;" class="text-left">Payment Address</td>

                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-left">Sakshi Rathore<br>Sakshi Rathore<br>C/O: Sakshi Rathore<br>Ward No.- 32, Jai Hind Chowk, Murwara, Post- Murwara,<br>Near- Murwara Bridge, Katni<br>Landmar<br>Katni 483501<br>Madhya Pradesh<br>India</td>
                                  </tr>
              </tbody>
            </table>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left">Product</td>
                  <td class="text-left">Model</td>
                  <td class="text-right">Quantity</td>
                  <td class="text-right">Unit Price</td>
                  <td class="text-right">Total</td>
                </tr>
              </thead>
              <tbody>
                                <tr>
                  <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=catalog/product/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;product_id=32">Garcinia Cambogia Herbs-1 Bottle</a>
                    </td>
                  <td class="text-left">Weight Loss</td>
                  <td class="text-right">1</td>
                  <td class="text-right">Rs. 2,288</td>
                  <td class="text-right">Rs. 2,288</td>
                </tr>
                                                                <tr>
                  <td colspan="4" class="text-right">Sub-Total</td>
                  <td class="text-right">Rs. 2,288</td>
                </tr>
                                <tr>
                  <td colspan="4" class="text-right">Total</td>
                  <td class="text-right">Rs. 2,288</td>
                </tr>
                              </tbody>
            </table>
                      </div>
        </div>
    </div>
    <div class="row">
       <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-comment-o"></i> Order History</h3>
        </div>
        <div class="panel-body">
          <ul class="nav nav-tabs">
            <li class="active"><a href="http://asianherbs.in/salesadminadmin/#tab-history" data-toggle="tab">History</a></li>
            <li><a href="http://asianherbs.in/salesadminadmin/#tab-additional" data-toggle="tab">Additional</a></li>
                      </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-history">
              <div id="history"><table class="table table-bordered">
  <thead>
    <tr>
      <td class="text-left">Date Added</td>
      <td class="text-left">Comment</td>
      <td class="text-left">Status</td>
      <td class="text-left">Customer Notified</td>
    </tr>
  </thead>
  <tbody>
            <tr>
      <td class="text-left">04/11/2019 13:52:01</td>
      <td class="text-left">ECOM EXPRESS:8563545236 (By Veda Vathi)</td>
      <td class="text-left">Pick Up Generated</td>
      <td class="text-left">Yes</td>
    </tr>
        <tr>
      <td class="text-left">04/11/2019 13:51:38</td>
      <td class="text-left">ECOM EXPRESS:8563545236 (By Veda Vathi)</td>
      <td class="text-left">Ready for Dispatch</td>
      <td class="text-left">Yes</td>
    </tr>
        <tr>
      <td class="text-left">04/11/2019 13:50:39</td>
      <td class="text-left">Address verified (By Veda Vathi)</td>
      <td class="text-left">Address Verified</td>
      <td class="text-left">No</td>
    </tr>
        <tr>
      <td class="text-left">04/11/2019 13:49:45</td>
      <td class="text-left">Approved (By Veda Vathi)</td>
      <td class="text-left">COD &nbsp;Confirmed</td>
      <td class="text-left">No</td>
    </tr>
        <tr>
      <td class="text-left">04/11/2019 13:49:35</td>
      <td class="text-left"> (By Veda Vathi)</td>
      <td class="text-left">Aproval Pending</td>
      <td class="text-left">No</td>
    </tr>
        <tr>
      <td class="text-left">04/11/2019 13:26:18</td>
      <td class="text-left"></td>
      <td class="text-left">COD &nbsp;Confirmed</td>
      <td class="text-left">No</td>
    </tr>
          </tbody>
</table>
<div class="row">
  <div class="col-sm-6 text-left"></div>
  <div class="col-sm-6 text-right">Showing 1 to 6 of 6 (1 Pages)</div>
</div>
</div>
              <br>
            </div>
            <div class="tab-pane" id="tab-additional">
                                                        <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2">Browser</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>IP Address</td>
                    <td></td>
                  </tr>
                                    <tr>
                    <td>User Agent</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Accept Language</td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
                      </div>
        </div>
       </div>
    </div>

 </div>



<div class="md-modal md-modal12 md-effect-1" id="modal-1" style="max-height: 450px; top: 50%;  overflow-y: scroll; ">
  <div class="md-content md-content12" id="editaddres">
      <div style="padding:10px;" id="html_results">
      </div>
     <button style="position: absolute; right: 0px; top: 0;" class="md-close btn btn-info">Close me!</button>
  </div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->



<script src="./Orders_files/jquery.bpopup.min.js.download" type="text/javascript"></script>
<script src="./Orders_files/jquery.easing.1.3.js.download" type="text/javascript"></script>
 <script src="./Orders_files/tokens.js.download" type="text/javascript"></script>
<!-- <script type="text/javascript">

/*$('element_to_pop_up').bPopup({
            onOpen: function() {
                content.html(self.data('bpopup') || '');
            },
            onClose: function() {
                content.empty();
            }
        });*/

function paymentstatus(n){
  $('#element_to_pop_up2').bPopup({
    easing: 'easeOutBack', //uses jQuery easing plugin
    speed: 1000,
    transition: 'slideDown'
  });
}



function findapistatus(l,n,m){
  if(l!='' && n!='' && m!=''){
          $.ajax({
            url: 'index.php?route=logistics/order/findshippingapi&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+l+'&city_code='+n+'&pin_code='+m,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
              /* if(json['state_id']=='1505' && json['total'] > '5000'){
                   alert('This order is belong to '+json['state_name']+' and price is greater than Rs. 5000 thousand. You will change price less than or equal to Rs.5000 thousand.');
               } else {*/

                    $('#element3').html(json['element_content']);
                    $('#element_to_pop_up3').bPopup({
                        easing: 'easeOutBack', //uses jQuery easing plugin
                        speed: 1000,
                        position: [150, 150], //x, y
                        transition: 'slideDown'
                    });
               /* } */
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });
  } else{
    alert("Check Following Input Value! \n 1. Order Id="+l+", \n 2. State Code="+n+", \n 3. Pin Code="+m);
  }
}
</script> -->
<!-- <script type="text/javascript">
function findaddressupdation(i){
  //alert(i);
         $.ajax({
            url: 'index.php?route=logistics/order/updations&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+i,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
          //  alert(json['html_success']);
              $('#html_results').html(json['html_success']);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });

}
</script> -->

  <!-- Element to pop up  End here-->



                <div id="element_to_pop_up2"> <a class="b-close" title="Close">x</a>
                  <div class="request-a-call">
                      <h2>Order History</h2>
                      <div class="request-a-call-form">
                        <div class="panel panel-default">
                           <div class="tab-content">
                              <div class="tab-pane active" id="tab-history">
                                <div id="history"></div>
                                <br>
                                <fieldset>
                                  <legend>Add Order History</legend>
                                  <form class="form-horizontal">

                                  <input type="hidden" name="user-name" value="Updated (By Veda Vathi)">

                                    <div class="form-group">
                                      <label class="col-sm-3 control-label" for="input-order-status">Order Status</label>
                                      <div class="col-sm-9">
                                        <select name="order_status_id" id="input-order-status" class="form-control">
                                                                                                                              <option value="41">Address Verified</option>
                                                                                                                                                                        <option value="35">Aproval Pending</option>
                                                                                                                                                                        <option value="7">Canceled</option>
                                                                                                                                                                        <option value="9">Canceled Reversal</option>
                                                                                                                                                                        <option value="17">COD &nbsp;Confirmed</option>
                                                                                                                                                                        <option value="37">Completed</option>
                                                                                                                                                                        <option value="29">Converted Lead</option>
                                                                                                                                                                        <option value="8">Customer Refuse To Accept</option>
                                                                                                                                                                        <option value="22">Delivered</option>
                                                                                                                                                                        <option value="36">Disapproved</option>
                                                                                                                                                                        <option value="30">Duplicate Order</option>
                                                                                                                                                                        <option value="40">Entry Blocked - Price Issue</option>
                                                                                                                                                                        <option value="10">Failed</option>
                                                                                                                                                                        <option value="39">Incomplete Address</option>
                                                                                                                                                                        <option value="28">Lead</option>
                                                                                                                                                                        <option value="23">Miss Route</option>
                                                                                                                                                                        <option value="27">Non - serviceable area</option>
                                                                                                                                                                        <option value="24">ODA- Out Of Delivery Area</option>
                                                                                                                                                                        <option value="34">On hold</option>
                                                                                                                                                                        <option value="38">Order Assigned</option>
                                                                                                                                                                        <option value="42">Payment Gateway Entry</option>
                                                                                                                                                                        <option value="1">Pending</option>
                                                                                                                                                                        <option value="18" selected="selected">Pick Up Generated</option>
                                                                                                                                                                        <option value="19">Prepaid</option>
                                                                                                                                                                        <option value="15">Processed</option>
                                                                                                                                                                        <option value="2">Processing</option>
                                                                                                                                                                        <option value="26">Re Order - dont use it</option>
                                                                                                                                                                        <option value="20">Ready for Dispatch</option>
                                                                                                                                                                        <option value="11">Refunded</option>
                                                                                                                                                                        <option value="21">Reschedule</option>
                                                                                                                                                                        <option value="12">Reversed</option>
                                                                                                                                                                        <option value="14">RTO-Returned To Origin</option>
                                                                                                                                                                        <option value="43">Shipment Damaged</option>
                                                                                                                                                                        <option value="13">Shipment Lost</option>
                                                                                                                                                                        <option value="3">Shipped</option>
                                                                                                                                                                        <option value="33">UnConfirmed</option>
                                                                                                                                                                        <option value="5">Undelivered</option>
                                                                                                                                                                        <option value="16">Voided</option>
                                                                                                                                                                        <option value="25">Wrong Delivery</option>
                                                                                                                            </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-3 control-label" for="input-override"><span data-toggle="tooltip" title="" data-original-title="If the customers order is being blocked from changing the order status due to a anti-fraud extension enable override.">Override</span></label>
                                      <div class="col-sm-9">
                                        <input type="checkbox" name="override" value="1" id="input-override">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-3 control-label" for="input-notify">Notify Customer</label>
                                      <div class="col-sm-9">
                                        <input type="checkbox" name="notify" value="1" id="input-notify">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-3 control-label" for="input-comment">Comment</label>
                                      <div class="col-sm-9">
                                        <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                                      </div>
                                    </div>
                                  </form>
                                </fieldset>
                                <div class="text-right">
                                  <button id="button-history" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add History</button>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>



                <div id="element_to_pop_up3"> <a class="b-close" title="Close">x</a>
                  <div class="request-a-call">
                     <h2>Select Courier Company</h2>
                    <div class="request-a-call-form">
                       <fieldset>
                            <form class="form-horizontal">
                              <input type="hidden" name="order_id" value="99998000562589">
                              <div id="element3"></div>

                                <!-- <div class="form-group" id="aip_company_name">
                                      <label class="col-sm-2 control-label other"><input type="radio" name="company_id" value="0" id="aip_sd_company_id" />OTHER</label><div class="col-sm-5"><input id="courier_name"  required="" name="courier_name" type="text" placeholder="Courier Company"></div><div class="col-sm-5"><input id="courier_awb" required="" name="courier_awb" type="text" placeholder="AWB Number"></div>
                               </div> -->
                            </form>
                        </fieldset>  <br>
                          <div class="text-right">
                          <button id="button-api-history" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Proceed</button>
                          </div>

                    </div>
                  </div>
              </div>


  <!-- Element to pop up  End here-->

<!-- ...........................WASIM UPDATE ADDRESS HERE STARTS HERE................................. -->


<!-- <script type="text/javascript">
function enquirypopup(n, user){

   $('#element_to_pop_up1').bPopup({
           // easing: 'easeOutBack', //uses jQuery easing plugin
           // speed: 1000,

               easing: 'easeOutBack', //uses jQuery easing plugin
              speed: 1000,
              position: [150, 150], //x, y
              transition: 'slideDown'
           // transition: 'slideDown'
          });
}
</script> -->
<style type="text/css">
#element_to_pop_up1{

  top: 500px !important;
}
</style>
      <!-- POPUP Change Order Address Element to pop up  START HERE-->
      <div id="element_to_pop_up1">
          <a class="b-close" title="Close">x</a>
          <div class="request-a-call">
            <h2>Change Order Address</h2>


                            <div class="container-fluid">

                                <div class="panel-body">
                                  <!-- <form class="form-horizontal" action="#"> -->
                                     <form class="form-horizontal" action="http://asianherbs.in/salesadminadmin/index.php?route=logistics/order/editaddress&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;order_id=99998000562589" method="post" enctype="multipart/form-data">
                                    <ul id="order" class="nav nav-tabs nav-justified">
                                      <li class="disabled active"><a href="http://asianherbs.in/salesadminadmin/#tab-customer" data-toggle="tab">1. Customer Details</a></li>
                                      <!-- <li class="disabled"><a href="#tab-cart" data-toggle="tab">2. Products</a></li> -->
                                      <li class="disabled"><a href="http://asianherbs.in/salesadminadmin/#tab-payment" data-toggle="tab">3. Payment Details</a></li>
                                      <li class="disabled"><a href="http://asianherbs.in/salesadminadmin/#tab-shipping" data-toggle="tab">4. Shipping Details</a></li>
                                      <!-- <li class="disabled"><a href="#tab-total" data-toggle="tab">5. Totals</a></li> -->
                                    </ul>
                                    <div class="tab-content">
                                       <div class="tab-pane active" id="tab-customer">
                                        <div class="form-group" style="display: none;">
                                          <label class="col-sm-2 control-label" for="input-store">Store</label>
                                          <div class="col-sm-10">
                                            <select name="store" id="input-store" class="form-control">
                                                                                                                                          <option value="http://asianherbs.in/salesadmin" selected="selected">Default</option>
                                                                                                                                        </select>
                                          </div>
                                        </div>
                                        <div class="form-group has-error" style="display: none;">
                                          <label class="col-sm-2 control-label" for="input-currency">Currency</label>
                                          <div class="col-sm-10">
                                            <select name="currency" id="input-currency" class="form-control">
                                                                                                                                          <option value="INR">Indian Rupee</option>
                                                                                                                                        </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                          <label class="col-sm-2 control-label" for="input-customer">Customer</label>
                                          <div class="col-sm-10">
                                            <input type="text" name="customer" value="Sakshi Rathore" placeholder="Customer" id="input-customer" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
                                            <input type="hidden" name="customer_id" value="586604">
                                          </div>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                          <label class="col-sm-2 control-label" for="input-customer-group">Customer Group</label>
                                          <div class="col-sm-10">
                                            <select name="customer_group_id" id="input-customer-group" class="form-control">
                                                                                                                                          <option value="6" selected="selected">Landing Page</option>
                                                                                                                                                                                        <option value="7">Vendor</option>
                                                                                                                                                                                        <option value="2">Website</option>
                                                                                                                                        </select>
                                          </div>
                                        </div>
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-firstname">First Name</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="firstname" value="Sakshi" id="input-firstname" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-lastname">Last Name</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="lastname" value="Rathore" id="input-lastname" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-email">E-Mail</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="email" value="sakshirathore665@gmail.com" id="input-email" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-telephone">Telephone</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="telephone" value="8839486499" id="input-telephone" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="input-fax">Fax</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="fax" value="" id="input-fax" class="form-control">
                                          </div>
                                        </div>
                                        <div class="text-right">
                                          <button type="button" id="button-customer" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-arrow-right"></i> Continue</button>
                                        </div>
                                       </div>


                                       <div class="tab-pane" id="tab-payment">
                                            <!-- <div class="form-group">
                                              <label class="col-sm-3 control-label" for="input-payment-address">Choose Address</label>
                                              <div class="col-sm-9">
                                                <select name="payment_address" id="input-payment-address" class="form-control">
                                                  <option value="0" selected="selected"> --- None --- </option>
                                                                                                  </select>
                                              </div>
                                            </div> -->
                                            <div class="form-group required">
                                              <label class="col-sm-3 control-label" for="input-payment-firstname">First Name</label>
                                              <div class="col-sm-9">
                                                <input type="text" name="firstname" value="Sakshi" id="input-payment-firstname" class="form-control">
                                              </div>
                                            </div>
                                            <div class="form-group required">
                                              <label class="col-sm-3 control-label" for="input-payment-lastname">Last Name</label>
                                              <div class="col-sm-9">
                                                <input type="text" name="lastname" value="Rathore" id="input-payment-lastname" class="form-control">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="col-sm-3 control-label" for="input-payment-company">Company</label>
                                              <div class="col-sm-9">
                                                <input type="text" name="company" value="" id="input-payment-company" class="form-control">
                                              </div>
                                            </div>
                                            <div class="form-group required">
                                              <label class="col-sm-3 control-label" for="input-payment-address-1">Address 1</label>
                                              <div class="col-sm-9">
                                                <input type="text" name="address_1" value="Sakshi Rathore
C/O: Sakshi Rathore
Ward No.- 32, Jai Hind Chowk, Murwara, Post- Murwara,
Near- Murwara Bridge, Katni
Landmar" id="input-payment-address-1" class="form-control">
                                              </div>
                                            </div>
                                            <div class="form-group required">
                                              <label class="col-sm-3 control-label" for="input-payment-address-2">Address 2</label>
                                              <div class="col-sm-9">
                                                <input type="text" name="address_2" value="" id="input-payment-address-2" class="form-control">
                                              </div>
                                            </div>
                                            <div class="form-group required">
                                              <label class="col-sm-3 control-label" for="input-payment-city">City</label>
                                              <div class="col-sm-9">
                                                <input type="text" name="city" value="Katni" id="input-payment-city" class="form-control">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="col-sm-3 control-label" for="input-payment-postcode">Postcode</label>
                                              <div class="col-sm-9">
                                                <input type="text" name="postcode" value="483501" id="input-payment-postcode" class="form-control">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="col-sm-3 control-label" for="input-payment-country">Country</label>
                                              <div class="col-sm-9">
                                                <select name="country_id" id="input-payment-country" class="form-control">
                                                  <option value=""> --- Please Select --- </option>
                                                                                                                                                      <option value="244">Aaland Islands</option>
                                                                                                                                                                                                        <option value="1">Afghanistan</option>
                                                                                                                                                                                                        <option value="2">Albania</option>
                                                                                                                                                                                                        <option value="3">Algeria</option>
                                                                                                                                                                                                        <option value="4">American Samoa</option>
                                                                                                                                                                                                        <option value="5">Andorra</option>
                                                                                                                                                                                                        <option value="6">Angola</option>
                                                                                                                                                                                                        <option value="7">Anguilla</option>
                                                                                                                                                                                                        <option value="8">Antarctica</option>
                                                                                                                                                                                                        <option value="9">Antigua and Barbuda</option>
                                                                                                                                                                                                        <option value="10">Argentina</option>
                                                                                                                                                                                                        <option value="11">Armenia</option>
                                                                                                                                                                                                        <option value="12">Aruba</option>
                                                                                                                                                                                                        <option value="252">Ascension Island (British)</option>
                                                                                                                                                                                                        <option value="13">Australia</option>
                                                                                                                                                                                                        <option value="14">Austria</option>
                                                                                                                                                                                                        <option value="15">Azerbaijan</option>
                                                                                                                                                                                                        <option value="16">Bahamas</option>
                                                                                                                                                                                                        <option value="17">Bahrain</option>
                                                                                                                                                                                                        <option value="18">Bangladesh</option>
                                                                                                                                                                                                        <option value="19">Barbados</option>
                                                                                                                                                                                                        <option value="20">Belarus</option>
                                                                                                                                                                                                        <option value="21">Belgium</option>
                                                                                                                                                                                                        <option value="22">Belize</option>
                                                                                                                                                                                                        <option value="23">Benin</option>
                                                                                                                                                                                                        <option value="24">Bermuda</option>
                                                                                                                                                                                                        <option value="25">Bhutan</option>
                                                                                                                                                                                                        <option value="26">Bolivia</option>
                                                                                                                                                                                                        <option value="245">Bonaire, Sint Eustatius and Saba</option>
                                                                                                                                                                                                        <option value="27">Bosnia and Herzegovina</option>
                                                                                                                                                                                                        <option value="28">Botswana</option>
                                                                                                                                                                                                        <option value="29">Bouvet Island</option>
                                                                                                                                                                                                        <option value="30">Brazil</option>
                                                                                                                                                                                                        <option value="31">British Indian Ocean Territory</option>
                                                                                                                                                                                                        <option value="32">Brunei Darussalam</option>
                                                                                                                                                                                                        <option value="33">Bulgaria</option>
                                                                                                                                                                                                        <option value="34">Burkina Faso</option>
                                                                                                                                                                                                        <option value="35">Burundi</option>
                                                                                                                                                                                                        <option value="36">Cambodia</option>
                                                                                                                                                                                                        <option value="37">Cameroon</option>
                                                                                                                                                                                                        <option value="38">Canada</option>
                                                                                                                                                                                                        <option value="251">Canary Islands</option>
                                                                                                                                                                                                        <option value="39">Cape Verde</option>
                                                                                                                                                                                                        <option value="40">Cayman Islands</option>
                                                                                                                                                                                                        <option value="41">Central African Republic</option>
                                                                                                                                                                                                        <option value="42">Chad</option>
                                                                                                                                                                                                        <option value="43">Chile</option>
                                                                                                                                                                                                        <option value="44">China</option>
                                                                                                                                                                                                        <option value="45">Christmas Island</option>
                                                                                                                                                                                                        <option value="46">Cocos (Keeling) Islands</option>
                                                                                                                                                                                                        <option value="47">Colombia</option>
                                                                                                                                                                                                        <option value="48">Comoros</option>
                                                                                                                                                                                                        <option value="49">Congo</option>
                                                                                                                                                                                                        <option value="50">Cook Islands</option>
                                                                                                                                                                                                        <option value="51">Costa Rica</option>
                                                                                                                                                                                                        <option value="52">Cote D'Ivoire</option>
                                                                                                                                                                                                        <option value="53">Croatia</option>
                                                                                                                                                                                                        <option value="54">Cuba</option>
                                                                                                                                                                                                        <option value="246">Curacao</option>
                                                                                                                                                                                                        <option value="55">Cyprus</option>
                                                                                                                                                                                                        <option value="56">Czech Republic</option>
                                                                                                                                                                                                        <option value="237">Democratic Republic of Congo</option>
                                                                                                                                                                                                        <option value="57">Denmark</option>
                                                                                                                                                                                                        <option value="58">Djibouti</option>
                                                                                                                                                                                                        <option value="59">Dominica</option>
                                                                                                                                                                                                        <option value="60">Dominican Republic</option>
                                                                                                                                                                                                        <option value="61">East Timor</option>
                                                                                                                                                                                                        <option value="62">Ecuador</option>
                                                                                                                                                                                                        <option value="63">Egypt</option>
                                                                                                                                                                                                        <option value="64">El Salvador</option>
                                                                                                                                                                                                        <option value="65">Equatorial Guinea</option>
                                                                                                                                                                                                        <option value="66">Eritrea</option>
                                                                                                                                                                                                        <option value="67">Estonia</option>
                                                                                                                                                                                                        <option value="68">Ethiopia</option>
                                                                                                                                                                                                        <option value="69">Falkland Islands (Malvinas)</option>
                                                                                                                                                                                                        <option value="70">Faroe Islands</option>
                                                                                                                                                                                                        <option value="71">Fiji</option>
                                                                                                                                                                                                        <option value="72">Finland</option>
                                                                                                                                                                                                        <option value="74">France, Metropolitan</option>
                                                                                                                                                                                                        <option value="75">French Guiana</option>
                                                                                                                                                                                                        <option value="76">French Polynesia</option>
                                                                                                                                                                                                        <option value="77">French Southern Territories</option>
                                                                                                                                                                                                        <option value="126">FYROM</option>
                                                                                                                                                                                                        <option value="78">Gabon</option>
                                                                                                                                                                                                        <option value="79">Gambia</option>
                                                                                                                                                                                                        <option value="80">Georgia</option>
                                                                                                                                                                                                        <option value="81">Germany</option>
                                                                                                                                                                                                        <option value="82">Ghana</option>
                                                                                                                                                                                                        <option value="83">Gibraltar</option>
                                                                                                                                                                                                        <option value="84">Greece</option>
                                                                                                                                                                                                        <option value="85">Greenland</option>
                                                                                                                                                                                                        <option value="86">Grenada</option>
                                                                                                                                                                                                        <option value="87">Guadeloupe</option>
                                                                                                                                                                                                        <option value="88">Guam</option>
                                                                                                                                                                                                        <option value="89">Guatemala</option>
                                                                                                                                                                                                        <option value="256">Guernsey</option>
                                                                                                                                                                                                        <option value="90">Guinea</option>
                                                                                                                                                                                                        <option value="91">Guinea-Bissau</option>
                                                                                                                                                                                                        <option value="92">Guyana</option>
                                                                                                                                                                                                        <option value="93">Haiti</option>
                                                                                                                                                                                                        <option value="94">Heard and Mc Donald Islands</option>
                                                                                                                                                                                                        <option value="95">Honduras</option>
                                                                                                                                                                                                        <option value="96">Hong Kong</option>
                                                                                                                                                                                                        <option value="97">Hungary</option>
                                                                                                                                                                                                        <option value="98">Iceland</option>
                                                                                                                                                                                                        <option value="99" selected="selected">India</option>
                                                                                                                                                                                                        <option value="100">Indonesia</option>
                                                                                                                                                                                                        <option value="101">Iran (Islamic Republic of)</option>
                                                                                                                                                                                                        <option value="102">Iraq</option>
                                                                                                                                                                                                        <option value="103">Ireland</option>
                                                                                                                                                                                                        <option value="254">Isle of Man</option>
                                                                                                                                                                                                        <option value="104">Israel</option>
                                                                                                                                                                                                        <option value="105">Italy</option>
                                                                                                                                                                                                        <option value="106">Jamaica</option>
                                                                                                                                                                                                        <option value="107">Japan</option>
                                                                                                                                                                                                        <option value="257">Jersey</option>
                                                                                                                                                                                                        <option value="108">Jordan</option>
                                                                                                                                                                                                        <option value="109">Kazakhstan</option>
                                                                                                                                                                                                        <option value="110">Kenya</option>
                                                                                                                                                                                                        <option value="111">Kiribati</option>
                                                                                                                                                                                                        <option value="253">Kosovo, Republic of</option>
                                                                                                                                                                                                        <option value="114">Kuwait</option>
                                                                                                                                                                                                        <option value="115">Kyrgyzstan</option>
                                                                                                                                                                                                        <option value="116">Lao People's Democratic Republic</option>
                                                                                                                                                                                                        <option value="117">Latvia</option>
                                                                                                                                                                                                        <option value="118">Lebanon</option>
                                                                                                                                                                                                        <option value="119">Lesotho</option>
                                                                                                                                                                                                        <option value="120">Liberia</option>
                                                                                                                                                                                                        <option value="121">Libyan Arab Jamahiriya</option>
                                                                                                                                                                                                        <option value="122">Liechtenstein</option>
                                                                                                                                                                                                        <option value="123">Lithuania</option>
                                                                                                                                                                                                        <option value="124">Luxembourg</option>
                                                                                                                                                                                                        <option value="125">Macau</option>
                                                                                                                                                                                                        <option value="127">Madagascar</option>
                                                                                                                                                                                                        <option value="128">Malawi</option>
                                                                                                                                                                                                        <option value="129">Malaysia</option>
                                                                                                                                                                                                        <option value="130">Maldives</option>
                                                                                                                                                                                                        <option value="131">Mali</option>
                                                                                                                                                                                                        <option value="132">Malta</option>
                                                                                                                                                                                                        <option value="133">Marshall Islands</option>
                                                                                                                                                                                                        <option value="134">Martinique</option>
                                                                                                                                                                                                        <option value="135">Mauritania</option>
                                                                                                                                                                                                        <option value="136">Mauritius</option>
                                                                                                                                                                                                        <option value="137">Mayotte</option>
                                                                                                                                                                                                        <option value="138">Mexico</option>
                                                                                                                                                                                                        <option value="139">Micronesia, Federated States of</option>
                                                                                                                                                                                                        <option value="140">Moldova, Republic of</option>
                                                                                                                                                                                                        <option value="141">Monaco</option>
                                                                                                                                                                                                        <option value="142">Mongolia</option>
                                                                                                                                                                                                        <option value="242">Montenegro</option>
                                                                                                                                                                                                        <option value="143">Montserrat</option>
                                                                                                                                                                                                        <option value="144">Morocco</option>
                                                                                                                                                                                                        <option value="145">Mozambique</option>
                                                                                                                                                                                                        <option value="146">Myanmar</option>
                                                                                                                                                                                                        <option value="147">Namibia</option>
                                                                                                                                                                                                        <option value="148">Nauru</option>
                                                                                                                                                                                                        <option value="149">Nepal</option>
                                                                                                                                                                                                        <option value="150">Netherlands</option>
                                                                                                                                                                                                        <option value="151">Netherlands Antilles</option>
                                                                                                                                                                                                        <option value="152">New Caledonia</option>
                                                                                                                                                                                                        <option value="153">New Zealand</option>
                                                                                                                                                                                                        <option value="154">Nicaragua</option>
                                                                                                                                                                                                        <option value="155">Niger</option>
                                                                                                                                                                                                        <option value="156">Nigeria</option>
                                                                                                                                                                                                        <option value="157">Niue</option>
                                                                                                                                                                                                        <option value="158">Norfolk Island</option>
                                                                                                                                                                                                        <option value="112">North Korea</option>
                                                                                                                                                                                                        <option value="159">Northern Mariana Islands</option>
                                                                                                                                                                                                        <option value="160">Norway</option>
                                                                                                                                                                                                        <option value="161">Oman</option>
                                                                                                                                                                                                        <option value="162">Pakistan</option>
                                                                                                                                                                                                        <option value="163">Palau</option>
                                                                                                                                                                                                        <option value="247">Palestinian Territory, Occupied</option>
                                                                                                                                                                                                        <option value="164">Panama</option>
                                                                                                                                                                                                        <option value="165">Papua New Guinea</option>
                                                                                                                                                                                                        <option value="166">Paraguay</option>
                                                                                                                                                                                                        <option value="167">Peru</option>
                                                                                                                                                                                                        <option value="168">Philippines</option>
                                                                                                                                                                                                        <option value="169">Pitcairn</option>
                                                                                                                                                                                                        <option value="170">Poland</option>
                                                                                                                                                                                                        <option value="171">Portugal</option>
                                                                                                                                                                                                        <option value="172">Puerto Rico</option>
                                                                                                                                                                                                        <option value="173">Qatar</option>
                                                                                                                                                                                                        <option value="174">Reunion</option>
                                                                                                                                                                                                        <option value="175">Romania</option>
                                                                                                                                                                                                        <option value="176">Russian Federation</option>
                                                                                                                                                                                                        <option value="177">Rwanda</option>
                                                                                                                                                                                                        <option value="178">Saint Kitts and Nevis</option>
                                                                                                                                                                                                        <option value="179">Saint Lucia</option>
                                                                                                                                                                                                        <option value="180">Saint Vincent and the Grenadines</option>
                                                                                                                                                                                                        <option value="181">Samoa</option>
                                                                                                                                                                                                        <option value="182">San Marino</option>
                                                                                                                                                                                                        <option value="183">Sao Tome and Principe</option>
                                                                                                                                                                                                        <option value="184">Saudi Arabia</option>
                                                                                                                                                                                                        <option value="185">Senegal</option>
                                                                                                                                                                                                        <option value="243">Serbia</option>
                                                                                                                                                                                                        <option value="186">Seychelles</option>
                                                                                                                                                                                                        <option value="187">Sierra Leone</option>
                                                                                                                                                                                                        <option value="188">Singapore</option>
                                                                                                                                                                                                        <option value="189">Slovak Republic</option>
                                                                                                                                                                                                        <option value="190">Slovenia</option>
                                                                                                                                                                                                        <option value="191">Solomon Islands</option>
                                                                                                                                                                                                        <option value="192">Somalia</option>
                                                                                                                                                                                                        <option value="193">South Africa</option>
                                                                                                                                                                                                        <option value="194">South Georgia &amp; South Sandwich Islands</option>
                                                                                                                                                                                                        <option value="113">South Korea</option>
                                                                                                                                                                                                        <option value="248">South Sudan</option>
                                                                                                                                                                                                        <option value="195">Spain</option>
                                                                                                                                                                                                        <option value="196">Sri Lanka</option>
                                                                                                                                                                                                        <option value="249">St. Barthelemy</option>
                                                                                                                                                                                                        <option value="197">St. Helena</option>
                                                                                                                                                                                                        <option value="250">St. Martin (French part)</option>
                                                                                                                                                                                                        <option value="198">St. Pierre and Miquelon</option>
                                                                                                                                                                                                        <option value="199">Sudan</option>
                                                                                                                                                                                                        <option value="200">Suriname</option>
                                                                                                                                                                                                        <option value="201">Svalbard and Jan Mayen Islands</option>
                                                                                                                                                                                                        <option value="202">Swaziland</option>
                                                                                                                                                                                                        <option value="203">Sweden</option>
                                                                                                                                                                                                        <option value="204">Switzerland</option>
                                                                                                                                                                                                        <option value="205">Syrian Arab Republic</option>
                                                                                                                                                                                                        <option value="206">Taiwan</option>
                                                                                                                                                                                                        <option value="207">Tajikistan</option>
                                                                                                                                                                                                        <option value="208">Tanzania, United Republic of</option>
                                                                                                                                                                                                        <option value="209">Thailand</option>
                                                                                                                                                                                                        <option value="210">Togo</option>
                                                                                                                                                                                                        <option value="211">Tokelau</option>
                                                                                                                                                                                                        <option value="212">Tonga</option>
                                                                                                                                                                                                        <option value="213">Trinidad and Tobago</option>
                                                                                                                                                                                                        <option value="255">Tristan da Cunha</option>
                                                                                                                                                                                                        <option value="214">Tunisia</option>
                                                                                                                                                                                                        <option value="215">Turkey</option>
                                                                                                                                                                                                        <option value="216">Turkmenistan</option>
                                                                                                                                                                                                        <option value="217">Turks and Caicos Islands</option>
                                                                                                                                                                                                        <option value="218">Tuvalu</option>
                                                                                                                                                                                                        <option value="219">Uganda</option>
                                                                                                                                                                                                        <option value="220">Ukraine</option>
                                                                                                                                                                                                        <option value="221">United Arab Emirates</option>
                                                                                                                                                                                                        <option value="222">United Kingdom</option>
                                                                                                                                                                                                        <option value="223">United States</option>
                                                                                                                                                                                                        <option value="224">United States Minor Outlying Islands</option>
                                                                                                                                                                                                        <option value="225">Uruguay</option>
                                                                                                                                                                                                        <option value="226">Uzbekistan</option>
                                                                                                                                                                                                        <option value="227">Vanuatu</option>
                                                                                                                                                                                                        <option value="228">Vatican City State (Holy See)</option>
                                                                                                                                                                                                        <option value="229">Venezuela</option>
                                                                                                                                                                                                        <option value="230">Viet Nam</option>
                                                                                                                                                                                                        <option value="231">Virgin Islands (British)</option>
                                                                                                                                                                                                        <option value="232">Virgin Islands (U.S.)</option>
                                                                                                                                                                                                        <option value="233">Wallis and Futuna Islands</option>
                                                                                                                                                                                                        <option value="234">Western Sahara</option>
                                                                                                                                                                                                        <option value="235">Yemen</option>
                                                                                                                                                                                                        <option value="238">Zambia</option>
                                                                                                                                                                                                        <option value="239">Zimbabwe</option>
                                                                                                                                                    </select>
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="col-sm-3 control-label" for="input-payment-zone">Region / State</label>
                                              <div class="col-sm-9">
                                                <select name="zone_id" id="input-payment-zone" class="form-control"><option value=""> --- Please Select --- </option><option value="1475">Andaman &amp; Nicobar Islands</option><option value="1476">Andhra Pradesh</option><option value="1477">Arunachal Pradesh</option><option value="1478">Assam</option><option value="1479">Bihar</option><option value="1480">Chandigarh</option><option value="4233">Chhattisgarh</option><option value="1481">Dadra &amp; Nagar Haveli</option><option value="1482">Daman and Diu</option><option value="1483">Delhi</option><option value="1484">Goa</option><option value="1485">Gujarat</option><option value="1486">Haryana</option><option value="1487">Himachal Pradesh</option><option value="1488">Jammu &amp; Kashmir</option><option value="4234">Jharkhand</option><option value="1489">Karnataka</option><option value="1490">Kerala</option><option value="1491">Lakshadweep Islands</option><option value="1492" selected="selected">Madhya Pradesh</option><option value="1493">Maharashtra</option><option value="1494">Manipur</option><option value="1495">Meghalaya</option><option value="1496">Mizoram</option><option value="1497">Nagaland</option><option value="1498">Odisha</option><option value="1499">Puducherry</option><option value="1500">Punjab</option><option value="1501">Rajasthan</option><option value="1502">Sikkim</option><option value="1503">Tamil Nadu</option><option value="4231">Telangana</option><option value="1504">Tripura</option><option value="1505">Uttar Pradesh</option><option value="4232">Uttarakhand</option><option value="1506">West Bengal</option></select>
                                              </div>
                                            </div>

                                            <div class="row">
                                              <div class="col-sm-6 text-left">
                                                <button type="button" onclick="$(&#39;a[href=\&#39;#tab-customer\&#39;]&#39;).tab(&#39;show&#39;);" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</button>
                                              </div>
                                              <div class="col-sm-6 text-right">
                                                <button type="button" id="button-payment-address" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-arrow-right"></i> Continue</button>
                                              </div>
                                            </div>
                                       </div>


                                       <div class="tab-pane" id="tab-shipping">
                                          <input type="hidden" name="order_id" value="99998000562589">
                                        <!-- <div class="form-group">
                                          <label class="col-sm-3 control-label" for="input-shipping-address">Choose Address</label>
                                          <div class="col-sm-9">
                                            <select name="shipping_address" id="input-shipping-address" class="form-control">
                                              <option value="0" selected="selected"> --- None --- </option>
                                                                                          </select>
                                          </div>
                                        </div> -->
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-shipping-firstname">First Name</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="firstname" value="Sakshi" id="input-shipping-firstname" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-shipping-lastname">Last Name</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="lastname" value="Rathore" id="input-shipping-lastname" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="input-shipping-company">Company</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="company" value="" id="input-shipping-company" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-shipping-address-1">Address 1</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="address_1" value="Sakshi Rathore
C/O: Sakshi Rathore
Ward No.- 32, Jai Hind Chowk, Murwara, Post- Murwara,
Near- Murwara Bridge, Katni
Landmar" id="input-shipping-address-1" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="input-shipping-address-2">Address 2</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="address_2" value="" id="input-shipping-address-2" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group required">
                                          <label class="col-sm-3 control-label" for="input-shipping-city">City</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="city" value="Katni" id="input-shipping-city" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="input-shipping-postcode">Postcode</label>
                                          <div class="col-sm-9">
                                            <input type="text" name="postcode" value="483501" id="input-shipping-postcode" class="form-control">
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="input-shipping-country">Country</label>
                                          <div class="col-sm-9">
                                            <select name="country_id" id="input-shipping-country" class="form-control">
                                              <option value=""> --- Please Select --- </option>
                                                                                                                                          <option value="244">Aaland Islands</option>
                                                                                                                                                                                        <option value="1">Afghanistan</option>
                                                                                                                                                                                        <option value="2">Albania</option>
                                                                                                                                                                                        <option value="3">Algeria</option>
                                                                                                                                                                                        <option value="4">American Samoa</option>
                                                                                                                                                                                        <option value="5">Andorra</option>
                                                                                                                                                                                        <option value="6">Angola</option>
                                                                                                                                                                                        <option value="7">Anguilla</option>
                                                                                                                                                                                        <option value="8">Antarctica</option>
                                                                                                                                                                                        <option value="9">Antigua and Barbuda</option>
                                                                                                                                                                                        <option value="10">Argentina</option>
                                                                                                                                                                                        <option value="11">Armenia</option>
                                                                                                                                                                                        <option value="12">Aruba</option>
                                                                                                                                                                                        <option value="252">Ascension Island (British)</option>
                                                                                                                                                                                        <option value="13">Australia</option>
                                                                                                                                                                                        <option value="14">Austria</option>
                                                                                                                                                                                        <option value="15">Azerbaijan</option>
                                                                                                                                                                                        <option value="16">Bahamas</option>
                                                                                                                                                                                        <option value="17">Bahrain</option>
                                                                                                                                                                                        <option value="18">Bangladesh</option>
                                                                                                                                                                                        <option value="19">Barbados</option>
                                                                                                                                                                                        <option value="20">Belarus</option>
                                                                                                                                                                                        <option value="21">Belgium</option>
                                                                                                                                                                                        <option value="22">Belize</option>
                                                                                                                                                                                        <option value="23">Benin</option>
                                                                                                                                                                                        <option value="24">Bermuda</option>
                                                                                                                                                                                        <option value="25">Bhutan</option>
                                                                                                                                                                                        <option value="26">Bolivia</option>
                                                                                                                                                                                        <option value="245">Bonaire, Sint Eustatius and Saba</option>
                                                                                                                                                                                        <option value="27">Bosnia and Herzegovina</option>
                                                                                                                                                                                        <option value="28">Botswana</option>
                                                                                                                                                                                        <option value="29">Bouvet Island</option>
                                                                                                                                                                                        <option value="30">Brazil</option>
                                                                                                                                                                                        <option value="31">British Indian Ocean Territory</option>
                                                                                                                                                                                        <option value="32">Brunei Darussalam</option>
                                                                                                                                                                                        <option value="33">Bulgaria</option>
                                                                                                                                                                                        <option value="34">Burkina Faso</option>
                                                                                                                                                                                        <option value="35">Burundi</option>
                                                                                                                                                                                        <option value="36">Cambodia</option>
                                                                                                                                                                                        <option value="37">Cameroon</option>
                                                                                                                                                                                        <option value="38">Canada</option>
                                                                                                                                                                                        <option value="251">Canary Islands</option>
                                                                                                                                                                                        <option value="39">Cape Verde</option>
                                                                                                                                                                                        <option value="40">Cayman Islands</option>
                                                                                                                                                                                        <option value="41">Central African Republic</option>
                                                                                                                                                                                        <option value="42">Chad</option>
                                                                                                                                                                                        <option value="43">Chile</option>
                                                                                                                                                                                        <option value="44">China</option>
                                                                                                                                                                                        <option value="45">Christmas Island</option>
                                                                                                                                                                                        <option value="46">Cocos (Keeling) Islands</option>
                                                                                                                                                                                        <option value="47">Colombia</option>
                                                                                                                                                                                        <option value="48">Comoros</option>
                                                                                                                                                                                        <option value="49">Congo</option>
                                                                                                                                                                                        <option value="50">Cook Islands</option>
                                                                                                                                                                                        <option value="51">Costa Rica</option>
                                                                                                                                                                                        <option value="52">Cote D'Ivoire</option>
                                                                                                                                                                                        <option value="53">Croatia</option>
                                                                                                                                                                                        <option value="54">Cuba</option>
                                                                                                                                                                                        <option value="246">Curacao</option>
                                                                                                                                                                                        <option value="55">Cyprus</option>
                                                                                                                                                                                        <option value="56">Czech Republic</option>
                                                                                                                                                                                        <option value="237">Democratic Republic of Congo</option>
                                                                                                                                                                                        <option value="57">Denmark</option>
                                                                                                                                                                                        <option value="58">Djibouti</option>
                                                                                                                                                                                        <option value="59">Dominica</option>
                                                                                                                                                                                        <option value="60">Dominican Republic</option>
                                                                                                                                                                                        <option value="61">East Timor</option>
                                                                                                                                                                                        <option value="62">Ecuador</option>
                                                                                                                                                                                        <option value="63">Egypt</option>
                                                                                                                                                                                        <option value="64">El Salvador</option>
                                                                                                                                                                                        <option value="65">Equatorial Guinea</option>
                                                                                                                                                                                        <option value="66">Eritrea</option>
                                                                                                                                                                                        <option value="67">Estonia</option>
                                                                                                                                                                                        <option value="68">Ethiopia</option>
                                                                                                                                                                                        <option value="69">Falkland Islands (Malvinas)</option>
                                                                                                                                                                                        <option value="70">Faroe Islands</option>
                                                                                                                                                                                        <option value="71">Fiji</option>
                                                                                                                                                                                        <option value="72">Finland</option>
                                                                                                                                                                                        <option value="74">France, Metropolitan</option>
                                                                                                                                                                                        <option value="75">French Guiana</option>
                                                                                                                                                                                        <option value="76">French Polynesia</option>
                                                                                                                                                                                        <option value="77">French Southern Territories</option>
                                                                                                                                                                                        <option value="126">FYROM</option>
                                                                                                                                                                                        <option value="78">Gabon</option>
                                                                                                                                                                                        <option value="79">Gambia</option>
                                                                                                                                                                                        <option value="80">Georgia</option>
                                                                                                                                                                                        <option value="81">Germany</option>
                                                                                                                                                                                        <option value="82">Ghana</option>
                                                                                                                                                                                        <option value="83">Gibraltar</option>
                                                                                                                                                                                        <option value="84">Greece</option>
                                                                                                                                                                                        <option value="85">Greenland</option>
                                                                                                                                                                                        <option value="86">Grenada</option>
                                                                                                                                                                                        <option value="87">Guadeloupe</option>
                                                                                                                                                                                        <option value="88">Guam</option>
                                                                                                                                                                                        <option value="89">Guatemala</option>
                                                                                                                                                                                        <option value="256">Guernsey</option>
                                                                                                                                                                                        <option value="90">Guinea</option>
                                                                                                                                                                                        <option value="91">Guinea-Bissau</option>
                                                                                                                                                                                        <option value="92">Guyana</option>
                                                                                                                                                                                        <option value="93">Haiti</option>
                                                                                                                                                                                        <option value="94">Heard and Mc Donald Islands</option>
                                                                                                                                                                                        <option value="95">Honduras</option>
                                                                                                                                                                                        <option value="96">Hong Kong</option>
                                                                                                                                                                                        <option value="97">Hungary</option>
                                                                                                                                                                                        <option value="98">Iceland</option>
                                                                                                                                                                                        <option value="99" selected="selected">India</option>
                                                                                                                                                                                        <option value="100">Indonesia</option>
                                                                                                                                                                                        <option value="101">Iran (Islamic Republic of)</option>
                                                                                                                                                                                        <option value="102">Iraq</option>
                                                                                                                                                                                        <option value="103">Ireland</option>
                                                                                                                                                                                        <option value="254">Isle of Man</option>
                                                                                                                                                                                        <option value="104">Israel</option>
                                                                                                                                                                                        <option value="105">Italy</option>
                                                                                                                                                                                        <option value="106">Jamaica</option>
                                                                                                                                                                                        <option value="107">Japan</option>
                                                                                                                                                                                        <option value="257">Jersey</option>
                                                                                                                                                                                        <option value="108">Jordan</option>
                                                                                                                                                                                        <option value="109">Kazakhstan</option>
                                                                                                                                                                                        <option value="110">Kenya</option>
                                                                                                                                                                                        <option value="111">Kiribati</option>
                                                                                                                                                                                        <option value="253">Kosovo, Republic of</option>
                                                                                                                                                                                        <option value="114">Kuwait</option>
                                                                                                                                                                                        <option value="115">Kyrgyzstan</option>
                                                                                                                                                                                        <option value="116">Lao People's Democratic Republic</option>
                                                                                                                                                                                        <option value="117">Latvia</option>
                                                                                                                                                                                        <option value="118">Lebanon</option>
                                                                                                                                                                                        <option value="119">Lesotho</option>
                                                                                                                                                                                        <option value="120">Liberia</option>
                                                                                                                                                                                        <option value="121">Libyan Arab Jamahiriya</option>
                                                                                                                                                                                        <option value="122">Liechtenstein</option>
                                                                                                                                                                                        <option value="123">Lithuania</option>
                                                                                                                                                                                        <option value="124">Luxembourg</option>
                                                                                                                                                                                        <option value="125">Macau</option>
                                                                                                                                                                                        <option value="127">Madagascar</option>
                                                                                                                                                                                        <option value="128">Malawi</option>
                                                                                                                                                                                        <option value="129">Malaysia</option>
                                                                                                                                                                                        <option value="130">Maldives</option>
                                                                                                                                                                                        <option value="131">Mali</option>
                                                                                                                                                                                        <option value="132">Malta</option>
                                                                                                                                                                                        <option value="133">Marshall Islands</option>
                                                                                                                                                                                        <option value="134">Martinique</option>
                                                                                                                                                                                        <option value="135">Mauritania</option>
                                                                                                                                                                                        <option value="136">Mauritius</option>
                                                                                                                                                                                        <option value="137">Mayotte</option>
                                                                                                                                                                                        <option value="138">Mexico</option>
                                                                                                                                                                                        <option value="139">Micronesia, Federated States of</option>
                                                                                                                                                                                        <option value="140">Moldova, Republic of</option>
                                                                                                                                                                                        <option value="141">Monaco</option>
                                                                                                                                                                                        <option value="142">Mongolia</option>
                                                                                                                                                                                        <option value="242">Montenegro</option>
                                                                                                                                                                                        <option value="143">Montserrat</option>
                                                                                                                                                                                        <option value="144">Morocco</option>
                                                                                                                                                                                        <option value="145">Mozambique</option>
                                                                                                                                                                                        <option value="146">Myanmar</option>
                                                                                                                                                                                        <option value="147">Namibia</option>
                                                                                                                                                                                        <option value="148">Nauru</option>
                                                                                                                                                                                        <option value="149">Nepal</option>
                                                                                                                                                                                        <option value="150">Netherlands</option>
                                                                                                                                                                                        <option value="151">Netherlands Antilles</option>
                                                                                                                                                                                        <option value="152">New Caledonia</option>
                                                                                                                                                                                        <option value="153">New Zealand</option>
                                                                                                                                                                                        <option value="154">Nicaragua</option>
                                                                                                                                                                                        <option value="155">Niger</option>
                                                                                                                                                                                        <option value="156">Nigeria</option>
                                                                                                                                                                                        <option value="157">Niue</option>
                                                                                                                                                                                        <option value="158">Norfolk Island</option>
                                                                                                                                                                                        <option value="112">North Korea</option>
                                                                                                                                                                                        <option value="159">Northern Mariana Islands</option>
                                                                                                                                                                                        <option value="160">Norway</option>
                                                                                                                                                                                        <option value="161">Oman</option>
                                                                                                                                                                                        <option value="162">Pakistan</option>
                                                                                                                                                                                        <option value="163">Palau</option>
                                                                                                                                                                                        <option value="247">Palestinian Territory, Occupied</option>
                                                                                                                                                                                        <option value="164">Panama</option>
                                                                                                                                                                                        <option value="165">Papua New Guinea</option>
                                                                                                                                                                                        <option value="166">Paraguay</option>
                                                                                                                                                                                        <option value="167">Peru</option>
                                                                                                                                                                                        <option value="168">Philippines</option>
                                                                                                                                                                                        <option value="169">Pitcairn</option>
                                                                                                                                                                                        <option value="170">Poland</option>
                                                                                                                                                                                        <option value="171">Portugal</option>
                                                                                                                                                                                        <option value="172">Puerto Rico</option>
                                                                                                                                                                                        <option value="173">Qatar</option>
                                                                                                                                                                                        <option value="174">Reunion</option>
                                                                                                                                                                                        <option value="175">Romania</option>
                                                                                                                                                                                        <option value="176">Russian Federation</option>
                                                                                                                                                                                        <option value="177">Rwanda</option>
                                                                                                                                                                                        <option value="178">Saint Kitts and Nevis</option>
                                                                                                                                                                                        <option value="179">Saint Lucia</option>
                                                                                                                                                                                        <option value="180">Saint Vincent and the Grenadines</option>
                                                                                                                                                                                        <option value="181">Samoa</option>
                                                                                                                                                                                        <option value="182">San Marino</option>
                                                                                                                                                                                        <option value="183">Sao Tome and Principe</option>
                                                                                                                                                                                        <option value="184">Saudi Arabia</option>
                                                                                                                                                                                        <option value="185">Senegal</option>
                                                                                                                                                                                        <option value="243">Serbia</option>
                                                                                                                                                                                        <option value="186">Seychelles</option>
                                                                                                                                                                                        <option value="187">Sierra Leone</option>
                                                                                                                                                                                        <option value="188">Singapore</option>
                                                                                                                                                                                        <option value="189">Slovak Republic</option>
                                                                                                                                                                                        <option value="190">Slovenia</option>
                                                                                                                                                                                        <option value="191">Solomon Islands</option>
                                                                                                                                                                                        <option value="192">Somalia</option>
                                                                                                                                                                                        <option value="193">South Africa</option>
                                                                                                                                                                                        <option value="194">South Georgia &amp; South Sandwich Islands</option>
                                                                                                                                                                                        <option value="113">South Korea</option>
                                                                                                                                                                                        <option value="248">South Sudan</option>
                                                                                                                                                                                        <option value="195">Spain</option>
                                                                                                                                                                                        <option value="196">Sri Lanka</option>
                                                                                                                                                                                        <option value="249">St. Barthelemy</option>
                                                                                                                                                                                        <option value="197">St. Helena</option>
                                                                                                                                                                                        <option value="250">St. Martin (French part)</option>
                                                                                                                                                                                        <option value="198">St. Pierre and Miquelon</option>
                                                                                                                                                                                        <option value="199">Sudan</option>
                                                                                                                                                                                        <option value="200">Suriname</option>
                                                                                                                                                                                        <option value="201">Svalbard and Jan Mayen Islands</option>
                                                                                                                                                                                        <option value="202">Swaziland</option>
                                                                                                                                                                                        <option value="203">Sweden</option>
                                                                                                                                                                                        <option value="204">Switzerland</option>
                                                                                                                                                                                        <option value="205">Syrian Arab Republic</option>
                                                                                                                                                                                        <option value="206">Taiwan</option>
                                                                                                                                                                                        <option value="207">Tajikistan</option>
                                                                                                                                                                                        <option value="208">Tanzania, United Republic of</option>
                                                                                                                                                                                        <option value="209">Thailand</option>
                                                                                                                                                                                        <option value="210">Togo</option>
                                                                                                                                                                                        <option value="211">Tokelau</option>
                                                                                                                                                                                        <option value="212">Tonga</option>
                                                                                                                                                                                        <option value="213">Trinidad and Tobago</option>
                                                                                                                                                                                        <option value="255">Tristan da Cunha</option>
                                                                                                                                                                                        <option value="214">Tunisia</option>
                                                                                                                                                                                        <option value="215">Turkey</option>
                                                                                                                                                                                        <option value="216">Turkmenistan</option>
                                                                                                                                                                                        <option value="217">Turks and Caicos Islands</option>
                                                                                                                                                                                        <option value="218">Tuvalu</option>
                                                                                                                                                                                        <option value="219">Uganda</option>
                                                                                                                                                                                        <option value="220">Ukraine</option>
                                                                                                                                                                                        <option value="221">United Arab Emirates</option>
                                                                                                                                                                                        <option value="222">United Kingdom</option>
                                                                                                                                                                                        <option value="223">United States</option>
                                                                                                                                                                                        <option value="224">United States Minor Outlying Islands</option>
                                                                                                                                                                                        <option value="225">Uruguay</option>
                                                                                                                                                                                        <option value="226">Uzbekistan</option>
                                                                                                                                                                                        <option value="227">Vanuatu</option>
                                                                                                                                                                                        <option value="228">Vatican City State (Holy See)</option>
                                                                                                                                                                                        <option value="229">Venezuela</option>
                                                                                                                                                                                        <option value="230">Viet Nam</option>
                                                                                                                                                                                        <option value="231">Virgin Islands (British)</option>
                                                                                                                                                                                        <option value="232">Virgin Islands (U.S.)</option>
                                                                                                                                                                                        <option value="233">Wallis and Futuna Islands</option>
                                                                                                                                                                                        <option value="234">Western Sahara</option>
                                                                                                                                                                                        <option value="235">Yemen</option>
                                                                                                                                                                                        <option value="238">Zambia</option>
                                                                                                                                                                                        <option value="239">Zimbabwe</option>
                                                                                                                                        </select>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="input-shipping-zone">Region / State</label>
                                          <div class="col-sm-9">
                                            <select name="zone_id" id="input-shipping-zone" class="form-control"><option value=""> --- Please Select --- </option><option value="1475">Andaman &amp; Nicobar Islands</option><option value="1476">Andhra Pradesh</option><option value="1477">Arunachal Pradesh</option><option value="1478">Assam</option><option value="1479">Bihar</option><option value="1480">Chandigarh</option><option value="4233">Chhattisgarh</option><option value="1481">Dadra &amp; Nagar Haveli</option><option value="1482">Daman and Diu</option><option value="1483">Delhi</option><option value="1484">Goa</option><option value="1485">Gujarat</option><option value="1486">Haryana</option><option value="1487">Himachal Pradesh</option><option value="1488">Jammu &amp; Kashmir</option><option value="4234">Jharkhand</option><option value="1489">Karnataka</option><option value="1490">Kerala</option><option value="1491">Lakshadweep Islands</option><option value="1492" selected="selected">Madhya Pradesh</option><option value="1493">Maharashtra</option><option value="1494">Manipur</option><option value="1495">Meghalaya</option><option value="1496">Mizoram</option><option value="1497">Nagaland</option><option value="1498">Odisha</option><option value="1499">Puducherry</option><option value="1500">Punjab</option><option value="1501">Rajasthan</option><option value="1502">Sikkim</option><option value="1503">Tamil Nadu</option><option value="4231">Telangana</option><option value="1504">Tripura</option><option value="1505">Uttar Pradesh</option><option value="4232">Uttarakhand</option><option value="1506">West Bengal</option></select>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-sm-6 text-left">
                                            <button type="button" onclick="$(&#39;a[href=\&#39;#tab-payment\&#39;]&#39;).tab(&#39;show&#39;);" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</button>
                                          </div>
                                          <div class="col-sm-6 text-right">
                                               <input type="submit" name="submit" class="btn btn-primary" value="Submit" id="input-shipping-postcode">
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </form>
                                </div>

                                    <!-- <script type="text/javascript"><!--
                                        // Disable the tabs
                                        $('#order a[data-toggle=\'tab\']').on('click', function(e) {
                                          return false;
                                        });
                                        var token = '';
                                        // Login to the API
                                        $.ajax({
                                          url: $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/login',
                                          type: 'post',
                                          data: 'key=ImzqPVN8TJvrJ6Y4fUtDtLCdLWQ1o7OIoNhrRLO2vnkaaBYdsG5AEb8iGC2ALzlzGIz0x6quKyseWPhfqDOb8JOcUjXzLt8d3ilZwb3Ptv7xcu3HRjObjIobeP6U51qMQ2vdDYo8jWFMjUju2C74RxLZyow64OFR04rPsi6xcaU1SbHVhAX11DYymnyyAzcSfFvyhGKV6iBmOMQHKhgYcPJXFXplW5UzMUBiUsbFWlKBQsf6qHMuSfbs5ViaJi8y',
                                          dataType: 'json',
                                          crossDomain: true,
                                          success: function(json) {
                                                $('.alert').remove();

                                                if (json['error']) {
                                                if (json['error']['key']) {
                                                  $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                }

                                                    if (json['error']['ip']) {
                                                  $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="Loading..." class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> Add IP</button></div>');
                                                }
                                                }
                                            if (json['token']) {
                                              token = json['token'];
                                                    $('select[name=\'currency\']').trigger('change');
                                            }
                                          },
                                          error: function(xhr, ajaxOptions, thrownError) {
                                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                          }
                                        });

                                        // Currency
                                        $('select[name=\'currency\']').on('change', function() {
                                          $.ajax({
                                            url: $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/currency&token=' + token,
                                            type: 'post',
                                            data: 'currency=' + $('select[name=\'currency\'] option:selected').val(),
                                            dataType: 'json',
                                            crossDomain: true,
                                            beforeSend: function() {
                                              $('select[name=\'currency\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                                            },
                                            complete: function() {
                                              $('.fa-spin').remove();
                                            },
                                            success: function(json) {
                                              $('.alert, .text-danger').remove();
                                              $('.form-group').removeClass('has-error');

                                              if (json['error']) {
                                                $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                                                // Highlight any found errors
                                                $('select[name=\'currency\']').parent().parent().addClass('has-error');
                                              }
                                            },
                                            error: function(xhr, ajaxOptions, thrownError) {
                                              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                            }
                                          });
                                        });

                                        // Add all products to the cart using the api

                                        // Customer
                                        $('input[name=\'customer\']').autocomplete({
                                          'source': function(request, response) {
                                            $.ajax({
                                              url: 'index.php?route=customer/customer/autocomplete&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&filter_name=' +  encodeURIComponent(request),
                                              dataType: 'json',
                                              success: function(json) {
                                                json.unshift({
                                                  customer_id: '0',
                                                  customer_group_id: '6',
                                                  name: ' --- None --- ',
                                                  customer_group: '',
                                                  firstname: '',
                                                  lastname: '',
                                                  email: '',
                                                  telephone: '',
                                                  fax: '',
                                                  custom_field: [],
                                                  address: []
                                                });

                                                response($.map(json, function(item) {
                                                  return {
                                                    category: item['customer_group'],
                                                    label: item['name'],
                                                    value: item['customer_id'],
                                                    customer_group_id: item['customer_group_id'],
                                                    firstname: item['firstname'],
                                                    lastname: item['lastname'],
                                                    email: item['email'],
                                                    telephone: item['telephone'],
                                                    fax: item['fax'],
                                                    custom_field: item['custom_field'],
                                                    address: item['address']
                                                  }
                                                }));
                                              }
                                            });
                                          },
                                          'select': function(item) {
                                            // Reset all custom fields
                                            $('#tab-customer input[type=\'text\'], #tab-customer textarea').not('#tab-customer input[name=\'customer\'], #tab-customer input[name=\'customer_id\']').val('');
                                            $('#tab-customer select option').removeAttr('selected');
                                            $('#tab-customer input[type=\'checkbox\'], #tab-customer input[type=\'radio\']').removeAttr('checked');

                                            $('#tab-customer input[name=\'customer\']').val(item['label']);
                                            $('#tab-customer input[name=\'customer_id\']').val(item['value']);
                                            $('#tab-customer select[name=\'customer_group_id\']').val(item['customer_group_id']);
                                            $('#tab-customer input[name=\'firstname\']').val(item['firstname']);
                                            $('#tab-customer input[name=\'lastname\']').val(item['lastname']);
                                            $('#tab-customer input[name=\'email\']').val(item['email']);
                                            $('#tab-customer input[name=\'telephone\']').val(item['telephone']);
                                            $('#tab-customer input[name=\'fax\']').val(item['fax']);

                                            for (i in item.custom_field) {
                                              $('#tab-customer select[name=\'custom_field[' + i + ']\']').val(item.custom_field[i]);
                                              $('#tab-customer textarea[name=\'custom_field[' + i + ']\']').val(item.custom_field[i]);
                                              $('#tab-customer input[name^=\'custom_field[' + i + ']\'][type=\'text\']').val(item.custom_field[i]);
                                              $('#tab-customer input[name^=\'custom_field[' + i + ']\'][type=\'hidden\']').val(item.custom_field[i]);
                                              $('#tab-customer input[name^=\'custom_field[' + i + ']\'][type=\'radio\'][value=\'' + item.custom_field[i] + '\']').prop('checked', true);

                                              if (item.custom_field[i] instanceof Array) {
                                                for (j = 0; j < item.custom_field[i].length; j++) {
                                                  $('#tab-customer input[name^=\'custom_field[' + i + ']\'][type=\'checkbox\'][value=\'' + item.custom_field[i][j] + '\']').prop('checked', true);
                                                }
                                              }
                                            }

                                            $('select[name=\'customer_group_id\']').trigger('change');

                                            html = '<option value="0"> --- None --- </option>';

                                            for (i in  item['address']) {
                                              html += '<option value="' + item['address'][i]['address_id'] + '">' + item['address'][i]['firstname'] + ' ' + item['address'][i]['lastname'] + ', ' + item['address'][i]['address_1'] + ', ' + item['address'][i]['city'] + ', ' + item['address'][i]['country'] + '</option>';
                                            }

                                            $('select[name=\'payment_address\']').html(html);
                                            $('select[name=\'shipping_address\']').html(html);

                                            $('select[name=\'payment_address\']').trigger('change');
                                            $('select[name=\'shipping_address\']').trigger('change');
                                          }
                                        });




                                        $('#button-customer').on('click', function() {

                                          $.ajax({
                                            url: $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/customer&token=' + token,
                                            type: 'post',
                                            data: $('#tab-customer input[type=\'text\'], #tab-customer input[type=\'hidden\'], #tab-customer input[type=\'radio\']:checked, #tab-customer input[type=\'checkbox\']:checked, #tab-customer select, #tab-customer textarea'),
                                            dataType: 'json',
                                            crossDomain: true,
                                            beforeSend: function() {
                                              $('#button-customer').button('loading');
                                            },
                                            complete: function() {
                                               $('#button-customer').button('reset');
                                            },
                                            success: function(json) {

                                                   $('#button-refresh').trigger('click');
                                                   $('a[href=\'#tab-payment\']').tab('show');


                                              $('.alert, .text-danger').remove();
                                              $('.form-group').removeClass('has-error');

                                              if (json['error']) {
                                                if (json['error']['warning']) {
                                                  $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                }

                                                for (i in json['error']) {
                                                  var element = $('#input-' + i.replace('_', '-'));

                                                  if (element.parent().hasClass('input-group')) {
                                                              $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                                                  } else {
                                                    $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                                                  }
                                                }

                                                // Highlight any found errors
                                                   $('.text-danger').parentsUntil('.form-group').parent().addClass('has-error');
                                              }
                                            },
                                            error: function(xhr, ajaxOptions, thrownError) {
                                              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                            }
                                          });
                                        });






                                            // Payment Address
                                            $('select[name=\'payment_address\']').on('change', function() {
                                              $.ajax({
                                                url: 'index.php?route=customer/customer/address&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&address_id=' + this.value,
                                                dataType: 'json',
                                                beforeSend: function() {
                                                  $('select[name=\'payment_address\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                                                },
                                                complete: function() {
                                                  $('#tab-payment .fa-spin').remove();
                                                },
                                                success: function(json) {
                                                  // Reset all fields
                                                  $('#tab-payment input[type=\'text\'], #tab-payment input[type=\'text\'], #tab-payment textarea').val('');
                                                  $('#tab-payment select option').not('#tab-payment select[name=\'payment_address\']').removeAttr('selected');
                                                  $('#tab-payment input[type=\'checkbox\'], #tab-payment input[type=\'radio\']').removeAttr('checked');

                                                  $('#tab-payment input[name=\'firstname\']').val(json['firstname']);
                                                  $('#tab-payment input[name=\'lastname\']').val(json['lastname']);
                                                  $('#tab-payment input[name=\'company\']').val(json['company']);
                                                  $('#tab-payment input[name=\'address_1\']').val(json['address_1']);
                                                  $('#tab-payment input[name=\'address_2\']').val(json['address_2']);
                                                  $('#tab-payment input[name=\'city\']').val(json['city']);
                                                  $('#tab-payment input[name=\'postcode\']').val(json['postcode']);
                                                  $('#tab-payment select[name=\'country_id\']').val(json['country_id']);

                                                  payment_zone_id = json['zone_id'];

                                                  for (i in json['custom_field']) {
                                                    $('#tab-payment select[name=\'custom_field[' + i + ']\']').val(json['custom_field'][i]);
                                                    $('#tab-payment textarea[name=\'custom_field[' + i + ']\']').val(json['custom_field'][i]);
                                                    $('#tab-payment input[name^=\'custom_field[' + i + ']\'][type=\'text\']').val(json['custom_field'][i]);
                                                    $('#tab-payment input[name^=\'custom_field[' + i + ']\'][type=\'hidden\']').val(json['custom_field'][i]);
                                                    $('#tab-payment input[name^=\'custom_field[' + i + ']\'][type=\'radio\'][value=\'' + json['custom_field'][i] + '\']').prop('checked', true);
                                                    $('#tab-payment input[name^=\'custom_field[' + i + ']\'][type=\'checkbox\'][value=\'' + json['custom_field'][i] + '\']').prop('checked', true);

                                                    if (json['custom_field'][i] instanceof Array) {
                                                      for (j = 0; j < json['custom_field'][i].length; j++) {
                                                        $('#tab-payment input[name^=\'custom_field[' + i + ']\'][type=\'checkbox\'][value=\'' + json['custom_field'][i][j] + '\']').prop('checked', true);
                                                      }
                                                    }
                                                  }

                                                  $('#tab-payment select[name=\'country_id\']').trigger('change');
                                                },
                                                error: function(xhr, ajaxOptions, thrownError) {
                                                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                }
                                              });
                                            });

                                            var payment_zone_id = '1492';

                                            $('#tab-payment select[name=\'country_id\']').on('change', function() {
                                              $.ajax({
                                                url: 'index.php?route=localisation/country/country&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&country_id=' + this.value,
                                                dataType: 'json',
                                                beforeSend: function() {
                                                  $('#tab-payment select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                                                },
                                                complete: function() {
                                                  $('#tab-payment .fa-spin').remove();
                                                },
                                                success: function(json) {
                                                  if (json['postcode_required'] == '1') {
                                                    $('#tab-payment input[name=\'postcode\']').parent().parent().addClass('required');
                                                  } else {
                                                    $('#tab-payment input[name=\'postcode\']').parent().parent().removeClass('required');
                                                  }

                                                  html = '<option value=""> --- Please Select --- </option>';

                                                  if (json['zone'] && json['zone'] != '') {
                                                    for (i = 0; i < json['zone'].length; i++) {
                                                          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                                                      if (json['zone'][i]['zone_id'] == payment_zone_id) {
                                                            html += ' selected="selected"';
                                                        }

                                                        html += '>' + json['zone'][i]['name'] + '</option>';
                                                    }
                                                  } else {
                                                    html += '<option value="0" selected="selected"> --- None --- </option>';
                                                  }

                                                  $('#tab-payment select[name=\'zone_id\']').html(html);
                                                },
                                                error: function(xhr, ajaxOptions, thrownError) {
                                                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                }
                                              });
                                            });

                                            $('#tab-payment select[name=\'country_id\']').trigger('change');

                                            $('#button-payment-address').on('click', function() {
                                              $.ajax({
                                                url: $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/payment/address&token=' + token,
                                                type: 'post',
                                                data: $('#tab-payment input[type=\'text\'], #tab-payment input[type=\'hidden\'], #tab-payment input[type=\'radio\']:checked, #tab-payment input[type=\'checkbox\']:checked, #tab-payment select, #tab-payment textarea'),
                                                dataType: 'json',
                                                crossDomain: true,
                                                beforeSend: function() {
                                                  $('#button-payment-address').button('loading');
                                                },
                                                complete: function() {
                                                  $('#button-payment-address').button('reset');
                                                },
                                                success: function(json) {

                                                  $('.alert, .text-danger').remove();
                                                  $('.form-group').removeClass('has-error');

                                                  // Check for errors
                                                  if (json['error']) {
                                                    if (json['error']['warning']) {
                                                      $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                    }

                                                    for (i in json['error']) {
                                                      var element = $('#input-payment-' + i.replace('_', '-'));

                                                      if ($(element).parent().hasClass('input-group')) {
                                                        $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                                                      } else {
                                                        $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                                                      }
                                                    }

                                                    // Highlight any found errors
                                                    $('.text-danger').parentsUntil('.form-group').parent().addClass('has-error');
                                                  } else {
                                                    // Payment Methods
                                                    $.ajax({
                                                      url: $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/payment/methods&token=' + token,
                                                      dataType: 'json',
                                                      crossDomain: true,
                                                      beforeSend: function() {
                                                        $('#button-payment-address i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                                                        $('#button-payment-address').prop('disabled', true);
                                                      },
                                                      complete: function() {
                                                        $('#button-payment-address i').replaceWith('<i class="fa fa-arrow-right"></i>');
                                                        $('#button-payment-address').prop('disabled', false);
                                                      },
                                                      success: function(json) {
                                                        if (json['error']) {
                                                          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                        } else {
                                                          html = '<option value=""> --- Please Select --- </option>';

                                                          if (json['payment_methods']) {
                                                            for (i in json['payment_methods']) {
                                                              if (json['payment_methods'][i]['code'] == $('select[name=\'payment_method\'] option:selected').val()) {
                                                                html += '<option value="' + json['payment_methods'][i]['code'] + '" selected="selected">' + json['payment_methods'][i]['title'] + '</option>';
                                                              } else {
                                                                html += '<option value="' + json['payment_methods'][i]['code'] + '">' + json['payment_methods'][i]['title'] + '</option>';
                                                              }
                                                            }
                                                          }

                                                          $('select[name=\'payment_method\']').html(html);
                                                        }
                                                      },
                                                      error: function(xhr, ajaxOptions, thrownError) {
                                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                      }
                                                    }).done(function() {
                                                                // Refresh products, vouchers and totals
                                                        $('#button-refresh').trigger('click');

                                                        // If shipping required got to shipping tab else total tabs
                                                        if ($('select[name=\'shipping_method\']').prop('disabled')) {
                                                          $('a[href=\'#tab-total\']').tab('show');
                                                        } else {
                                                          $('a[href=\'#tab-shipping\']').tab('show');
                                                        }
                                                            });
                                                  }
                                                },
                                                error: function(xhr, ajaxOptions, thrownError) {
                                                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                }
                                              });
                                            });

                                            // Shipping Address
                                            $('select[name=\'shipping_address\']').on('change', function() {
                                              $.ajax({
                                                url: 'index.php?route=customer/customer/address&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&address_id=' + this.value,
                                                dataType: 'json',
                                                beforeSend: function() {
                                                  $('select[name=\'shipping_address\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                                                },
                                                complete: function() {
                                                  $('#tab-shipping .fa-spin').remove();
                                                },
                                                success: function(json) {
                                                  // Reset all fields
                                                  $('#tab-shipping input[type=\'text\'], #tab-shipping input[type=\'text\'], #tab-shipping textarea').val('');
                                                  $('#tab-shipping select option').not('#tab-shipping select[name=\'shipping_address\']').removeAttr('selected');
                                                  $('#tab-shipping input[type=\'checkbox\'], #tab-shipping input[type=\'radio\']').removeAttr('checked');

                                                  $('#tab-shipping input[name=\'firstname\']').val(json['firstname']);
                                                  $('#tab-shipping input[name=\'lastname\']').val(json['lastname']);
                                                  $('#tab-shipping input[name=\'company\']').val(json['company']);
                                                  $('#tab-shipping input[name=\'address_1\']').val(json['address_1']);
                                                  $('#tab-shipping input[name=\'address_2\']').val(json['address_2']);
                                                  $('#tab-shipping input[name=\'city\']').val(json['city']);
                                                  $('#tab-shipping input[name=\'postcode\']').val(json['postcode']);
                                                  $('#tab-shipping select[name=\'country_id\']').val(json['country_id']);

                                                  shipping_zone_id = json['zone_id'];

                                                  for (i in json['custom_field']) {
                                                    $('#tab-shipping select[name=\'custom_field[' + i + ']\']').val(json['custom_field'][i]);
                                                    $('#tab-shipping textarea[name=\'custom_field[' + i + ']\']').val(json['custom_field'][i]);
                                                    $('#tab-shipping input[name^=\'custom_field[' + i + ']\'][type=\'text\']').val(json['custom_field'][i]);
                                                    $('#tab-shipping input[name^=\'custom_field[' + i + ']\'][type=\'hidden\']').val(json['custom_field'][i]);
                                                    $('#tab-shipping input[name^=\'custom_field[' + i + ']\'][type=\'radio\'][value=\'' + json['custom_field'][i] + '\']').prop('checked', true);
                                                    $('#tab-shipping input[name^=\'custom_field[' + i + ']\'][type=\'checkbox\'][value=\'' + json['custom_field'][i] + '\']').prop('checked', true);

                                                    if (json['custom_field'][i] instanceof Array) {
                                                      for (j = 0; j < json['custom_field'][i].length; j++) {
                                                        $('#tab-shipping input[name^=\'custom_field[' + i + ']\'][type=\'checkbox\'][value=\'' + json['custom_field'][i][j] + '\']').prop('checked', true);
                                                      }
                                                    }
                                                  }

                                                  $('#tab-shipping select[name=\'country_id\']').trigger('change');
                                                },
                                                error: function(xhr, ajaxOptions, thrownError) {
                                                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                }
                                              });
                                            });

                                            var shipping_zone_id = '1492';

                                            $('#tab-shipping select[name=\'country_id\']').on('change', function() {
                                              $.ajax({
                                                url: 'index.php?route=localisation/country/country&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&country_id=' + this.value,
                                                dataType: 'json',
                                                beforeSend: function() {
                                                  $('#tab-shipping select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                                                },
                                                complete: function() {
                                                  $('#tab-shipping .fa-spin').remove();
                                                },
                                                success: function(json) {
                                                  if (json['postcode_required'] == '1') {
                                                    $('#tab-shipping input[name=\'postcode\']').parent().parent().addClass('required');
                                                  } else {
                                                    $('#tab-shipping input[name=\'postcode\']').parent().parent().removeClass('required');
                                                  }

                                                  html = '<option value=""> --- Please Select --- </option>';

                                                  if (json['zone'] && json['zone'] != '') {
                                                    for (i = 0; i < json['zone'].length; i++) {
                                                          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                                                      if (json['zone'][i]['zone_id'] == shipping_zone_id) {
                                                            html += ' selected="selected"';
                                                        }

                                                        html += '>' + json['zone'][i]['name'] + '</option>';
                                                    }
                                                  } else {
                                                    html += '<option value="0" selected="selected"> --- None --- </option>';
                                                  }

                                                  $('#tab-shipping select[name=\'zone_id\']').html(html);
                                                },
                                                error: function(xhr, ajaxOptions, thrownError) {
                                                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                }
                                              });
                                            });

                                            $('#tab-shipping select[name=\'country_id\']').trigger('change');

                                            $('#button-shipping-address').on('click', function() {
                                              $.ajax({
                                                url: $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/shipping/address&token=' + token,
                                                type: 'post',
                                                data: $('#tab-shipping input[type=\'text\'], #tab-shipping input[type=\'hidden\'], #tab-shipping input[type=\'radio\']:checked, #tab-shipping input[type=\'checkbox\']:checked, #tab-shipping select, #tab-shipping textarea'),
                                                dataType: 'json',
                                                crossDomain: true,
                                                beforeSend: function() {
                                                  $('#button-shipping-address').button('loading');
                                                },
                                                complete: function() {
                                                  $('#button-shipping-address').button('reset');
                                                },
                                                success: function(json) {
                                                  $('.alert, .text-danger').remove();
                                                  $('.form-group').removeClass('has-error');

                                                  // Check for errors
                                                  if (json['error']) {
                                                    if (json['error']['warning']) {
                                                      $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                    }

                                                    for (i in json['error']) {
                                                      var element = $('#input-shipping-' + i.replace('_', '-'));

                                                      if ($(element).parent().hasClass('input-group')) {
                                                        $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                                                      } else {
                                                        $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                                                      }
                                                    }

                                                    // Highlight any found errors
                                                    $('.text-danger').parentsUntil('.form-group').parent().addClass('has-error');
                                                  } else {
                                                    // Shipping Methods
                                                    var request = $.ajax({
                                                      url: $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/shipping/methods&token=' + token,
                                                      dataType: 'json',
                                                      beforeSend: function() {
                                                        $('#button-shipping-address i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                                                        $('#button-shipping-address').prop('disabled', true);
                                                      },
                                                      complete: function() {
                                                        $('#button-shipping-address i').replaceWith('<i class="fa fa-arrow-right"></i>');
                                                        $('#button-shipping-address').prop('disabled', false);
                                                      },
                                                      success: function(json) {
                                                        if (json['error']) {
                                                          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                        }
                                                      },
                                                      error: function(xhr, ajaxOptions, thrownError) {
                                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                      }
                                                    }).done(function() {
                                                        // Refresh products, vouchers and totals
                                                        $('#button-refresh').trigger('click');

                                                                $('a[href=\'#tab-total\']').tab('show');
                                                            });
                                                  }
                                                },
                                                error: function(xhr, ajaxOptions, thrownError) {
                                                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                }
                                              });
                                            });

                                            // Checkout
                                            $('#button-save').on('click', function() {
                                              if ($('input[name=\'order_id\']').val() == 0) {
                                                var url = $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/order/add&token=' + token;
                                              } else {
                                                var url = $('select[name=\'store\'] option:selected').val() + 'index.php?route=api/order/edit&token=' + token + '&order_id=' + $('input[name=\'order_id\']').val();
                                              }

                                              $.ajax({
                                                url: url,
                                                type: 'post',
                                                //data: $('select[name=\'payment_method\'] option:selected,  select[name=\'shipping_method\'] option:selected'),
                                                dataType: 'json',
                                                crossDomain: true,
                                                beforeSend: function() {
                                                  $('#button-save').button('loading');
                                                },
                                                complete: function() {
                                                  $('#button-save').button('reset');
                                                },
                                                success: function(json) {
                                                  $('.alert, .text-danger').remove();

                                                  if (json['error']) {
                                                    $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                  }

                                                  if (json['success']) {
                                                    $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '  <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                                    // Refresh products, vouchers and totals
                                                    $('#button-refresh').trigger('click');
                                                    }

                                                  if (json['order_id']) {
                                                    $('input[name=\'order_id\']').val(json['order_id']);
                                                  }
                                                },
                                                error: function(xhr, ajaxOptions, thrownError) {
                                                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                }
                                              });
                                            });


                                     </script> -->



                                        </div>



                                      </div>
                                 </div>
                             </div>
 <!-- POPUP Change Order Address Element to pop up  END HERE-->

<!-- ...........................WASIM UPDATE ADDRESS HERE END HERE................................. -->



<!-- Ajax PopUP; -->
  <script type="text/javascript"><!--



$(document).delegate('#button-invoice', 'click', function() {
  $.ajax({
    url: 'index.php?route=logistics/order/createinvoiceno&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589',
    dataType: 'json',
    beforeSend: function() {
      $('#button-invoice').button('loading');
    },
    complete: function() {
      $('#button-invoice').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['invoice_no']) {
        $('#invoice').html(json['invoice_no']);

        $('#button-invoice').replaceWith('<button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-reward-add', 'click', function() {
  $.ajax({
    url: 'index.php?route=logistics/order/addreward&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589',
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('#button-reward-add').button('loading');
    },
    complete: function() {
      $('#button-reward-add').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('#button-reward-add').replaceWith('<button id="button-reward-remove" data-toggle="tooltip" title="Remove Reward Points" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-reward-remove', 'click', function() {
  $.ajax({
    url: 'index.php?route=logistics/order/removereward&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589',
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('#button-reward-remove').button('loading');
    },
    complete: function() {
      $('#button-reward-remove').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('#button-reward-remove').replaceWith('<button id="button-reward-add" data-toggle="tooltip" title="Add Reward Points" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-commission-add', 'click', function() {
  $.ajax({
    url: 'index.php?route=logistics/order/addcommission&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589',
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('#button-commission-add').button('loading');
    },
    complete: function() {
      $('#button-commission-add').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('#button-commission-add').replaceWith('<button id="button-commission-remove" data-toggle="tooltip" title="Remove Commission" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$(document).delegate('#button-commission-remove', 'click', function() {
  $.ajax({
    url: 'index.php?route=logistics/order/removecommission&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589',
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('#button-commission-remove').button('loading');
    },
    complete: function() {
      $('#button-commission-remove').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
      }

      if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        $('#button-commission-remove').replaceWith('<button id="button-commission-add" data-toggle="tooltip" title="Add Commission" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});


/*.......Changes Code Start Here By Wasim..............*/

$('#button-api-history').on('click', function() {

    var selectedVal = "";
    var selected = $(".request-a-call-form #aip_company_name label input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
    } else {
       selectedVal = selected.val();
    }

   if(selectedVal==0){
         var company = $('#courier_name').val();
         var awbno = $('#courier_awb').val();
         var urls='index.php?route=logistics/order/addshippingapi&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589&api_company_id='+selectedVal+'&courier_company='+company+'&awbno='+awbno;
   } else  if(selectedVal==13){
         var awbno = $('#india_post_courier_awb').val();
         var urls='index.php?route=logistics/order/addshippingapi&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589&api_company_id='+selectedVal+'&indpostawbno='+awbno;
   }  else {
      var urls='index.php?route=logistics/order/addshippingapi&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589&api_company_id='+selectedVal;
   }

   // alert(urls);


  $.ajax({
    url: urls,
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('#button-api-history').button('loading');
    },
    complete: function() {
      $('#button-api-history').button('reset');
    },
    success: function(json) {
     if (json['success']) {
         $('.b-close').click();
           window.location.reload();
      }
    }
  });
});

/*.......Changes Code End Here By Wasim..............*/




$('#history').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

  $('#history').load(this.href);
});

$('#history').load('index.php?route=logistics/order/history&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589');


$('#button-history').on('click', function() {
  if (typeof verifyStatusChange == 'function'){
    if (verifyStatusChange() == false){
      return false;
    } else{
      addOrderInfo();
    }
  } else{
    addOrderInfo();
  }




  $.ajax({
    url: 'http://asianherbs.in/salesadminindex.php?route=api/order/history&token=' + token + '&order_id=99998000562589',
    type: 'post',
    dataType: 'json',
    data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment='  + encodeURIComponent($('input[name=\'user-name\']').val())+'->'+ encodeURIComponent($('textarea[name=\'comment\']').val()),
    beforeSend: function() {
      $('#button-history').button('loading');
    },
    complete: function() {
      $('#button-history').button('reset');
    },
    success: function(json) {
     ///alert('order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()));

      $('.alert').remove();

      if (json['error']) {

        $('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }

      if (json['success']) {
        $('#history').load('index.php?route=sale/order/history&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589');

        $('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        $('textarea[name=\'comment\']').val('');
        $('.b-close').click();
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});





function changeStatus(){
  var status_id = $('select[name="order_status_id"]').val();

  $('#openbay-info').remove();

  $.ajax({
    url: 'index.php?route=extension/openbay/getorderinfo&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589&status_id=' + status_id,
    dataType: 'html',
    success: function(html) {
      $('#history').after(html);
    }
  });
}

function addOrderInfo(){
  var status_id = $('select[name="order_status_id"]').val();

  $.ajax({
    url: 'index.php?route=extension/openbay/addorderinfo&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id=99998000562589&status_id=' + status_id,
    type: 'post',
    dataType: 'html',
    data: $(".openbay-data").serialize()
  });
}

$(document).ready(function() {
  changeStatus();
});

$('select[name="order_status_id"]').change(function(){
  changeStatus();
});
//--></script>

<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Orders_files/classie.js.download" type="text/javascript"></script>
<script src="./Orders_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE -->
</section>
      <!--  ORDER DETAILS END HERE -->


      </div>
    </div>
  </div>


<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
  url = 'index.php?route=logistics/order&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

  var filter_order_id = $('input[name=\'filter_order_id\']').val();

  if (filter_order_id) {
    url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
  }

  var filter_customer = $('input[name=\'filter_customer\']').val();

  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }

 //CHANGES CODE HERE//
  var filter_mobile = $('input[name=\'filter_mobile\']').val();

  if (filter_mobile) {
    url += '&filter_mobile=' + encodeURIComponent(filter_mobile);
  }
  var filter_state = $('input[name=\'filter_state\']').val();

  if (filter_state) {
    url += '&filter_state=' + encodeURIComponent(filter_state);
  }
  var filter_city = $('input[name=\'filter_city\']').val();

  if (filter_city) {
    url += '&filter_city=' + encodeURIComponent(filter_city);
  }


   var filter_to_date = $('input[name=\'filter_to_date\']').val();

  if (filter_to_date) {
    url += '&filter_to_date=' + encodeURIComponent(filter_to_date);
  }

   var filter_fron_date = $('input[name=\'filter_fron_date\']').val();

  if (filter_fron_date) {
    url += '&filter_from_date=' + encodeURIComponent(filter_fron_date);
  }


  var filter_api_name = $('input[name=\'filter_api_name\']').val();

  if (filter_api_name) {
    url += '&filter_api_name=' + encodeURIComponent(filter_api_name);
  }

  var filter_awb_number = $('input[name=\'filter_awb_number\']').val();

  if (filter_awb_number) {
    url += '&filter_awb_number=' + encodeURIComponent(filter_awb_number);
  }
  //CHANGES CODE HERE//

  var filter_order_status = $('select[name=\'filter_order_status\']').val();

  if (filter_order_status != '*') {
    url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
  }

  var filter_total = $('input[name=\'filter_total\']').val();

  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_date_added = $('input[name=\'filter_date_added\']').val();

  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_shipement_date = $('input[name=\'filter_shipement_date\']').val();

  if (filter_shipement_date) {
    url += '&filter_shipement_date=' + encodeURIComponent(filter_shipement_date);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  var filter_payment_mode = $('select[name=\'filter_payment_mode\']').val();

  if (filter_payment_mode != '*') {
    url += '&filter_payment_mode=' + encodeURIComponent(filter_payment_mode);
  }

  location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_customer\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=customer/customer/autocomplete&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['customer_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_customer\']').val(item['label']);
  }
});
//--></script>




<script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
  $('#button-shipping, #button-invoice, #button-label, #button-manifast, #button-generate-pickup').prop('disabled', true);

  var selected = $('input[name^=\'selected\']:checked');

  if (selected.length) {
    $('#button-invoice').prop('disabled', false);
  }

  if (selected.length) {
    $('#button-generate-pickup').prop('disabled', false);
  }

  if (selected.length) {
    $('#button-manifast').prop('disabled', false);
  }

  if (selected.length) {
    $('#button-label').prop('disabled', false);
  }

  if (selected.length) {
    $('#button-shipping').prop('disabled', false);
  }

  /*for (i = 0; i < selected.length; i++) {
    if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
      $('#button-shipping').prop('disabled', false);
      break;
    }
  }*/
});

$('input[name^=\'selected\']:first').trigger('change');

// Login to the API
var token = '';

$.ajax({
  url: 'http://asianherbs.in/salesadminindex.php?route=api/login',
  type: 'post',
  data: 'key=ImzqPVN8TJvrJ6Y4fUtDtLCdLWQ1o7OIoNhrRLO2vnkaaBYdsG5AEb8iGC2ALzlzGIz0x6quKyseWPhfqDOb8JOcUjXzLt8d3ilZwb3Ptv7xcu3HRjObjIobeP6U51qMQ2vdDYo8jWFMjUju2C74RxLZyow64OFR04rPsi6xcaU1SbHVhAX11DYymnyyAzcSfFvyhGKV6iBmOMQHKhgYcPJXFXplW5UzMUBiUsbFWlKBQsf6qHMuSfbs5ViaJi8y',
  dataType: 'json',
  crossDomain: true,
  success: function(json) {
        $('.alert').remove();

        if (json['error']) {
        if (json['error']['key']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

            if (json['error']['ip']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="Loading..." class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> Add IP</button></div>');
        }
        }

    if (json['token']) {
      token = json['token'];
    }
  },
  error: function(xhr, ajaxOptions, thrownError) {
    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  }
});

$(document).delegate('#button-ip-add', 'click', function() {
  $.ajax({
    url: 'index.php?route=user/api/addip&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&api_id=2',
    type: 'post',
    data: 'ip=157.49.237.147',
    dataType: 'json',
    beforeSend: function() {
      $('#button-ip-add').button('loading');
    },
    complete: function() {
      $('#button-ip-add').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }

      if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('button[id^=\'button-delete\']').on('click', function(e) {
  if (confirm('Are you sure?')) {
    var node = this;

    $.ajax({
      url: 'http://asianherbs.in/salesadminindex.php?route=api/order/delete&token=' + token + '&order_id=' + $(node).val(),
      dataType: 'json',
      crossDomain: true,
      beforeSend: function() {
        $(node).button('loading');
      },
      complete: function() {
        $(node).button('reset');
      },
      success: function(json) {
        $('.alert').remove();

        if (json['error']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['success']) {
          $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
});
//--></script>
  <script src="./Orders_files/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
  <link href="./Orders_files/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>


<!-- ..........................................WASIM JS CODE CHANGES START HERE ...........................................-->
<script type="text/javascript">
$(document).ready(function(){
    $('#orders-list tbody tr:first').addClass('active');
  if($('#orders-list tbody tr:first').attr('data-id')){
    $('#orderno').text('#'+$('#orders-list tbody tr:first').attr('data-id')+' - '+$('#orders-list tbody tr:first span.order-status').text());

    $.ajax({
        type  : "GET",
        url   : 'index.php?route=logistics/order/info&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+$('#orders-list tbody tr:first').attr('data-id'),
        beforeSend: function() {
                $('.alert, .success, .warning').remove();
                $('#order_loading').show();
              },
        complete: function() {
                $('#order_loading').hide();
              },
        success: function(html) {
                $('#order_shortinfo').html(html);
              },
      });

  } else {

    $('#orderno').text('#');
    $('#order_shortinfo').html('<div class="alert alert-error fade in"><strong>Oh no!</strong> No order is selected. </div>');

  }

    $('#orders-list tr td, #orders-list tr td, #orders-list tr td').click(function(){

     // var checkbox=$(this).parents('tr');

    $('#orderno').text('#'+$(this).parents('tr').attr('data-id')+' - '+$(this).parents('tr').find('.order-status').text());
    $('#orders-list tr').removeClass('active');
    $(this).parents('tr').addClass('active');

    $('html, body').animate({scrollTop:400}, 'slow');


    $.ajax({
      type  : "GET",
      url   : 'index.php?route=logistics/order/info&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+$(this).parents('tr').attr('data-id'),
      beforeSend: function() {
              $('.alert, .success, .warning').remove();
              $('#order_loading').show();
            },
      complete: function() {
              $('#order_loading').hide();
            },
      success: function(html) {
              $('#order_shortinfo').html(html);
            },
    });

  });
});
</script>
<!-- ..........................................WASIM JS CODE CHANGES END HERE ...........................................-->







  <script src="./Orders_files/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
  <link href="./Orders_files/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>
</div>
<?php include ('../includes/footer.php') ?>
</div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Orders_files/classie.js.download" type="text/javascript"></script>
<script src="./Orders_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
