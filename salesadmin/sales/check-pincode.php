<!DOCTYPE html>
<!-- saved from url=(0113)http://asianherbs.in/salesadminadmin/index.php?route=sale/order1/pincodetracks&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Orders</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true"  style="margin-top:5%;background:#f0f3fa;">
<div id="container">
  <?php include ('../includes/head.php'); ?>
  <?php include ('../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1 style="color:black;">Pincode Tracks</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=sale/order1/pincodetracks&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Pincode Tracks</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Find Pincode List</h3>
      </div>

<style type="text/css">
.form-bnt1{ margin-top: 20px;}
.form1{ padding: 10px 0px;}
</style>


      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
           <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label" for="input-pincode">Checked Pincode</label>
                  <input type="text" name="filter_pincode" minlength="6" maxlength="6" onkeypress="return checkZipcodeIt(event)" value="" placeholder="Enter Zip/Postal Code" id="input-pincode" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                  <div class="form-group form-bnt1">
                  <label class="control-label">&nbsp;</label>
                 <a href="http://asianherbs.in/salesadminadmin/index.php?route=sale/order1/pincodetracks&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="padding: 8px;" class="btn btn-alt btn-warning"><i class="fa fa-refresh"></i> Refresh</a>
                  <div class="pull-left"> <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Checked</button>
                    </div>
                 </div>

            </div>
          </div>
        </div>

       <!--  <div class="row">
          <div class="col-sm-6 text-left"></div>

          <div class="col-sm-6 text-right"></div>
        </div><br/> -->


        <form method="post" enctype="multipart/form-data" id="form-order">
          <!-- <div class="table-responsive" style="overflow-y: scroll; max-height: 840px;"> -->
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-fixed-header">
              <thead>
                <tr>
                  <td class="text-left"><a>Sin No.</a></td>
                  <td class="text-left"><a>Curier Name</a></td>
                  <td class="text-left"><a>Pincode</a></td>
                  <td class="text-left"><a>Hub City</a></td>

                  <td class="text-left"><a>Hub State</a></td>
                  <td class="text-center"><a>COD Status</a></td>
                  <td class="text-center"><a>Prepaid Status</a></td>
                  <td class="text-center"><a>Active Status</a></td>

                </tr>
              </thead>
              <tbody>
             			                                  <tr>
                  <td class="text-center" colspan="14">No results!</td>
                </tr>
                              </tbody>
            </table>

          </div>
        </form>
        <!-- <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right"></div>
        </div> -->
      </div>
    </div>
  </div>




<!-- WASIM POPUP OF DUBLICATE DATA START HERE -->
<script type="text/javascript">


function checkZipcodeIt(evt) {
  evt = (evt) ? evt : window.event

  var charCode = (evt.which) ? evt.which : evt.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
     status = "This field accepts numbers only."
     return false
  }
  status = ""
  return true
}




//postCode field


function validatePostcode(postCode) {

  var filter = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
  var zips=['12345','123456', '23456', '34567', '45678', '56789', '67890'];
  if($.inArray(postCode ,zips)==-1){
      return false
  } else {
     return true;
  }
}

function validatePostcodellength(postCode) {
var length = $.trim(postCode).length;
  if(length > 6 || length < 6)
    return true;
  else
    return false;
}



$('#button-filter').on('click', function() {

  url = 'index.php?route=sale/order1/pincodetracks&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

  var filter_pincode = $('input[name=\'filter_pincode\']').val();


        if(filter_pincode == '')
        {
        //    e.preventDefault();
            alert("Please fill Zip/Pincode");
            document.getElementById('input-pincode').focus();
        }

        //postcode field use only numeric value

        else if (validatePostcode(filter_pincode))
        {
        alert('Invalid Zip/Pincode format');
      //  e.preventDefault();
         document.getElementById('input-pincode').focus();

        }

        // postcode filed length too short

        else if (validatePostcodellength(filter_pincode))
        {
        alert('Enter correct Zip/Pincode length');
      //  e.preventDefault();
         document.getElementById('input-pincode').focus();

        } else {

            if (filter_pincode) {
              url += '&filter_pincode=' + encodeURIComponent(filter_pincode);
            }
            var filter_mobile = $('input[name=\'filter_mobile\']').val();

            if (filter_mobile) {
              url += '&filter_mobile=' + encodeURIComponent(filter_mobile);
            }

            location = url;
      }
});
//--></script>

</div>
<?php include ('../includes/footer.php'); ?>
</div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Check Pincode_files/classie.js.download" type="text/javascript"></script>
<script src="./Check Pincode_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE -->
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
