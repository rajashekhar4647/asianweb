<!DOCTYPE html>
<!-- saved from url=(0101)http://asianherbs.in/salesadminadmin/index.php?route=report/report&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Sales Report</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script type="text/javascript" src="../../js/jquery-2.1.1.min.js.download"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js.download"></script>
<link href="../../css/bootstrap.css" type="text/css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" type="text/css" rel="stylesheet">
<link href="../../css/summernote.css" rel="stylesheet">
<script type="text/javascript" src="../../js/summernote.js.download"></script>
<script src="../../js/moment.js.download" type="text/javascript"></script>
<script src="../../js/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
<link href="../../css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/stylesheet.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/popup.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/style-popup.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/validationEngine.jquery.css" type="text/css">
<script src="../../js/common.js.download" type="text/javascript"></script>

<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/lobibox.min.css">
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<script src="../../js/lobibox.js.download"></script>
<script src="../../js/jquery.validationEngine-en.js.download" type="text/javascript" charset="utf-8"></script>
<script src="../../js/jquery.validationEngine.js.download" type="text/javascript" charset="utf-8"></script>
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../../includes/head.php'); ?>
<?php include ('../../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
             <h1 style="color:black;">Sales Report</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/sale_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Sales Report</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> Sales List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start">Date Start</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="2019-11-01" placeholder="Date Start" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end">Date End</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="2019-11-04" placeholder="Date End" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-group">Group By</label>
                <select name="filter_group" id="input-group" class="form-control">
                                                      <option value="year">Years</option>
                                                                        <option value="month">Months</option>
                                                                        <option value="week" selected="selected">Weeks</option>
                                                                        <option value="day">Days</option>
                                                    </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status">Order Status</label>
                <select name="filter_order_status_id" id="input-status" class="form-control">
                  <option></option>
                                                      <option value="41">Address Verified</option>
                                                                        <option value="35">Aproval Pending</option>
                                                                        <option value="7">Canceled</option>
                                                                        <option value="9">Canceled Reversal</option>
                                                                        <option value="17">COD &nbsp;Confirmed</option>
                                                                        <option value="37">Completed</option>
                                                                        <option value="29">Converted Lead</option>
                                                                        <option value="8">Customer Refuse To Accept</option>
                                                                        <option value="22">Delivered</option>
                                                                        <option value="36">Disapproved</option>
                                                                        <option value="30">Duplicate Order</option>
                                                                        <option value="40">Entry Blocked - Price Issue</option>
                                                                        <option value="10">Failed</option>
                                                                        <option value="39">Incomplete Address</option>
                                                                        <option value="28">Lead</option>
                                                                        <option value="23">Miss Route</option>
                                                                        <option value="27">Non - serviceable area</option>
                                                                        <option value="24">ODA- Out Of Delivery Area</option>
                                                                        <option value="34">On hold</option>
                                                                        <option value="38">Order Assigned</option>
                                                                        <option value="42">Payment Gateway Entry</option>
                                                                        <option value="1">Pending</option>
                                                                        <option value="18">Pick Up Generated</option>
                                                                        <option value="19">Prepaid</option>
                                                                        <option value="15">Processed</option>
                                                                        <option value="2">Processing</option>
                                                                        <option value="26">Re Order - dont use it</option>
                                                                        <option value="20">Ready for Dispatch</option>
                                                                        <option value="11">Refunded</option>
                                                                        <option value="21">Reschedule</option>
                                                                        <option value="12">Reversed</option>
                                                                        <option value="14">RTO-Returned To Origin</option>
                                                                        <option value="43">Shipment Damaged</option>
                                                                        <option value="13">Shipment Lost</option>
                                                                        <option value="3">Shipped</option>
                                                                        <option value="33">UnConfirmed</option>
                                                                        <option value="5">Undelivered</option>
                                                                        <option value="16">Voided</option>
                                                                        <option value="25">Wrong Delivery</option>
                                                    </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
              <button type="button" id="button-export" class="btn btn-warning pull-right" style="margin-right: 10px;"><i class="fa fa-search"></i> Export</button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">Date Start</td>
                <td class="text-left">Date End</td>
                <td class="text-right">No. Orders</td>
                <td class="text-right">No. Products</td>
                <td class="text-right">Tax</td>
                <td class="text-right">Total</td>
              </tr>
            </thead>
            <tbody>
                                          <tr>
                <td class="text-left">03/11/2019</td>
                <td class="text-left">04/11/2019</td>
                <td class="text-right">9</td>
                <td class="text-right">10</td>
                <td class="text-right">Rs. 0</td>
                <td class="text-right">Rs. 34,013</td>
              </tr>
                            <tr>
                <td class="text-left">01/11/2019</td>
                <td class="text-left">02/11/2019</td>
                <td class="text-right">5</td>
                <td class="text-right">5</td>
                <td class="text-right">Rs. 0</td>
                <td class="text-right">Rs. 11,250</td>
              </tr>
                                        </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right">Showing 1 to 2 of 2 (1 Pages)</div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/report&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	var filter_group = $('select[name=\'filter_group\']').val();

	if (filter_group) {
		url += '&filter_group=' + encodeURIComponent(filter_group);
	}

	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').val();

	if (filter_order_status_id != 0) {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}

	location = url;
});




$('#button-export').on('click', function() {

  url = 'index.php?route=report/report/reportdowmload&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  var filter_group = $('select[name=\'filter_group\']').val();

  if (filter_group) {
    url += '&filter_group=' + encodeURIComponent(filter_group);
  }

  var filter_order_status_id = $('select[name=\'filter_order_status_id\']').val();

  if (filter_order_status_id != 0) {
    url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
  }

  location = url;

});


//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php include ('../../includes/footer.php'); ?></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day active">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./ORD Report_files/classie.js.download" type="text/javascript"></script>
<script src="./ORD Report_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
