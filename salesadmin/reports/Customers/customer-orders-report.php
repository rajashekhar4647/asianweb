<!DOCTYPE html>
<!-- saved from url=(0109)http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Customer Orders Report</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script type="text/javascript" src="../../js/jquery-2.1.1.min.js.download"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js.download"></script>
<link href="../../css/bootstrap.css" type="text/css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" type="text/css" rel="stylesheet">
<link href="../../css/summernote.css" rel="stylesheet">
<script type="text/javascript" src="../../js/summernote.js.download"></script>
<script src="../../js/moment.js.download" type="text/javascript"></script>
<script src="../../js/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
<link href="../../css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/stylesheet.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/popup.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/style-popup.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/validationEngine.jquery.css" type="text/css">
<script src="../../js/common.js.download" type="text/javascript"></script>

<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/lobibox.min.css">
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<script src="../../js/lobibox.js.download"></script>
<script src="../../js/jquery.validationEngine-en.js.download" type="text/javascript" charset="utf-8"></script>
<script src="../../js/jquery.validationEngine.js.download" type="text/javascript" charset="utf-8"></script>
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true"  style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../../includes/head.php'); ?>
<?php include ('../../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1 style="color:black;">Customer Orders Report</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Customer Orders Report</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> Customer Orders List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start">Date Start</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="" placeholder="Date Start" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end">Date End</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="" placeholder="Date End" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-status">Order Status</label>
                <select name="filter_order_status_id" id="input-status" class="form-control">
                  <option value="0">All Statuses</option>
                                                      <option value="41">Address Verified</option>
                                                                        <option value="35">Aproval Pending</option>
                                                                        <option value="7">Canceled</option>
                                                                        <option value="9">Canceled Reversal</option>
                                                                        <option value="17">COD &nbsp;Confirmed</option>
                                                                        <option value="37">Completed</option>
                                                                        <option value="29">Converted Lead</option>
                                                                        <option value="8">Customer Refuse To Accept</option>
                                                                        <option value="22">Delivered</option>
                                                                        <option value="36">Disapproved</option>
                                                                        <option value="30">Duplicate Order</option>
                                                                        <option value="40">Entry Blocked - Price Issue</option>
                                                                        <option value="10">Failed</option>
                                                                        <option value="39">Incomplete Address</option>
                                                                        <option value="28">Lead</option>
                                                                        <option value="23">Miss Route</option>
                                                                        <option value="27">Non - serviceable area</option>
                                                                        <option value="24">ODA- Out Of Delivery Area</option>
                                                                        <option value="34">On hold</option>
                                                                        <option value="38">Order Assigned</option>
                                                                        <option value="42">Payment Gateway Entry</option>
                                                                        <option value="1">Pending</option>
                                                                        <option value="18">Pick Up Generated</option>
                                                                        <option value="19">Prepaid</option>
                                                                        <option value="15">Processed</option>
                                                                        <option value="2">Processing</option>
                                                                        <option value="26">Re Order - dont use it</option>
                                                                        <option value="20">Ready for Dispatch</option>
                                                                        <option value="11">Refunded</option>
                                                                        <option value="21">Reschedule</option>
                                                                        <option value="12">Reversed</option>
                                                                        <option value="14">RTO-Returned To Origin</option>
                                                                        <option value="43">Shipment Damaged</option>
                                                                        <option value="13">Shipment Lost</option>
                                                                        <option value="3">Shipped</option>
                                                                        <option value="33">UnConfirmed</option>
                                                                        <option value="5">Undelivered</option>
                                                                        <option value="16">Voided</option>
                                                                        <option value="25">Wrong Delivery</option>
                                                    </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left">Customer Name</td>
                <td class="text-left">E-Mail</td>
                <td class="text-left">Customer Group</td>
                <td class="text-left">Status</td>
                <td class="text-right">No. Orders</td>
                <td class="text-right">No. Products</td>
                <td class="text-right">Total</td>
                <td class="text-right">Action</td>
              </tr>
            </thead>
            <tbody>
                                          <tr>
                <td class="text-left">Smriti kana Ghosh</td>
                <td class="text-left">iamsmritikanaghosh@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">2</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 12,099</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277918" data-toggle="tooltip" title="" class="btn btn-warning" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">krishna chaitanya</td>
                <td class="text-left">krishnachaitanya0401@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 11,998</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=70423" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Rohit Gulati</td>
                <td class="text-left">gulatishoes22@yahoo.in</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">3</td>
                <td class="text-right">3</td>
                <td class="text-right">Rs. 6,294</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277919" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">ravi pal</td>
                <td class="text-left">vinitapal05@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 6,100</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277943" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Atmaram  Gupta</td>
                <td class="text-left">guptaprt1@yahoo.in</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 6,100</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277951" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Pradeep Kumar  Biswal </td>
                <td class="text-left">Ssbinfrastructure1968@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 6,100</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277920" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Satyam Sharma</td>
                <td class="text-left">expadproduction@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277939" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">a b</td>
                <td class="text-left">deep279714@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277948" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">V sireesha  Rachakonda</td>
                <td class="text-left">rvsireesha@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277917" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Manoj Jain</td>
                <td class="text-left">Jainmanoj.87@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277932" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Prasad Palakodety</td>
                <td class="text-left">bobee1304@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277956" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Ifra Kak</td>
                <td class="text-left">Ifrakak@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277926" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Khasgatesh  Hiremath</td>
                <td class="text-left">Coau@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277934" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Dharmender  Singh</td>
                <td class="text-left">dharmadharm@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277958" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Abd Maji</td>
                <td class="text-left">Majee1987@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277921" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Chhavi  Saxena </td>
                <td class="text-left">Chhavi.saxenaa@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277945" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">DHARMENDER KUMAR</td>
                <td class="text-left">Kumardharmender018@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277938" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Shaila  kundur </td>
                <td class="text-left">Shailapkundur@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277949" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Mamta Agarwal</td>
                <td class="text-left">bajrangtravels@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277933" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Devina  Kakar</td>
                <td class="text-left">Devinaverma12@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277935" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Irfan  Shaikh </td>
                <td class="text-left">vasikagroup5@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277944" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">a s</td>
                <td class="text-left">sjkckna@yahoo.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277928" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Chanky X</td>
                <td class="text-left">Arorachanky0@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277937" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Syed Amjad</td>
                <td class="text-left">syedamjadh705@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277953" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Dr Manjushree  Nayak</td>
                <td class="text-left">mshree45@hotmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277922" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Jyori H</td>
                <td class="text-left">jyotih2409@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,699</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277952" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Suhas Dusane</td>
                <td class="text-left">ssdusane@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,699</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277957" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Sukanya Baruah</td>
                <td class="text-left">Sukanyabaruah@hotmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 4,900</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277947" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">VK OLLIES</td>
                <td class="text-left">vkolliesnaga@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 4,898</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277916" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Venkat chellumkuri</td>
                <td class="text-left">Chellarao@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 4,559</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277954" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Maripi Ramesh</td>
                <td class="text-left">rammibbl@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">2</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 4,196</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277959" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Kashish Sehgal</td>
                <td class="text-left">Kashish.sehgal06@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 3,324</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277941" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Arti Pandey</td>
                <td class="text-left">itrapandey@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 3,000</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277927" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Pradeep Saberwal </td>
                <td class="text-left">Pradeep_saberwal@rediffmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,200</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277924" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">SETHURAO NARESH</td>
                <td class="text-left">snareshlic@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277950" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Prajakta Surve</td>
                <td class="text-left">chandraraoprajkta7@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277929" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Nisha Kator</td>
                <td class="text-left">nishakator40@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=200472" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">DINESH SINGH</td>
                <td class="text-left">dineshsingh2918@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277923" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Zeeshan Khan</td>
                <td class="text-left">zeeshankhan.712@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277931" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">swathi ashi</td>
                <td class="text-left">Swathia2192@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277940" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Palla Naresh</td>
                <td class="text-left">psvn401@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277955" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Amit Kumar</td>
                <td class="text-left">cmaamitkr@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277925" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Venkat Muthukuru</td>
                <td class="text-left">Venkatrockz421431@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277942" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">manju nath</td>
                <td class="text-left">mnath0069@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277946" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">MAHEBOOB Munir</td>
                <td class="text-left">maheboobsk1989@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 2,098</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277930" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                            <tr>
                <td class="text-left">Jitendra Rai</td>
                <td class="text-left">jitendra.rai30@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">1</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 1,993</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=277936" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                                        </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><ul class="pagination"><li class="active"><span>1</span></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">2</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=3">3</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=4">4</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=5">5</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=6">6</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=7">7</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=8">8</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=9">9</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">&gt;</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=6194">&gt;|</a></li></ul></div>
          <div class="col-sm-6 text-right">Showing 1 to 50 of 309663 (6194 Pages)</div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/customer_order&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').val();

	if (filter_order_status_id != 0) {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php include ('../../includes/footer.php'); ?></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Customer Orders Report_files/classie.js.download" type="text/javascript"></script>
<script src="./Customer Orders Report_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
