<!DOCTYPE html>
<!-- saved from url=(0112)http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Customer Activity Report</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script type="text/javascript" src="../../js/jquery-2.1.1.min.js.download"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js.download"></script>
<link href="../../css/bootstrap.css" type="text/css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" type="text/css" rel="stylesheet">
<link href="../../css/summernote.css" rel="stylesheet">
<script type="text/javascript" src="../../js/summernote.js.download"></script>
<script src="../../js/moment.js.download" type="text/javascript"></script>
<script src="../../js/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
<link href="../../css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/stylesheet.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/popup.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/style-popup.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/validationEngine.jquery.css" type="text/css">
<script src="../../js/common.js.download" type="text/javascript"></script>

<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/lobibox.min.css">
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<script src="../../js/lobibox.js.download"></script>
<script src="../../js/jquery.validationEngine-en.js.download" type="text/javascript" charset="utf-8"></script>
<script src="../../js/jquery.validationEngine.js.download" type="text/javascript" charset="utf-8"></script>
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">

<?php include ('../../includes/head.php'); ?>
<?php include ('../../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1 style="color:black;">Customer Activity Report</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Customer Activity Report</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> Customer Activity List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start">Date Start</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="" placeholder="Date Start" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end">Date End</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="" placeholder="Date End" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-customer">Customer</label>
                <input type="text" name="filter_customer" value="" id="input-customer" class="form-control">
              </div>
              <div class="form-group">
                <label class="control-label" for="input-ip">IP</label>
                <input type="text" name="filter_ip" value="" id="input-ip" class="form-control">
              </div>
              <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">Comment</td>
                <td class="text-left">IP</td>
                <td class="text-left">Date Added</td>
              </tr>
            </thead>
            <tbody>
                                          <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=496876">Prince  Rajpoot</a> registered for an account.</td>
                <td class="text-left">47.247.5.226</td>
                <td class="text-left">04/11/2019 16:22:04</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586605">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">04/11/2019 13:41:52</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586604">Sakshi Rathore</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">04/11/2019 13:26:18</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586603">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">04/11/2019 11:50:29</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586603">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">04/11/2019 11:04:52</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586602">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">04/11/2019 10:55:06</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=547279">Nandha  kumar</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">03/11/2019 16:43:01</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=489910">Raghu N</a> registered for an account.</td>
                <td class="text-left">116.75.101.212</td>
                <td class="text-left">03/11/2019 16:21:46</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=398285">Hemavathi.r Hema</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">03/11/2019 09:14:31</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=583917">Naveena reddy NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">02/11/2019 23:56:03</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586601">Danikumar NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">02/11/2019 20:47:58</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=569876">kamatham bhargav</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">02/11/2019 18:39:23</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=275090">PRAVEEN KUMAR NAMPALLY</a> registered for an account.</td>
                <td class="text-left">114.79.177.218</td>
                <td class="text-left">02/11/2019 16:48:27</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586130">Azra Khan</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">01/11/2019 22:48:50</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586600">kumar reddy</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">31/10/2019 11:22:42</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=561009">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">31/10/2019 07:52:30</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=561009">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">31/10/2019 07:44:43</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=586599">VARSHA KAMBLE</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">30/10/2019 22:54:32</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=573373">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">30/10/2019 16:56:28</td>
              </tr>
                            <tr>
                <td class="text-left"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=573373">missedcaller_name NA</a> registered for an account.</td>
                <td class="text-left">52.66.185.133</td>
                <td class="text-left">30/10/2019 16:49:08</td>
              </tr>
                                        </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><ul class="pagination"><li class="active"><span>1</span></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">2</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=3">3</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=4">4</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=5">5</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=6">6</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=7">7</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=8">8</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=9">9</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">&gt;</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_activity&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=5153">&gt;|</a></li></ul></div>
          <div class="col-sm-6 text-right">Showing 1 to 50 of 257622 (5153 Pages)</div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/customer_activity&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

	var filter_customer = $('input[name=\'filter_customer\']').val();

	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}
	var filter_ip = $('input[name=\'filter_ip\']').val();

	if (filter_ip) {
		url += '&filter_ip=' + encodeURIComponent(filter_ip);
	}

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php include ('../../includes/footer.php'); ?></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Customer Activity Report_files/classie.js.download" type="text/javascript"></script>
<script src="./Customer Activity Report_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
