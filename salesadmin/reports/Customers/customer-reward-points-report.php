<!DOCTYPE html>
<!-- saved from url=(0110)http://asianherbs.in/salesadminadmin/index.php?route=report/customer_reward&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Customer Reward Points Report</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script type="text/javascript" src="../../js/jquery-2.1.1.min.js.download"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js.download"></script>
<link href="../../css/bootstrap.css" type="text/css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" type="text/css" rel="stylesheet">
<link href="../../css/summernote.css" rel="stylesheet">
<script type="text/javascript" src="../../js/summernote.js.download"></script>
<script src="../../js/moment.js.download" type="text/javascript"></script>
<script src="../../js/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
<link href="../../css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/stylesheet.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/popup.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/style-popup.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/validationEngine.jquery.css" type="text/css">
<script src="../../js/common.js.download" type="text/javascript"></script>

<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/lobibox.min.css">
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<script src="../../js/lobibox.js.download"></script>
<script src="../../js/jquery.validationEngine-en.js.download" type="text/javascript" charset="utf-8"></script>
<script src="../../js/jquery.validationEngine.js.download" type="text/javascript" charset="utf-8"></script>
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../../includes/head.php'); ?>
<?php include ('../../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1 style="color:black;">Customer Reward Points Report</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/customer_reward&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Customer Reward Points Report</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> Customer Reward Points List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start">Date Start</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="" placeholder="Date Start" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-end">Date End</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="" placeholder="Date End" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">Customer Name</td>
                <td class="text-left">E-Mail</td>
                <td class="text-left">Customer Group</td>
                <td class="text-left">Status</td>
                <td class="text-right">Reward Points</td>
                <td class="text-right">No. Orders</td>
                <td class="text-right">Total</td>
                <td class="text-right">Action</td>
              </tr>
            </thead>
            <tbody>
                                          <tr>
                <td class="text-left">Ambati Brahmaiah</td>
                <td class="text-left">ambati984@gmail.com</td>
                <td class="text-left">Landing Page</td>
                <td class="text-left">Enabled</td>
                <td class="text-right">0</td>
                <td class="text-right">0</td>
                <td class="text-right">Rs. 0</td>
                <td class="text-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=203686" data-toggle="tooltip" title="" class="btn btn-warning" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
              </tr>
                                        </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right">Showing 1 to 1 of 1 (1 Pages)</div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/customer_reward&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php include ('../../includes/footer.php'); ?></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Customer Reward Points Report_files/classie.js.download" type="text/javascript"></script>
<script src="./Customer Reward Points Report_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
