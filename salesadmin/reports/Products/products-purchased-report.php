<!DOCTYPE html>
<!-- saved from url=(0112)http://asianherbs.in/salesadminadmin/index.php?route=report/product_purchased&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Products Purchased Report</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script type="text/javascript" src="../../js/jquery-2.1.1.min.js.download"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js.download"></script>
<link href="../../css/bootstrap.css" type="text/css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" type="text/css" rel="stylesheet">
<link href="../../css/summernote.css" rel="stylesheet">
<script type="text/javascript" src="../../js/summernote.js.download"></script>
<script src="../../js/moment.js.download" type="text/javascript"></script>
<script src="../../js/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
<link href="../../css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/stylesheet.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/popup.css" rel="stylesheet" media="screen">
<link type="text/css" href="../../css/style-popup.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/validationEngine.jquery.css" type="text/css">
<script src="../../js/common.js.download" type="text/javascript"></script>

<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../../css/lobibox.min.css">
<link type="text/css" href="../../css/component.css" rel="stylesheet" media="screen">
<script src="../../js/lobibox.js.download"></script>
<script src="../../js/jquery.validationEngine-en.js.download" type="text/javascript" charset="utf-8"></script>
<script src="../../js/jquery.validationEngine.js.download" type="text/javascript" charset="utf-8"></script>
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../../includes/head.php'); ?>
<?php include ('../../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1 style="color:black;">Products Purchased Report</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/product_purchased&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Products Purchased Report</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> Products Purchased List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start">Date Start</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="" placeholder="Date Start" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end">Date End</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="" placeholder="Date End" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-status">Order Status</label>
                <select name="filter_order_status_id" id="input-status" class="form-control">
                  <option value="0">All Statuses</option>
                                                      <option value="41">Address Verified</option>
                                                                        <option value="35">Aproval Pending</option>
                                                                        <option value="7">Canceled</option>
                                                                        <option value="9">Canceled Reversal</option>
                                                                        <option value="17">COD &nbsp;Confirmed</option>
                                                                        <option value="37">Completed</option>
                                                                        <option value="29">Converted Lead</option>
                                                                        <option value="8">Customer Refuse To Accept</option>
                                                                        <option value="22">Delivered</option>
                                                                        <option value="36">Disapproved</option>
                                                                        <option value="30">Duplicate Order</option>
                                                                        <option value="40">Entry Blocked - Price Issue</option>
                                                                        <option value="10">Failed</option>
                                                                        <option value="39">Incomplete Address</option>
                                                                        <option value="28">Lead</option>
                                                                        <option value="23">Miss Route</option>
                                                                        <option value="27">Non - serviceable area</option>
                                                                        <option value="24">ODA- Out Of Delivery Area</option>
                                                                        <option value="34">On hold</option>
                                                                        <option value="38">Order Assigned</option>
                                                                        <option value="42">Payment Gateway Entry</option>
                                                                        <option value="1">Pending</option>
                                                                        <option value="18">Pick Up Generated</option>
                                                                        <option value="19">Prepaid</option>
                                                                        <option value="15">Processed</option>
                                                                        <option value="2">Processing</option>
                                                                        <option value="26">Re Order - dont use it</option>
                                                                        <option value="20">Ready for Dispatch</option>
                                                                        <option value="11">Refunded</option>
                                                                        <option value="21">Reschedule</option>
                                                                        <option value="12">Reversed</option>
                                                                        <option value="14">RTO-Returned To Origin</option>
                                                                        <option value="43">Shipment Damaged</option>
                                                                        <option value="13">Shipment Lost</option>
                                                                        <option value="3">Shipped</option>
                                                                        <option value="33">UnConfirmed</option>
                                                                        <option value="5">Undelivered</option>
                                                                        <option value="16">Voided</option>
                                                                        <option value="25">Wrong Delivery</option>
                                                    </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">Product Name</td>
                <td class="text-left">Model</td>
                <td class="text-right">Quantity</td>
                <td class="text-right">Total</td>
              </tr>
            </thead>
            <tbody>
                                          <tr>
                <td class="text-left">Garcinia Cambogia Herbs-6 Bottles</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">107862</td>
                <td class="text-right">Rs. 647,712,030</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia Herbs-1 Bottle</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">150787</td>
                <td class="text-right">Rs. 365,208,008</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia And Apple Cider Pack - 1 bottle</td>
                <td class="text-left">GCUTLAPC001</td>
                <td class="text-right">49102</td>
                <td class="text-right">Rs. 113,234,726</td>
              </tr>
                            <tr>
                <td class="text-left">Hair Eternity &amp; Oil - 1 Bottle</td>
                <td class="text-left">Hair Eternity</td>
                <td class="text-right">42612</td>
                <td class="text-right">Rs. 89,916,084</td>
              </tr>
                            <tr>
                <td class="text-left">Apple Cider Vinegar</td>
                <td class="text-left">ACV0001</td>
                <td class="text-right">39473</td>
                <td class="text-right">Rs. 58,816,191</td>
              </tr>
                            <tr>
                <td class="text-left">Buy 1 Get 1(Garcinia Cambogia 2 Bottle)+Complimentary 1 Bottle Apple Cidar</td>
                <td class="text-left">GCUTLAPC002</td>
                <td class="text-right">16714</td>
                <td class="text-right">Rs. 50,191,264</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia + 2 Pack Green Coffee Beans/Powder &amp; 2 Bottle Apple Cidar - 6 Bottles</td>
                <td class="text-left">GCUTLAPC006</td>
                <td class="text-right">8001</td>
                <td class="text-right">Rs. 48,117,979</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia Herbs 3 Bottles</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">10364</td>
                <td class="text-right">Rs. 36,354,610</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia + 1 Pack Green Coffee Beans/Powder &amp; 2 Bottle Apple Cidar - 4 Bottles</td>
                <td class="text-left">GCUTLAPC004</td>
                <td class="text-right">5678</td>
                <td class="text-right">Rs. 27,296,712</td>
              </tr>
                            <tr>
                <td class="text-left">G3 Fat Burner  Supplement</td>
                <td class="text-left">PGG3001</td>
                <td class="text-right">873</td>
                <td class="text-right">Rs. 21,298,977</td>
              </tr>
                            <tr>
                <td class="text-left">#1 Garcinia Cambogia</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">4996</td>
                <td class="text-right">Rs. 20,903,168</td>
              </tr>
                            <tr>
                <td class="text-left">Green Coffee Beans Decaffeinated Unroasted Arabica - 200 gms</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">3639</td>
                <td class="text-right">Rs. 8,205,057</td>
              </tr>
                            <tr>
                <td class="text-left">Buy 3 Hair Eternity &amp; Get 3 Oil Free Bottles</td>
                <td class="text-left">Hair Eternity</td>
                <td class="text-right">2279</td>
                <td class="text-right">Rs. 7,981,219</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia Herbs-4 Bottles</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">1380</td>
                <td class="text-right">Rs. 6,920,158</td>
              </tr>
                            <tr>
                <td class="text-left">Buy 6 Hair Eternity &amp; Get 6 Oil Free Bottles</td>
                <td class="text-left">Hair Eternity</td>
                <td class="text-right">1318</td>
                <td class="text-right">Rs. 6,873,078</td>
              </tr>
                            <tr>
                <td class="text-left">Hair Eternity Oil</td>
                <td class="text-left">HRETYO01</td>
                <td class="text-right">1189</td>
                <td class="text-right">Rs. 5,505,600</td>
              </tr>
                            <tr>
                <td class="text-left">Hair Eternity Buy One Get One Free With Oil</td>
                <td class="text-left">Hair Eternity</td>
                <td class="text-right">1623</td>
                <td class="text-right">Rs. 4,528,170</td>
              </tr>
                            <tr>
                <td class="text-left">Hair Eternity - 1 Bottle</td>
                <td class="text-left">Hair Eternity</td>
                <td class="text-right">499</td>
                <td class="text-right">Rs. 3,648,422</td>
              </tr>
                            <tr>
                <td class="text-left">#3 Garcinia Cambogia</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">602</td>
                <td class="text-right">Rs. 3,322,890</td>
              </tr>
                            <tr>
                <td class="text-left">Green Coffee Bean Herbs-1 Bottle</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">632</td>
                <td class="text-right">Rs. 2,910,880</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia Supplement Buy One Get One Free</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">557</td>
                <td class="text-right">Rs. 1,694,435</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia Herbs Buy 3 Get 3 Bottles</td>
                <td class="text-left">weight loss</td>
                <td class="text-right">171</td>
                <td class="text-right">Rs. 1,121,813</td>
              </tr>
                            <tr>
                <td class="text-left">G3 Burner - 4 bottles</td>
                <td class="text-left">PGG3004</td>
                <td class="text-right">77</td>
                <td class="text-right">Rs. 544,891</td>
              </tr>
                            <tr>
                <td class="text-left">G3 Burner - 6 bottles</td>
                <td class="text-left">PGG3006</td>
                <td class="text-right">33</td>
                <td class="text-right">Rs. 533,911</td>
              </tr>
                            <tr>
                <td class="text-left">#2 Garcinia Cambogia</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">98</td>
                <td class="text-right">Rs. 382,788</td>
              </tr>
                            <tr>
                <td class="text-left">Green Coffee Herbs Buy One Get One Free</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">90</td>
                <td class="text-right">Rs. 331,514</td>
              </tr>
                            <tr>
                <td class="text-left">Green Coffee Bean Herbs-4 Bottles</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">44</td>
                <td class="text-right">Rs. 219,956</td>
              </tr>
                            <tr>
                <td class="text-left">Green Coffee Bean Herbs-6 Bottles</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">30</td>
                <td class="text-right">Rs. 179,970</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia And Green Coffee Beans Pack - 1 bottle</td>
                <td class="text-left">GCGR001</td>
                <td class="text-right">51</td>
                <td class="text-right">Rs. 126,195</td>
              </tr>
                            <tr>
                <td class="text-left">Ultra Skin Repair Face Pack-1 Bottle</td>
                <td class="text-left">SKCA001PH1</td>
                <td class="text-right">22</td>
                <td class="text-right">Rs. 113,886</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia And Green Coffee Beans Pack - 2 bottles</td>
                <td class="text-left">GCGR002</td>
                <td class="text-right">34</td>
                <td class="text-right">Rs. 101,966</td>
              </tr>
                            <tr>
                <td class="text-left">Cure Piles  - Best Medicine for Hemorrhoids</td>
                <td class="text-left">HealthCare</td>
                <td class="text-right">16</td>
                <td class="text-right">Rs. 44,982</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia And Green Coffee Beans Pack - 6 bottles</td>
                <td class="text-left">GCGR006</td>
                <td class="text-right">4</td>
                <td class="text-right">Rs. 23,996</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia And Green Coffee Beans Pack - 4 bottles</td>
                <td class="text-left">GCGR004</td>
                <td class="text-right">5</td>
                <td class="text-right">Rs. 23,995</td>
              </tr>
                            <tr>
                <td class="text-left">Moringa Capsules</td>
                <td class="text-left">Medicinal Herb</td>
                <td class="text-right">7</td>
                <td class="text-right">Rs. 18,149</td>
              </tr>
                            <tr>
                <td class="text-left">Cure Piles - 4 Bottles</td>
                <td class="text-left">Health Cure Piles</td>
                <td class="text-right">3</td>
                <td class="text-right">Rs. 14,997</td>
              </tr>
                            <tr>
                <td class="text-left">Testo Boost Pack - 1 Bottle</td>
                <td class="text-left">TSTBS001</td>
                <td class="text-right">4</td>
                <td class="text-right">Rs. 6,796</td>
              </tr>
                            <tr>
                <td class="text-left">Cure Piles - 6 Bottles</td>
                <td class="text-left">Health Care</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 5,999</td>
              </tr>
                            <tr>
                <td class="text-left">Balanced Diet Plan for Weight Loss</td>
                <td class="text-left">PHDT@10001</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 3,798</td>
              </tr>
                            <tr>
                <td class="text-left">Garcinia Cambogia Plus Green Coffee Herbs Combo</td>
                <td class="text-left">Weight Loss</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 3,599</td>
              </tr>
                            <tr>
                <td class="text-left">Gold Bleach</td>
                <td class="text-left">GB001</td>
                <td class="text-right">5</td>
                <td class="text-right">Rs. 1,350</td>
              </tr>
                            <tr>
                <td class="text-left">Astaberry Skin Whitening Creme</td>
                <td class="text-left">ASW0005</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 520</td>
              </tr>
                            <tr>
                <td class="text-left">Almond &amp; Aloe Moisturizer</td>
                <td class="text-left">AAM0002</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 468</td>
              </tr>
                            <tr>
                <td class="text-left">Aqua Life 24 Hr Moisturiser</td>
                <td class="text-left">ALM0001</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 355</td>
              </tr>
                            <tr>
                <td class="text-left">After Wax Lotion</td>
                <td class="text-left">AWL001</td>
                <td class="text-right">2</td>
                <td class="text-right">Rs. 324</td>
              </tr>
                            <tr>
                <td class="text-left">Wine Shampoo</td>
                <td class="text-left">SFSH001</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 135</td>
              </tr>
                            <tr>
                <td class="text-left">Fruit Face Wash</td>
                <td class="text-left">FRW001</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 81</td>
              </tr>
                            <tr>
                <td class="text-left">AHA Detan (Sun Tan Removal)</td>
                <td class="text-left">AHA1001</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 76</td>
              </tr>
                            <tr>
                <td class="text-left">Papaya Moisturiser</td>
                <td class="text-left">PM010</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 70</td>
              </tr>
                            <tr>
                <td class="text-left">Fruit Nourishing Massage Cream</td>
                <td class="text-left">FNM0008</td>
                <td class="text-right">1</td>
                <td class="text-right">Rs. 67</td>
              </tr>
                                        </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><ul class="pagination"><li class="active"><span>1</span></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/product_purchased&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">2</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/product_purchased&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">&gt;</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=report/product_purchased&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">&gt;|</a></li></ul></div>
          <div class="col-sm-6 text-right">Showing 1 to 50 of 53 (2 Pages)</div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/product_purchased&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').val();

	if (filter_order_status_id != 0) {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script>
</div>
<?php include ('../../includes/footer.php'); ?></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Products Purchased Report_files/classie.js.download" type="text/javascript"></script>
<script src="./Products Purchased Report_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
