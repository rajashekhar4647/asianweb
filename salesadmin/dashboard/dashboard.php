<!DOCTYPE html>
<!-- saved from url=(0104)http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Dashboard</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script type="text/javascript" src="./Dashboard_files/jquery-2.1.1.min.js.download"></script>
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../includes/head.php'); ?>
<?php include ('../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
         <button type="button" id="button-sync-reports" class="btn btn-info" data-original-title="Update order reports">Sync</button>
       </div>
      <h1 style="color:black;">Dashboard</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Dashboard</a></li>
              </ul>
       <style type="text/css">
          #search{display: inline-block; width: auto; height: auto;}
          .input-group .form-control {display: inline-block;}
          .total-css001{text-align:left !important;background:#999999 !important;}
          .tile .tile-body h2 {font-size: 26px;}
          .db9 h2 {font-size: 26px !important;}
      </style>

      <div id="search">
        <table class="tftable">
          <tbody><tr>
          <td><div class="input-group date">
          <input type="text" name="filter_date_start" value="2019-11-04" placeholder="date start " data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span>
          </div>
          </td>
          <td>  <div class="input-group date">
          <input type="text" name="filter_date_end" value="2019-11-04" placeholder="date end " data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control">
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div></td>
          <td><button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> </button>
          </td>
          </tr>
        </tbody></table>
      </div>

    </div>
  </div>
  <div class="container-fluid">
        <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6">         <div class="tile"><div class="tl1 tile-heading"></div>
            <div class="tile-body db1" style="background:white;color:black;border:1px solid #bf170d;"><i class="fas fa-sign-in-alt"></i><h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">6</span> <br><span style="font-size:10px;">Total Registrations</span></h2></div>
         </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl2 tile-heading"></div>
            <div class="tile-body db2" style="background:white;color:black;border:1px solid #223e1a;"><i class="fas fa-link"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">4</span> <br><span style="font-size:10px;">Total Leads</span></h2>
						</div>
         </div></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl3 tile-heading"></div>
            <div class="tile-body db3" style="background:white;color:black;border:1px solid #1420bf;"><i class="fas fa-money-bill-alt"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">1</span> <br><span style="font-size:10px;">Total COD</span></h2>
						</div>
         </div></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl4 tile-heading"></div>
            <div class="tile-body db4" style="background:white;color:black;border:1px solid #0b886c;"><i class="fab fa-cc-visa"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">0+0 = 0</span> <br><span style="font-size:10px;">Total Prepaid</span></h2>
						</div>
         </div></div>
    </div>

    <div class="row">


      <!-- <div class="col-lg-3 col-md-3 col-sm-6">         <div class="tile"><div class="tl5 tile-heading">Total Order Converted</div>
            <div class="tile-body db5"><i class="fa fa-shopping-cart"></i><h2 class="pull-right">5</h2></div>
         </div>
      </div> -->

      <div class="col-lg-3 col-md-3 col-sm-6">         <div class="tile"><div class="tl5 tile-heading" style="background:#babd00;"></div>
            <div class="tile-body db5" style="padding:8px 15px 0px 15px;line-height: 32px;background:white;color:black;border:1px solid #babd00;"><i class="fa fa-shopping-cart"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">5</span> <br><span style="font-size:10px;">Total Order Converted</span></h2>
              <a href="http://asianherbs.in/salesadminadmin/index.php?route=sale/order1&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;filter_date_type1=2&amp;filter_to_date=2019-11-04&amp;filter_from_date=2019-11-04&amp;filter_order_status=35" style="display: inline-block;vertical-align: text-bottom;font-size: 16px;"> Total approval pending : 0</a></div>
         </div>
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl6 tile-heading" style="background:#89ba16;"></div>
            <div class="tile-body db6" style="background:white;color:black;border:1px solid #89ba16;"><i class="fas fa-link"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">0</span> <br><span style="font-size:10px;">Total Lead Converted</span></h2>
						</div>
         </div></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl7 tile-heading" style="background:#ff0059;"></div>
            <div class="tile-body db7" style="background:white;color:black;border:1px solid #ff0059;"><i class="fas fa-money-bill-alt"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">5</span> <br><span style="font-size:10px;">Total COD Converted</span></h2>
						</div>
         </div></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl8 tile-heading"></div>
            <div class="tile-body db8" style="background:white;color:black;border:1px solid #4caf50;"><i class="fab fa-cc-visa"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">0</span> <br><span style="font-size:10px;">Total Prepaid Converted</span></h2>
						</div>
         </div></div>
    </div>

    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6">         <div class="tile"><div class="tl9 tile-heading"></div>
            <div class="tile-body db9" style="background:white;color:black;border:1px solid #e91e63;"><!-- <i class="fa fa-shopping-cart"></i> -->
              <h2>
               <a title="This Order Before Placed And Today Shipped">0+0</a><a title="This Order Today Placed And Today Shipped">+5</a>=5<br>
							 <span>Total Order Shipped</span>
            </h2>

					</div>
         </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading"></div>
            <div class="tile-body db10" style="background:white;color:black;border:1px solid #223e1a;"><i class="fas fa-truck"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">0</span> <br><span style="font-size:10px;">Total Lead Converted Shipped</span></h2>
						</div>
         </div></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl11 tile-heading"></div>
            <div class="tile-body db11" style="background:white;color:black;border:1px solid #1420bf;"><i class="fas fa-truck"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">5</span> <br><span style="font-size:10px;">Total COD Converted Shipped</span></h2>
						</div>
         </div></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl12 tile-heading"></div>
            <div class="tile-body db12" style="background:white;color:black;border:1px solid #0b886c;"><i class="fas fa-truck"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">0</span> <br><span style="font-size:10px;">Total Prepaid Converted Shipped</span></h2>
						</div>
         </div></div>
    </div>

     <div class="row">

	<div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tile-heading" style="background:black"></div>
            <div class="tile-body" style="background:white;color:black;border:1px solid black;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">4</span> <br><span style="font-size:10px;">Total Approval Pending Revenue</span></h2>
						</div>
         </div></div>


      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#029496;"></div>
            <div class="tile-body"  style="background:white;color:black;border:1px solid #029496;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">11,440</span> <br><span style="font-size:10px;">Total Approved Revenue</span></h2>
						</div>
         </div></div>


          <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#6531a8;"></div>
            <div class="tile-body"  style="background:white;color:black;border:1px solid #6531a8;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">41,500</span> <br><span style="font-size:10px;">Total Dispatched Revenue</span></h2>
						</div>
         </div></div>

          <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#b30c89;"></div>
            <div class="tile-body"  style="background:white;color:black;border:1px solid #b30c89;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">6,480</span> <br><span style="font-size:10px;">Total Delivered Revenue</span></h2>
						</div>
         </div></div>
     </div>

     <div class="row">
       <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading"></div>
            <div class="tile-body db10" style="background:white;color:black;border:1px solid #223e1a;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">4</span> <br><span style="font-size:10px;">Approved Revenue: BGLR A</span></h2>
						</div>
         </div></div>

	<div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#ff0015;"></div>
            <div class="tile-body db10" style="background:white;color:black;border:1px solid #ff0015;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">4</span> <br><span style="font-size:10px;">Approved Revenue: BGLR B</span></h2>
						</div>
         </div></div>

	<div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#ed4088;"></div>
            <div class="tile-body db10" style="background:white;color:black;border:1px solid #ed4088;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">4</span> <br><span style="font-size:10px;">Approved Revenue: BGLR C</span></h2>
						</div>
         </div></div>

	<div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#1ed950;"></div>
            <div class="tile-body db10" style="background:white;color:black;border:1px solid #1ed950;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">0</span> <br><span style="font-size:10px;">Approved Revenue: BGLR D</span></h2>
						</div>
         </div></div>

     </div>

    <div class="row">
	 <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#198cb3;"></div>
            <div class="tile-body db10" style="background:white;color:black;border:1px solid #198cb3;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">11,440</span> <br><span style="font-size:10px;">Approved Revenue: COD</span></h2>
						</div>
         </div></div>
	<!--<div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading">Approved Revenue: Prepaid</div>
            <div class="tile-body db10"><i class="fa fa-shopping-cart"></i><h2 class="pull-right">0</h2></div>
         </div></div>-->

	 <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile"><div class="tl10 tile-heading" style="background:#f77d31;"></div>
            <div class="tile-body db10" style="background:white;color:black;border:1px solid #f77d31;"><i class="fas fa-rupee-sign"></i>
							<h2 class="pull-right" style="text-align:center;"><span style="text-align:center;">0</span> <br><span style="font-size:10px;">Approved Revenue: Retention</span></h2>
						</div>
         </div></div>

    </div>



















    <!--TABING CODE STARTS HERE-->
      <div class="container-fluid" style="display: none;">
          <ul class="nav nav-tabs dboard">
            <li class="active"><a href="http://asianherbs.in/salesadminadmin/#tab-agent-day" data-toggle="tab">Agent Performance Orders </a></li>

            <li><a href="http://asianherbs.in/salesadminadmin/#tab-confirmed-order" data-toggle="tab">Approved Orders</a></li>

            <li><a href="http://asianherbs.in/salesadminadmin/#tab-sale" data-toggle="tab">Sale Orders</a></li>

          <!--<li><a href="#tab-agent" data-toggle="tab">Agent Orders</a></li> -->
            <li><a href="http://asianherbs.in/salesadminadmin/#tab-logistics" data-toggle="tab">Logistics Orders</a></li>
            <li><a href="http://asianherbs.in/salesadminadmin/#tab-option" data-toggle="tab">Courier Orders</a></li>
            <li><a href="http://asianherbs.in/salesadminadmin/#tab-products" data-toggle="tab">Products Orders</a></li>
            <li><a href="http://asianherbs.in/salesadminadmin/#tab-image" data-toggle="tab">Dispatch Orders</a></li>
            <!-- <li><a href="#tab-market" data-toggle="tab">Market</a></li> -->

          </ul>
          <div class="tab-content">
              <div class="tab-pane active sale-report-dashboard" id="tab-agent-day"><h2>Agent Reports Days</h2>
                    <table class="report-table" border="0" style=" display: inline-block; overflow: scroll; padding-bottom: 20px;">
                      <thead>
                          <tr>
                            <td colspan="2">Detail</td>
			                                                <td colspan="4">Lead</td>
                            <td colspan="4">Cod</td>
                            <td colspan="4">Prepaid</td>

                            <td colspan="2">Total&nbsp;Sale</td>
                                                            <td colspan="4">On Dispatched</td>
                                <td colspan="6">Sale Status</td>
                                <td colspan="2">Sale Target</td>
                                                      </tr>
                          <tr>
                              <td><a title="Sirial Number">SNO</a></td>
                              <td><a title="Agent Name">Agent</a></td>
                              <td><a title="Total Lead Assigned">TLDAS</a></td>
                              <td><a title="Total Lead Converted">TLCD</a></td>

                              <td><a title="Total Lead Sale Amount">TLDSAM</a></td>
                              <td><a title="Total Lead Ticket Converted">TLDTKC</a></td>

                              <td><a title="Total Cod Assigned">TCAS</a></td>
                              <td><a title="Total COD Converted">TCCD</a></td>
                                                          <td><a title="Total Cod Amount">TCSAM</a></td>
                              <td><a title="Total Cod Ticket Converted">TCTKC</a></td>

                              <td><a title="Total Prepaid Assigned">TPRAS</a></td>
                              <td><a title="Total Prepaid Orders">TPCD</a></td>

                              <td><a title="Total Prepaid Sale">TPSAM</a></td>
                              <td><a title="Total Prepaid Ticket  Size">TPTKC</a></td>


                              <td><a title="Total Converted Order">TCO</a></td>
                              <td><a title="Total Sale Amount">Sale</a></td>

                                      <td><a title="Total Sale Amount Dispatched">SADP</a></td>
                                      <td><a title="Total Sale Ticket  Dispatched">STDP</a></td>
                                      <td><a title="Total Sale Amount Delivered">TSAD</a></td>
                                      <td><a title="Total Sale Ticket Delivered">TSTD</a></td>
                                      <td><a title="Total Fake Order">FKO</a></td>
                                      <td><a title="Total Order Delivered">TOD</a></td>
                                      <td><a title="Total Order Delivered Amaunt">DA</a></td>
                                      <td><a title="Total Monthly Order RTO">MRTO</a></td>
                                      <td><a title="Total Intransits">INS</a></td>
                                      <td><a title="Total On Hold">TOH</a></td>
                                      <td><a title="Total Day Sale Target">TDST</a></td>
                                      <td><a title="Total day_t_s_achieved">DTSA</a></td>


                          </tr>
                      </thead>
                      <tbody>



                              </tbody>
                      <tfoot>
                          <tr>
                          <td colspan="2"><b>TOTAL ORDERS</b></td>
                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>

                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b>0</b></td>

                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>

                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b>0</b></td>


                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>

                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>

                          <td style="text-align:left;"><b>0</b></td>
                          <td style="text-align:left;"><b>0</b></td>


                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>
                                <td style="text-align:left;"><b></b></td>

                          </tr><tr>
                      </tr></tfoot><tfoot>
                  </tfoot></table>
               </div>

                <div class="tab-pane" id="tab-sale">
                  <h2>Sale Orders</h2>
                   <table class="report-table" border="0">
                      <thead>
                          <tr> <td>Add date</td>
                              <td><a title="Register Order">Orders</a></td>
                              <td><a title="Total Lead">leads</a></td>
                              <td><a title="Total COD">COD</a></td>
                              <td><a title="Total Prepaid">PAYU</a></td>
                              <td><a title="Total Lead Converted">LC</a></td>
                              <td><a title="Total Cod Converted">CC</a></td>
                              <td><a title="Total Prepaid Converted">PC</a></td>
                              <td><a title="Total Converted">TC</a></td>
                              <td><a title="Total Lead Shipped">TLS</a></td>
                              <td><a title="Total Cod Shipped">TCS</a></td>
                              <td><a title="Total Prepaid Shipped">TPS</a></td>
                              <td><a title="Total Shipped">TS</a></td>
                              <td><a title="Pre Confirm Order Shipped">PS</a></td>
                              <td><a title="Today Confirm Order Shipped">TS</a></td>
                              <td><a title="Pre Confirm Order Shipped Ticket Size">PTS</a></td>
                              <td><a title="Today Confirm Order Shipped Ticket Size">TTS</a></td>
                              <td><a title="Total Ticket Size">TTS</a></td>
                              <td><a title="Total Assign AWB">TWB</a></td>
                              <td><a title="Total Assign LEAD">TL</a></td>
                              <td><a title="Total Reads">TR</a></td>
                              <td><a title="Total Unreads">TUR</a></td>
                              <td><a title="Total Delivered">TD</a></td>
                              <td><a title="Total Cancels">TC</a></td>
                              <td><a title="Total RTO">TR</a></td>
                              <td><a title="Total Intransits">TIN</a></td>
                          </tr>
                      </thead>
                      <tbody>

                                             </tbody>
                       <tfoot style="background-color:#0b886c;color:#fff;">
                          <tr>
                              <td style="text-align:left;"><b>TOTAL ORDERS</b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>

                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>


                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>

                              <td style="text-align:left;"><b></b></td>


                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b>
                              (0%)                              </b></td>
                              <td style="text-align:left;"><b></b></td>
                              <td style="text-align:left;"><b>(0%)</b></td>
                              <td style="text-align:left;"><b></b></td>

                          </tr><tr>
                      </tr></tfoot><tfoot>
                  </tfoot></table>

               </div>


               <div class="tab-pane" id="tab-confirmed-order"><h2>Total Approved Orders </h2>
                  <table class="report-table" border="0">
                      <thead>
                          <tr>
                              <td>Agent Confirmed By</td>
                              <td>Lead</td>
                              <td>Cod</td>
                              <td>Prepaid</td>
                              <td>Total</td>

                          </tr>
                      </thead>
                                                 <tbody>



                         <tr>
                          <td style="text-align:left;">Veda Vathi</td>
                          <td style="text-align:right;">0</td>
                          <td style="text-align:right;">5</td>
                          <td style="text-align:right;">0</td>
                          <td style="text-align:right;">5</td>
                          </tr>



                      </tbody>
                       <tfoot style="background-color:#0b886c;color:#fff;">
                          <tr>
                          <td style="text-align:left;">Total Order</td>
                          <td>0</td>
                          <td>5</td>
                          <td>0</td>
                          <td>5</td>
                          </tr>
                      </tfoot>
                  </table>
                </div>




                <div class="tab-pane" id="tab-logistics"><h2>Logistics Orders</h2>
                  <table class="report-table-bd1" border="0">
                      <thead>
                          <tr>
                              <td>PRODUCT WISE</td>
                              <td>COURIER WISE</td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                             <td>
                              <table class="report-table-bd1" border="0">
                                  <thead>
                                      <tr>
                                          <td>SINO</td>
                                          <td title="Products Name" style="width:500px !important;">Name</td>
                                          <!-- <td title="Products Qty">Qty</td>
                                          <td title="Products Price">Price</td>
                                          <td title="Products Total">Total</td>  -->
                                          <td title="Received">REC</td>
                                          <td title="Dispatched">DIS</td>
                                          <td title="Non Dispatched">NDIS</td>
                                          <td title="Delivered">DEL</td>
                                          <td title="RTO">RTO</td>
                                          <td title="Canceled">CAN</td>
                                          <td title="On Hold">HO</td>
                                         <!--  <td>Intransit</td>  -->
                                      </tr>
                                  </thead>
                                  <tbody>



                                  </tbody>
                                 </table>
                              </td>
                              <td>



                                <table class="report-table-bd1" border="0">
                                    <thead>
                                        <tr>
                                            <td>SIN NO</td>
                                            <!-- <td>DATES</td>     -->
                                            <td>Courier Name</td>
                                            <!--<td>Other</td>
                                             <td>Received</td> -->
                                            <td>Dispatched</td>
                                            <td>Non Dispatched</td>
                                            <td>Delivered</td>
                                            <td>RTO</td>
                                            <td>Canceled</td>
                                            <td>Hold</td>
                                            <td>Intransit</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                            </tbody>
                                </table>

                              </td>
                         </tr>
                      </tbody>
                  </table>
                </div>

               <div class="tab-pane" id="tab-products"><h2>Logistics Products Orders</h2>
                  <table class="report-table-bd1" border="0">
                      <thead>
                          <tr>
                              <td>Products Wise</td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                             <td>
                              <table class="report-table-bd1" border="0">
                                  <thead>
                                      <tr>
                                          <td>SIN NO</td>
                                          <td>Name</td>
                                          <td>Qty</td>
                                          <td>Price</td>
                                          <td>Total</td>
                                          <td>Received</td>
                                          <td>Dispatched</td>
                                          <td>Non Dispatched</td>
                                          <td>Delivered</td>
                                          <td>RTO</td>
                                          <td>Canceled</td>
                                          <td>Hold</td>
                                         <!--  <td>Intransit</td>  -->
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>

                                      </tr>
                                  </tbody>
                                 </table>
                              </td>
                         </tr>
                      </tbody>
                  </table>
               </div>


               <div class="tab-pane" id="tab-option"><h2>Courier Orders</h2>

                <table class="report-table-bd1" border="0">
                                    <thead>
                                        <tr>
                                            <td>SIN NO</td>
                                            <!-- <td>DATES</td>     -->
                                            <td>Courier Name</td>
                                            <!--<td>Other</td>
                                             <td>Received</td> -->
                                            <td>Dispatched</td>
                                            <td>Non Dispatched</td>
                                            <td>Delivered</td>
                                            <td>RTO</td>
                                            <td>Canceled</td>
                                            <td>Hold</td>
                                            <td>Intransit</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                            </tbody>
                </table>

               </div>
               <div class="tab-pane" id="tab-image"><h2>Dispatch Orders</h2>

                   <table class="report-table-bd1 GeneratedTablew" border="0">
                    <thead>
                         <tr>
                            <td>Courier&nbsp;Name</td>
                            <td>Products</td>
                            <td>Qty</td>
                            <td>Price</td>
                            <td>Total</td>
                            <td>Received</td>
                            <td>Dispatched</td>
                            <td>Non&nbsp;Dispatched</td>
                            <td>Delivered</td>
                            <td>RTO</td>
                            <td>Canceled</td>
                           <!--  <td>Hold</td>
                            <td>Intransit</td> -->
                          </tr>
                      </thead>

                      <tbody>

                      </tbody>
                       <tfoot>
                          <tr style="background:#000000;">
                          <td colspan="2"><b>TOTAL ORDERS</b></td>
                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b>0</b></td>
                          <td style="text-align:left;"><b>0</b></td>
                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>
                          <td style="text-align:left;"><b></b></td>
                          </tr><tr>
                      </tr></tfoot><tfoot>
                  </tfoot></table>
               </div>
               <div class="tab-pane" id="tab-market"><h2>Market Orders</h2></div>
          </div>
      </div>

<!--TABING CODE STARTS HERE-->


  </div>
</div>




<style type="text/css">
.table2122{border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD;  margin-bottom: 20px;}
.table2122 thead tr td{font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;}
.table2122 tbody tr td{font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left;}
.table2122 tfoot tr td{font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 5px;}
.table2122 tbody tr td input{ padding: 5px; width: 100%;}
.table2122 tbody tr td select{ padding: 5px; width: 100%;}

</style>


<script type="text/javascript">
$('#button-sync-reports').on('click', function() {

  url = 'index.php?route=dashboard/report_activity/sync&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }
       $.ajax({
          url: url,
          dataType: 'json',
          success: function(json) {
           if(json['success']){
           // $('#button-filter').click();
             window.location.reload(true);
           } else {
              alert("Technical error... Contact wasim haidar or lajpat singh.");
           }

          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
});



$('#button-status-show').on('click', function() {
  url = 'index.php?route=dashboard/shipped/getStatus&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

   var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  // alert(url);

       $.ajax({
          url: url,
          dataType: 'json',
          success: function(json) {

            html='';

            if (json['order_status'] && json['order_status'] != '') {
              for (i = 0; i < json['order_status'].length; i++) {
                  html += '<tr><td>#'+i+'</td><td>'+json['order_status'][i]['status']+'</td><td>'+json['order_status'][i]['total']+'</td></tr>';
              }
           }

            $('#result_info').html(html);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
});


function GetConvertedUser(user_id){
        $.ajax({
          url: 'index.php?route=sale/order1/convertedOrdersPopup&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&get_user_id='+user_id,
          dataType: 'json',
          success: function(json) {
          html=json['converted_info'];
            $('#result_info').html(html);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
}
</script>
<!-- POPUP FOR STATUS HERE -->
<div class="md-modal md-modalw1 md-effect-10" id="modal-10" style="top: 52%; max-width: 750px !important; min-width: 550px;">
    <div class="md-content"><button class="md-close btn btn-danger">Close!</button>
        <div style="text-align:center; overflow: scroll; padding: 0px !important; height:650px;" id="result_info">

       </div>
    </div>
</div>

<div class="md-overlay"></div><!-- the overlay element -->




  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
  url = 'index.php?route=common/dashboard&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

  var filter_date_start = $('input[name=\'filter_date_start\']').val();

  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').val();

  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }
  location = url;
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>
<?php include ('../includes/footer.php'); ?>
</div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Dashboard_files/classie.js.download" type="text/javascript"></script>
<script src="./Dashboard_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
