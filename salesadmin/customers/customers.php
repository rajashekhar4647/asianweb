<!DOCTYPE html>
<!-- saved from url=(0105)http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Customers</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
  <?php include('../includes/head.php'); ?>
<?php include('../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/add&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="Add New"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="" class="btn btn-danger" onclick="confirm(&#39;Are you sure?&#39;) ? $(&#39;#form-customer&#39;).submit() : false;" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1 style="color:black;">Customers</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Customers</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Customer List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-name">Customer Name</label>
                <input type="text" name="filter_name" value="" placeholder="Customer Name" id="input-name" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-email">E-Mail</label>
                <input type="text" name="filter_email" value="" placeholder="E-Mail" id="input-email" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-customer-group">Customer Group</label>
                <select name="filter_customer_group_id" id="input-customer-group" class="form-control">
                  <option value="*"></option>
                                                      <option value="6">Landing Page</option>
                                                                        <option value="7">Vendor</option>
                                                                        <option value="2">Website</option>
                                                    </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status">Status</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                                    <option value="1">Enabled</option>
                                                      <option value="0">Disabled</option>
                                  </select>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-approved">Approved</label>
                <select name="filter_approved" id="input-approved" class="form-control">
                  <option value="*"></option>
                                    <option value="1">Yes</option>
                                                      <option value="0">No</option>
                                  </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-ip">IP</label>
                <input type="text" name="filter_ip" value="" placeholder="IP" id="input-ip" class="form-control">
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-date-added">Date Added</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="" placeholder="Date Added" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control">
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </div>
        <form action="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/delete&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" method="post" enctype="multipart/form-data" id="form-customer">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$(&#39;input[name*=\&#39;selected\&#39;]&#39;).prop(&#39;checked&#39;, this.checked);"></td>
                  <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=name&amp;order=DESC" class="asc">Customer Name</a>
                    </td>
                  <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=c.email&amp;order=DESC">E-Mail</a>
                    </td>
                  <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=customer_group&amp;order=DESC">Customer Group</a>
                    </td>
                  <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=c.status&amp;order=DESC">Status</a>
                    </td>
                  <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=c.ip&amp;order=DESC">IP</a>
                    </td>
                  <td class="text-left">                    <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=c.date_added&amp;order=DESC">Date Added</a>
                    </td>
                  <td class="text-right">Action</td>
                </tr>
              </thead>
              <tbody>
                                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="451233">
                    </td>
                  <td class="text-left">       SHEHZADI    BEGUM   SHEHZADI   BEGUM </td>
                  <td class="text-left">begumiqbal1955@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">14.98.14.194</td>
                  <td class="text-left">27/03/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-primary" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=451233&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-success" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=451233" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="558908">
                    </td>
                  <td class="text-left">      deepakroy1357 NA</td>
                  <td class="text-left">deepakroy1357</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">14.98.14.194</td>
                  <td class="text-left">28/08/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=558908&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=558908" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="428697">
                    </td>
                  <td class="text-left">     Arumilli Dorababu</td>
                  <td class="text-left">dorababu5213@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">30/01/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=428697&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=428697" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="449522">
                    </td>
                  <td class="text-left">     Indra Gerald</td>
                  <td class="text-left">prathipamary333@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">14.98.14.194</td>
                  <td class="text-left">21/03/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=449522&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=449522" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="450973">
                    </td>
                  <td class="text-left">    farhana                      siddiqui</td>
                  <td class="text-left">farhana.admin@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">106.51.77.185</td>
                  <td class="text-left">26/03/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=450973&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=450973" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="28026">
                    </td>
                  <td class="text-left">    Ramesh Devmane </td>
                  <td class="text-left">devmane.ramesh@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">14.98.14.194</td>
                  <td class="text-left">09/07/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=28026&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=28026" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="335905">
                    </td>
                  <td class="text-left">    ROSHAN  KHANDARE</td>
                  <td class="text-left">roshankhandare@yahoo.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">25/07/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=335905&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=335905" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="259030">
                    </td>
                  <td class="text-left">   Bunty Anthony</td>
                  <td class="text-left">Buntyanthony7040463929@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">24/03/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=259030&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=259030" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="422957">
                    </td>
                  <td class="text-left">   DDA FLATSMamjit Duggal</td>
                  <td class="text-left">manjitduggal48@yahoo.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">14/01/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=422957&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=422957" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="301876">
                    </td>
                  <td class="text-left">   Hmm sabir</td>
                  <td class="text-left">vl@purelyherbs.in</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">02/06/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=301876&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=301876" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="556653">
                    </td>
                  <td class="text-left">   Raj S Arkasali NA</td>
                  <td class="text-left">arkasali69@gmail.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">52.66.185.133</td>
                  <td class="text-left">25/08/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=556653&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=556653" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="181603">
                    </td>
                  <td class="text-left">   Ram Naghera</td>
                  <td class="text-left">Ramahir2298@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">31/10/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=181603&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=181603" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="80134">
                    </td>
                  <td class="text-left">   Ramjit  Mardi</td>
                  <td class="text-left">bams.sec@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">01/11/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=80134&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=80134" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="134548">
                    </td>
                  <td class="text-left">   Sreenivas B N</td>
                  <td class="text-left">Sukruthaintl@yahoo.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">24/03/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=134548&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=134548" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="325709">
                    </td>
                  <td class="text-left">  ABHISHEK  KUMAR</td>
                  <td class="text-left">abhishek.kumar394@yahoo.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">13/07/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=325709&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=325709" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="156702">
                    </td>
                  <td class="text-left">  Anand kumar Jha</td>
                  <td class="text-left">anandkumarjha1987@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">16/06/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=156702&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=156702" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="427221">
                    </td>
                  <td class="text-left">  B L R  Prasad</td>
                  <td class="text-left">blrprasad1975@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">23/01/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=427221&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=427221" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="153484">
                    </td>
                  <td class="text-left">  Biswajit  De</td>
                  <td class="text-left">biswajitdey.766.bd@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">182.76.4.161</td>
                  <td class="text-left">06/06/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=153484&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=153484" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="101060">
                    </td>
                  <td class="text-left">  bv bn  jn ihuyh</td>
                  <td class="text-left">mishra.rakesh146@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">12/01/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=101060&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=101060" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="410284">
                    </td>
                  <td class="text-left">  Debabrata Bhattacharyya</td>
                  <td class="text-left">debu_bhattacharyya@yahoo.co.in</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">12/12/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=410284&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=410284" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="468679">
                    </td>
                  <td class="text-left">  Dr.Chandrabose Damodharan</td>
                  <td class="text-left">chandrabosed1954@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">106.51.77.185</td>
                  <td class="text-left">12/05/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=468679&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=468679" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="23348">
                    </td>
                  <td class="text-left">  gulshad   gulshad</td>
                  <td class="text-left">gulshad@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">25/06/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=23348&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=23348" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="23347">
                    </td>
                  <td class="text-left">  gulshad  gulshad</td>
                  <td class="text-left">gulshad.sbl@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">25/06/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=23347&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=23347" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="154721">
                    </td>
                  <td class="text-left">  H J</td>
                  <td class="text-left">Hasnatsk4321@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">10/06/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=154721&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=154721" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="302142">
                    </td>
                  <td class="text-left">  Hlo Chamkour sidhu </td>
                  <td class="text-left">vl@purelyherbs.in</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">02/06/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=302142&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=302142" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="509022">
                    </td>
                  <td class="text-left">  Homtara  Terangpi  NA</td>
                  <td class="text-left">homtaraterangpi@8688.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">106.51.77.185</td>
                  <td class="text-left">02/07/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=509022&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=509022" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="268426">
                    </td>
                  <td class="text-left">  irfan                          taibani</td>
                  <td class="text-left">irfanaqsa77@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">16/04/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=268426&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=268426" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="110249">
                    </td>
                  <td class="text-left">  jai   kumar kumar</td>
                  <td class="text-left">86pkumar@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">06/02/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=110249&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=110249" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="322539">
                    </td>
                  <td class="text-left">  JAYA PRAKASH</td>
                  <td class="text-left">jpmeenchira@yahoo.co.in</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">08/07/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=322539&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=322539" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="553736">
                    </td>
                  <td class="text-left">  Karthip NA</td>
                  <td class="text-left">karthip198621@gmail.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">52.66.185.133</td>
                  <td class="text-left">22/08/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=553736&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=553736" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="467348">
                    </td>
                  <td class="text-left">  Kumar R</td>
                  <td class="text-left">Kumar_ambattur@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">157.51.230.162</td>
                  <td class="text-left">09/05/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=467348&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=467348" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="425742">
                    </td>
                  <td class="text-left">  M.habeeb nisha Nisha</td>
                  <td class="text-left">nishaubaish3301@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">20/01/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=425742&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=425742" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="71453">
                    </td>
                  <td class="text-left">  MAHESH BAID</td>
                  <td class="text-left">maheshbaid17@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">15/10/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=71453&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=71453" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="151533">
                    </td>
                  <td class="text-left">  Manan Lulla</td>
                  <td class="text-left">Vishwadeeptuteja297@gmail.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">06/05/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=151533&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=151533" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="496360">
                    </td>
                  <td class="text-left">  Muhammed  A N NA</td>
                  <td class="text-left">rasiyamuhsin98@gmail.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">52.66.185.133</td>
                  <td class="text-left">17/06/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=496360&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=496360" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="522094">
                    </td>
                  <td class="text-left">  NA</td>
                  <td class="text-left">shamalav@13gmail.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">106.51.77.185</td>
                  <td class="text-left">15/07/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=522094&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=522094" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="565010">
                    </td>
                  <td class="text-left">  NA</td>
                  <td class="text-left">kunwar@gmail.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">52.66.185.133</td>
                  <td class="text-left">04/09/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=565010&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=565010" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="399469">
                    </td>
                  <td class="text-left">  Naren Thiran</td>
                  <td class="text-left">narenmanidme1997@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">11/11/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=399469&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=399469" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="430643">
                    </td>
                  <td class="text-left">  NARENDER  KUMAR  NEELAM </td>
                  <td class="text-left">narendermudiraj@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">04/02/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=430643&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=430643" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="419695">
                    </td>
                  <td class="text-left">  Naveen T S Naveen</td>
                  <td class="text-left">naveenchanni@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">04/01/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=419695&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=419695" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="522152">
                    </td>
                  <td class="text-left">  Navya  B N </td>
                  <td class="text-left">Navyananaiah@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">106.51.77.185</td>
                  <td class="text-left">15/07/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=522152&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=522152" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="179404">
                    </td>
                  <td class="text-left">  Nikhil  Birla </td>
                  <td class="text-left">nikhilbirla3027@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">26/10/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=179404&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=179404" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="84184">
                    </td>
                  <td class="text-left">  preeti  surya</td>
                  <td class="text-left">parisurya4@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">07/11/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=84184&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=84184" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="308034">
                    </td>
                  <td class="text-left">  Radhika kymari ..</td>
                  <td class="text-left">Please Enter the Email ID</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">21/06/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=308034&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=308034" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="109573">
                    </td>
                  <td class="text-left">  Sarfaraz    chappu</td>
                  <td class="text-left">Sarfarazadyar@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">05/02/2017</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=109573&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=109573" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="55811">
                    </td>
                  <td class="text-left">  shirin  nachan</td>
                  <td class="text-left">sheefaf@yahoo.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">14/09/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=55811&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=55811" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="27257">
                    </td>
                  <td class="text-left">  Shruti Gulati</td>
                  <td class="text-left">Shrutinayyar89@gmail.com</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">07/07/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=27257&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=27257" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="32287">
                    </td>
                  <td class="text-left">  YFYF YG7GB</td>
                  <td class="text-left">GURMIOOO1@GMAIL.COM</td>
                  <td class="text-left">Landing Page</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.99.74</td>
                  <td class="text-left">22/07/2016</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=32287&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=32287" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="552726">
                    </td>
                  <td class="text-left">  ग जा न न दे व क र  NA</td>
                  <td class="text-left">gpdeokar@mail.com</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">52.66.185.133</td>
                  <td class="text-left">21/08/2019</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=552726&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=552726" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="301735">
                    </td>
                  <td class="text-left"> 6822 singh</td>
                  <td class="text-left">vl@purelyherbs.in</td>
                  <td class="text-left">Website</td>
                  <td class="text-left">Enabled</td>
                  <td class="text-left">172.16.102.147</td>
                  <td class="text-left">01/06/2018</td>
                  <td class="text-right">                    <button type="button" class="btn btn-success" disabled=""><i class="fa fa-thumbs-o-up"></i></button>
                                        <div class="btn-group" data-toggle="tooltip" title="" data-original-title="Login into Store">
                      <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-lock"></i></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/login&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=301735&amp;store_id=0" target="_blank">Default</a></li>
                                              </ul>
                    </div>
                                        <button type="button" class="btn btn-warning" disabled=""><i class="fa fa-unlock"></i></button>
                                        <a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer/edit&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;customer_id=301735" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><ul class="pagination"><li class="active"><span>1</span></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">2</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=3">3</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=4">4</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=5">5</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=6">6</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=7">7</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=8">8</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=9">9</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=2">&gt;</a></li><li><a href="http://asianherbs.in/salesadminadmin/index.php?route=customer/customer&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;page=11718">&gt;|</a></li></ul></div>
          <div class="col-sm-6 text-right">Showing 1 to 50 of 585863 (11718 Pages)</div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=customer/customer&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_email = $('input[name=\'filter_email\']').val();

	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}

	var filter_customer_group_id = $('select[name=\'filter_customer_group_id\']').val();

	if (filter_customer_group_id != '*') {
		url += '&filter_customer_group_id=' + encodeURIComponent(filter_customer_group_id);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

	var filter_approved = $('select[name=\'filter_approved\']').val();

	if (filter_approved != '*') {
		url += '&filter_approved=' + encodeURIComponent(filter_approved);
	}

	var filter_ip = $('input[name=\'filter_ip\']').val();

	if (filter_ip) {
		url += '&filter_ip=' + encodeURIComponent(filter_ip);
	}

	var filter_date_added = $('input[name=\'filter_date_added\']').val();

	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_email\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&filter_email=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['email'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_email\']').val(item['label']);
	}
});
//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php include ('../includes/footer.php'); ?>
</div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Customers_files/classie.js.download" type="text/javascript"></script>
<script src="./Customers_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE -->
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
