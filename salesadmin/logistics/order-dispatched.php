<!DOCTYPE html>
<!-- saved from url=(0103)http://asianherbs.in/salesadminadmin/index.php?route=logistics1/ship&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Orders</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../includes/head.php'); ?>
<?php include ('../includes/side-nav.php'); ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1 style="color:black;">Orders Shipped</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/ship&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Orders</a></li>
              </ul>


  <div class="pull-right"><a href="javascript:void(0)" id="button-download-btn" class="btn btn-success" onclick="GetReports()" data-toggle="tooltip" title="" data-original-title="Preview Report files"><i class="fa fa-eye"></i></a></div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Shipped Order List</h3>
      </div>
      <div class="panel-body">

      <form method="post" enctype="multipart/form-data" id="form-order">

        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
             <div class="col-sm-12">
                <div class="funkyradio">
                                 </div>     <!--API checkbox ends here-->
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label" for="input-order-id">Order ID</label>
                  <input type="text" name="order_id" value="" placeholder="Order ID" id="input-order-id" class="form-control">
                </div>  <!--order Id textbox ends here-->
				<div id="msg"></div>
              </div>
          </div>
        </div>
      </form>
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
             <div class="col-sm-12">
                <!-- <div class="form-group"> -->

                <div class="funkyradio">
                                     <div class="funkyradio-success" style="width: 190px;display: inline-block;">
                       <input type="radio">
                        <label id="afterapi4"> ECOM EXPRESS(2)</label>
                    </div>
                                      <div class="funkyradio-success" style="width: 190px;display: inline-block;">
                       <input type="radio">
                        <label id="afterapi18"> XPRESSBEES(3)</label>
                    </div>
                                  </div>
              </div>
          </div>
        </div>
          <!-- ORDER TAB START HERE -->
      </div>
    </div>
  </div>
  <script type="text/javascript" src="./Order Dispatched_files/notify.js.download"></script>
<link href="./Order Dispatched_files/notify.css" media="all" rel="stylesheet" type="text/css">


<script type="text/javascript">
$('#form-order').on('submit', function(e) {
	e.preventDefault();
    var api_id=$('input[name="api_id"]:checked', '#form-order').val();
    if(api_id){
        var order_id = $('input[name=\'order_id\']').val();
          if(order_id){
              $.ajax({
                url: 'index.php?route=logistics1/ship/updateShippedStatus&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&api_id='+api_id,
                dataType: 'json',
                success: function(json) {
                      $('.alert').remove();


          PNotify.prototype.options.styling = "bootstrap3";

          if (json['success']) {
				      	//var siteurl = $(location).attr('protocol')+'//'+$(location).attr('hostname');
    						/*$.ajax({
    							url: siteurl+'/inventory/admin/opencart/sell?token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&api_id='+api_id,
    							dataType: 'json',
    							success: function(json) {
    							Lobibox.notify('success', {
    							size: 'mini',
    							delayIndicator: false,
    							position: 'top right',
    							msg: 'OrderID '+order_id+' successfully added to Inventory System'
    					   	});
    							}
    						  });*/


          						$('#msg').prepend('<span class="alert alert-success">' + json['success'] + '</span>');
          						new PNotify({title: 'Success', text: json['success'],type: 'success',icon: 'ui-icon ui-icon-flag'});
          } else {

						$('#msg').prepend('<span class="alert alert-danger">' + json['error'] + '</span>');
						new PNotify({title: 'Error Occured', text: json['error'], type: 'sticky', icon: 'ui-icon ui-icon-signal-diag'});


					}


					$("#input-order-id").val("");


					$('#beforeapi'+json['api_id']).html(json['before']);
					$('#afterapi'+json['api_id']).html(json['after']);

                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });


          } else {
             alert("Please enter valid Order Id ...!");
          }


    } else {
       alert("Please select a Courier Company first...!");
    }

});

function show_stack_context(type, modal) {
    if (typeof stack_context === "undefined") stack_context = {
        "dir1": "down",
        "dir2": "left",
        "context": $("#stack-context")
    };
    if (typeof stack_context_modal === "undefined") stack_context_modal = {
        "dir1": "down",
        "dir2": "left",
        "context": $("#stack-context"),
        "modal": true,
        "overlay_close": true
    };
    var opts = {
        title: "Over Here",
        text: "Check me out. I'm in a different stack.",
        stack: modal ? stack_context_modal : stack_context,
        addclass: modal ? "stack-modal" : ""
    };
    switch (type) {
    case 'error':
        opts.title = "Oh No";
        opts.text = "Watch out for that water tower!";
        opts.type = "error";
        break;
    case 'info':
        opts.title = "Breaking News";
        opts.text = "Have you met Ted?";
        opts.type = "info";
        break;
    case 'success':
        opts.title = "Good News Everyone";
        opts.text = "I've invented a device that bites shiny metal asses.";
        opts.type = "success";
        break;
    }
    new PNotify(opts);
}

</script>







  <script src="./Order Dispatched_files/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
  <link href="./Order Dispatched_files/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script>
</div>





<style type="text/css">
@import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css')
.funkyradio div {
  clear: both;
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 2.5em;
  text-indent: 3.25em;
  margin-top: 2em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  /*background: #f00;*/
  border-radius: 3px 0 0 3px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #777;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}

.funkyradio-danger input[type="radio"]:checked ~ label:before,
.funkyradio-danger input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #d9534f;
}

.funkyradio-warning input[type="radio"]:checked ~ label:before,
.funkyradio-warning input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #f0ad4e;
}

.funkyradio-info input[type="radio"]:checked ~ label:before,
.funkyradio-info input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5bc0de;
}
</style>

<style type="text/css">

.rtp_dsp{border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin:-11px 0px;}
.rtp_dsp thead td{font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#00b050;font-weight:bold;text-align:center;padding:7px;color:#222222;}
.rtp_dsp tbody td{font-size:12px;color:#000;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:center;padding:7px 7px;background-color:#fff;}
.rtp_dsp tfoot tr td{font-size:12px;color:#e5e5e5;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color: #3a3838;text-align:center;padding:7px}
</style>



<script type="text/javascript">

  function GetReports(){

           $.ajax({
              url: 'index.php?route=logistics1/ship/files&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&action=preview',
              type: 'post',
              dataType: 'json',
              beforeSend: function() {
                $('#input-order-tl').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
              },
              complete: function() {
                $('.fa-spin').remove();
              },
              success: function(json) {
               // alert(json['result']);
               var html = '';

              //var fhtml = '';
              html += '<thead><tr><td>#1. Orders</td><td colspan="3">1st November, 2019 To Till Date</td><td colspan="3">Previous Day Dispatched</td><td colspan="3">Today Dispatched</td><td colspan="3">Todays Delivered</td><td colspan="3">Today RTO</td></tr><tr><td>Couriers</td><td>Count</td><td>Amount</td><td>%</td><td>Count</td><td>Amount</td><td>%</td><td>Count</td><td>Amount</td><td>%</td><td>Count</td><td>Amount</td><td>%</td><td>Count</td><td>Amount</td><td>%</td></tr></thead>';
              html += '<tbody>';
               if (json['report'] && json['report'] != '') {
                  for (i = 0; i < json['report'].length; i++) {
                      html += '<tr>';
                      html += '<td style="background-color:#ffff00">'+json['report'][i]['courier']+'</td>';
                      html += '<td>'+json['report'][i]['mothly_total']+'</td>';
                      html += '<td>Rs.'+json['report'][i]['mothly_amt']+'</td>';
                      html += '<td>'+(json['report'][i]['mothly_total']/json['total']['m_tl']*100).toFixed(2)+'%</td>';
                       html += '<td>'+json['report'][i]['previous_total']+'</td>';
                      html += '<td>Rs.'+json['report'][i]['previous_amt']+'</td>';
                      html += '<td>'+(json['report'][i]['previous_total']/json['total']['pr_tl']*100).toFixed(2)+'%</td>';
                       html += '<td>'+json['report'][i]['current_total']+'</td>';
                      html += '<td>Rs.'+json['report'][i]['current_amt']+'</td>';
                      html += '<td>'+(json['report'][i]['current_total']/json['total']['cr_t']*100).toFixed(2)+'%</td>';
                       html += '<td>'+json['report'][i]['del_total']+'</td>';
                      html += '<td>Rs.'+json['report'][i]['del_amt']+'</td>';
                      html += '<td>'+(json['report'][i]['del_total']/json['total']['dl_t']*100).toFixed(2)+'%</td>';
                      html += '<td>'+json['report'][i]['rto_total']+'</td>';
                      html += '<td>Rs.'+json['report'][i]['rto_amt']+'</td>';
                      html += '<td>'+(json['report'][i]['rto_total']/json['total']['rt_t'] *100).toFixed(2)+'%</td>';
                      html += '</tr>';
                  }
             }

           html += '</tbody>';
             html += '<tfoot>';
             if (json['total'] && json['total'] != '') {
                      html += '<tr>';
                      html += '<td style="background-color:#312e2e">Total</td>';
                      html += '<td>'+json['total']['m_tl']+'</td>';
                      html += '<td>Rs.'+(json['total']['m_a']).toFixed(2)+'</td>';
                      html += '<td>100%</td>';
                       html += '<td>'+json['total']['pr_tl']+'</td>';
                      html += '<td>Rs.'+(json['total']['pr_a']).toFixed(2)+'</td>';
                      html += '<td>100%</td>';
                       html += '<td>'+json['total']['cr_t']+'</td>';
                      html += '<td>Rs.'+(json['total']['cr_a']).toFixed(2)+'</td>';
                      html += '<td>100%</td>';
                       html += '<td>'+json['total']['dl_t']+'</td>';
                      html += '<td>Rs.'+(json['total']['dl_a']).toFixed(2)+'</td>';
                      html += '<td>100%</td>';
                      html += '<td>'+json['total']['rt_t']+'</td>';
                      html += '<td>Rs.'+(json['total']['rt_a']).toFixed(2)+'</td>';
                      html += '<td>100%</td>';
                      html += '</tr>';

             }
              html += '</tfoot>';

             $('#shrpt').html(html);
            // $('#sh_total').html(fhtml);


                    $('#element_to_pop_up2').bPopup({
                        easing: 'easeOutBack', //uses jQuery easing plugin
                        speed: 1000,
                        position: [150, 150], //x, y
                        transition: 'slideDown'
                    });

              },
              error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
           });



}





</script>
<script src="./Order Dispatched_files/jquery.bpopup.min.js.download" type="text/javascript"></script>
<script src="./Order Dispatched_files/jquery.easing.1.3.js.download" type="text/javascript"></script>
<script src="./Order Dispatched_files/tokens.js.download" type="text/javascript"></script>

    <div id="element_to_pop_up2" style="width: 54%;"> <a class="b-close" title="Close" style="padding: 9px 15px;">x</a>
      <div class="request-a-call">
        <h2>Purelyherbs Logistics Activities Reports</h2>
        <p id="info-adj-alert"></p>
        <div style="display: inline-block;">
            <table class="rtp_dsp" id="shrpt"></table>
        </div>
          <!--  <form id="info-adj-price-form" method="post">
                    <input type="hidden" name="order_id" id="amt_ord_id" value="">
                    <div class="form-group">
                    <div class="col-sm-12"><strong>Amount:</strong><br/>
                        <input type="text" name="total" value=""  id="payable-amt" class="form-control">
                    </div>
                    <div class="col-sm-12" style="margin-top: 15px;"><strong>Instruction:</strong><br/>
                    <textarea name="instruction" rows="8" id="input-amt-template" class="form-control"></textarea>
                    </div>
                    <div class="col-sm-12">
                       <button  id="button-edit-adj-price" type="button" style="float: right;margin: 10px 0px;" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Click To Updated</button>
                    </div>
                    </div>
                </form> -->


        </div>
    </div>

<!-- Ajax PopUP; -->


<?php include ('../includes/footer.php'); ?>
</div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Order Dispatched_files/classie.js.download" type="text/javascript"></script>
<script src="./Order Dispatched_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
