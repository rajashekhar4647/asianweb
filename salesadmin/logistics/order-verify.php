<!DOCTYPE html>
<!-- saved from url=(0104)http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Orders</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">

<?php include ('../includes/head.php'); ?>
<?php include ('../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
     <!--  <div class="pull-right">
        <button type="submit" id="button-shipping" form="form-order" formaction="" data-toggle="tooltip" title="Print Shipping List" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="" data-toggle="tooltip" title="Print Invoice" class="btn btn-info"><i class="fa fa-print"></i></button>
        <a href="" data-toggle="tooltip" title="Add New" class="btn btn-primary"><i class="fa fa-plus"></i></a></div> -->
      <h1 style="color:black;">Orders</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Orders</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Order List</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-order-id">Order ID</label>
                  <input type="text" name="filter_order_id" value="" placeholder="Order ID" id="input-order-id" class="form-control">
                </div>
               <!--<div class="form-group">
                  <label class="control-label" for="input-customer">Customer</label>
                  <input type="text" name="filter_customer" value="" placeholder="Customer" id="input-customer" class="form-control" />
                </div> -->
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-telephone">Telephone</label>
                  <input type="text" name="filter_telephone" value="" placeholder="Telephone" id="input-telephone" class="form-control">
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-order-status">Order Status</label>
                  <select name="filter_order_status" id="input-order-status" class="form-control">
                    <option value="*"></option>






                        							<option value="35">Aproval Pending</option>






                        							<option value="7">Canceled</option>










                        							<option value="17">COD &nbsp;Confirmed</option>






                        							<option value="37">Completed</option>










                        							<option value="8">Customer Refuse To Accept</option>






                        							<option value="22">Delivered</option>






                        							<option value="36">Disapproved</option>






                        							<option value="30">Duplicate Order</option>






                        							<option value="40">Entry Blocked - Price Issue</option>










                        							<option value="39">Incomplete Address</option>






                        							<option value="28">Lead</option>










                        							<option value="27">Non - serviceable area</option>










                        							<option value="34">On hold</option>














                        							<option value="1">Pending</option>






                        							<option value="18">Pick Up Generated</option>






                        							<option value="19">Prepaid</option>














                        							<option value="26">Re Order - dont use it</option>






















                        							<option value="14">RTO-Returned To Origin</option>














                        							<option value="3">Shipped</option>






                        							<option value="33">UnConfirmed</option>
















                                      </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="input-date-type1">Date Type</label>
                    <select name="filter_date_type" id="input-date-type" class="form-control">
                         <option selected="selected" disabled="disabled"> Select Date Type</option>
                              <option value="1">Date Added</option>
                              <option value="2">Date Modified</option>
                              <option value="3">Date Assigned</option>
                              <option value="4" selected="selected">Date Order Converted</option>
                              <option value="5">Date Order Dispatched</option>
                       </select>
                  </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-from-date1">From Date</label>
                  <div class="input-group date">
                    <input type="text" name="filter_from_date" value="2019-11-04" placeholder="From Date" data-date-format="YYYY-MM-DD" id="input-from-date" class="form-control">
                    <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-to-date1">To Date</label>
                  <div class="input-group date">
                  <input type="text" name="filter_to_date" value="2019-11-04" placeholder="To Date" data-date-format="YYYY-MM-DD" id="input-to-date" class="form-control">
                  <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>
                </div>

               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
                  <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="padding: 8px 2px;" class="btn btn-alt btn-warning"><i class="fa fa-refresh"></i> Refresh</a>
               </div>
              </div>
          </div>
        </div>


        <div class="row">
          <div class="col-sm-12 text-center">

            <a id="return-order" data-toggle="tooltip" title="" class="btn btn-warning btnw btn-sm pointer" data-original-title="Today&#39;s Returned Orders">Returned(0)
			</a><a id="cancel" class="btn1 btn-danger btnw btn-sm pointer"> Canceled (0)</a>
            <a id="verify" class="btn1 btn-success btnw btn-sm pointer"> Verified(3)</a>
            <a id="edit-btn" class="btn1 btn-primary btnw btn-sm pointer"> Edit Address(0)</a>
		</div>
        </div>


          <!-- ORDER TAB START HERE -->
          <ul class="nav nav-tabs dboard">
            <li class="active"><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;filter_payment_mode=lead&amp;order=ASC">LEAD Orders</a></li>
            <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;filter_payment_mode=cod&amp;order=ASC">COD Orders</a></li>
            <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;filter_payment_mode=payu&amp;order=ASC">PREPAID Orders</a></li>
			<li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;filter_payment_mode=bank_transfer&amp;order=ASC">Bank Transfer/PayTM</a></li>
          </ul>
          <div class="tab-content">
              <div class="tab-pane active "><h2>LEAD</h2>
                <div class="row">
                  <div class="col-sm-4 text-left"></div>
                  <div class="col-sm-4 text-center text-success text-large"><h5 id="msg-text"></h5></div>
                  <div class="col-sm-4 text-right">
				  Showing 0 to 0 of 0 (0 Pages)				  <div class="text-right" id="selected-order">0 order Selected.</div>
				  </div>
                </div>
                <form method="post" enctype="multipart/form-data" target="_blank" id="form-order1">
                  <div class="table-responsive">
                    <table class="table table-bordered logistcs001 table-hover">
                      <thead>
                        <tr>
                          <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$(&#39;input[name*=\&#39;selected\&#39;]&#39;).prop(&#39;checked&#39;, this.checked);"></td>
                          <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.order_id&amp;order=ASC" class="desc">Order ID</a>
                            </td>
                          <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=customer&amp;order=ASC">Customer</a>
                            </td>
                          <td class="text-left"><a>Addresses</a></td>
                          <td class="text-left"><a>Product Detail</a></td>
                            <!-- <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=status&amp;order=ASC">Status</a>
                            </td>
                          <td class="text-right">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.total&amp;order=ASC">Total</a>
                            </td>-->
                          <td class="text-center"><a>Serviceable</a></td>

                         <!--  <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.date_added&amp;order=ASC">Date Added</a>
                            </td>-->
                          <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/order&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.date_modified&amp;order=ASC">Date Modified</a>
                            </td>
                          <td class="text-center">Action</td>
                        </tr>
                      </thead>
                      <tbody>

                                                <tr>
                          <td class="text-center" colspan="9">No results!</td>
                        </tr>
                                              </tbody>
                    </table>
                  </div>
                </form>
                <div class="row">
                  <div class="col-sm-6 text-left"></div>
                  <div class="col-sm-6 text-right">Showing 0 to 0 of 0 (0 Pages)</div>
                </div>
              </div>
           </div>
          <!-- ORDER TAB START HERE -->

      </div>
    </div>
  </div>




<!-- POPUP BOX START HERE -->

<div id="element_to_pop_up2"><a class="b-close" title="Close">x</a>
<div class="request-a-call" id="result001"></div>
</div>

<div id="element_to_pop_up">
    <a class="b-close" title="Close">x</a>
    <div class="request-a-call" id="results002" style="max-height: 600px; overflow-y: scroll;"></div>
</div>



<div class="md-modal md-effect-1" id="modal-1" style="max-height: 450px; top: 40%; ">
  <div class="md-content" id="address-1">
      <div style="padding:10px;" id="html_results">
      </div>
     <button style="position: absolute; right: 0px; top: 0;" class="md-close btn btn-info">Close me!</button>
  </div>
</div>


 <div id="element_to_pop_up3">
    <a class="b-close close-button" title="Close">x</a>
    <div class="request-a-call" id="results003" style="max-height: 600px; overflow-y: scroll;"></div>
</div>

<div class="md-overlay"></div><!-- the overlay element -->
<!-- POPUP BOX END HERE -->





<script src="./Order Verify_files/jquery.bpopup.min.js.download" type="text/javascript"></script>
<script src="./Order Verify_files/jquery.easing.1.3.js.download" type="text/javascript"></script>
<script src="./Order Verify_files/tokens.js.download" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="./Order Verify_files/dataTables.bootstrap.min.css">


<script src="./Order Verify_files/jquery.dataTables.min.js.download" type="text/javascript"></script>
<script src="./Order Verify_files/dataTables.bootstrap.min.js.download" type="text/javascript"></script>

<script type="text/javascript">
$(document).delegate('#return-order,#cancel,#verify,#edit-btn', 'click', function() {
	url='';
	var filter_order_id = $('input[name=\'filter_order_id\']').val();
	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}

	var filter_telephone = $('input[name=\'filter_telephone\']').val();
	if (filter_telephone) {
	url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
	}
	var filter_date_type = $('select[name=\'filter_date_type\']').val();
	if (filter_date_type) {
	url += '&filter_date_type=' + encodeURIComponent(filter_date_type);
	}
	var filter_from_date = $('input[name=\'filter_from_date\']').val();
	if (filter_from_date) {
	url += '&filter_from_date=' + encodeURIComponent(filter_from_date);
	}
	var filter_to_date = $('input[name=\'filter_to_date\']').val();
	if (filter_to_date) {
	url += '&filter_to_date=' + encodeURIComponent(filter_to_date);
	}

	var filter_customer = $('input[name=\'filter_customer\']').val();
	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}

	var filter_order_status = $('select[name=\'filter_order_status\']').val();

	if (filter_order_status != '*') {
		url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
	}

	var filter_total = $('input[name=\'filter_total\']').val();

	if (filter_total) {
		url += '&filter_total=' + encodeURIComponent(filter_total);
	}

	var filter_date_added = $('input[name=\'filter_date_added\']').val();

	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}

  var buttonid = this.id;
  if(buttonid == 'cancel')
	var query_string = '&canceled_order=1';
  if(buttonid == 'verify')
	var query_string = '&verify_status=1';
  if(buttonid == 'edit-btn')
	var query_string = '&address_status=1';
  var url = 'index.php?route=logistics1/order&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA'+url+query_string;

  if(buttonid == 'return-order')
	var url = 'index.php?route=logistics1/order/ShowReturnedEdited&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA'+url+'&buttonid='+buttonid;

         $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
            $('#result001').html(json['html_success']);
           // alert(json['html_success']);
           $('#element_to_pop_up2').bPopup({
              easing: 'easeOutBack', //uses jQuery easing plugin
              speed: 1000,
              transition: 'slideDown'
            });

            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });

});

function statuschanges(order_id, button, status){
         $.ajax({
            url: 'index.php?route=logistics1/order/ChangeStatusByReturned&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&button='+button+'&status='+status,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
            $('#result001').html(json['html_success']);
           // alert(json['html_success']);
           $('#element_to_pop_up2').bPopup({
              easing: 'easeOutBack', //uses jQuery easing plugin
              speed: 1000,
              transition: 'slideDown'
            });

            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });

}

function getduplicateOrders(order_id, email, telephone){
         $.ajax({
            url: 'index.php?route=logistics1/order/duplicateOrder&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&email='+email+'&telephone='+telephone,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
            $('#results002').html(json['html_success']);
           // alert(json['html_success']);
          $('#element_to_pop_up').bPopup({
              easing: 'easeOutBack', //uses jQuery easing plugin
              speed: 1000,
              transition: 'slideDown'
            });

            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });

}


function findaddressupdation(i){
         $.ajax({
            url: 'index.php?route=logistics1/order/updations&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+i,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
           // alert(json['html_success']);
              $('#html_results').html(json['html_success']);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });

}

function changeStatus(order_id, button, status){
 // alert(order_id+button+status);
       $.ajax({
          url: 'index.php?route=logistics1/order/ChangeStatusByButton&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&order_status_id='+status+'&button='+button,
          dataType: 'json',
          success: function(json) {
             html=json['success'];
            // alert(json['success']);
             $('#msg-text').html(html);
             $("#form-order1 table tbody #id"+order_id).fadeOut('slow');//addClass("remove");
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
}
</script>




<script type="text/javascript">
$(document).delegate('#button-history', 'click', function() {
var ppost = $("#input-comment").validationEngine('validate');
var selection = $("select[name=\'order_status_id\']").validationEngine('validate');
if(ppost && selection){
$.ajax({
    url: 'index.php?route=logistics1/order/changeOrderHistory&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA',
    type: 'post',
    dataType: 'json',
    data: 'order_id=' + encodeURIComponent($('input[name=\'order_id\']').val()) +'&order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&button=' + encodeURIComponent($('input[name=\'order_button\']').val())+'&status=' + encodeURIComponent($('input[name=\'order_status\']').val())+'&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
    beforeSend: function() {
      $('#button-history').button('loading');
    },
    complete: function() {
      $('#button-history').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }

      if (json['success']) {
         $('textarea[name=\'comment\']').val('');
         $('.b-close').click();
      }
	  var id= encodeURIComponent($('input[name=\'order_id\']').val());

	  $("#form-order1 table tbody #id"+id).fadeOut('slow');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
}
});

function refresh_ship(order_id){
 var host = window.location.hostname;
 var url ='https://'+host+'/API/response.php?order_id='+order_id;
		 $.ajax({
          url: url,
          dataType: 'json',
          success: function(json) {
            $("#loading3").hide();
			$("#loading4").show();
			Lobibox.notify('warning', {
						size: 'mini',
						delayIndicator: false,
			    		position: 'top right',
						msg: 'Courier Info is updating please wait....'
					});
			setTimeout(function(){
			window.location.href = window.location.href
			},3000);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });


}

</script>

<script type="text/javascript">
$('#button-filter').on('click', function() {
	url = 'index.php?route=logistics1/order&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

	var filter_order_id = $('input[name=\'filter_order_id\']').val();
	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}


  var filter_telephone = $('input[name=\'filter_telephone\']').val();
  if (filter_telephone) {
    url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
  }
  var filter_date_type = $('select[name=\'filter_date_type\']').val();
  if (filter_date_type) {
    url += '&filter_date_type=' + encodeURIComponent(filter_date_type);
  }
  var filter_from_date = $('input[name=\'filter_from_date\']').val();
  if (filter_from_date) {
    url += '&filter_from_date=' + encodeURIComponent(filter_from_date);
  }
  var filter_to_date = $('input[name=\'filter_to_date\']').val();
  if (filter_to_date) {
    url += '&filter_to_date=' + encodeURIComponent(filter_to_date);
  }

	var filter_customer = $('input[name=\'filter_customer\']').val();
	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}

	var filter_order_status = $('select[name=\'filter_order_status\']').val();

	if (filter_order_status != '*') {
		url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
	}

	var filter_total = $('input[name=\'filter_total\']').val();

	if (filter_total) {
		url += '&filter_total=' + encodeURIComponent(filter_total);
	}

	var filter_date_added = $('input[name=\'filter_date_added\']').val();

	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}

	location = url;
});
//--></script>



  <script src="./Order Verify_files/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
  <link href="./Order Verify_files/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
  <script type="text/javascript"><!--
	$('.date').datetimepicker({
		pickTime: false
	});

	var countChecked = function() {
		//alert($("input:checked").attr('name'));
	  var n = $( ".checkbox-select:checked" ).length;
	  var n1 = n-1;
	  $( "#selected-order" ).text( n + (n <= 1 ? " order" : " orders") + " Selected." );
	};
	countChecked();

	$( "input[type=checkbox]" ).on( "click", countChecked );

	function getorderinfo(order_id){
			 $.ajax({
				url: 'index.php?route=logistics1/order/ViewOrder&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id,
				type: 'GET',
				crossDomain: true,
				success: function(json) {
				$('#results003').html(json['html_success']);
			   // alert(json['html_success']);
			  $('#element_to_pop_up3').bPopup({
				  easing: 'easeOutBack', //uses jQuery easing plugin
				  speed: 1000,
				  transition: 'slideDown'
				});

				},
				error: function(xhr, ajaxOptions, thrownError) {
				  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			  });

	}
//--></script>
<style type="text/css">
.remove{display: none !important;}
.is_duplicate{padding:0px; margin:0px; position:absolute; border-bottom-left-radius:400px; width:25px; font-size:12px; height:25px; top:0px; right: 0px}
</style>
</div>


<?php include ('../includes/footer.php'); ?>
</div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Order Verify_files/classie.js.download" type="text/javascript"></script>
<script src="./Order Verify_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
