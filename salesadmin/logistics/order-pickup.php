<!DOCTYPE html>
<!-- saved from url=(0105)http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA -->
<html dir="ltr" lang="en" class="gr__purelyherbs_in"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style">div[class*="item-container-obpd"], a[data-redirect*="paid.outbrain.com"], a[onmousedown*="paid.outbrain.com"] { display: none !important; } a div[class*="item-container-ad"] { height: 0px !important; overflow: hidden !important; position: absolute !important; } div[data-item-syndicated="true"] { display: none !important; } .grv_is_sponsored { display: none !important; } .zergnet-widget-related { display: none !important; } </style>

<title>Orders</title>
<!--<base href="http://asianherbs.in/salesadminadmin/">--><base href=".">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script>
	Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
	Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
		iconSource: 'fontAwesome'
	});

</script>
<!-- WASIM CSS FILE ADD OF DUBLICATE ORDER LIST END HERE -->

<script type="text/javascript">
/*function pingServer() {
    $.ajax({ url: location.href });
}
$(document).ready(function() {
    setInterval('pingServer()', 20000);
});*/
</script>



</head>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="margin-top:5%;background:#f0f3fa;">
<div id="container">
<?php include ('../includes/head.php'); ?>
<?php include ('../includes/side-nav.php'); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
       <div class="pull-right">


        <button type="button" id="button-download-btn" class="btn btn-success" data-toggle="tooltip" title="" data-original-title="Download File Click Here"><i class="fa fa-download"></i></button>

        <button type="submit" id="button-manifast" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup/manifest&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Print Manifest"><i class="fa fa-folder"></i></button>
         <button type="submit" id="button-label" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup/label&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Print Label"><i class="fa fa-print"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup/invoice&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Print Invoice"><i class="fa fa-print"></i></button>

        <button type="submit" id="button-pickup" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup/pickupgenerate&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" formtarget="_blank" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Generate Pickup">Generate Pickup</button>



        <!-- <button type="submit" id="button-shipping" form="form-order" formaction="" data-toggle="tooltip" title="" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup/invoice&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" data-toggle="tooltip" title="" class="btn btn-info"><i class="fa fa-print"></i></button>
        <a href="" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus"></i></a> --></div>
      <h1 style="color:black;">Orders Pickup</h1>
      <ul class="breadcrumb">
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=common/dashboard&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="color:black;">Home</a></li>
                <li><a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA">Orders</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Order Pickup Generates</h3>
      </div>
      <div class="panel-body">
        <div class="well" style="background:white;border:0.5px solid black;">
          <div class="row">
              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-order-id">Order ID</label>
                  <input type="text" name="filter_order_id" value="" placeholder="Order ID" id="input-order-id" class="form-control">
                </div>
               <!--<div class="form-group">
                  <label class="control-label" for="input-customer">Customer</label>
                  <input type="text" name="filter_customer" value="" placeholder="Customer" id="input-customer" class="form-control" />
                </div> -->
                <div class="form-group">
                  <label class="control-label" for="input-state1">State</label>
                  <select name="filter_state" id="input-state" class="form-control">
                       <option selected="selected" disabled="disabled">Select State</option>
                                                  <option value="1475">Andaman &amp; Nicobar Islands</option>
                                                   <option value="1476">Andhra Pradesh</option>
                                                   <option value="1477">Arunachal Pradesh</option>
                                                   <option value="1478">Assam</option>
                                                   <option value="1479">Bihar</option>
                                                   <option value="1480">Chandigarh</option>
                                                   <option value="4233">Chhattisgarh</option>
                                                   <option value="1481">Dadra &amp; Nagar Haveli</option>
                                                   <option value="1482">Daman and Diu</option>
                                                   <option value="1483">Delhi</option>
                                                   <option value="1484">Goa</option>
                                                   <option value="1485">Gujarat</option>
                                                   <option value="1486">Haryana</option>
                                                   <option value="1487">Himachal Pradesh</option>
                                                   <option value="1488">Jammu &amp; Kashmir</option>
                                                   <option value="4234">Jharkhand</option>
                                                   <option value="1489">Karnataka</option>
                                                   <option value="1490">Kerala</option>
                                                   <option value="1491">Lakshadweep Islands</option>
                                                   <option value="1492">Madhya Pradesh</option>
                                                   <option value="1493">Maharashtra</option>
                                                   <option value="1494">Manipur</option>
                                                   <option value="1495">Meghalaya</option>
                                                   <option value="1496">Mizoram</option>
                                                   <option value="1497">Nagaland</option>
                                                   <option value="1498">Odisha</option>
                                                   <option value="1499">Puducherry</option>
                                                   <option value="1500">Punjab</option>
                                                   <option value="1501">Rajasthan</option>
                                                   <option value="1502">Sikkim</option>
                                                   <option value="1503">Tamil Nadu</option>
                                                   <option value="4231">Telangana</option>
                                                   <option value="1504">Tripura</option>
                                                   <option value="1505">Uttar Pradesh</option>
                                                   <option value="4232">Uttarakhand</option>
                                                   <option value="1506">West Bengal</option>

                     </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-telephone">Telephone</label>
                  <input type="text" name="filter_telephone" value="" placeholder="Telephone" id="input-telephone" class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label" for="input-courier1">Courier Name</label>
                    <select name="filter_courier" id="input-courier" class="form-control">
                         <option selected="selected" value="">Select Courier Name</option>
                                                      <option value="7">BLUE DART</option>
                                                       <option value="6">DELHIVERY</option>
                                                       <option value="15">DOTZOT</option>
                                                       <option value="4">ECOM EXPRESS</option>
                                                       <option value="13">INDIA POST</option>
                                                       <option value="17">LAST MILE XPRESS</option>
                                                       <option value="22">SELF DELIVERY</option>
                                                       <option value="16">SHIP DELIGHT</option>
                                                       <option value="21">SMARTSHIP</option>
                                                       <option value="12">WOW EXPRESS</option>
                                                       <option value="18">XPRESSBEES</option>

                       </select>
                  </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-order-status">Order Status</label>
                  <select name="filter_order_status" id="input-order-status" class="form-control">
                    <option value="*"></option>
                                                                <option value="41">Address Verified</option>
                                                        <option value="35">Aproval Pending</option>
                                                        <option value="7">Canceled</option>
                                                        <option value="9">Canceled Reversal</option>
                                                        <option value="17">COD &nbsp;Confirmed</option>
                                                        <option value="37">Completed</option>
                                                        <option value="29">Converted Lead</option>
                                                        <option value="8">Customer Refuse To Accept</option>
                                                        <option value="22">Delivered</option>
                                                        <option value="36">Disapproved</option>
                                                        <option value="30">Duplicate Order</option>
                                                        <option value="40">Entry Blocked - Price Issue</option>
                                                        <option value="10">Failed</option>
                                                        <option value="39">Incomplete Address</option>
                                                        <option value="28">Lead</option>
                                                        <option value="23">Miss Route</option>
                                                        <option value="27">Non - serviceable area</option>
                                                        <option value="24">ODA- Out Of Delivery Area</option>
                                                        <option value="34">On hold</option>
                                                        <option value="38">Order Assigned</option>
                                                        <option value="42">Payment Gateway Entry</option>
                                                        <option value="1">Pending</option>
                                                        <option value="18">Pick Up Generated</option>
                                                        <option value="19">Prepaid</option>
                                                        <option value="15">Processed</option>
                                                        <option value="2">Processing</option>
                                                        <option value="26">Re Order - dont use it</option>
                                                        <option value="20">Ready for Dispatch</option>
                                                        <option value="11">Refunded</option>
                                                        <option value="21">Reschedule</option>
                                                        <option value="12">Reversed</option>
                                                        <option value="14">RTO-Returned To Origin</option>
                                                        <option value="43">Shipment Damaged</option>
                                                        <option value="13">Shipment Lost</option>
                                                        <option value="3">Shipped</option>
                                                        <option value="33">UnConfirmed</option>
                                                        <option value="5">Undelivered</option>
                                                        <option value="16">Voided</option>
                                                        <option value="25">Wrong Delivery</option>
                                                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-name">Product Name</label>
                  <input type="text" name="filter_name" value="" placeholder="Product Name" id="input-name" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
                </div>

              </div>

              <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label" for="input-date-type1">Date Type</label>
                    <select name="filter_date_type" id="input-date-type" class="form-control">
                         <option selected="selected" disabled="disabled">Select Date Type</option>
                              <option value="1" selected="selected">Date Added(Pickup)</option>
                              <option value="2">Date Modified</option>
                                                  <option value="5">Date Order Dispatched</option>
                       </select>
                </div>
				<div class="form-group">
                  <label class="control-label" for="filter_order_awb">AWB Number</label>
                  <input type="text" name="filter_order_awb" value="" placeholder="AWB Number" id="filter_order_awb" class="form-control">
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-from-date1">From Date</label>
                  <div class="input-group date">
                    <input type="text" name="filter_from_date" value="2019-11-04" placeholder="From Date" data-date-format="YYYY-MM-DD" id="input-from-date" class="form-control">
                    <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>
                </div>
                 <div class="form-group">
                    <label class="control-label" for="input-pay-mode">Payment Type</label>
                    <select name="filter_pay_mode" id="input-pay-mode" class="form-control">
                         <option value="*" selected="selected">Select Pay Type</option>
                              <option value="lead">Lead</option>
                              <option value="cod">Cod</option>
                              <option value="prepaid">Prepaid</option>
                       </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" for="input-to-date1">To Date</label>
                  <div class="input-group date">
                  <input type="text" name="filter_to_date" value="2019-11-04" placeholder="To Date" data-date-format="YYYY-MM-DD" id="input-to-date" class="form-control">
                  <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>
                </div>
                <div class="form-group">
                 <label class="control-label">&nbsp;</label>
                  <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA" style="padding: 8px 2px;" class="btn btn-alt pull-left btn-warning"><i class="fa fa-refresh"></i> Refresh</a>
                  <button type="button" id="button-filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Filter</button>
                </div>
              </div>
          </div>
        </div>

        <div class="table-responsive">
          <table class="table table-bordered" style="background: #eee;">
            <thead>
            <tr><td style="width: 1px;" class="text-left">COURIER WISE ASSIGNED</td></tr>
            </thead>
             <tbody>
              <tr><td style="width: 80%;" class="text-left">
                                   <label style="padding:0px 10px 0px 0px;"><!-- <input type="radio" name="api_id[]" value="18"/> -->
                    <i style="color: #f00; font-weight: bold;" class="fa fa-angle-double-right"></i> XPRESSBEES(3)                  </label>

               </td></tr>

              </tbody>
          </table>
        </div>


        <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right">Showing 1 to 5 of 5 (1 Pages)      <div class="text-right" id="selected-order">0 order Selected.</div>
      </div>
        </div>
    <div class="row">
        <div class="col-sm-6 text-left" id="selected-order"></div>
    </div>
          <!-- ORDER TAB START HERE -->
          <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
                  <div class="table-responsive">
                    <table class="table table-bordered logistcs001 table-hover">
                      <thead>
                        <tr>
                          <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$(&#39;input[name*=\&#39;selected\&#39;]&#39;).prop(&#39;checked&#39;, this.checked);"></td>
                          <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.order_id&amp;order=ASC" class="desc">Order ID</a>
                            </td>
                          <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=customer&amp;order=ASC">Customer</a>
                            </td>
                          <!-- <td class="text-left"><a>Addresses</a></td> -->
                            <!-- <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=status&amp;order=ASC">Status</a>
                            </td>
                          <td class="text-right">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.total&amp;order=ASC">Total</a>
                            </td>-->
                          <td class="text-center"><a>State</a></td>
                          <td class="text-center">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=aw.api_id&amp;order=ASC">Assigned Courier</a>
                                                      </td>
                          <td class="text-center"><a>Serviceable</a></td>
                          <td class="text-center"><a>Products Details</a></td>

                         <!--  <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.date_added&amp;order=ASC">Date Added</a>
                            </td> -->
                          <td class="text-left">                            <a href="http://asianherbs.in/salesadminadmin/index.php?route=logistics1/pickup&amp;token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&amp;sort=o.date_modified&amp;order=ASC">Date Modified</a>
                            </td>
                          <td class="text-center">Action</td>
                        </tr>
                      </thead>
                      <tbody>

                        						                        <tr id="id99998000562589" class="">
														<td class="text-center">							<input type="checkbox" name="selected[]" class="checkbox-select" value="99998000562589">
														</td>
														<td class="text-left" style="position:relative"><span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562589<br><a id="appr">Pick Up Generated</a>
							<a href="javascript:void(0)" onclick="getorderinfo(&#39;99998000562589&#39;)" data-toggle="tooltip" class="btn btn-warning view-icon-left" data-original-title="View Order"><i class="fa fa-eye"></i></a></td>
							<td class="text-left">Sakshi Rathore<br>sakshirathore665@gmail.com<br>8839486499</td>
                          <!-- <td class="text-left"><a data-toggle="tooltip" title="Payment Address"><i class="fa fa-home" aria-hidden="true"></i> => </a>Sakshi Rathore
C/O: Sakshi Rathore
Ward No.- 32, Jai Hind Chowk, Murwara, Post- Murwara,
Near- Murwara Bridge, Katni
Landmar , Katni, Madhya Pradesh-483501, India<br/><br/><a data-toggle="tooltip" title="Shipping Address"><i class="fa fa-truck"></i> <= </a>Sakshi Rathore
C/O: Sakshi Rathore
Ward No.- 32, Jai Hind Chowk, Murwara, Post- Murwara,
Near- Murwara Bridge, Katni
Landmar, , Katni, Madhya Pradesh-483501, India</td> -->
                          <td class="text-center">Madhya Pradesh</td>
                          <td class="text-center">
                          <strong style="width: 120px;display: inline-block;">
                           <br>ECOM EXPRESS<br>8563545236</strong>
                          </td>
                          <td class="text-center">
                            <a title="ECOM EXPRESS" class="btn btn-info ecom-expess btn-sm">ECE</a><a title="DELHIVERY" class="btn btn-warning delhivery btn-sm">DEV</a><a title="DOTZOT" class="btn btn-info dotzot btn-sm">DZT</a><a title="SMARTSHIP" class="btn btn-info smartship btn-sm">SMS</a>                          </td>
                          <td class="text-left">
                            Garcinia Cambogia Herbs-1 Bottle (Weight Loss - 1)<br><strong>Rs. 2,288</strong></td>
                          <td class="text-left">04/11/2019 13:52:01</td>
                          <td class="text-center">
                             <a class="btn btn-info btnw btn-sm" data-toggle="tooltip" title="" onclick="reassigned(&#39;99998000562589&#39;)" href="javascript:void(0)" data-original-title="Re Assigned Other Courier Campany">Re AWB</a>
                            <button type="button" onclick="statuschanges(&#39;99998000562589&#39;,&#39;cancel&#39;,&#39;7&#39;)" data-toggle="tooltip" title="" class="btn btn-danger btnw btn-sm" data-original-title="Cancel">Cancel</button>
                            <button type="button" onclick="statuschanges(&#39;99998000562589&#39;,&#39;return&#39;,&#39;28&#39;)" data-toggle="tooltip" title="" class="btn btn-success btnw btn-sm" data-original-title="Returned">Return</button>
                          </td>
                        </tr>

                        						                        <tr id="id99998000562585" class="">
														<td class="text-center">							<input type="checkbox" name="selected[]" class="checkbox-select" value="99998000562585">
														</td>
														<td class="text-left" style="position:relative"><span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562585<br><a id="appr">Pick Up Generated</a>
							<a href="javascript:void(0)" onclick="getorderinfo(&#39;99998000562585&#39;)" data-toggle="tooltip" class="btn btn-warning view-icon-left" data-original-title="View Order"><i class="fa fa-eye"></i></a></td>
							<td class="text-left">Nandha  kumar<br>nandhakumar0021@gmail.com<br>8637401921</td>
                          <!-- <td class="text-left"><a data-toggle="tooltip" title="Payment Address"><i class="fa fa-home" aria-hidden="true"></i> => </a>Nandha
kumar
Door #19\5, Grace Garder, 1st lane, Royapuram (P) Chennai
Land mark:: Behind-Benney Bakes n Cakes,
43, West roa , Chennai, Tamil Nadu-600013, India<br/><br/><a data-toggle="tooltip" title="Shipping Address"><i class="fa fa-truck"></i> <= </a>Nandha
kumar
Door #19\5, Grace Garder, 1st lane, Royapuram (P) Chennai
Land mark:: Behind-Benney Bakes n Cakes,
43, West roa, , Chennai, Tamil Nadu-600013, India</td> -->
                          <td class="text-center">Tamil Nadu</td>
                          <td class="text-center">
                          <strong style="width: 120px;display: inline-block;">
                           <br>XPRESSBEES<br>1341117150808</strong>
                          </td>
                          <td class="text-center">
                            <a title="ECOM EXPRESS" class="btn btn-info ecom-expess btn-sm">ECE</a><a title="DELHIVERY" class="btn btn-warning delhivery btn-sm">DEV</a><a title="BLUE DART" class="btn btn-success blue-dart btn-sm">BLD</a><a title="DOTZOT" class="btn btn-info dotzot btn-sm">DZT</a><a title="SHIP DELIGHT" class="btn btn-info shipdelight btn-sm">SPD</a><a title="XPRESSBEES" class="btn btn-info xpressbees btn-sm">XPRB</a><a title="SMARTSHIP" class="btn btn-info smartship btn-sm">SMS</a>                          </td>
                          <td class="text-left">
                            Garcinia Cambogia Herbs-1 Bottle (Weight Loss - 1)<br><strong>Rs. 2,288</strong></td>
                          <td class="text-left">04/11/2019 10:45:05</td>
                          <td class="text-center">
                             <a class="btn btn-info btnw btn-sm" data-toggle="tooltip" title="" onclick="reassigned(&#39;99998000562585&#39;)" href="javascript:void(0)" data-original-title="Re Assigned Other Courier Campany">Re AWB</a>
                            <button type="button" onclick="statuschanges(&#39;99998000562585&#39;,&#39;cancel&#39;,&#39;7&#39;)" data-toggle="tooltip" title="" class="btn btn-danger btnw btn-sm" data-original-title="Cancel">Cancel</button>
                            <button type="button" onclick="statuschanges(&#39;99998000562585&#39;,&#39;return&#39;,&#39;28&#39;)" data-toggle="tooltip" title="" class="btn btn-success btnw btn-sm" data-original-title="Returned">Return</button>
                          </td>
                        </tr>

                        						                        <tr id="id99998000562583" class="">
														<td class="text-center">							<input type="checkbox" name="selected[]" class="checkbox-select" value="99998000562583">
														</td>
														<td class="text-left" style="position:relative"><span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562583<br><a id="appr">Pick Up Generated</a>
							<a href="javascript:void(0)" onclick="getorderinfo(&#39;99998000562583&#39;)" data-toggle="tooltip" class="btn btn-warning view-icon-left" data-original-title="View Order"><i class="fa fa-eye"></i></a></td>
							<td class="text-left">Hemavathi.r Hema<br>brindavan04@gmail.com<br>9845963399</td>
                          <!-- <td class="text-left"><a data-toggle="tooltip" title="Payment Address"><i class="fa fa-home" aria-hidden="true"></i> => </a>52/A 2nd main rood kamalanagaraV B theter , Bangalore, Karnataka-560079, India<br/><br/><a data-toggle="tooltip" title="Shipping Address"><i class="fa fa-truck"></i> <= </a>52/A 2nd main rood kamalanagaraV B theter, , Bangalore, Karnataka-560079, India</td> -->
                          <td class="text-center">Karnataka</td>
                          <td class="text-center">
                          <strong style="width: 120px;display: inline-block;">
                           <br>XPRESSBEES<br>1341117150809</strong>
                          </td>
                          <td class="text-center">
                            <a title="ECOM EXPRESS" class="btn btn-info ecom-expess btn-sm">ECE</a><a title="DELHIVERY" class="btn btn-warning delhivery btn-sm">DEV</a><a title="BLUE DART" class="btn btn-success blue-dart btn-sm">BLD</a><a title="WOW EXPRESS" class="btn btn-info wow-expess btn-sm">WWE</a><a title="DOTZOT" class="btn btn-info dotzot btn-sm">DZT</a><a title="SHIP DELIGHT" class="btn btn-info shipdelight btn-sm">SPD</a><a title="XPRESSBEES" class="btn btn-info xpressbees btn-sm">XPRB</a><a title="SMARTSHIP" class="btn btn-info smartship btn-sm">SMS</a><a title="SELF DELIVERY" class="btn btn-info selfdelivery btn-sm">SLF</a>                          </td>
                          <td class="text-left">
                            Garcinia Cambogia Herbs-1 Bottle (Weight Loss - 1)<br><strong>Rs. 2,288</strong></td>
                          <td class="text-left">04/11/2019 10:45:06</td>
                          <td class="text-center">
                             <a class="btn btn-info btnw btn-sm" data-toggle="tooltip" title="" onclick="reassigned(&#39;99998000562583&#39;)" href="javascript:void(0)" data-original-title="Re Assigned Other Courier Campany">Re AWB</a>
                            <button type="button" onclick="statuschanges(&#39;99998000562583&#39;,&#39;cancel&#39;,&#39;7&#39;)" data-toggle="tooltip" title="" class="btn btn-danger btnw btn-sm" data-original-title="Cancel">Cancel</button>
                            <button type="button" onclick="statuschanges(&#39;99998000562583&#39;,&#39;return&#39;,&#39;28&#39;)" data-toggle="tooltip" title="" class="btn btn-success btnw btn-sm" data-original-title="Returned">Return</button>
                          </td>
                        </tr>

                        						                        <tr id="id99998000562580" class="">
														<td class="text-center">							<input type="checkbox" name="selected[]" class="checkbox-select" value="99998000562580">
														</td>
														<td class="text-left" style="position:relative"><span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562580<br><a id="appr">Pick Up Generated</a>
							<a href="javascript:void(0)" onclick="getorderinfo(&#39;99998000562580&#39;)" data-toggle="tooltip" class="btn btn-warning view-icon-left" data-original-title="View Order"><i class="fa fa-eye"></i></a></td>
							<td class="text-left">kamatham bhargav<br>kamathambhargav@gmail.com<br>9063863835</td>
                          <!-- <td class="text-left"><a data-toggle="tooltip" title="Payment Address"><i class="fa fa-home" aria-hidden="true"></i> => </a>Kamatham
Bhargav
C/o Kamatham
Bhargav.Tirupati Annamayya Nagar,Chittoor(D)
Landmark-DMart Supermarket
Tirupati 517501
Andh , Tirupati, Andhra Pradesh-517501, India<br/><br/><a data-toggle="tooltip" title="Shipping Address"><i class="fa fa-truck"></i> <= </a>Kamatham
Bhargav
C/o Kamatham
Bhargav.Tirupati Annamayya Nagar,Chittoor(D)
Landmark-DMart Supermarket
Tirupati 517501
Andh, , Tirupati, Andhra Pradesh-517501, India</td> -->
                          <td class="text-center">Andhra Pradesh</td>
                          <td class="text-center">
                          <strong style="width: 120px;display: inline-block;">
                           <br>XPRESSBEES<br>1341117150810</strong>
                          </td>
                          <td class="text-center">
                            <a title="ECOM EXPRESS" class="btn btn-info ecom-expess btn-sm">ECE</a><a title="DELHIVERY" class="btn btn-warning delhivery btn-sm">DEV</a><a title="BLUE DART" class="btn btn-success blue-dart btn-sm">BLD</a><a title="DOTZOT" class="btn btn-info dotzot btn-sm">DZT</a><a title="XPRESSBEES" class="btn btn-info xpressbees btn-sm">XPRB</a><a title="SMARTSHIP" class="btn btn-info smartship btn-sm">SMS</a>                          </td>
                          <td class="text-left">
                            Garcinia Cambogia Herbs-1 Bottle (Weight Loss - 1)<br><strong>Rs. 2,288</strong></td>
                          <td class="text-left">04/11/2019 10:45:06</td>
                          <td class="text-center">
                             <a class="btn btn-info btnw btn-sm" data-toggle="tooltip" title="" onclick="reassigned(&#39;99998000562580&#39;)" href="javascript:void(0)" data-original-title="Re Assigned Other Courier Campany">Re AWB</a>
                            <button type="button" onclick="statuschanges(&#39;99998000562580&#39;,&#39;cancel&#39;,&#39;7&#39;)" data-toggle="tooltip" title="" class="btn btn-danger btnw btn-sm" data-original-title="Cancel">Cancel</button>
                            <button type="button" onclick="statuschanges(&#39;99998000562580&#39;,&#39;return&#39;,&#39;28&#39;)" data-toggle="tooltip" title="" class="btn btn-success btnw btn-sm" data-original-title="Returned">Return</button>
                          </td>
                        </tr>

                        						                        <tr id="id99998000562578" class="">
														<td class="text-center">							<input type="checkbox" name="selected[]" class="checkbox-select" value="99998000562578">
														</td>
														<td class="text-left" style="position:relative"><span style="text-transform: uppercase;font-weight: bold;color: #4fbe13;">cod</span>#99998000562578<br><a id="appr">Pick Up Generated</a>
							<a href="javascript:void(0)" onclick="getorderinfo(&#39;99998000562578&#39;)" data-toggle="tooltip" class="btn btn-warning view-icon-left" data-original-title="View Order"><i class="fa fa-eye"></i></a></td>
							<td class="text-left">Azra Khan<br>sufiyankhan5@outlook.com<br>9219544496</td>
                          <!-- <td class="text-left"><a data-toggle="tooltip" title="Payment Address"><i class="fa fa-home" aria-hidden="true"></i> => </a>House no.66 kot mohalla pujabiyan khaveshgyan khurjaHr girls school , Khurja, Uttar Pradesh-203131, India<br/><br/><a data-toggle="tooltip" title="Shipping Address"><i class="fa fa-truck"></i> <= </a>House no.66 kot mohalla pujabiyan khaveshgyan khurjaHr girls school, , Khurja, Uttar Pradesh-203131, India</td> -->
                          <td class="text-center">Uttar Pradesh</td>
                          <td class="text-center">
                          <strong style="width: 120px;display: inline-block;">
                           <br>ECOM EXPRESS<br>8563499885</strong>
                          </td>
                          <td class="text-center">
                            <a title="ECOM EXPRESS" class="btn btn-info ecom-expess btn-sm">ECE</a><a title="DELHIVERY" class="btn btn-warning delhivery btn-sm">DEV</a><a title="DOTZOT" class="btn btn-info dotzot btn-sm">DZT</a><a title="SMARTSHIP" class="btn btn-info smartship btn-sm">SMS</a>                          </td>
                          <td class="text-left">
                            Garcinia Cambogia Herbs-1 Bottle (Weight Loss - 1)<br><strong>Rs. 2,288</strong></td>
                          <td class="text-left">04/11/2019 10:45:07</td>
                          <td class="text-center">
                             <a class="btn btn-info btnw btn-sm" data-toggle="tooltip" title="" onclick="reassigned(&#39;99998000562578&#39;)" href="javascript:void(0)" data-original-title="Re Assigned Other Courier Campany">Re AWB</a>
                            <button type="button" onclick="statuschanges(&#39;99998000562578&#39;,&#39;cancel&#39;,&#39;7&#39;)" data-toggle="tooltip" title="" class="btn btn-danger btnw btn-sm" data-original-title="Cancel">Cancel</button>
                            <button type="button" onclick="statuschanges(&#39;99998000562578&#39;,&#39;return&#39;,&#39;28&#39;)" data-toggle="tooltip" title="" class="btn btn-success btnw btn-sm" data-original-title="Returned">Return</button>
                          </td>
                        </tr>
                                                                      </tbody>
                    </table>
                  </div>
                </form>
                <div class="row">
                  <div class="col-sm-6 text-left"></div>
                  <div class="col-sm-6 text-right">Showing 1 to 5 of 5 (1 Pages)</div>
                </div>
          <!-- ORDER TAB START HERE -->

      </div>
    </div>
  </div>




<!-- POPUP BOX START HERE -->

<div id="element_to_pop_up2"><a class="b-close" title="Close">x</a>
<div class="request-a-call" id="result001"></div>
</div>


<div class="md-modal md-effect-1" id="modal-1" style="max-height: 450px; top: 40%; ">
  <div class="md-content" id="address-1">
      <div style="padding:10px;" id="html_results">
      </div>
     <button style="position: absolute; right: 0px; top: 0;" class="md-close btn btn-info">Close me!</button>
  </div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->

<div id="element_to_pop_up3"> <a class="b-close" title="Close">x</a>
  <div class="request-a-call">
    <h2>Select Courier Company</h2>
      <div class="request-a-call-form">
         <fieldset>
              <form class="form-horizontal">
                <div id="element3"></div>
              </form>
          </fieldset>  <br>
          <div class="text-right">
          <button id="button-api-history" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Save</button>
          </div>
      </div>
  </div>
</div>
 <div id="element_to_pop_up">
    <a class="b-close close-button" title="Close">x</a>
    <div class="request-a-call" id="results002" style="max-height: 600px; overflow-y: scroll;"></div>
</div>

  <!-- Element to pop up  End here-->

<!-- POPUP BOX END HERE -->






<script src="./Order Pick Up_files/jquery.bpopup.min.js.download" type="text/javascript"></script>
<script src="./Order Pick Up_files/jquery.easing.1.3.js.download" type="text/javascript"></script>
<script src="./Order Pick Up_files/tokens.js.download" type="text/javascript"></script>
<script type="text/javascript">
function statuschanges(order_id, button, status){
         $.ajax({
            url: 'index.php?route=logistics1/order/ChangeStatusByReturned&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&button='+button+'&status='+status,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
            $('#result001').html(json['html_success']);
           // alert(json['html_success']);
           $('#element_to_pop_up2').bPopup({
              easing: 'easeOutBack', //uses jQuery easing plugin
              speed: 1000,
              transition: 'slideDown'
            });

            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });
}


function reassigned(order){

          $.ajax({
            url: 'index.php?route=logistics1/pickup/ReAssignedCourier&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order,
            type: 'GET',
            crossDomain: true,
            success: function(json) {
              /* if(json['state_id']=='1505' && json['total'] > '5000'){
                   alert('This order is belong to '+json['state_name']+' and price is greater than Rs. 5000 thousand. You will change price less than or equal to Rs.5000 thousand.');
               } else {*/

                    $('#element3').html(json['view']);
                    $('#element_to_pop_up3').bPopup({
                        easing: 'easeOutBack', //uses jQuery easing plugin
                        speed: 1000,
                        position: [150, 150], //x, y
                        transition: 'slideDown'
                    });

            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });
}

$('#button-api-history').on('click', function() {
    var order_id = document.getElementById("order_id_company_name").value;
    var selectedVal = "";
    var selected = $(".request-a-call-form #aip_company_name label input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
    } else {
       selectedVal = selected.val();
    }


  /* if(selectedVal==0){
         var company = $('#courier_name').val();
         var awbno = $('#courier_awb').val();
         var urls='index.php?route=skygain/order/addshippingapi&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&filter_courier='+selectedVal+'&courier_company='+company+'&awbno='+awbno;
   } else  if(selectedVal==13){
         var awbno = $('#india_post_courier_awb').val();
         var urls='index.php?route=skygain/order/addshippingapi&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&filter_courier='+selectedVal+'&indpostawbno='+awbno;
   }  else {*/
      var urls='index.php?route=logistics1/ready_dispatched/readyfordispatched&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&filter_courier='+selectedVal+'&re_assign_action_status=reassigned';

  // }

  if(selectedVal==13){
         var awbno = $('#india_post_courier_awb').val();
         var urls='index.php?route=logistics1/ready_dispatched/readyfordispatched&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&filter_courier='+selectedVal+'&indpostawbno='+awbno+'&re_assign_action_status=reassigned';
   }  else {
    var urls='index.php?route=logistics1/ready_dispatched/readyfordispatched&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&filter_courier='+selectedVal+'&re_assign_action_status=reassigned';

   }
   //alert(urls);


  $.ajax({
    url: urls,
    type: 'GET',
    dataType: 'json',
    beforeSend: function() {
      $('#button-api-history').button('loading');
    },
    complete: function() {
      $('#button-api-history').button('reset');
    },
    success: function(json) {
    if (json['success']) {
    Lobibox.notify('success', {
                size: 'mini',
                position: 'bottom right',
                msg: json['success']+'. Order list is refreshing......'
                });

         $('.b-close').click();
           window.location.reload();
      }
    }
  });


});


function changeStatus(order_id, button, status){
 // alert(order_id+button+status);
       $.ajax({
          url: 'index.php?route=logistics1/pickup/ChangeStatusByButton&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id+'&order_status_id='+status+'&button='+button,
          dataType: 'json',
          success: function(json) {
             html=json['status'];
             alert(json['success']);
            // $('#appr').html(html);
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
}
</script>

<script type="text/javascript">
$(document).delegate('#button-history', 'click', function() {
var ppost = $("#input-comment").validationEngine('validate');
var selection = $("select[name=\'order_status_id\']").validationEngine('validate');
if(ppost && selection){
$.ajax({
    url: 'index.php?route=logistics1/order/changeOrderHistory&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA',
    type: 'post',
    dataType: 'json',
    data: 'order_id=' + encodeURIComponent($('input[name=\'order_id\']').val()) +'&order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&button=' + encodeURIComponent($('input[name=\'order_button\']').val())+'&status=' + encodeURIComponent($('input[name=\'order_status\']').val())+'&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
    beforeSend: function() {
      $('#button-history').button('loading');
    },
    complete: function() {
      $('#button-history').button('reset');
    },
    success: function(json) {
      $('.alert').remove();

      if (json['error']) {
        $('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }

      if (json['success']) {
         $('textarea[name=\'comment\']').val('');
         $('.b-close').click();
      }
  $("#form-order table tbody #id"+encodeURIComponent($('input[name=\'order_id\']').val())).fadeOut('slow');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
}
});
</script>

<script type="text/javascript">
$('#button-filter').on('click', function() {
  url = 'index.php?route=logistics1/pickup&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';

  var filter_order_id = $('input[name=\'filter_order_id\']').val();
  if (filter_order_id) {
    url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
  }

  var filter_telephone = $('input[name=\'filter_telephone\']').val();
  if (filter_telephone) {
    url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
  }
  var filter_date_type = $('select[name=\'filter_date_type\']').val();
  if (filter_date_type) {
    url += '&filter_date_type=' + encodeURIComponent(filter_date_type);
  }
  var filter_from_date = $('input[name=\'filter_from_date\']').val();
  if (filter_from_date) {
    url += '&filter_from_date=' + encodeURIComponent(filter_from_date);
  }
  var filter_to_date = $('input[name=\'filter_to_date\']').val();
  if (filter_to_date) {
    url += '&filter_to_date=' + encodeURIComponent(filter_to_date);
  }
  var filter_customer = $('input[name=\'filter_customer\']').val();
  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }
  var filter_courier = $('select[name=\'filter_courier\']').val();
  if (filter_courier) {
    url += '&filter_courier=' + encodeURIComponent(filter_courier);
  }

  var filter_pay_mode = $('select[name=\'filter_pay_mode\']').val();
  if (filter_pay_mode != '*') {
    url += '&filter_pay_mode=' + encodeURIComponent(filter_pay_mode);
  }


  var filter_state = $('select[name=\'filter_state\']').val();
  if (filter_state) {
    url += '&filter_state=' + encodeURIComponent(filter_state);
  }


  var filter_order_status = $('select[name=\'filter_order_status\']').val();
  if (filter_order_status != '*') {
    url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
  }
  var filter_total = $('input[name=\'filter_total\']').val();
  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_name = $('input[name=\'filter_name\']').val();
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_order_awb = $('input[name=\'filter_order_awb\']').val();
  if (filter_order_awb) {
    url += '&filter_order_awb=' + encodeURIComponent(filter_order_awb);
  }



  var filter_date_added = $('input[name=\'filter_date_added\']').val();
  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();
  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }

  location = url;
});




$('#button-download-btn').on('click', function() {
  url = 'index.php?route=logistics1/pickup/saleDownloadOrder&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA';


  var filter_order_id = $('input[name=\'filter_order_id\']').val();
  if (filter_order_id) {
    url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
  }

  var filter_telephone = $('input[name=\'filter_telephone\']').val();
  if (filter_telephone) {
    url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
  }
  var filter_date_type = $('select[name=\'filter_date_type\']').val();
  if (filter_date_type) {
    url += '&filter_date_type=' + encodeURIComponent(filter_date_type);
  }
  var filter_from_date = $('input[name=\'filter_from_date\']').val();
  if (filter_from_date) {
    url += '&filter_from_date=' + encodeURIComponent(filter_from_date);
  }
  var filter_to_date = $('input[name=\'filter_to_date\']').val();
  if (filter_to_date) {
    url += '&filter_to_date=' + encodeURIComponent(filter_to_date);
  }
  var filter_customer = $('input[name=\'filter_customer\']').val();
  if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);
  }
  var filter_courier = $('select[name=\'filter_courier\']').val();
  if (filter_courier) {
    url += '&filter_courier=' + encodeURIComponent(filter_courier);
  }

  var filter_pay_mode = $('select[name=\'filter_pay_mode\']').val();
  if (filter_pay_mode != '*') {
    url += '&filter_pay_mode=' + encodeURIComponent(filter_pay_mode);
  }


  var filter_state = $('select[name=\'filter_state\']').val();
  if (filter_state) {
    url += '&filter_state=' + encodeURIComponent(filter_state);
  }


  var filter_order_status = $('select[name=\'filter_order_status\']').val();
  if (filter_order_status != '*') {
    url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
  }
  var filter_total = $('input[name=\'filter_total\']').val();
  if (filter_total) {
    url += '&filter_total=' + encodeURIComponent(filter_total);
  }

  var filter_name = $('input[name=\'filter_name\']').val();
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_order_awb = $('input[name=\'filter_order_awb\']').val();
  if (filter_order_awb) {
    url += '&filter_order_awb=' + encodeURIComponent(filter_order_awb);
  }



  var filter_date_added = $('input[name=\'filter_date_added\']').val();
  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();
  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }



/*
  var filter_date_added = $('input[name=\'filter_date_added\']').val();
  if (filter_date_added) {
    url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
  }

  var filter_date_modified = $('input[name=\'filter_date_modified\']').val();
  if (filter_date_modified) {
    url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
  }
*/


  location = url;
});



//--></script>
 <script type="text/javascript">
  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_name\']').val(item['label']);
  }
});
</script>

  <script src="./Order Pick Up_files/bootstrap-datetimepicker.min.js.download" type="text/javascript"></script>
  <link href="./Order Pick Up_files/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
  <script type="text/javascript"><!--
  $('.date').datetimepicker({
    pickTime: false
  });
  var countChecked = function() {
    //alert($("input:checked").attr('name'));
    var n = $( ".checkbox-select:checked" ).length;
    var n1 = n-1;
    $( "#selected-order" ).text( n + (n <= 1 ? " order" : " orders") + " Selected." );
  };
  countChecked();

  $( "input[type=checkbox]" ).on( "click", countChecked );
  function getorderinfo(order_id){
       $.ajax({
        url: 'index.php?route=logistics1/order/ViewOrder&token=zCOGYToQ0JDSruoRkatkBdCvR8Zcv9IA&order_id='+order_id,
        type: 'GET',
        crossDomain: true,
        success: function(json) {
        $('#results002').html(json['html_success']);
         // alert(json['html_success']);
        $('#element_to_pop_up').bPopup({
          easing: 'easeOutBack', //uses jQuery easing plugin
          speed: 1000,
          transition: 'slideDown'
        });

        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
        });

  }
//--></script></div>
<?php include ('/includes/footer.php'); ?>
</div>

<div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div><div class="bootstrap-datetimepicker-widget dropdown-menu"><div class="datepicker"><div class="datepicker-days" style="display: block;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">November 2019</th><th class="next">›</th></tr><tr><th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td><td class="day">2</td></tr><tr><td class="day">3</td><td class="day active today">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td><td class="day">9</td></tr><tr><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td><td class="day">16</td></tr><tr><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td><td class="day">23</td></tr><tr><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td><td class="day">30</td></tr><tr><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td><td class="day new">6</td><td class="day new">7</td></tr></tbody></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month active">Nov</span><span class="month">Dec</span></td></tr></tbody></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev">‹</th><th colspan="5" class="picker-switch">2010-2019</th><th class="next">›</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year active">2019</span><span class="year old">2020</span></td></tr></tbody></table></div></div></div>


<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST START HERE -->
<script src="./Order Pick Up_files/classie.js.download" type="text/javascript"></script>
<script src="./Order Pick Up_files/modalEffects.js.download" type="text/javascript"></script>
<!-- WASIM JS FILE ADD OF DUBLICATE ORDER LIST END HERE --><style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
