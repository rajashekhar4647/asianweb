<!doctype html>
<html>
<?php
session_start();
require('./includes/connection.php');
$productid='1';
$sqlbasic=mysqli_query($link,"SELECT * FROM `products` WHERE `id`='$productid'");
$productbasic=mysqli_fetch_array($sqlbasic);
?>
<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Asian Herbs - Buy</title>


    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico"/>
    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico"/>
	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">
<style>
.addcart_custom_btn
{
	cursor: pointer;
    background: white;
    height: 50px;
    line-height: 48px;
    display: inline-block;
    border: 2px solid #222;
    padding: 0 18px;
    border-radius: 0;
    font-size: 14px;
    color: #222;
    font-weight: 700;
    letter-spacing: 2px;
    -webkit-transition: 0.3s;
    -o-transition: 0.3s;
    transition: 0.3s;
    margin-left: 5px;
}
.addcart_custom_btn:hover
{
	    background: #3ad82e;
    color: #fff;
    border: 2px solid #fff;
}
@media screen and (max-width: 700px) {
  .mob-hide {
    display:none !important;
  }
  .mob-show {
    display:block !important;
  }
   .tabmobtext
   {
	   font-size:14px !important;
   }
   .mob-tab
   {
	   width:49%
   }
   .mobinc{
      width: -webkit-fill-available !important;
	  margin-left:0px !important;
   }
   .cap1
   {
   left: 190px !important;
   }
   .cap2
   {
   left: 285px !important;
   }
   .cap3
   {
   left: 285px !important;
   }
}
.mm-usp-section
{
	display: -webkit-flex;
    display: -ms-flexbox;
    display: -webkit-box;
    display: -moz-box;
    display: flex;
    margin-bottom: 1em;
    border-radius: 8px;
    padding: 1em;
    align-items: center;
    border: 1px solid #CACACA;
    min-height: 120px;
}
.mm-usp-section .mm-usp-text {
    margin-left: 2em;
}
.mm-usp-image
{
	margin-left: 2em;
}
</style>

</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
	<?php include('includes/headerhome.php')
    ?>

		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class=" " style="margin-top:70px;">
    <div class="row" style="margin:0;border-top: solid 5px #54993f;">
    <div class=" col-md-12 col-lg-1" style="background: #eef3f5;">
					<img src="media/images/backgrounds/moneyback.png" alt="certificates" style="width:100%;margin-top:10px;">
    </div>
    <div class=" col-md-12 col-lg-11" style="background: #eef3f5;">
    <h2 class="pt-3" style="color:black;font-size:35px;font-weight:bold;"><u>100% Money Back Guarantee</u></h2>
  <p>Our Promies: Easy Refund and Pick up - If you don't loose weight, simply call us and we wiil give you a full 100% hassle-free refund. We stand behind our product. Try it, Risk Free.</p>
    </div>
		</section>
		<!-- /.breadcrumb_area -->

		<!--=========================-->
		<!--=        Ingredient2         =-->
		<!--=========================-->
		<section class="ingredeint_section ingredeint2_section ingredeint4_section" style=";background: #eef3f5;width:100%;padding-right:20px">
			<div class="vigo_container_two">
				<div class="ingredient_slider_flex">
				
					<div class="ingredient_slider_main mob-show" style="display:none;">
						<h4 style="
    font-size: 28px;
    font-weight: 800;
    text-align: center;
">FREE & FAST DELIVERY</h4>
					</div>
					<div class="ingredient_slider_detail mobinc" style="
    width: 100%;padding-left: 10px;">
						<div style="box-shadow: 5px 5px 20px 5px grey;">
						<div class="row" style="margin-right: 0px;margin-left: 0px;;background:#0e598c;">
                        <div class="col-md-8 text-center mob-tab">
                            <h6 class="wow fadeInUp tabmobtext" style="color:white;font-size:16px;text-align:left;padding:10px;padding-bottom:0px;">BUY 1 BOTTLE</h6>
                        </div>
                        <div class="col-md-4 col-md-offset-3 text-center mob-tab">
                           <h6 class="wow fadeInUp tabmobtext" style="color:white;font-size: 19px;
    letter-spacing: 1px;
    font-weight: 800;text-align:right;padding:10px;padding-bottom:0px;">FREE SHIPPING</h6>
                        </div>
						</div>
						<div class="row">
                        <div class="col-md-6 text-center">
                         <img src="img/1pack.png" style="height:300px">
						 <span class="mm-off cap1" style="
    width: 40px;
    height: 40px;
    background: #A1421E;
    display: inline-block;
    border-radius: 50%;
    text-align: center;
    line-height: 1;
    padding: 9px;
    font-size: 13px;
    color: #fff;
    position: absolute;
    left: 260px;
    top: 16px;
    ">
                    
                    30% off
                    
                  </span>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                           <h3 class="wow fadeInUp" style="font-size:28px;font-weight:700;padding-top:20px">1 MONTH SUPPLY</h3>
						   <span style="font-weight:600">M.R.P  ₹ 2499/-</span></br>
						   <span style="font-weight:600;color:#5595CD">Only @ Price  ₹1799/-</span></br>
						    <a href="processorder.php?packid=1"><button class="mm-add" data-variant="30332949233754" style="    background: #000;
    color: #fff;
    padding: 0.7em;
    border: 2px solid #000;
    border-radius: 5px;
	font-weight:600;
	margin-top:10px;
    min-width: 180px;
    outline: 0;
    line-height: 1;
    position: relative;
    transition: all .5s ease;">
                      SELECT PACKAGE <i class="fa fa-caret-right" aria-hidden="true"></i>
                    </button></a>
                        </div>
						</div>
						</div>
						<div style="box-shadow: 5px 5px 20px 5px grey;margin-top:60px;">
						<div class="row" style="margin-right: 0px;margin-left: 0px;;background:#0e598c">
                        <div class="col-md-8 text-center mob-tab">
                            <h6 class="wow fadeInUp tabmobtext" style="color:white;font-size:16px;text-align:left;padding:10px;padding-bottom:0px;">BUY 2 GET 1 FREE
</h6>
                        </div>
                        <div class="col-md-4  col-sm-4 col-md-offset-3 text-center mob-tab">
                           <h6 class="wow fadeInUp tabmobtext" style="color:white;font-size: 19px;
    letter-spacing: 1px;
    font-weight: 800;text-align:right;padding:10px;padding-bottom:0px;">FREE SHIPPING</h6>
                        </div>
						</div>
						<div class="row">
                        <div class="col-md-6 text-center">
                         <img src="img/3pack.png" style="height:300px">
						 <span class="mm-off cap2" style="
    width: 40px;
    height: 40px;
    background: #A1421E;
    display: inline-block;
    border-radius: 50%;
    text-align: center;
    line-height: 1;
    padding: 9px;
    font-size: 13px;
    color: #fff;
    position: absolute;
    left: 360px;
    top: 30px;">
                    
                    56% off
                    
                  </span>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                           <h3 class="wow fadeInUp" style="font-size:28px;font-weight:700;padding-top:20px">3 MONTHS SUPPLY</h3>
						   <span style="font-weight:600">M.R.P ₹2499*3 = ₹ 7499/-</span></br>
						   <span style="font-weight:600;color:#5595CD">Only @ Price  ₹3299/-</span></br>
						    <a href="processorder.php?packid=2"><button class="mm-add" data-variant="30332949233754" style="    background: #000;
    color: #fff;
    padding: 0.7em;
    border: 2px solid #000;
    border-radius: 5px;
	font-weight:600;
	margin-top:10px;
    min-width: 180px;
    outline: 0;
    line-height: 1;
    position: relative;
    transition: all .5s ease;">
                      SELECT PACKAGE <i class="fa fa-caret-right" aria-hidden="true"></i>
                    </button></a>
                        </div>
						</div>
						</div>
						<div style="box-shadow: 5px 5px 20px 5px grey;margin-top:60px;">
						<div class="row" style="margin-right: 0px;margin-left: 0px;;background:#0e598c">
                        <div class="col-md-8 col-sm-8 text-center mob-tab">
                            <h6 class="wow fadeInUp tabmobtext" style="color:white;font-size:16px;text-align:left;padding:10px;padding-bottom:0px;">BUY 3 GET 3 FREE
</h6>
                        </div>
                        <div class="col-md-4 col-sm-4 col-md-offset-3 text-center mob-tab">
                           <h6 class="wow fadeInUp tabmobtext" style="color:white;font-size: 19px;
    letter-spacing: 1px;
    font-weight: 800;text-align:right;padding:10px;padding-bottom:0px;">FREE SHIPPING</h6>
                        </div>
						</div>
						<div class="row">
                        <div class="col-md-6 text-center">
                         <img src="img/6pack.png" style="height:300px">
						 <span class="mm-off cap3" style="
    width: 40px;
    height: 40px;
    background: #A1421E;
    display: inline-block;
    border-radius: 50%;
    text-align: center;
    line-height: 1;
    padding: 9px;
    font-size: 13px;
    color: #fff;
    position: absolute;
    left: 360px;
    top: 40px;
    ">
                    
                    64% off
                    
                  </span>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                           <h3 class="wow fadeInUp" style="font-size:28px;font-weight:700;padding-top:20px">6 MONTHS SUPPLY</h3>
						   <span style="font-weight:600">M.R.P  ₹2499*6 = ₹ 14999/-</span></br>
						   <span style="font-weight:600;color:#5595CD">Only @ Price  ₹5399/-</span></br>
						   <a href="processorder.php?packid=3"><button class="mm-add" data-variant="30332949233754" style="    background: #000;
    color: #fff;
    padding: 0.7em;
    border: 2px solid #000;
    border-radius: 5px;
	font-weight:600;
	margin-top:10px;
    min-width: 180px;
    outline: 0;
    line-height: 1;
    position: relative;
    transition: all .5s ease;">
                      SELECT PACKAGE <i class="fa fa-caret-right" aria-hidden="true"></i>
                    </button></a>
                        </div>
						</div>
						</div>
					
					</div>
				</div>
			</div>
			<div class="row" style="margin-top:100px;padding-left: 50px;
    padding-right: 50px;">
	  
      	
      	
        <div class="col-lg-4">
		  <div class="mm-usp-section">
            <div class="mm-usp-image" style="display:inline-block;">
              
              <img src="//cdn.shopify.com/s/files/1/0085/7191/2282/files/deliver_medium.png?v=1570123582" alt="USP Icon">
              
            </div>
            <div class="mm-usp-text" style="display:inline-block;text-align:right;">
              <h4 style="width: 210px;">ON TIME DELIVERY</h4>
            </div>
          </div>
        </div>
      
      	
      	
        <div class="col-lg-4">
		  <div class="mm-usp-section">
            <div class="mm-usp-image" style="display:inline-block;">
              
              <img src="//cdn.shopify.com/s/files/1/0085/7191/2282/files/tele_medium.png?v=1570123599" alt="USP Icon">
              
            </div>
            <div class="mm-usp-text" style="display:inline-block;">
              <h4 style="width: 210px;">FRIENDLY<br> CUSTOMER SERVICE</h4>
            </div>
          </div>
        </div>
      
      	
      	
        <div class="col-lg-4">
		  <div class="mm-usp-section">
            <div class="mm-usp-image" style="display:inline-block;">
              
              <img src="//cdn.shopify.com/s/files/1/0085/7191/2282/files/secure_medium.png?v=1570123609" alt="USP Icon">
              
            </div>
            <div class="mm-usp-text" style="display:inline-block;">
              <h4 style="width: 210px;">SECURE PAYMENTS</h4>
            </div>
          </div>
        </div>
      
      </div>
		</section>

<?php include('includes/footer.php')
    ?>


	</div>
	<!-- /#site -->
	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>
<script>
function price_cal(price,packid)
{
	$('#price_selling').text('₹'+price);
	$('#price_mrp').text('₹'+price*1.18);
	$('#packid').val(packid);
}
</script>
<script>
$("form#addbookcart").submit(function(e)
{
    e.preventDefault();
    var session='<?php if(isset($_SESSION['UNISAP_USER_ID'])){ echo '1'; } else { echo '';} ?>';
    if(session!='')
    {
    var formData = new FormData(this);
    console.log(formData);
    $.ajax({
       url: "./functions/addtocart.php",
        type: 'POST',
        data: formData,
        success: function (data) {
        $('#gotocart').show();
		$('#addcartsuccess').show();
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
else
{
	var url = "sign-in.php";
	$(location).attr('href',url);
}
});
</script>
</body>
</html>
