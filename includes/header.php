<?php
session_start();
?>
<style media="screen">
	.close-bars-two-cross::before{
		background:black !important;
	}
	.close-bars-two-cross::after{
		background:black !important;
	}
</style>
<header id="header" class="header_area hdr_area_two hdr_area_four headroom headroom--not-bottom headroom--pinned headroom--top" style="top:0;">
			<!-- Start top toolbar -->
			<section class="top_toolbar top_toolbar_new">
				<div class="vigo_container_one">
					<div class="row">
						<div class="col-xl-6 col-lg-6">
							<div class="toolbar_left">
								<a href="tel:7892359417" style="color:white;"><i class="fas fa-phone-volume" style="color:#3ad82e;font-size:30px;"><span style="color:white;font-size:20px;margin-left:5px;">7892359417</span></i></a>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6">
							<div class="toolbar_right">
								<ul>
									<li class="search">
										<i class="fas fa-search"></i>
									</li>


								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top toolbar -->

			<!-- Start Main Menu -->
			<section class="header_nav">
				<div class="vigo_container_one">
					<div class="row">
						<div class="col-auto mr-auto">
							<div class="header_logo">
								<a href="index.php">
	<img class="logo-white" style="display:block;width: 110px;" src="media/images/home6/footer-logo-final.jpg" alt="asianherbs">
						</a>
							</div>
						</div>
						<div class="col-auto no-position">
							<nav class="main_menu">
								<ul id="example-one">

									<li>
										<a class="current_page_item active" href="index.php" style="font-weight:600">Home</a>

									</li>

									<li>
										<a href="index.php#ingredients" style="font-weight:600">Ingredients</a>

									</li>

									<li>
										<a href="index.php#customer-reviews" style="font-weight:600">Customer Reviews</a>
										<!-- <ul class="sub-menu">
											<li><a href="collection.php">Productlist</a></li>
											<li><a href="product-detail.php">Product Detail</a></li>
											<li><a href="collection-all.php">Collection All</a></li>
										</ul> -->
									</li>
								    <li>
										<a  href="index.php#about-us" style="font-weight:600">About US</a>

									</li>

									<li>
										<a href="http://www.asianherbs.in/blog" style="font-weight:600">blog</a>
									</li>
									<li>
										<a href="index.php#asian-herb-for" style="font-weight:600">Why Asian Herbs</a>

									</li>

								</ul>
							</nav>
						</div>

					 <!-- <div class="col-auto">
	                     <div class="toolbar_right">
												 	<ul>

									<li class="cart">
										<i class="fas fa-shopping-cart"style="font-size:40px; margin-top:20px; color:#3ad82e;"></i>
										<div class="cart_detail">
											<div class="single_cart">
												<div class="cart_left">
													<img src="media/images/banner-two/cart-one.png" alt="">
												</div>
												<div class="cart_right">
													<h3>Vaxin Regular Big Name</h3>
													<p>$66 <sup>USD</sup></p>
												</div>
											</div>
											<div class="single_cart">
												<div class="cart_left">
													<img src="media/images/banner-two/cart-two.png" alt="">
												</div>
												<div class="cart_right">
													<h3>Vaxin Woman</h3>
													<p>$76 <sup>USD</sup></p>
												</div>
											</div>
											<div class="cart_more">
												<a href="#">View Cart <i class="fa fa-angle-right"></i></a>
											</div>
										</div>
									</li>

								</ul>
												</div>
						</div> -->
					</div>
				</div>
				<div class="search_detail_two">
					<form action="#">
						<input type="text" placeholder="Search Your key...">
						<button><i class="material-icons">
				search
				</i>
				</button>
					</form>
					<div class="search_detail_two_close">
						<i class="material-icons">
					clear
				</i>
					</div>
				</div>
			</section>
			<!-- End Main Menu -->

			<!-- Start Mobile Menu outer-->
			<section id="mobile-nav-wrap" class="clearfix" style="background:white">
				<div class="mobile_toolbar">
					<div class="vigo_container_one">
						<div class="top_toolbar_right">
							<div class="phone_number" style="padding-top:8px;">
								<a href="tel:7892359417" style="color:white;"><i class="fas fa-phone-volume" style="color:#3ad82e;font-size:30px;"><span style="color:white;font-size:20px;margin-left:5px;">7892359417</span></i></a>

							</div>
						</div>
					</div>
				</div>
				<div class="vigo_container_one">
					<div class="bottom_nav bottom_nav_two">
					<div id="mobile-logo" style="line-height: 0;">
							<a href="index.php">
							<img class="logo-white" style="display:block;width: 60px;margin-top:10px;" src="media/images/home6/footer-logo-final.jpg" alt="asianherbs">
					</a>
						</div>
						<div class="toggle-inner">
							<i class="fas fa-bars" style="color:#3ad82e;"></i>
						</div>
					</div>
				</div>
			</section>
			<!-- End Mobile Menu outer-->
		</header>
<section class="mobile-menu-inner mobile-menu-inner-two">
			<div class="mobile_accor_togo" style="background:white;">
				<div class="mobile_accor_logo">
					<a href="index.php">
							<img class="logo-white" style="display:block;width: 60px;" src="media/images/home6/footer-logo-final.jpg" alt="asianherbs">
					</a>
				</div>
				<div class="close-menu">
					<span class="close-bars-two-cross"></span>
				</div>
			</div>
			<nav id="accordian">
				<ul class="accordion-menu">
					<li class="current_page_item">
						<a href="index.php">Home</a>

					</li>
					<li>
						<a href="index.php#ingredients">Ingredients</a>

					</li>

					<li>
						<a href="index.php#customer-reviews">Customer Reviews</a>

					</li>
					<li>
						<a href="http://www.asianherbs.in/blog" class="dropdownlink">Blog</a>
					</li>
					<li>
						<a href="index.php#about-us">About us</a>
					</li>
					<li>
						<a href="index.php#asian-herb-for">Why Asian Herbs</a>

					</li>
				</ul>
			</nav>
			<form action="#" id="moble-search">
				<input type="text" placeholder="Search....">
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
			<ul class="footer-social-link">
				<li class="fb-bg"><a href="https://www.facebook.com/Asian-Herbs-106409074084642/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
				<li class="in-bg"><a href="https://www.pinterest.co.uk/asianherbs/" target="_blank"><i class="fab fa-pinterest"></i></a></li>
				<li class="tw-bg"><a href="https://twitter.com/asianherbss" target="_blank"><i class="fab fa-twitter"></i></a></li>
				<li class="gp-bg"><a href="https://www.linkedin.com/company/asianherbs" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
			</ul>
		</section>
