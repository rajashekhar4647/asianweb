<header id="header" class="header_area hdr_area_two hdr_area_four headroom headroom--not-bottom headroom--pinned headroom--top" style="top:0;">
			<!-- Start top toolbar -->
			<section class="top_toolbar top_toolbar_new">
				<div class="vigo_container_one">
					<div class="row">
						<div class="col-xl-6 col-lg-6">
							<div class="toolbar_left">
								<a href="tel:" style="color:white;">Click here to give a missed call</a>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6">
							<div class="toolbar_right">
								<ul>
									<li class="search">
										<i class="fas fa-search"></i>
									</li>

									<li class="phone">
										<a href="#">
							<i class="fas fa-phone"></i>
							Call +123 4567890
						</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End top toolbar -->

			<!-- Start Main Menu -->
			<section class="header_nav">
				<div class="vigo_container_one">
					<div class="row">
						<div class="col-auto mr-auto">
							<div class="header_logo">
								<a href="index.php">
<!--
							<img class="logo-default" src="media/images/home6/logo.png" alt="">
							<img class="logo-white" src="media/images/home6/footer-logo.png" alt="">
-->
						</a>
							</div>
						</div>
						<div class="col-auto no-position">
							<nav class="main_menu">
								<ul id="example-one">
									<li>
										<a class="current_page_item active" href="index.php/#ingredients">Ingredients</a>

									</li>
									<li>
										<a class="current_page_item active" href="index.php">Home</a>

									</li>


									<li>
										<a href="collection.php">Product</a>
										<ul class="sub-menu">
											<li><a href="collection.php">Productlist</a></li>
											<li><a href="product-detail.php">Product Detail</a></li>
											<li><a href="collection-all.php">Collection All</a></li>
										</ul>
									</li>

									<li>
										<a href="blog.php">blog</a>
										<ul class="sub-menu">
											<li><a href="blog.php">blog</a></li>
											<li><a href="blog-details.php">blog-details</a></li>

										</ul>
									</li>
									<li>
										<a  href="about.php">about</a>

									</li>
									<li>
										<a href="contact.php">Contact</a>

									</li>

								</ul>
							</nav>
						</div>
						<div class="col-auto">
							<div class="hdr_btn_wrapper">

								<a href="sign-in.php" class="btn btn-headerss" style="width: 142px;
    padding: 0;
    height: 40px;
    border-radius: 0;
    text-align: center;
    line-height: 38px;
    color: #3ad82e;
    font-size: 14px;

    letter-spacing: 2.5px;
    text-transform: uppercase;
    position: relative;
    z-index: 1;
    -webkit-transition: 0.5s linear;
    -o-transition: 0.5s linear;
    transition: 0.5s linear;
    background: #fff;
    border: 2px solid #3ad82e;
	">sign in</a>


								<a href="sign-up.php" class="btn .btn-hover   "style="width: 142px;
    padding: 0;
    height: 40px;
    border-radius: 0;
    text-align: center;
    line-height: 38px;
    color: #3ad82e;
    font-size: 14px;

    letter-spacing: 2.5px;
    text-transform: uppercase;
    position: relative;
    z-index: 1;
    -webkit-transition: 0.5s linear;
    -o-transition: 0.5s linear;
    transition: 0.5s linear;
    background: #fff;
	border: 2px solid #3ad82e;">sign up</a>

							</div>
						</div>
					 <div class="col-auto">
	                     <div class="toolbar_right">
								<ul>

									<li class="cart">
										<i class="fas fa-shopping-cart"style="font-size:40px; margin-top:20px; color:#3ad82e;"></i>
										<div class="cart_detail">
											<div class="single_cart">
												<div class="cart_left">
													<img src="media/images/banner-two/cart-one.png" alt="">
												</div>
												<div class="cart_right">
													<h3>Vaxin Regular Big Name</h3>
													<p>$66 <sup>USD</sup></p>
												</div>
											</div>
											<div class="single_cart">
												<div class="cart_left">
													<img src="media/images/banner-two/cart-two.png" alt="">
												</div>
												<div class="cart_right">
													<h3>Vaxin Woman</h3>
													<p>$76 <sup>USD</sup></p>
												</div>
											</div>
											<div class="cart_more">
												<a href="#">View Cart <i class="fa fa-angle-right"></i></a>
											</div>
										</div>
									</li>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="search_detail_two">
					<form action="#">
						<input type="text" placeholder="Search Your key...">
						<button><i class="material-icons">
				search
				</i>
				</button>
					</form>
					<div class="search_detail_two_close">
						<i class="material-icons">
					clear
				</i>
					</div>
				</div>
			</section>
			<!-- End Main Menu -->

			<!-- Start Mobile Menu outer-->
			<section id="mobile-nav-wrap" class="clearfix">
				<div class="mobile_toolbar">
					<div class="vigo_container_one">
						<div class="top_toolbar_right">
							<div class="phone_number">
								<span class="flaticon-phone-call"></span> <a href="#">+1 (895) 852–6523</a>
							</div>
							<div class="header_login">
								<div class="whc_toolbar_main_login">
									<a href="sign-up.php">register</a>|
									<a href="sign-in.php">login</a>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="vigo_container_one">
					<div class="bottom_nav bottom_nav_two">
						<div id="mobile-logo">
							<a href="index.php">
						<img src="assets/img/hm-two-logo.png" class="svg" alt="">
					</a>
						</div>
						<div class="toggle-inner">
							<i class="fas fa-bars"></i>
						</div>
					</div>
				</div>
			</section>
			<!-- End Mobile Menu outer-->
		</header>
<section class="mobile-menu-inner mobile-menu-inner-two">
			<div class="mobile_accor_togo">
				<div class="mobile_accor_logo">
					<a href="index.html">
				<img src="assets/img/hm-two-logo.png" class="svg" alt="">
			</a>
				</div>
				<div class="close-menu">
					<span></span>
				</div>
			</div>
			<nav id="accordian">
				<ul class="accordion-menu">
					<li class="current_page_item">
						<a href="index.php">Home</a>

					</li>

					<li>
						<a href="collection.php" class="dropdownlink">Productlist</a>
						<ul class="submenuItems">
							<li><i class="flaticon-right-arrow-angle"></i><a href="collection.php">Productlist</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="collection-all.php">Product Sidebar</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="product-detail.php">Product Detail</a></li>
						</ul>
					</li>

					<li>
						<a href="blog.php" class="dropdownlink">blog</a>
						<ul class="submenuItems">
							<li><i class="flaticon-right-arrow-angle"></i><a href="blog.php"> Blog</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="blog-details.php"> Blog details</a></li>
						</ul>
					</li>
					<li>
						<a href="about.php">About us</a>
					</li>
					<li>
						<a href="contact.php" class="dropdownlink">Contact</a>
						<ul class="submenuItems">
							<li><i class="flaticon-right-arrow-angle"></i><a href="contact.php">Contact page</a></li>

							<li><i class="flaticon-right-arrow-angle"></i><a href="privacy.php">Privacy</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="reset-pass.html">Reset Pass</a></li>
							<li><i class="flaticon-right-arrow-angle"></i><a href="faq.php">FAQ</a></li>
						</ul>
					</li>
				</ul>
			</nav>
			<form action="#" id="moble-search">
				<input type="text" placeholder="Search....">
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
			<ul class="footer-social-link">
				<li class="fb-bg"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
				<li class="in-bg"><a href="#"><i class="fab fa-instagram"></i></a></li>
				<li class="tw-bg"><a href="#"><i class="fab fa-twitter"></i></a></li>
				<li class="gp-bg"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
			</ul>
		</section>
