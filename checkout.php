<?php
include './includes/config.php';
session_start();
$curr_order_id=$_SESSION['NEW_ORDER_ID'];
$addquery=mysqli_query($link,"SELECT * FROM `orders_addresses` WHERE `order_id`='$curr_order_id'");
$adddata=mysqli_fetch_array($addquery);
?>
<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Asian Herbs - Checkout</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
     <!-- Bootstrap css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- Fav Icon -->

    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico"/>
    <link rel="shortcut icon" type="image/png" href="http://asianherbs.in/media/herbs.ico"/>
    <script type="text/javascript" src="./assets/js/checkout.js "></script>
    <script type="text/javascript" src="./assets/js/jquery.js "></script>
    <script type="text/javascript" src="./assets/js/jquery-migrate.js "></script>
	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">

  <script type="text/javascript">//<![CDATA[

    window.onload=function(){
      
// Use loadScript to load the Razorpay checkout.js
// -----------------------------------------------
var callRazorPayScript = function(itemId, amount, qty, processorId) {
  var merchangeName = "Asian Herbs",
      img = "media/images/home6/footer-logo-final.jpg",
      name = "Asian Herbs",
      description = "Health Care",
      amount = amount,
      qty = qty;
      
  loadExternalScript('https://checkout.razorpay.com/v1/checkout.js').then(function() { 
    var options = {
      key: 'rzp_live_YAHFAqlokmMRaY',
      protocol: 'https',
      hostname: 'api.razorpay.com',
      amount: amount,
      name: 'Asian Herbs',
      description: description,
      image: img,
      prefill: {
        name: '<?php echo $_SESSION['username'];?>',
		email: '<?php echo $_SESSION['useremail'];?>',
		contact: '<?php echo $_SESSION['userphone'];?>'
      },
      theme: {
        color: '#3AD82E'
      },
      handler: function (transaction, response){
		var subtotal=document.getElementById("subtotal").value;
		var taxvalue=document.getElementById("taxvalue").value;
		var shippingvalue=document.getElementById("shippingvalue").value
		var totalvalue=document.getElementById("totalvalue").value
		$.ajax({
		url: "./functions/ordersubmit.php",
        type: 'POST',
		data: {'subtotal':subtotal,taxvalue:taxvalue,shippingvalue:shippingvalue,totalvalue:totalvalue},
        success: function (data) {
        var url = "invoice.php";
		window.open(url);
		//$(".ordercolor").css("background-color", "rgb(26, 111, 42)");
		$("#title1").hide();
		$("#title2").show();
		$("#place_order").hide();
		$("#cod").hide();
		$("#continue_shopping").show();
		$("#getinvoiceshow").html(data);
		},
        cache: false,
        contentType: false,
        processData: false
    });
      }
    };

    window.rzpay = new Razorpay(options);

	console.log('Item Id: ', amount);
    console.log('Amount: ', amount);
    console.log('Quantity: ', qty);
    console.log('Processor Id: ', processorId);

    rzpay.open();
  });
}

// Trigger call to action buttons depending on the bundle packs
// -----------------------------------------------
var $payBundle = $('.js-pay-bundle');

$payBundle.on('click', function() {
	var itemId = $(this).data('itemid'),
			amount = $(this).data('amount'),
			processorid = $(this).data('processorid'),
			qty = $(this).data('qty');

	callRazorPayScript(itemId, amount, processorid, qty);
});

    }

  //]]></script>
  <script>
  // Function to lazy load a script
  // -----------------------------------------------
  var loadExternalScript = function(path) {
    var result = $.Deferred(),
        script = document.createElement("script");

    script.async = "async";
    script.type = "text/javascript";
    script.src = path;
    script.onload = script.onreadystatechange = function(_, isAbort) {
      if (!script.readyState || /loaded|complete/.test(script.readyState)) {
        if (isAbort)
          result.reject();
        else
          result.resolve();
      }
    };

    script.onerror = function() {
      result.reject();
    };

    $("head")[0].appendChild(script);

    return result.promise();
  };
</script>
    <style>
    .heading th {
        width:300px;
        min-width:100px;
    }
    
    }
    </style>
</head>

<body id="home-version-1" class="home-version-1" data-style="default" style="background: #eef3f5;">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->


	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/headerhome.php')?>
		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<!--=========================-->
		<div class="container mx-auto" >
        <div class="row">
            <div class="col" style="margin-top: 150px;">
                    <h3 class="text-dark text-center mx-auto"  id="title1">Your Order</h3>
					<h2 style="margin-top:20px;text-align:center;color: #1c641e;font-weight: 700;display:none;" id="title2">Your Order Is Placed Successfully!</h2>
				<form action="" id="checkoutform">
                <table class="table table-borderless text-light ">
                    
                    <thead class="text-center" style="background:#0e598c;">
                        <tr class="heading">
                            <th style="width:auto;color:#eef3f5;">Product </th>
                            <th style="width:auto;color:#eef3f5;">Price</th>
                        </tr>
                    </thead>
					<tbody>
				        <tr class="heading text-center text-muted" style="background:#f6f8f6;">
                            <td style="padding:20px;">Ketogenic - Advance Weight Loss</td>
                            <td style="padding:20px;">₹<?php echo $_SESSION['product_price']; ?>/-</td>
                        </tr>
					    <tr style="background:#0e598c;" >
                            <td>
                                <p>Deliver at :</p><p><?php echo $adddata['address'];?><br> <?php echo $adddata['city'];?>, <?php echo $adddata['state'];?>,<br> <?php echo $adddata['country'];?> - <?php echo $adddata['pincode'];?></p>
                            </td>
                            <td class="text-right">
                                <P>Total : <span>₹ <?php echo $_SESSION['product_price']; ?></span></P>
								<div><?php 
								$online_price=$_SESSION['product_price'] - ($_SESSION['product_price'] * (5/100));?>
                                <button type="button" data-amount="<?php echo $online_price*100;?>" class="btn-sm px-3 py-2 rounded-pill text-dark bg-light js-pay-bundle pay-but" id="place_order" value="Place order" data-value="Place order" style="min-width: 145px;margin-top:30px"><b>Pay Online</b></button>
                                 <button type="submit"  class="btn-sm px-3 py-2 rounded-pill text-dark bg-light" id="cod" value="cod" data-value="Place order" style="min-width: 145px;margin-top:30px"><b>Cash On Delivery</b></button>
								 </div>
                            </td>
                        </tr>  
					</tbody>
                </table>
				<input type="hidden" name="subtotal" id="subtotal" value="<?php echo $_SESSION['product_price']; ?>">
				<input type="hidden" name="taxvalue" id="taxvalue" value="0">
				<input type="hidden" name="shippingvalue" id="shippingvalue" value="0">
				<input type="hidden" name="totalvalue" id="totalvalue" value="<?php echo $_SESSION['product_price']; ?>">
				</form>
            </div>
        </div>
    </div>
<div class="container mx-auto"><h4 style="background:#0e598c;padding: 20px;text-align: center;color: white;border-radius: 5px;">EXTRA 5% OFF ON ONLINE PAYMENTS</h4></div>
		<!--==========================-->
		<!--=        footer2         =-->
		<!--==========================-->
<?php include('includes/footer.php')?>
	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>
<script>
$("form#checkoutform").submit(function(e) {
   e.preventDefault();
    var formData = new FormData(this);
$.ajax({
        url: "./functions/ordersubmitcod.php",
        type: 'POST',
        data: formData,
        success: function (data) {
    	   $('#cod').hide();
		    var url = "invoice.php";
		window.open(url);
		//$(".ordercolor").css("background-color", "rgb(26, 111, 42)");
		$("#title1").hide();
		$("#title2").show();
		$("#place_order").hide();
		$("#continue_shopping").show();
		$("#getinvoiceshow").html(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>

</body>
</html>