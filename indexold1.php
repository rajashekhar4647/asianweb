<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Asian Herbs - UniSap Nutri Care</title>

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->

	<link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">

     <style Type=text/css>
		 .licence{
			 color:white;
			 background:#fc0303;
			 font-size:400%;
			 margin-top:2%;
		 }
		 @media screen and (max-width:1351px){
			 .licence{
				 font-size:300%;
				 margin-top:2%;
			 }
		 }
		 @media screen and (max-width:1036px){
			.licence{
				font-size:200%;
				margin-top:2%;
			}
		}
		@media screen and (max-width:721px){
		 .licence{
			 font-size:150%;
			 margin-top:2%;
		 }
	 }
	 @media screen and (max-width:721px){
		.licence{
			font-size:100%;
			margin-top:2%;
		}
	}

		 /* moving headings */
		 .moving-heading{
			 animation: 5s moving-heading 1;
		 }
		 @keyframes moving-heading {
		 	0%{
				font-size:50px;
			}
		 }

		 .moving-1{
			 animation: 2s moving-1 1;
		 }
		 @keyframes moving-1 {
		 	0%{
				margin-left: 50%;
			}

		 }
		 .moving-2{
			 animation: 4s moving-2 1;
		 }
		 @keyframes moving-2 {
		 	0%{
				margin-left: 50%;
			}
			20%{
				margin-left: 50%;
			}
		 }
		 .moving-3{
			 animation: 6s moving-3 1;
		 }
		 @keyframes moving-3 {
		 	0%{
				margin-left: 50%;
			}
			30%{
				margin-left: 50%;
			}
		 }
		 .moving-4{
			 animation: 8s moving-4 1;
		 }
		 @keyframes moving-4 {
		 	0%{
				margin-left: 50%;
			}
			40%{
				margin-left: 50%;
			}
		}
		.moving-5{
			animation: 10s moving-5 1;
		}
		@keyframes moving-5 {
		 0%{
			 margin-left: 50%;
		 }
		 50%{
			 margin-left: 50%;
		 }
	 }
		 /* moving headings end */
		 p, li {
			 font-family: 'Bree Serif', serif;
			 font-weight:550 !important;
			 letter-spacing: 1px;
			 line-height: 35px;
			 font-size:25px;
		 }
		 .center{
			 text-align:center;
		 }
		 @media screen and (min-width:300px){
			 .carousel{
				 display:block;
			 }
		 }
		 @media screen and (max-width:500px){
			 .tophead{
				 font-size:30px;
			 }
		 }
		 @media screen and (min-width:501px){
			 .tophead{
				 font-size:40px;
			 }
		 }
		 @media screen and (min-width:992px){
			 .first-image{
		 		z-index: 2 !important;
		 		position: absolute !important;
		 		top:19% !important;
		 		left:42% !important;
				width:19%;
		 	}


		 }
		 @media screen and (min-width:1026px){
			 .second-image{
				 z-index: 1 !important;
				 position: absolute;
				 width:35% !important;
				 top:24%;
				 left:32%;
				 animation:rotate 60s infinite;
			 }
			 .sticky a{
 				bottom:0;
 			}
		 }
		 @media screen and (min-width:1000px) and (max-width:1024px){
			 .first-image{
		 		z-index: 2;
		 		position: absolute;
		 		top:10% !important;
		 		left:52% !important;
				width:20%;
		 	}
			.form{
				margin-left: 200px !important;
			}
		}
			.second-image{
			 z-index:-1000;
			 position:absolute;
			 width:0%;
		 }
			.container-first{
				margin-top:90px !important;
			}
		 }
		 @media screen and (max-width:992px){
			 .second-image{
 				z-index:-1000;
				position:absolute;
				width:0%;
 			}
			.first-image{
				position:static;
				margin-left:200px;
			}
		 }

		 @media screen and (max-width:768px){
			 .first-image{
 				position:static;
 				margin-left:280px;
 			}
			.container-first{
				margin-top:75px !important;
			}
		 }
		 @media screen and (min-width:501px){
			 .sticky-2 a i{
 				position:fixed;
				bottom:0%;
				width:0px;
 			 height:0px;
 			}
			.sticky a {
			 position:fixed;
			 bottom:0%;
			 width:0px;
			 height:0px;
		 }
		 }
		 @media screen and (max-width:500px){
			 .first-image{
 				position:static;
 				margin-left:120px;
				width:50%;
 			}
			.container-first{
				margin-top:75px !important;
			}
			.sticky-2{

			}
			.sticky-2 a i{
				color:green;
				font-size:60px;
				position:fixed;
				left:10%;
				bottom:2%;
			}
			.sticky a {
			 position:fixed;
			 bottom:2%;
			 right:10%;
			 opacity:0.7;

		 }
		 }

      .apple img {

        text-align:center;
        width:300px;
      }
     .apple{


         position:absolute;

         top:25%;
         left:25%;




         overflow:hidden;
        animation-name: round ;
    animation-duration: 16s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
    animation-direction: reverse;
     }
     .bottle
     {
        position:relative;
        top:25%;



     }
     .bottle  img
     {


        width:150px;
     }
     @keyframes round
{
    0%{transform: rotate(360deg);}
}



     }


.fa-star {
    color:#f2d930;
}
@media only screen and (max-width: 1950px)
{.apple{
         top:25%;
         left:33%;
    }
}
@media only screen and (max-width: 1650px)
{.apple{
         top:25%;
         left:29%;
    }
}
@media only screen and (max-width: 1350px)
{.apple{
         top:25%;
         left:26%;
    }
}
@media only screen and (max-width: 1200px)
{.apple{
         top:25%;
         left:22%;
    }
}
@media only screen and (max-width: 1100px)
{.apple{
         top:25%;
         left:21%;
    }
}
@media only screen and (max-width: 990px)
{
    .apple{
         top:25%;
         left:33%;
    }

}
@media only screen and (max-width: 850px)
{
    .apple{
         top:25%;
         left:30%;
    }

}
@media only screen and (max-width: 750px)
{
    .apple{
         top:25%;
         left:27%;
    }

}
@media only screen and (max-width: 650px)
{
    .apple{
         top:25%;
         left:24%;
    }

}

@media only screen and (max-width: 576px)
{
    .apple{

        display:none;

    }


}
@media screen and (min-width:992px){
	.form{
		width:60%;
		border-radius:8px;
		margin-top:20px;
		margin-left:230px;
		overflow:hidden;
		box-shadow: 0 0 2px 2px grey;
	}

}
@media screen and (max-width:991px){
	.form{
		width:80%;
		border-radius:8px;
		margin:50px 10%;
		overflow:hidden;
		box-shadow: 0 0 2px 2px grey;
	}
}

.form h2{
	background:#3ad82e;
	color:white;
	margin-bottom:20px;
	padding:20px 0;
}
.form input{
	width:80%;
	margin-top:20px;
	height:50px;
	border:1px solid #3ad82e;
	color:black;
	padding:5px;
}
.form input::placeholder{
	color:black;
}
.form select{
	width:80%;
	margin-top:20px;
	height:50px;
	border:1px solid #3ad82e;
	color:black;
	padding:5px;
}
.form .fsubmit{
	background:#3ad82e;
	color:white;
	font-size:20px;
	font-weight:bold;
	margin-bottom:20px;
}
@keyframes rotate {
	100%{
		transform:rotate(360deg);
	}
}
     </style>
</head>

<body id="home-version-1" class="home-version-1" data-style="default">


	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/header.php');
    ?>

		<!--==========================-->
		<!--=        Content         =-->
		<!--==========================-->


		<!--==========================-->
		<!--=        Banner     #3ad82e    =-->
		<!--==========================-->
		<section class="the-first-section"  >
			<div class="container-fluid ">
				<div class="row  container-first " style="margin-top:125px; ">
					<div class="col-lg-6 col-md-12 col-sm-12 col-12  pl-5"  style="background:black; margin-left:0;">
                        <h1 class="tophead moving-heading" style=" margin-top:120px; margin-bottom:50px;  transform: rotate(-5deg); color:white; " >
                        Easy Formula For
							<p class="moving-heading" style="color:#3ad82e;margin-left:100px;font-size:35px;"> weight loss</p>
                        </h1 >
													<h3 class="moving-1" style="margin-left:40px;line-height:40px;color:white;"><i class="fas fa-fire" style="color:red;"></i> Fat Block & Burner</h3>
													<h3 class="moving-2" style="margin-left:40px;line-height:40px;color:white;"><i class="fas fa-arrow-down" style="color:lightgreen"></i> Appetite Control</h3>
													<h3 class="moving-3" style="margin-left:40px;line-height:40px;color:white;"><i class="fas fa-bolt" style="color: #990000"></i> Metabolism Boost</h3>
													<h3 class="moving-4" style="margin-left:40px;line-height:40px;color:white;"><i class="fab fa-envira" style="color:green"></i> Detox Naturally & Safety</h3>
													<h3 class="moving-5" style="margin-left:40px;line-height:40px;color:white;"><i class="fas fa-sun" style="color:orange"></i> Revitalize Energy & Mood</h3><br>
						<div class=" ">
							<p style="color:white;">
								<i class="material-icons">
						done_all
						</i> FEEL LIGHTER
							</p>
							<p style="color:white;">
								<i class="material-icons" >
						done_all
						</i> FEEL STRONGER
							</p>
						</div>
						<div class="banner_static_download " style="padding-bottom:100px;">
							<p style="color:white;">
								Get our expert diet chart for free
							</p>
							<a href="#" class="btn btn_download">
						Order Now
						<i class="fas fa-cart-plus"></i>
					</a>
						</div>
					</div>
					<div class="postion-absolute">
						<img src="media/images/home6/banner-bottle.png" class="first-image" style="">
						<img src="media/images/home6/chart1.png" class="second-image ">
					</div>
					<div class="col-lg-6 col-md-12 col-sm-12 col-12 text-center  " style="background:white;">

						<form class="form" action="index.html" method="post" id="form" style="margin-top:50px;">
							<h2 class="form-heading">Fill The Form</h2>
							<input class="fname" type="text" name="name" placeholder="Enter Full Name">
							<input class="fphone" type="number" name="phone" placeholder="Enter Phone Number">
							<input class="fmail" type="email" name="mail" placeholder="Enter E-mail">
							<select class="fstate" name="state">
								<option hidden>Select State</option>
								<option disabled>States</option>
								<option value="Andhra Pradesh">Andhra Pradesh</option>
								<option value="Arunachal Pradesh">Arunachal Pradesh</option>
								<option value="Assam">Assam</option>
								<option value="Bihar">Bihar</option>
								<option value="Chhattisgarh">Chhattisgarh</option>
								<option value="Goa">Goa</option>
								<option value="Gujarat">Gujarat</option>
								<option value="Haryana">Haryana</option>
								<option value="Himachal Pradesh">Himachal Pradesh</option>
								<option value="Jammu and Kashmir">Jammu and Kashmir</option>
								<option value="Jharkhand">Jharkhand</option>
								<option value="Karnataka">Karnataka</option>
								<option value="Kerala">Kerala</option>
								<option value="Madhya Pradesh">Madhya Pradesh</option>
								<option value="Maharashtra">Maharashtra</option>
								<option value="Manipur">Manipur</option>
								<option value="Meghalaya">Meghalaya</option>
								<option value="Mizoram">Mizoram</option>
								<option value="Nagaland">Nagaland</option>
								<option value="Odisha">Odisha</option>
								<option value="Punjab">Punjab</option>
								<option value="Rajasthan">Rajasthan</option>
								<option value="Sikkim">Sikkim</option>
								<option value="Tamil Nadu">Tamil Nadu</option>
								<option value="Telangana">Telangana</option>
								<option value="Tripura">Tripura</option>
								<option value="Uttar Pradesh">Uttar Pradesh</option>
								<option value="Uttarakhand">Uttarakhand</option>
								<option value="West Bengal">West Bengal</option>
								<option disabled>Union Territories</option>
								<option value="Ladakh">Ladakh</option>
								<option value="Jammu and Kashmir">Jammu and Kashmir</option>
								<option value="Delhi">Delhi</option>
								<option value="Chandigarh">Chandigarh</option>
								<option value="Andaman and Nicobar">Andaman and Nicobar</option>
								<option value="Pondicherry">Pondicherry</option>
								<option value="Lakshadweep">Lakshadweep</option>
								<option value="Daman and Diu">Daman and Diu</option>
								<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
							</select>
							<input class="fsubmit" type="submit" name="submit" value="Submit">
						</form><br><br>

						<img src="my includes/1.svg" alt="certificates" style="width:10%;;margin:20px;">
						<img src="my includes/2.jpg" alt="certificates" style="width:15%;margin:20px;">
						<img src="my includes/3.jpg" alt="certificates" style="width:10%;margin:20px;">
						<img src="my includes/4.jpg" alt="certificates" style="width:10%;margin:20px;">
						<img src="my includes/5.jpg" alt="certificates" style="width:15%;margin:20px;">
						<img src="my includes/6.svg" alt="certificates" style="width:10%;margin:20px;">
						<img src="my includes/7.svg" alt="certificates" style="width:10%;margin:20px;">
						<img src="my includes/8.svg" alt="certificates" style="width:10%;margin:20px;">
						<img src="my includes/9.svg" alt="certificates" style="width:10%;margin:20px;">
						<img src="my includes/10.svg" alt="certificates" style="width:10%;margin:20px;">
						<!-- <div class="bottle" class=""  >
                <img src="media/images/home6/banner-bottle.png"    class=" text-center; " style="z-index: 1;position: inherit;">

                        </div>
                        <div class="apple" >
                        <img src="media/images/home6/chart1.png"  class=" text-center; "      >
                        </div> -->
					</div>
				</div>
			</div>

		</section><hr style="background:grey;margin:0 !important">
<!-- customer reviews -->
<section style="">
	<div class="row"  style="font-family:verdana;border-bottom:1px solid grey;margin:0;" >


		<div class=" col-md-12 col-sm-12 col-lg-7" style="padding:60px 8%;">



			<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="2000">
	<div class="carousel-inner" style="line-height:30px;">
	<div class="carousel-item active">
	<h2>Customer Name: </h2>
	<p class="d-block w-100" style="color:black;font-weight:bold;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">Order Id:</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">State:</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">City:</p>
	</div>
	<div class="carousel-item ">
	<h2>Customer Name: </h2>
	<p class="d-block w-100" style="color:black;font-weight:bold;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">Order Id:</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">State:</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">City:</p>
	</div>
	<div class="carousel-item ">
	<h2>Customer Name: </h2>
	<p class="d-block w-100"style="color:black;font-weight:bold;" >Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">Order Id:</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">State:</p>
	<p style="text-decoration:underline;color:black;font-weight:bold;">City:</p>
	</div>
	</div>
	</div>




		</div>




		<div class="col-lg-5 col-md-12" style="padding:0;background:#fad46b;">

			<div class="container-fluid" style="padding:40px 8%;">
				<div class="row">
					<h1 style="margin:auto;margin-top:20px;margin-bottom:20px;color:black;" class="center">Asian Herbs is for you</h1>
				</div>
				<div class="row">
					<div class="col-md-12" style="color:black;padding:5px 20px;">
						<p style="font-weight:bold;margin:5px 0px 20px;">
							We are for you. It doesn’t matter who you are, if you love fitness and are looking forward to it then we are here to support and help you. AsianHerbs is all about you and promotion of the idea: Be yourself. So, whoever you are, wherever you are, we are for you, because we are with you. Asian Herbs weight loss products are available Online in our Website!
						</p><br>
							<a href="#" style="padding:5px 40px;background:black;color:white;margin-left:30%;">Order Now</a>
					</div>


				</div>

			</div>

		</div>


	</div>

</section>



		<section class="megamenu-cookies">
			<div class="remove">
				<i class="material-icons">
			clear
		</i>
			</div>
			<p style="font-size:15px;line-height:20px;">This Website uses cookies to ensure you get the best experience in our website. We also use analytics software to track data of visitors. See more info <a href="#">here</a></p>
			<a class="agree" href="#">I agree</a>
		</section>

		<!--==========================-->
		<!--=        Banner         =-->
		<!--==========================-->
		<section class="home_five_service" style="padding-top:0;">
			<div class="container mt-5 ">
				<div class=" row  " >
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 text-center" style="margin-top:100px;">
                    <img src="media/images/home6/working.png" class="mx-auto  " alt="Responsive image">
                </div>
				</div>

			</div>
		</section><hr style="background:grey;margin:0 !important">

		<!--==========================-->
		<!--=        Banner         =-->
		<!--==========================-->
		<!-- certifications -->
<section >

		<img src="my includes/certificates.jpg" alt="certificates" style="width:100%;">

</section><hr style="background:grey;margin:0 !important">
		<!-- certifications -->

		<!-- customer review -->
		<section  style="background:#a7ff8a;color:black;padding:40px 8%;" id="ingredients">
			<div class="row" style="margin:0;">
				<div class="col-md-12 col-lg-6 class=""" style="border-right:0.5px solid white;">
					<h1 style="color:brown;">Where did Garcinia
	Cambogia Originate?</h1>
	<p>Garcinia cambogia, a tropical fruit also known as the Malabar tamarind, is a popular weight-loss supplement. It blocks your body's ability to make fat and it puts the brakes on your appetite. It could help keep blood sugar and cholesterol levels in check, too.</p><br>
	<p>Garcinia (gummi-gutta) is grown for its fruit in Southeast Asia,central Africa and rarely found in coastal Karnataka/Kerala, India, and west  . It thrives in most moist forests.</p><br>
	<p>Asianherbs Garcinia is having 70% HCA, which helps to reduce weight faster & easier than any other "so called" garcinia products which you see in many ads.</p><br>
	<hr style="background:black; height:1px;">
	<div class="how-to-use pl-3 pt-3" >
		<h1 style="color:brown;">How to use Garcinia
		Cambogia Capsule?</h1>
		<p>Take one capsule of Garcina cambogia two times a day with luke warm lemon water and Reach your goal.</p>
		<div class="row py-3" >
			<div class="col-xs-4 col-md-12 col-lg-6 text-center">
				<img src="assets/img/how.png" alt="">
			</div>
			<div class="col-xs-8 col-md-12 col-lg-6 mt-4">
				<a href="#" style="text-decoration:none;background:black;color:white;padding:10px 60px;font-size:20px;border-radius:0;border:1px solid white;">Order Now</a>
			</div>
		</div><br>
	</div>

				</div>
				<div class="col-md-12 col-lg-6">
					<h1 style="color:brown;">Purpose of using
	Garcinia Cambogia</h1>
	<p>The vital bit in the fruit's peel, hydroxicitric, or HCA can reduce fat and mark reduces appetite.</p>
	<p>It aids in reducing of fat and further fat accumilation.</p><br>
	<h1 style="color:brown;">What is HCA?</h1>
	<p>Hydroxycitric Acid - 70%</p>
	<ul>
		<li style="list-style-type: circle;"><p>Active Ingredient<br>The active ingredient in the fruit's rind, hydroxycitric acid, or HCA, has boosted fat-burning and cut-back appetite. It appears to block an enzyme called citrate lyase, which your body uses to make fat. It also raises levels of the brain chemical serotonin, which makes you feel less hungry.</p></li>
		<li style="list-style-type: circle;"><p>Burns Fat</p></li>
		<li style="list-style-type: circle;"><p>Reduce the Appetite</p></li>
	</ul>
	<div class="boxbox mt-5" style="width:90%;border:2px solid #0000b3; text-align:center;padding:20px 40px;margin-left:10%;box-shadow: 0 0 10px 1px #0000b3;">
		<p style="color:#0000b3">Does not allow deposition / accumulation of fat</p>
	</div><br>
				</div>
			</div>
		</section>
<section>
	<div class="row" style="margin:0;">
			<div class="col-lg-6 col-md-12 col-sm-12 col-12 " style="padding:40px 8%;background: #ffc936;">
					<h1 style="color:black;">Green Tea with ECGC:</h1>
					<h3 style="color:black">Asianherbs Garcinia Cambogia
Containes 25 % Green Tea</h3>
					<ul style="font-weight:bold;color:black;">
						<li style="list-style-type: circle;">Naturally detoxifies the body</li>
						<li style="list-style-type: circle;">Activates fat burning</li>
						<li style="list-style-type: circle;">Eliminates toxins</li>
						<li style="list-style-type: circle;">Works as an anti-oxidant</li>
						<li style="list-style-type: circle;">Improve heart and Metal health</li>
						<li style="list-style-type: circle;">Prevents cancer</li>
						<li style="list-style-type: circle;">Helps in digestion</li>
						<li style="list-style-type: circle;">Helps in boosting immunity</li>
						<li style="list-style-type: circle;">Regulate body temperature</li>
						<li style="list-style-type: circle;">The Bioactive Compounds present in the tea helps in improve health</li>
					</ul>


			</div>
			<div class="col-lg-6 col-md-12 col-sm-12 col-12 " style="background: #59cfc9;padding:40px 8%;">
					<h1 style="color:black;">Guggul :</h1>
					<ul style="font-weight:bold;color:black;">
						<li style="list-style-type: circle;">Revitalize Energy</li>
						<li style="list-style-type: circle;">Increases your metabolism</li>
						<li style="list-style-type: circle;">Activates fat burning</li>
						<li style="list-style-type: circle;">Works as an anti-oxidant</li>
						<li style="list-style-type: circle;">Helps in digestion</li>
						<li style="list-style-type: circle;">Improve heart and Metal health</li>
						<li style="list-style-type: circle;">Regulate body temperature</li>
						<li style="list-style-type: circle;">Helps in boosting immunity</li>
						<li style="list-style-type: circle;">The Bioactive Compounds present in the tea helps in improve health</li>
					</ul>


			</div>
	</div>



	<div class="row" style="margin:0;">
			<div class="col-lg-6 col-md-12 col-sm-12 col-12" style="background:white;padding:40px 8%;border:10px solid #4e2278;" >
				<h1 style="color:#4e2278;">Green coffee extract:</h1>
				<ul style="font-weight:bold;color:#4e2278;">
					<li style="list-style-type: circle;">Boosts energy and metabolism</li>
					<li style="list-style-type: circle;">Enables fat burning</li>
					<li style="list-style-type: circle;">Restrains body from gaining weight</li>
					<li style="list-style-type: circle;">Provides antioxidant support</li>
				</ul>

			</div>
			<div class="col-lg-6 col-md-12 col-sm-12 col-12" style="background:#4e2278;padding:40px 8%;">
				<h1 style="color:white;">Piperine:</h1>
				<ul style="font-weight:bold;color:white;">
					<li style="list-style-type: circle;">Helps manage weight gain</li>
					<li style="list-style-type: circle;">Enables stress management</li>
					<li style="list-style-type: circle;">Gives instant energy by the production of adrenaline</li>
					<li style="list-style-type: circle;">Paves way for smooth digestion</li>
				</ul>


			</div>
	</div>











</section><hr style="background:grey;margin:0 !important">
		<!-- customer review -->
		<!-- why it works -->
<section>
	<div class="container ">
			<div class="row mb-4 mt-5">
					<div class="col-lg-6 col-md-12 col-sm-12 col-12">
							<img src="media/images/home6/ch.png" class="p-5" style="width:100%;">
					</div>

							<div class="col-lg-6  col-md-12 col-sm-12 col-12  " style=" ">
									<h1 class="  text-center" style="color:black;">Why does it work ?</h1>
									<hr class="" style="border:10px solid #3ad82e; height:0px; width:200px; margin-right:auto;">
									<p class="  py-3   text-left "  style="color:black;">Ketosis is the process of the body turning to fat for the production of energy when sugar and glucose levels are low. Ketosis gives birth to Ketones, which are, generally, naturally produced in the body.
</p>
									<p class="  py-3   text-left "  style="color:black;">Our diet ensures that sugar and glucose levels in the body are restricted so that the body will look for alternative sources. Following our prescribed diet, you can induce a state of Ketosis and burn your fat without having to impose restrictions on your everyday routine.
Ketogenic works as <b><i>Weight Loss product for Men as well as Women</i></b>.
</p><br>
<a href="#" style="padding:5px 40px;background:black;color:white;margin-left:40%;">Order Now</a>

							</div>



			</div>
	</div>
</section><hr style="background:grey;margin:0 !important">
		<!-- why it works -->

		<div class="container-fluid ">
            <div class="row ">

                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="media/images/home6/img1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="media/images/home6/img2.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="media/images/home6/img3.jpg" alt="Third slide">
    </div>
  </div>
	<a class="carousel-control-prev h-25 my-auto" ; href="#carouselExampleControls" role="button" data-slide="prev">
	    <span class="" aria-hidden="true" style="color:black;font-size:50px;"><i class="fas fa-arrow-left"></i></span>
	  </a>
	  <a class="carousel-control-next  h-25 my-auto" style="" href="#carouselExampleControls"  role="button" data-slide="next">
	    <span class=" "  aria-hidden="true" style="color:black;font-size:50px;" ><i class="fas fa-arrow-right"></i></span>
	  </a>

</div>



            </div>
        </div>

				<!-- how it works -->
				<section>
					<div class="container-fluid pt-5" style="background:#d9b721;padding:0 8%;">

							<h1 style="text-align:center;margin-top:5px;margin-bottom:20px;color:black;">How does it Work ?</h1>

						<div class="row">
							<div class="col-md-6" style="padding:40px 20px;color:black;">
								<p style="font-weight:bold;margin:5px 0px 20px;">
												AsianHerbs is a system that works by combining the collective power of 4 natural herbs. The formula that has been derived by our R&D team is proven to be effective in reducing weight by quickening your metabolism. This ensures that you digest glucose quickly so that the body can use alternative sources – fat – for energy, thereby burning fat.
											</p>
											<p style="font-weight:bold;margin:5px 0px 20px;">
												We have successfully synthesized the use of BHB (Beta-Hydroxybutyrate) in our program and hence, our formula is capable of inducing ketosis in your body without depending on the body to produce ketones.

											</p>
							</div>
							<div class="col-md-6" style="padding:40px 20px;color:black;">
								<p style="font-weight:bold;margin:5px 0px 20px;">
												Ketosis is a stage where your body doesn’t have enough glucose (or sugar) to produce energy and depends on fat for energy. The body, then, enters a stage of ketosis by producing ketones (which are produced out of fat). We have found a way to induce this state in the most optimum way so that it doesn’t cause any hindrance to your day.
											</p><br><br>
											<a href="#" style="padding:5px 40px;background:black;color:white;margin-left:30%;">Order Now</a>

							</div>

						</div>

					</div>
				</section><hr style="background:grey;margin:0 !important">
				<!-- how it works -->

				<!-- what is bhb -->
				<section>
					<div class="container-fluid pt-5" style="background:#3fbfbf;padding:0 8%;">
						<div class="row">
							<h1 style="margin:auto;margin-top:20px;margin-bottom:20px;color:black;">What is BHB?</h1>
						</div>
						<div class="row">
							<div class="col-md-6" style="padding:40px 20px;color:black;">
								<p style="font-weight:bold;margin:5px 0px 20px;">
									BHB or Beta-Hydroxybutyrate is an endogenic ketone that is produced by the body in the absence of carbohydrates. It is the most prominent of the three ketones produced by the body during the process of ketosis. This ketone is essential during the process of Ketosis because it is made out of fat and hence, helps burn fat. BHB helps in cognitive function of the brain and helps counter oxidative stress.

									</p>
								<div class="text-center">
										<img src="assets/img/bhb.png" alt="">
								</div>
							</div>
							<div class="col-md-6"  style="padding:40px 20px;color:black;">
								<p style="font-weight:bold;margin:5px 0px 20px;">
										This was all about endogenous BHB, i.e., the ketone that is produced by the body. Ketones can also be supplemented in other forms from external sources. In those cases, they come to be known as ‘Exogenous Ketones’. There is a confusion as to why must one depend on exogenous ketones if the body produces that same supplement during ketosis. The answer is simple: when a person with a high-carb diet suddenly changes his diet to low-carb, the body takes time to adjust and produce ketones. During this span of adjustment, there is a scarcity of energy in the body, rendering a phase of sluggishness. Exogenous Ketones help overcome this problem by proving the body with its ketone requirement for energy production so that there are no hindrances to the person’s daily routine.
											</p>

							</div>

						</div>

					</div>
				</section><hr style="background:grey;margin:0 !important">
				<!-- what is bhb -->

				<!-- asian herbs for you -->
				<section>

				</section><hr style="background:grey;margin:0 !important">
				<!-- what is ingredients -->

  <!-- dummy section as customer reviews -->
	<section>

				<div class="container-fluid " style="padding:40px 50px;background: #88ff61; color:black;padding:20px 8%;">
					<h2 style="margin-top:40px;color:red;">Proven results:</h2>
					<p style="font-weight:bold;">With over 100,000 happily served, AsianHerbs The Next-Level Weight Loss is the most optimum way to accomplish your weight goals. Seize the day and this opportunity to prepare yourself for the event you’ve been looking forward to – the day of a fitter you!
</p><br>
<h2 style="color:red;">No strict dieting:</h2>
<p style="font-weight:bold;">Break the stereotypes of strict and exhausting dieting with AsianHerbs. Our product KETOGENIC having Keto Diet facors, which is designed to make sure that you can crave and have the life you dream of, without stopping to live. We are compatible with your lifestyle so that you can get compatible with yours!
</p><br>
<h2 style="color:red;">It’s easy to use:</h2>
<p style="font-weight:bold;">AsianHerbs KETOGENIC comes to you as naturally as your morning tea (yes, it is that easy!). Our program is designed in a way that will cause no drastic changes to your routine. No complications, no hassle, just keep on keeping on!
</p><br>


					</div>


		<div class="row" style="font-family:verdana; margin:0px;">
				<div class="container-fluid col-lg-6 col-md-12 col-sm-12 col-12 text-center" style="padding:40px 8%;">

					<img src="assets/img/money.svg" style="width:50%;margin-top:20%;" alt="money-back">

				</div>
				<div class="container-fluid col-lg-6 col-md-12 col-sm-12 col-12" style="padding:40px 8%;">
<h1>90-days money-back guarantee:</h1>
<p style="font-weight:bold;">We understand your confusion and that’s why we emphasize the fact that we are there for you. Our R&D team has spent years trying to perfect the formula that will help you accomplish your weight goals. It is with the utmost pride we offer this product to you, but we leave the choice to you. We have faith in our work and to demonstrate this faith, we offer a 90-days money-back guarantee if you are not satisfied with us. Order AsianHerbs KETOGENIC Weight Loss product Online!
</p><br>
<a href="#" style="padding:5px 40px;background:black;color:white;margin-left:30%;">Order Now</a>


				</div>
		</div>
	</section><hr style="background:grey;margin:0 !important">
	<!-- dummy section as customer review -->

		<!-- image container -->

		<section>
			<img src="" alt="pending">
		</section><hr style="background:grey;margin:0 !important">
		<!-- about us -->
		<section>
			<div class="container-fluid" >


				<div class="row">
					<div class="container-fluid col-md-12 col-lg-6" style="padding:40px 8%;">

						<div class="container-fluid col-sm-6">
							<img src="media/images/home6/banner-bottle.png" alt="about" style="margin-top:50%;">
						</div>
						<div class="container-fluid col-sm-6">
							<h2 style="line-height:55px;margin-top:30px;">FIGHT BLOATING</h2>
							<h2 style="line-height:55px">BOOST METABOLISM</h2>
							<h2 style="line-height:55px">SUPPRESS APPETITE</h2>
							<h2 style="line-height:55px">ANTIOXIDENT SUPPORT</h2>
							<h2 style="line-height:55px">RELEASE TOXINS</h2>
							<h2 style="line-height:55px">INCREASE ENERGY</h2>
							<h2 style="line-height:55px">BURN FAT</h2>

						</div>
					</div>
					<div class="container-fluid col-md-12 col-lg-6 bg-dark text-light"style="padding:40px 8%;">
						<h1 class="text-light">About Us</h1>
						<p>We are for you, it’s that simple. We had a dream to change lives and hence, created AsianHerbs – a company that is dedicated to your dreams.
</p>
						<p>Asian Herb is a product of UniSap Nutricare PVT.LTD located in Bangalore. Asian Herbs prepares weight loss products by using G4 formula. The intention was simple, make a product that can be used by anyone and can be used effectively. That is what we set out to do and hopefully, have done.</p>
						<p>We believe that the world needs to live healthily. Practicing what we preach, we have ensured that our products give you that healthy lifestyle in a healthy way because at the end of the day, we are for you; be it your dreams, your aspirations or your goals – we are here.
</p>
						<h3 class="text-light">Loved us? Call us!
</h3>
						<p>Want us to become better in serving you? PLEASE CALL US!
</p><a href="#" style="color:black;background:white;padding:5px 40px;MARGIN-LEFT:60%;marsdgin-top:20px;">Order Now</a>
					</div>

				</div>



			</div>
		</section><hr style="background:grey;margin:0 !important">
		<!-- about us -->
		<section>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-3"style="background:#fc0303;">
						<img src="my includes/1.svg" style="width:50%;float:right;margin-right:10%;" alt="">
					</div>
					<div class="col-xs-9" style="background:#fc0303;">
						<p class="licence">Licence Number : 12214027000662</p>
					</div>
				</div>
			</div>
		</section>
		<section>
			<img src="my includes/partners.jpg" alt="partners" style="width:100%;">
		</section>
		<div class="sticky">
			<a href="#form" style="width:200px;height:40px;position:fixed;right:30px;background:#3ad82e;text-align:center;color:white;font-weight:bold;font-size:20px;">Fill the Form</a>
		</div>
		<div class="sticky-2">
			<a href="#"><i class="fab fa-whatsapp"></i></a>
		</div>
			<!-- image container -->








		<!--==========================-->
		<!--=        Offer         =-->
		<!--==========================-->
		<!-- <section class="home5_offer">
			<div class="vigo_container_two">
				<div class="home5_offer_detail">
					<div class="home5_offer_detail_bg">
						<img src="media/images/home6/offer-bg.png" alt="">
					</div>
					<div class="home5_offer_inner">
						<div class="home5_offer_left">
							<img src="media/images/home6/offer-left.png" alt="">
						</div>
						<div class="home5_offer_center">
							<h2>ONETIME OFFER &#60; </h2>
							<p>We have limited time offerings for our special suppliment that goes with a special offer. Grab now!</p>
							<a href="#" class="btn_three">I’M IN</a>
						</div>
						<div class="home5_offer_right">
							<span>$12</span>
							<img src="media/images/home6/offer-right.png" alt="">
						</div>
					</div>
					<div class="home5_offer_social">
						<span>Share Us-</span>
						<a href="#">
					<i class="fab fa-facebook-f"></i>
				</a>
						<a href="#">
					<i class="fab fa-twitter"></i>
				</a>
						<a href="#">
					<i class="fab fa-medium-m"></i>
				</a>
						<a href="#">
					<i class="fab fa-tumblr"></i>
				</a>
					</div>
				</div>
			</div>
        </section>
        <!--==========================-->
		<!--= End       Offer         =-->
		<!--==========================-->



		<!--==========================-->
		<!--=        customer review         =-->
		<!--==========================-->
		<!-- <div class="container ">
                    <h1 class="  text-left">One Lakh happy customers </h1>
                  <hr class="" style="border:10px solid #3ad82e; height:0px; width:200px; margin-left:0;">
            <div class="row mb-5 mt-5">

                <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                <p class="  py-3   text-justify "  >Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  </p>
                <p> - Rajesh <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></p>
                <i style="color: #3ad82e ">Verified Buyer</i>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                <p class="  py-3   text-justify "  >Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  </p>
                <p> - Rajesh <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></p>
                <i style="color: #3ad82e ">Verified Buyer</i>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                <p class="  py-3   text-justify "  >Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  </p>
                <p> - Rajesh <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></p>
                <i style="color: #3ad82e ">Verified Buyer</i>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                <p class="  py-3   text-justify "  >Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  </p>
                <p> - Rajesh <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></p>
                <i style="color: #3ad82e ">Verified Buyer</i>
                </div>





            </div>
        </div>
        <!--==========================-->
		<!--=    End    customer review         =-->
		<!--==========================-->
		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<!-- <section class="home5_video">
			<div class="vigo_container_two">
				<div class="home5_video_total">
					<div class="section_title_four">
						<h2>LAB SNEAK PEEK</h2>
					</div>
					<div class="home5_video_left">
						<p>Lorem ipsum dolor sit ametures & consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<div class="home5_video_social">
							<span>SHARE US -</span>
							<a href="#">
						<i class="fab fa-facebook-f"></i>
					</a>
							<a href="#">
						<i class="fab fa-twitter"></i>
					</a>
							<a href="#">
						<i class="fab fa-medium-m"></i>
					</a>
							<a href="#">
						<i class="fab fa-tumblr"></i>
					</a>
						</div>
					</div>
					<div class="youtube-wrapper home5_video_right">
						<div class="youtube-poster" data-bg-image="media/images/home6/video-5.png"></div>
						<iframe src="https://www.youtube.com/embed/pCo40Y6UpWg" allowfullscreen></iframe>
						<i class="material-icons play">
					play_arrow
				</i>
						<i class="material-icons pause">
					pause
				</i>
					</div>
				</div>
			</div>
		</section> -->

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<!-- <section class="home5_pricing">
			<div class="vigo_container_two">
				<div class="row">
					<div class="col-xl-4 col-md-6">
						<div class="home5_pricing_single">
							<h3>FREE</h3>
							<span>SHIPPING POLICY</span>
							<i class="fas fa-ship"></i>
						</div>
					</div>
					<div class="col-xl-4 col-md-6">
						<div class="home5_pricing_single">
							<h3>14 DAYS</h3>
							<span>MONEYBACK GURANTEED</span>
							<i class="fab fa-android"></i>
						</div>
					</div>
					<div class="col-xl-4 col-md-6">
						<div class="home5_pricing_single">
							<h3>CALL</h3>
							<span>SUPPORT INCLUDED</span>
							<i class="fas fa-headphones"></i>
						</div>
					</div>
				</div>
			</div>
		</section> -->

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<!-- <section class="home5-most-sold" data-bg-image="media/images/home6/most-sold.jpg">
			<div class="vigo_container_one">
				<div class="section_title_four">
					<h2>MOST SOLD</h2>
				</div>
				<div class="row">
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-one.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/relate-pd-two.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-three.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-four.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-five.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-one.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/relate-pd-two.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-three.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-four.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-five.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/related-pd-one.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-sm-6 col-lg-3">
						<div class="sn_related_product">
							<div class="sn_pd_img">
								<a href="#">
							<img src="media/images/banner-two/relate-pd-two.png" alt="">
						</a>
							</div>
							<div class="sn_pd_rating">
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
								<a href="#">
							<i class="far fa-star"></i>
						</a>
							</div>
							<div class="sn_pd_detail">
								<h5>
									<a href="#">Vaxin Regular (500mg), Mild Intake</a>
								</h5>
								<ins>
							<span>$16.00</span>
						</ins>
								<del>
							<span>$20.00</span>
						</del>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<!-- <section class="call_to_action_green">
			<div class="vigo_container_two">
				<div class="call_to_action_area_two">
					<div class="row">
						<div class="col-xl-10 offset-xl-2">
							<div class="call_to_action_hello">
								<div class="call_to_action_left_two">
									<h2>LIVE HEALTHY?</h2>
									<p>Try out our suppliment & enjoy the healthiest life. Discounts end soon!</p>
								</div>
								<div class="call_to_action_right_two">
									<a href="#" class="btn_four">Purchase</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->

		<!--==========================-->
		<!--=        Footer         =-->
		<!--==========================-->
		<?php include('includes/footer.php')
    ?>


	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>


</body>

</html>
