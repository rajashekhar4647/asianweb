<!doctype html>
<html>

<head>
	<!-- Meta Data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Uni Health Care</title>

	<!-- Fav Icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/fav-icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/fav-icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/fav-icons/favicon-16x16.png">

	<!-- Dependency Styles -->
	<link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/fontawesome/css/fontawesome-all.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/flaticon/css/flaticon.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/owl.carousel/css/owl.theme.default.min.css" type="text/css">
	<link rel="stylesheet" href="dependencies/magnific-popup/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="dependencies/animate.css/css/animate.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css" type="text/css">
	<link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css" type="text/css">
	<link rel="stylesheet" href="dependencies/material-design-icons/css/material-icons.css">
	<link rel="stylesheet" href="dependencies/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="dependencies/aos/css/aos.css">
	<link rel="stylesheet" href="dependencies/rangeslider.js/css/rangeslider.css">

	<!-- Site Stylesheet -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">

	<link id="theme" rel="stylesheet" href="assets/css/theme-color/theme-default.css" type="text/css">

	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900%7CRoboto:300,400,500,700,900" rel="stylesheet">


</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<!-- <div id="loader-wrapper">
        <div class="loader">
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
            <div class="loader-dot"></div>
        </div>
    </div> -->

	<div id="site">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
<?php include('includes/header.php')?>
		<!--=========================-->
		<!--=        Breadcrumb         =-->
		<!--=========================-->
		<section class="breadcrumb_area">
			<div class="vigo_container_two">
				<div class="page_header">
					<h1>Blog</h1>
				</div>
				<!-- /.page-header -->
			</div>
			<!-- /.vigo_container_two -->
		</section>
		<!-- /.breadcrumb_area -->

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<section class="blog_list_area">
			<div class="vigo_container_two">
				<div class="blog_list_flex">
					<div class="blog_list_flex_item_big">
						<div class="blog_all_list">
							<div class="blog_single_list">
								<div class="blog_single_list_img">
									<div class="post-thumbnail">
										<img src="media/images/home6/blog-one.jpg" alt="!!">
									</div>
									<a href="#" class="blog_single_list_btn">
								<span>read</span>
								<i class="material-icons">
									arrow_forward
								</i>
							</a>
								</div>
								<div class="blog_single_list_content">
									<h3 class="blog_title">
										<a href="#">Rich Diet You Must Take Along With All Body Suppliment Support.</a>
									</h3>
									<div class="blog_meta">
										<a href="#">
									<span>24<sup>th</sup> December</span>
								</a>
									</div>
								</div>
							</div>
							<div class="blog_single_list">
								<div class="blog_single_list_img">
									<div class="post-thumbnail">
										<img src="media/images/home6/blog-two.jpg" alt="!!">
									</div>
									<a href="#" class="blog_single_list_btn">
								<span>read</span>
								<i class="material-icons">
									arrow_forward
								</i>
							</a>
								</div>
								<div class="blog_single_list_content">
									<h3 class="blog_title">
										<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</a>
									</h3>
									<div class="blog_meta">
										<a href="#">
									<span>24<sup>th</sup> December</span>
								</a>
									</div>
								</div>
							</div>
							<div class="blog_single_list">
								<div class="blog_single_list_img">
									<div class="post-thumbnail">
										<img src="media/images/home6/blog-three.jpg" alt="!!">
									</div>
									<a href="#" class="blog_single_list_btn">
								<span>read</span>
								<i class="material-icons">
									arrow_forward
								</i>
							</a>
								</div>
								<div class="blog_single_list_content">
									<h3 class="blog_title">
										<a href="#">Rich Diet You Must Take Along With All Body Suppliment Support.</a>
									</h3>
									<div class="blog_meta">
										<a href="#">
									<span>24<sup>th</sup> December</span>
								</a>
									</div>
								</div>
							</div>
							<div class="blog_single_list">
								<div class="blog_single_list_img">
									<div class="post-thumbnail">
										<img src="media/images/home6/blog-one.jpg" alt="!!">
									</div>
									<a href="#" class="blog_single_list_btn">
								<span>read</span>
								<i class="material-icons">
									arrow_forward
								</i>
							</a>
								</div>
								<div class="blog_single_list_content">
									<h3 class="blog_title">
										<a href="#">Rich Diet You Must Take Along With All Body Suppliment Support.</a>
									</h3>
									<div class="blog_meta">
										<a href="#">
									<span>24<sup>th</sup> December</span>
								</a>
									</div>
								</div>
							</div>
						</div>
						<nav class="blog_list_pagination">
							<ul class="blog_list_nav_links">
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">6</a></li>
								<li><a href="#">
							<span>.</span>
							<span>.</span>
							<span>.</span>
							<span>.</span>
						</a></li>
								<li><a href="#">9</a></li>
							</ul>
							<ul class="blog_list_nav_links two">
								<li><a href="#" class="prev"><i class="fas fa-angle-double-left"></i> prev</a></li>
								<li><a href="#" class="next">next <i class="fas fa-angle-double-right"></i></a></li>
							</ul>
						</nav>
					</div>
					<div class="blog_list_flex_item">
						<aside class="blog_list_sidebar sidebar">
							<section class="widget widget_search">
								<form role="search" method="get" class="search-form" action="http://wptest.io/demo/">
									<label>
								<i class="fas fa-search"></i>
								<input class="search-field" placeholder="Search here…" type="search">
							</label>
									<button type="submit" class="search-submit">
								<i class="fas fa-arrow-right"></i>
							</button>
								</form>
							</section>
							<section class="widget widget_categories">
								<h2 class="widget-title">Category</h2>
								<ul>
									<li class="cat-item">
										<i class="fas fa-burn"></i>
										<p>
											FAT<br> BURNERS
										</p>
									</li>
									<li class="cat-item">
										<i class="far fa-star"></i>
										<p>
											BUDGET<br> SUPPLIMENT
										</p>
									</li>
									<li class="cat-item">
										<i class="fas fa-heartbeat"></i>
										<p>
											HEART<br> DISEASES
										</p>
									</li>
									<li class="cat-item">
										<i class="fas fa-magic"></i>
										<p>
											MAGICAL<br> DIET
										</p>
									</li>
									<li class="cat-item">
										<i class="fas fa-glass-martini"></i>
										<p>
											DRINK<br> CHART
										</p>
									</li>
									<li class="cat-item">
										<i class="fas fa-leaf"></i>
										<p>
											HARB<br> INTAKES
										</p>
									</li>
								</ul>
							</section>
							<section class="widget widget_social">
								<div class="widget_social_list">
									<ul class="nav" role="tablist">
										<li>
											<a id="facebook-tab" href="#facebook" class="active" data-toggle="tab">
										<i class="fab fa-facebook-f"></i>
									</a>
										</li>
										<li>
											<a id="twitter-tab" href="#twitter" data-toggle="tab">
										<i class="fab fa-twitter"></i>
									</a>
										</li>
										<li>
											<a id="linkedin-tab" href="#linkedin" data-toggle="tab">
										<i class="fab fa-linkedin-in"></i>
									</a>
										</li>
										<li>
											<a id="instagram-tab" href="#instagram" data-toggle="tab">
										<i class="fab fa-instagram"></i>
									</a>
										</li>
									</ul>
								</div>

								<div class="widget_social_content tab-content">
									<div id="facebook" class="widget_facebook widget_social_item tab-pane fade show active" aria-labelledby="facebook-tab">
										<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fthemeim%2F%3Fref%3Dbr_rs&amp;tabs=timeline&amp;width=246px&amp;height=326px&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=true&amp;show_facepile=true&amp;appId"></iframe>
									</div>
									<div id="twitter" class="widget_twitter widget_social_item tab-pane" aria-labelledby="twitter-tab">
										<a class="twitter-timeline" data-width="246" data-height="326" href="https://twitter.com/imthemedesigner?ref_src=twsrc%5Etfw">Tweets by imthemedesigner</a>
									</div>
									<div id="linkedin" class="widget_linkedin widget_social_item tab-pane" aria-labelledby="linkedin-tab">
										<blockquote class="embedly-card">
											<h4>
												<a href="https://www.linkedin.com/in/sonia-shurmi-7b4a12a8/">
										Sonia Shurmi - Web Developer - Euro Bangla iT | LinkedIn</a>
											</h4>
											<p>
												View Sonia Shurmi's profile on LinkedIn, the world's largest professional community. Sonia has 4 jobs listed on their profile. See the complete profile on LinkedIn and discover Sonia's connections and jobs at similar companies.
											</p>
										</blockquote>
									</div>

									<div id="instagram" class="widget_instagram widget_social_item tab-pane" aria-labelledby="instagram-tab">
										<blockquote class="embedly-card">
											<h4>
												<a href="https://www.linkedin.com/in/sonia-shurmi-7b4a12a8/">
										Sonia Shurmi - Web Developer - Euro Bangla iT | LinkedIn</a>
											</h4>
											<p>
												View Sonia Shurmi's profile on LinkedIn, the world's largest professional community. Sonia has 4 jobs listed on their profile. See the complete profile on LinkedIn and discover Sonia's connections and jobs at similar companies.
											</p>
										</blockquote>
									</div>
								</div>
							</section>
							<section class="widget widget_advertise">
								<a href="#">
							<img src="media/images/home6/ad.jpg" alt="#">
						</a>
							</section>
						</aside>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Video         =-->
		<!--==========================-->
		<section class="call_to_action_green">
			<div class="vigo_container_two">
				<div class="call_to_action_area_two">
					<div class="row">
						<div class="col-xl-10 offset-xl-2">
							<div class="call_to_action_hello">
								<div class="call_to_action_left_two">
									<h2>LIVE HEALTHY?</h2>
									<p>Try out our suppliment & enjoy the healthiest life. Discounts end soon!</p>
								</div>
								<div class="call_to_action_right_two">
									<a href="#" class="btn_four">Purchase</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--==========================-->
		<!--=        Footer         =-->
		<!--==========================-->
	<?php include('includes/footer.php')?>
	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<script src="dependencies/jquery/jquery.min.js"></script>
	<script src="dependencies/popper.js/popper.min.js"></script>
	<script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="dependencies/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
	<script src="dependencies/slick-carousel/js/slick.min.js"></script>
	<script src="dependencies/jquery.countdown/js/jquery.countdown.min.js"></script>
	<script src="dependencies/gmap3/gmap3.min.js"></script>
	<script src="dependencies/headroom/js/headroom.js"></script>
	<script src="dependencies/countUp.js/js/countUp.min.js"></script>
	<script src="dependencies/twitter-fetcher/js/twitterFetcher_min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="dependencies/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="dependencies/aos/js/aos.js"></script>
	<script async src="../../../platform.twitter.com/widgets.js" charset="utf-8"></script>
	<script async src="../../../cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>
	<script src="dependencies/rangeslider.js/js/rangeslider.min.js"></script>
	<script src="dependencies/waypoints/js/jquery.waypoints.min.js"></script>
	<!-- Site Scripts -->
	<script src="assets/js/middle.js"></script>
	<script src="assets/js/app.js"></script>


</body>
</html>